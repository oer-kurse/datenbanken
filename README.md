# Datenbanken

Grundlagen Datenbanken und angrenzende Themen.

Konzipiert als Kurs und Nachschlagewerk.

Die aktuelle Build-Version ist direkt abrufbar unter:

https://oer-kurse.gitlab.io/datenbanken/

Peter Koppatz (2022)
