

DROP TABLE landkreise;

CREATE TABLE landkreise (
  kuerzel_lk varchar(3),
  landkreis VARCHAR(30),
  kreisstadt VARCHAR(30),
  kfz VARCHAR(20),
  einwohner INT,
  flaeche FLOAT,
  anmerkungen TEXT);

  
INSERT INTO landkreise VALUES(
 'BAR',
 'Barnim',
 'Eberswalde',
 'BAR, BER, EW',
 177411,
 1479.67,
 'grenzt an Berlin und Polen; entstanden aus den Kreisen Bernau, Eberswalde und Teilen von Bad Freienwalde');


INSERT INTO landkreise VALUES(
  'BRB',
  'Brandenburg an der Havel',
  'Brandenburg an der Havel',
  'BRB',
  71574,
  229.71,
  'flächengrößte kreisfreie Stadt Brandenburgs');


-- Wappen der Stadt Brandenburg an der Havel
-- Lage der Stadt Brandenburg an der Havel im Land Brandenburg
-- Dom St. Peter und Paul

INSERT INTO landkreise VALUES(
  'CB',
  'Cottbus',
  'Cottbus',
  'CB',
  '99687',
  '165.15',
  '(Chóśebuz) -- kreisfreie Stadt');


INSERT INTO landkreise VALUES(
  'LDS',
  'Dahme-Spreewald',
  'Lübben',
  'LDS, KW, LC, LN',
  '164528',
  '2274.48',
  '(Damna-Błota) grenzt an Berlin; größter natürlicher See Brandenburgs (Schwielochsee; 13,3 km²); entstanden aus den Kreisen Königs Wusterhausen, Lübben und Luckau Schlossinsel in Lübben');

INSERT INTO landkreise VALUES(
  'EE',
  'Elbe-Elster',
  'Herzberg',
  'EE, FI, LIB',
  '104673',
  '1899.54',
  'höchster Punkt Brandenburgs (Heidehöhe, 201,4 m); grenzt an Sachsen und Sachsen-Anhalt; entstanden aus den Kreisen Finsterwalde, Herzberg und Teilen von Bad Liebenwerda Elstermühle in Plessa');

INSERT INTO landkreise VALUES(
  'FF',
  'Frankfurt/Oder',
  'Frankfurt/Oder',
  'FF',
  '58092',
  '147.85',
  'kleinste kreisfreie Stadt Brandenburgs; grenzt an Polen');

INSERT INTO landkreise VALUES(
  'HVL',
  'Havelland',
  'Rathenow',
  'HVL, NAU, RN',
  '158236',
  '1727.3',
  'grenzt an Berlin und Sachsen-Anhalt; entstanden aus den Kreisen Nauen und Rathenow Großer Havelländischer Hauptkanal');

INSERT INTO landkreise VALUES(
  'MOL',
  'Märkisch-Oderland',
  'Seelow',
  'MOL, FRW, SEE, SRB',
  '190714',
  '2158.67',
  'grenzt an Polen und Berlin; entstanden aus den Kreisen Seelow, Strausberg und Teilen von Bad Freienwalde');
  
INSERT INTO landkreise VALUES(
  'OHV',  
  'Oberhavel',
  'Oranienburg',
  'OHV',
  '207524',
  '1808.18',
  'grenzt an Berlin und Mecklenburg-Vorpommern; entstanden aus den Kreisen Gransee und Oranienburg');
  
INSERT INTO landkreise VALUES(
  'OSL',
  'Oberspreewald-Lausitz',
  'Senftenberg',
  'OSL, CA, SFB',
  '112450',
  '1223.08',
  '(Górne Błota-Łužyca) flächenmäßig kleinster Landkreis Brandenburgs; grenzt an Sachsen; größter künstlicher See Brandenburgs (Sedlitzer See; 13,3 km²); entstanden aus den Kreisen Calau, Senftenberg und Teilen von Bad Liebenwerda');

INSERT INTO landkreise VALUES(
  'LOS',
  'Oder-Spree',
  'Beeskow',
  'LOS',
  '182397',
  '2256.78',
  'grenzt an Polen und Berlin; entstanden aus den Kreisen Beeskow, Eisenhüttenstadt-Land und Fürstenwalde, sowie der kreisfreien Stadt Eisenhüttenstadt');

INSERT INTO landkreise VALUES(
  'OPR',
  'Ostprignitz-Ruppin',
  'Neuruppin',
  'OPR, KY, NP, WK',
  '99110',
  '2526.55',
  'grenzt an Mecklenburg-Vorpommern und Sachsen-Anhalt; entstanden aus den Kreisen Neuruppin, Wittstock und Teilen der Kreise Kyritz und Pritzwalk');

INSERT INTO landkreise VALUES(
  'P',
  'Potsdam',
  'Potsdam',
  'P',
  '167745',
  '188.25',
  'Landeshauptstadt; bevölkerungsreichste Stadt Brandenburgs; grenzt an Berlin');

INSERT INTO landkreise VALUES(
  'PM',
  'Potsdam-Mittelmark',
  'Bad Belzig',
  'PM',
  '210910',
  '2591.97',
  'bevölkerungsreichster Landkreis Brandenburgs; Kreisgebiet durch die Stadt Brandenburg an der Havel geteilt, grenzt an Berlin und Sachsen-Anhalt; entstanden aus den Kreisen Brandenburg, Belzig und Potsdam');

INSERT INTO landkreise VALUES(
  'PR',
  'Prignitz',
  'Perleberg',
  'PR',
  '77573',
  '2138.61',
  'bevölkerungsärmster Landkreis Brandenburgs; grenzt an Mecklenburg-Vorpommern, Niedersachsen und Sachsen-Anhalt; entstanden aus dem Kreis Perleberg und Teilen der Kreise Kyritz und Pritzwalk');

INSERT INTO landkreise VALUES(
  'SPN',
  'Spree-Neiße',
  'Forst',
  'SPN, FOR, GUB, SPB',
  '117635',
  '1657.45',
  '(Sprjewja-Nysa) grenzt an Polen und Sachsen; entstanden aus den Kreisen Cottbus-Land, Forst, Guben und Spremberg');

INSERT INTO landkreise VALUES(
  'TF',
  'Teltow-Fläming',
  'Luckenwalde',
  'TF',
  '163553',
  '2104.19',
  'grenzt an Berlin und Sachsen-Anhalt; entstanden aus den Kreisen Jüterbog, Luckenwalde und Zossen	Kloster Zinna');
  
INSERT INTO landkreise VALUES(
  'UM',
  'Uckermark',
  'Prenzlau',
  'UM, ANG, PZ, SDT, TP',
  '121014',
  '3076.93',
  'flächengrößter Landkreis Brandenburgs; grenzt an Polen und Mecklenburg-Vorpommern; entstanden aus den Kreisen Angermünde, Prenzlau und Templin, sowie der kreisfreien Stadt Schwedt/Oder');

DROP TABLE images;

CREATE TABLE images (
  id SERIAL,
  kuerzel_lk character varying(3),
  bild VARCHAR(100),
  alt varchar(50),
  kategorie character varying(20), 
  bildquelle text,
  tags character varying(50), 
  datum date,
  lizenz character varying(10)); 

INSERT INTO images (bild, alt, kategorie, bildquelle, kuerzel_lk)
VALUES(
 'Wappen_Landkreis_Barnim.svg',
 'Wappen des Landkreises Barnim',
 'wappen',
 'https://de.wikipedia.org/wiki/Datei:Wappen_Landkreis_Barnim.svg',
 'BAR');

INSERT INTO images (bild, alt, kategorie, bildquelle, kuerzel_lk)
values(
  'Chorin_abbey2.JPG', 
  'Kloster Chorin',
  'gebauede',
  'https://de.wikipedia.org/wiki/Datei:Chorin_abbey2.JPG',
  'BAR');

INSERT INTO images (bild, alt, kategorie, bildquelle, kuerzel_lk)
VALUES(
 'Brandenburg_BAR.svg', 
 'Lage des Landkreises Barnim im Land Brandenburg',
 'karte',
 'https://de.wikipedia.org/wiki/Datei:Brandenburg_BAR.svg',
 'BAR');

DROP TABLE gewaesser;

CREATE TABLE gewaesser (
  id          INTEGER,
  kuerzel_lk  VARCHAR(3),
  bezeichnung VARCHAR(100),
  ort         VARCHAR(100),
  zusatz      VARCHAR(100),
  praedikat   INTEGER,
  anmerkung   VARCHAR(50)
);

insert into gewaesser values (1, 'BAR', 'Bernsteinsee', 'Ruhlsdorf', 'Strand',  1, '');
insert into gewaesser values (2, 'BAR', 'Gamensee', 'Tiefensee', 'CP „Country-Camping“', 1, '');
insert into gewaesser values (3, 'BAR', 'Gorinsee', 'Schönwalde,', 'Badewiese am Campingplatz', 1, '');
insert into gewaesser values (4, 'BAR', 'Grimnitzsee', 'Joachimsthal', 'Feriendorf', 1, '');
insert into gewaesser values (5, 'BAR', 'Grimnitzsee', 'Joachimsthal', 'Strandbad', 1, '');
insert into gewaesser values (6, 'BAR', 'Großer Wukensee', 'Biesenthal', 'Strandbad', 1, '');
insert into gewaesser values (7, 'BAR', 'Liepnitzsee', 'Lanke', 'Waldbad',  1, '');
insert into gewaesser values (8, 'BAR', 'Obersee',  'Lanke', 'Badewiese', 1, '');
insert into gewaesser values (9, 'BAR', 'Parsteiner See', 'Brodowin/Pehlitz', 'CP „Pehlitz/Werder“', 1, '');
insert into gewaesser values (10, 'BAR', 'Parsteiner See', 'Parstein', 'CP „Am Parsteiner See“', 1, '');
insert into gewaesser values (260, 'BAR', 'Ruhlesee', 'Ruhlsdorf', 'Strand Feriendorf „Dorado“', 1, '');
insert into gewaesser values (11, 'BAR', 'Stolzenhagener See', 'Stolzenhagen', 'Strandbad', 1, '');
insert into gewaesser values (12, 'BAR', 'Üdersee',  'Finowfurt', 'Ferienpark „Üdersee-Camp“', 1, '');
insert into gewaesser values (13, 'BAR', 'Wandlitzsee', 'Wandlitz', 'Strandbad', 1, '');
insert into gewaesser values (14, 'BAR', 'Werbellinsee', 'Eichhorst', 'BEROLINA Campingparadies am Werbellinsee', 1, '');
insert into gewaesser values (15, 'BAR', 'Werbellinsee', 'Joachimsthal', 'CP „Am Spring“',  1, '');
insert into gewaesser values (16, 'BAR', 'Werbellinsee', 'Joachimsthal', 'Badewiese „Am Stein“', 1, '');
insert into gewaesser values (17, 'BAR', 'Werbellinsee', 'Joachimsthal', 'EJB',  1, '');
insert into gewaesser values (18, 'BAR', 'Werbellinsee', 'Joachimsthal', 'Holzablage Michen', 1, '');
insert into gewaesser values (19, 'BRB', 'Beetzsee', 'Massowburg', '' , NULL, 'Bewirtschaftungsmaßnahmen');
insert into gewaesser values (20, 'BRB', 'Breitlingsee', 'Malge', '',  1, '');
insert into gewaesser values (21, 'BRB', 'Großer Wendsee', 'Wendseeufer', '',  1, '');
insert into gewaesser values (22, 'BRB', 'Möserscher See', 'Brandenburg an der Havel OT Kirchmöser Arke', '', 1, '');
insert into gewaesser values (23, 'BRB', 'Plauer See', 'Plau am See', 'Camping- und Ferienpark am Plauer See', 1, '');
insert into gewaesser values (266, 'BRB', 'Beetzsee', 'Grillendamm', '',  NULL, '2015 Neuausweisung');
insert into gewaesser values (24, 'EE', 'Badesee „Hauptteich“','Schönborn OT Lindena', 'Bad Erna',  1, '');
insert into gewaesser values (25, 'EE', 'Badesee Rückersdorf', 'Rückersdorf', 'Hauptstrand',  1, '');
insert into gewaesser values (26, 'EE', 'Waldbad',  'Zeischa',  'Am Rettungsturm',  1, '');
insert into gewaesser values (27, 'EE', 'Grünewalder Lauch Strandbereich', 'Gorden', '',  1, '');
insert into gewaesser values (28, 'EE', 'Kiebitz',  'Falkenberg', 'Am Rettungsturm',  1, '');
insert into gewaesser values (31, 'EE', 'Badesee',  'Brandis',  'Air force Beach',  1, '');
insert into gewaesser values (32, 'FF', 'Helenesee',  'Frankfurt (Oder)', 'Hauptstrand',  1, '');
insert into gewaesser values (33, 'FF', 'Helenesee',  'Frankfurt (Oder)', 'Oststrand',  1, '');
insert into gewaesser values (34, 'FF', 'Helenesee',  'Frankfurt (Oder)', 'Weststrand (FKK)', 1, '');
insert into gewaesser values (35, 'HVL', 'Havel',  'Ketzin/Havel', 'Strandbad',  1, '');
insert into gewaesser values (36, 'HVL', 'Hohennauener See', 'Hohennauen', '',  1, '');
insert into gewaesser values (37, 'HVL', 'Hohennauener See', 'Semlin,',  'Bauerndeich',  1, '');
insert into gewaesser values (38, 'HVL', 'Hohennauener See', 'Ferchesar', 'Dranseschlucht', 1, '');
insert into gewaesser values (39, 'HVL', 'Hohennauener See',  'Wassersuppe', '', 1, '');
insert into gewaesser values (40, 'HVL', 'Hohennauener See', 'Ferchesar', 'Zeltplatz',  1, '');
insert into gewaesser values (41, 'HVL', 'Kleßener See', 'Kleßen', '',  1, '');
insert into gewaesser values (42, 'HVL', 'Nymphensee', 'Brieselang', '',  1, '');
insert into gewaesser values (43, 'LDS', 'Briesener See', 'Briesensee', '',  1, '');
insert into gewaesser values (44, 'LDS', 'Frauensee',  'Gräbendorf', 'KITZ Frauensee', 1, '');
insert into gewaesser values (46, 'LDS', 'Groß Leuthener See', 'Groß Leuthen', '',  1, '');
insert into gewaesser values (47, 'LDS', 'Großer Tonteich', 'Bestensee', 'Körbiskruger Tonsee', 1, '');
insert into gewaesser values (259, 'LDS', 'Heidesee', 'Halbe', '',  1, '');
insert into gewaesser values (48, 'LDS', 'Hölzerner See', 'Gräbendorf', 'KIEZ Hölzerner See', 1, '');
insert into gewaesser values (49, 'LDS', 'Horstteich', 'Bornsdorf', '',  1, '');
insert into gewaesser values (50, 'LDS', 'Kiessee II', 'Bestensee,', 'Liegewiese', 1, '');
insert into gewaesser values (51, 'LDS', 'Klein Köriser See', 'Groß Köris OT Klein Köris', 'Jugendherberge', 1, '');
insert into gewaesser values (52, 'LDS', 'Köthener See', 'Köthen', 'Jugendherberge', 1, '');
insert into gewaesser values (53, 'LDS', 'Krimnicksee', 'Königs Wusterhausen OT Neue Mühle', '', 1, '');
insert into gewaesser values (54, 'LDS', 'Krossinsee', 'Wernsdorf', '',  1, '');
insert into gewaesser values (55, 'LDS', 'Krummer See', 'Krummensee', '',  1, '');
insert into gewaesser values (56, 'LDS', 'Langer See', 'Dolgenbrodt', '',  1, '');
insert into gewaesser values (57, 'LDS', 'Miersdorfer See', 'Zeuthen',  'Freibad',  1, '');
insert into gewaesser values (58, 'LDS', 'Mochowsee',  'Lamsfeld,', 'Campingplatz', 1, '');
insert into gewaesser values (59, 'LDS', 'Motzener See', 'Motzen', '',  1, '');
insert into gewaesser values (60, 'LDS', 'Neuendorfer See', 'Hohenbrück', '',  1, '');
insert into gewaesser values (61, 'LDS', 'Pätzer Vordersee', 'Pätz', '',  1, '');
insert into gewaesser values (62, 'LDS', 'Schweriner See', 'Schwerin', '',  1, '');
insert into gewaesser values (63, 'LDS', 'Schwielochsee', 'Goyatz', '',  1, '');
insert into gewaesser values (64, 'LDS', 'Schwielochsee', 'Jessern',  '',  1, '');
insert into gewaesser values (65, 'LDS', 'Schwielochsee', 'Ressen-Zaue,', 'Campingplatz Zaue', 1, '');
insert into gewaesser values (66, 'LDS', 'Spree', 'Lübben/Steinkirchen', 'Naturbadestelle', 1, '');
insert into gewaesser values (265, 'LDS', 'Spree',  'Lübben/Spreewald', ' SpreeLagune', NULL, '2014 Neuausweisung');
insert into gewaesser values (67, 'LDS', 'Teupitzer See', 'Teupitz',  '',  1, '');
insert into gewaesser values (68, 'LDS', 'Teupitzer See', 'Teupitz,', 'Südufer',  1, '');
insert into gewaesser values (69, 'LDS', 'Todnitzsee', 'Bestensee', '',  1, '');
insert into gewaesser values (70, 'LDS', 'Tonsee Groß', 'Köris OT Klein Köris', '',  1, '');
insert into gewaesser values (71, 'LDS', 'Wolziger See', 'Kolberg',  '',  1, '');
insert into gewaesser values (72, 'LDS', 'Wolziger See', 'Wolzig', '',  1, '');
insert into gewaesser values (73, 'LDS', 'Zeuthener See', 'Eichwalde', '',  1, '');
insert into gewaesser values (74, 'LDS', 'Ziestsee', 'Bindow', '',  1, '');
insert into gewaesser values (76, 'LOS', 'Flakensee',  'Woltersdorf', 'Zeltplatz E 42', 1, '');
insert into gewaesser values (77, 'LOS', 'Glower See', 'Leißnitz OT Glowe', '',  1, '');
insert into gewaesser values (78, 'LOS', 'Großer Kolpiner See', 'Kolpin', '',  1, '');
insert into gewaesser values (79, 'LOS', 'Großer Müllroser See','Müllrose', 'Freibad',  1, '');
insert into gewaesser values (80, 'LOS', 'Großer Müllroser See','Müllrose', 'Strandbad',  2, '');
insert into gewaesser values (81, 'LOS', 'Großer Treppelsee', 'Bremsdorf', 'Zeltplatz',  1, '');
insert into gewaesser values (106, 'LOS', 'Grubensee',  'Limsdorf', '',  1, '');
insert into gewaesser values (82, 'LOS', 'Kalksee',  'Woltersdorf', 'Richard-Wagner-Straße', 1, '');
insert into gewaesser values (83, 'LOS', 'Kiessee',  'Kagel',  'Zeltplatz E 40', 1, '');
insert into gewaesser values (84, 'LOS', 'Möllensee',  'Kagel',  'Grünheide Zeltplatz E 37', 1, '');
insert into gewaesser values (85, 'LOS', 'Peetzsee', 'Grünheide', 'Zeltplatz E 34', 1, '');
insert into gewaesser values (87, 'LOS', 'Ranziger See', 'Ranzig', '',  1, '');
insert into gewaesser values (88, 'LOS', 'Scharmützelsee', 'Bad Saarow', 'Cecilienpark', 1, '');
insert into gewaesser values (89, 'LOS', 'Scharmützelsee', 'Bad Saarow', 'Pieskow',  1, '');
insert into gewaesser values (90, 'LOS', 'Scharmützelsee', 'Bad Saarow', 'Strandbad Mitte',  2, '');
insert into gewaesser values (92, 'LOS', 'Scharmützelsee', 'Diensdorf', '',  1, '');
insert into gewaesser values (93, 'LOS', 'Scharmützelsee', 'Wendisch Rietz', 'Campingplatz Schwarzhorn', 1, '');
insert into gewaesser values (94, 'LOS', 'Scharmützelsee', 'Wendisch Rietz', 'Ferienpark', 1, '');
insert into gewaesser values (95, 'LOS', 'Schervenzsee', 'Schernsdorf', 'Bungalows',  1, '');
insert into gewaesser values (96, 'LOS', 'Schwielochsee', 'Campingplatz Trebatsch - Sawall', '', 1, '');
insert into gewaesser values (97, 'LOS', 'Schwielochsee', 'Niewisch', '',  1, '');
insert into gewaesser values (98, 'LOS', 'Spree',  'Berkenbrück', '',  1, '');
insert into gewaesser values (99, 'LOS', 'Spree bei Beeskow', 'Beeskow',  'Spreepark',  1, '');
insert into gewaesser values (100, 'LOS', 'Springsee',  'Limsdorf', '',  1, '');
insert into gewaesser values (101, 'LOS', 'Störitzsee', 'Spreeau',  'Störitzland',  1, '');
insert into gewaesser values (102, 'LOS', 'Storkower See', 'Dahmsdorf', '',  1, '');
insert into gewaesser values (263, 'LOS', 'Storkower See', 'Storkow,', 'Karlslust',  1, '');
insert into gewaesser values (103, 'LOS', 'Storkower See', 'Storkow,', 'Strandbad',  1, '');
insert into gewaesser values (104, 'LOS', 'Storkower See', 'Storkow,', 'Wolfswinkel',  1, '');
insert into gewaesser values (105, 'LOS', 'Tiefer See', 'Ranzig', '',  1, '');
insert into gewaesser values (107, 'LOS', 'Trebuser See', 'Fürstenwalde-Trebus','Strand',  1, '');
insert into gewaesser values (264, 'LOS', 'Werlsee',  'Grünheide,', 'Nordstrand', 1, '');
insert into gewaesser values (108, 'LOS', 'Werlsee',  'Grünheide,', 'Südstrand',  1, '');
insert into gewaesser values (109, 'MOL', 'Baggersee',  'Gusow',  '',  1, '');
insert into gewaesser values (110, 'MOL', 'Bötzsee',  'Eggersdorf,', 'Strandbad',  1, '');
insert into gewaesser values (111, 'MOL', 'Bötzsee',  '??', 'FKK - „Hochspannung - Postbruch“', 1, '');
insert into gewaesser values (112, 'MOL', 'Dieksee',  'Falkenhagen', '',  1, '');
insert into gewaesser values (113, 'MOL', 'Freibad',  'Zechin', 'Zechin',  1, '');
insert into gewaesser values (114, 'MOL', 'Gabelsee', 'Falkenhagen', '',  1, '');
insert into gewaesser values (115, 'MOL', 'Großer Däbersee', 'Waldsieversdorf,', 'Volksbad', 1, '');
insert into gewaesser values (116, 'MOL', 'Großer Klobichsee', 'Münchehofe', '',  1, '');
insert into gewaesser values (117, 'MOL', 'Großer Stienitzsee', 'Hennickendorf', '',  1, '');
insert into gewaesser values (118, 'MOL', 'Hohenjesarscher See', 'Alt Zeschdorf', '',  1, '');
insert into gewaesser values (119, 'MOL', 'Klostersee', 'Altfriedland', '',  1, '');
insert into gewaesser values (122, 'MOL', 'Schermützelsee', 'Buckow,',  'Strandbad',  1, '');
insert into gewaesser values (123, 'MOL', 'Schwarzer See', 'Falkenhagen', '',  1, '');
insert into gewaesser values (124, 'MOL', 'Straussee',  'Strausberg', 'Jenseits des Sees', 1, '');
insert into gewaesser values (125, 'MOL', 'Straussee',  'Strausberg', 'Liegewiesen Nord - Badstraße', 1, '');
insert into gewaesser values (126, 'MOL', 'Straussee',  'Strausberg,', 'Strandbad',  1, '');
insert into gewaesser values (127, 'MOL', 'Vorder- oder Haussee','Obersdorf', '',  1, '');
insert into gewaesser values (128, 'MOL', 'Waldbad',  'Wriezen',  '',  1, '');
insert into gewaesser values (129, 'MOL', 'Weinbergsee', 'Diedersdorf', '',  1, '');
insert into gewaesser values (130, 'OHV', 'Bernsteinsee', 'Velten', '',  1, '');
insert into gewaesser values (131, 'OHV', 'Große Plötze', 'Löwenberger Land OT Neuendorf', '', 1, '');
insert into gewaesser values (132, 'OHV', 'Großer Stechlinsee', 'Gransee Gem. Stechlin OT Neuglobsow', '', 1, '');
insert into gewaesser values (133, 'OHV', 'Großer Wentowsee', 'Zehdenick OT Marienthal', '',  1, '');
insert into gewaesser values (134, 'OHV', 'Haussee',  'Fürstenberg OT Himmelpfort-Pian', '', 1, '');
insert into gewaesser values (135, 'OHV', 'Kiessee',  'Mühlenbecker Land OT Schildow', '',  1, '');
insert into gewaesser values (136, 'OHV', 'Kleiner Wentowsee', 'Gransee OT Seilershof', '',  1, '');
insert into gewaesser values (137, 'OHV', 'Lehnitzsee', 'Oranienburg', '',  1, '');
insert into gewaesser values (138, 'OHV', 'Menowsee', 'Fürstenberg OT Steinförde', '', 1, '');
insert into gewaesser values (139, 'OHV', 'Moderfitzsee', 'Fürstenberg OT Himmelpfort', '',  1, '');
insert into gewaesser values (140, 'OHV', 'Mühlensee',  'Liebenwalde', '',  1, '');
insert into gewaesser values (141, 'OHV', 'Nieder Neuendorfer See', 'Hennigsdorf OT Nieder Neuendorf', '', 1, '');
insert into gewaesser values (142, 'OHV', 'Peetschsee', 'Fürstenberg OT Steinförde', '', 1, '');
insert into gewaesser values (143, 'OHV', 'Rahmer See', 'Mühlenbecker Land OT Zühlsdorf', '', 1, '');
insert into gewaesser values (144, 'OHV', 'Röblinsee',  'Fürstenberg', '',  1, '');
insert into gewaesser values (145, 'OHV', 'Roofensee',  'Gransee Gem. Stechlin OT Menz', '', 1, '');
insert into gewaesser values (146, 'OHV', 'Stolpsee', 'Fürstenberg OT Himmelpfort', 'Campingplatz', 1, '');
insert into gewaesser values (147, 'OHV', 'Stolpsee', 'Fürstenberg OT Himmelpfort', 'Fürstenberger Straße', 1, '');
insert into gewaesser values (148, 'OHV', 'Waldbad',  'Zehdenick-Neuhof', '',  1, '');
insert into gewaesser values (149, 'OHV', 'Waldsee',  'Oranienburg OT Germendorf', 'Tier- und Freizeitpark', 1, '');
insert into gewaesser values (151, 'OPR', 'Dranser See', 'Schweinrich', '',  1, '');
insert into gewaesser values (152, 'OPR', 'Dranser See', 'Schweinrich', 'Blanschen', 1, '');
insert into gewaesser values (153, 'OPR', 'Grienericksee', 'Rheinsberg', '',  1, '');
insert into gewaesser values (154, 'OPR', 'Großer Prebelowsee', 'Kleinzerlang', '',  1, '');
insert into gewaesser values (155, 'OPR', 'Großer Zechliner See','Kagar', '',  1, '');
insert into gewaesser values (270, 'OPR', 'Großer Zechliner See','Flecken Zechlin', '',  NULL,'2016 Neuausweisung');
insert into gewaesser values (156, 'OPR', 'Gudelacksee', 'Lindow',  '',  1, '');
insert into gewaesser values (157, 'OPR', 'Kalksee',  'Binenwalde', '',  1, '');
insert into gewaesser values (158, 'OPR', 'Kleiner Pälitzsee', 'Kleinzerlang', '',  1, '');
insert into gewaesser values (159, 'OPR', 'Klempowsee', 'Wusterhausen,', 'Freibad', 1, '');
insert into gewaesser values (160, 'OPR', 'Königsberger See', 'Königsberg', '',  1, '');
insert into gewaesser values (161, 'OPR', 'Molchowsee', 'Neuruppin OT Molchow', '',  1, '');
insert into gewaesser values (162, 'OPR', 'Ruppiner See', 'Neuruppin OT Altruppin', 'Seebad',  1, '');
insert into gewaesser values (163, 'OPR', 'Ruppiner See', 'Neuruppin OT Gnewikow', '',  1, '');
insert into gewaesser values (164, 'OPR', 'Ruppiner See', 'Neuruppin', 'Hotel Waldfrieden', 1, '');
insert into gewaesser values (165, 'OPR', 'Ruppiner See', 'Neuruppin', 'Jahnbad', 1, '');
insert into gewaesser values (166, 'OPR', 'Ruppiner See', 'Wustrau', 'Am Schloß', 1, '');
insert into gewaesser values (167, 'OPR', 'Schlabornsee', 'Zechlinerhütte', '',  1, '');
insert into gewaesser values (168, 'OPR', 'Untersee', 'Bantikow',  '',  1, '');
insert into gewaesser values (169, 'OPR', 'Untersee', 'Kyritz',  'Freibad', 1, '');
insert into gewaesser values (170, 'OPR', 'Wutzsee',  'Lindow',  'Schönbirken', 1, '');
insert into gewaesser values (171, 'OPR', 'Zermittensee', 'Kagar', '',  1, '');
insert into gewaesser values (172, 'OPR', 'Zermützelsee', 'Neuruppin,', 'Zermützel', 1, '');
insert into gewaesser values (173, 'OPR', 'Zootzensee', 'Zechlinerhütte', '',  1, '');
insert into gewaesser values (262, 'OSL', 'Gräbendorfer See', 'Laasow',  'Tauchschule', 1, '');
insert into gewaesser values (174, 'OSL', 'Grünewalder Lauch', 'Grünewalde', '',  1, '');
insert into gewaesser values (175, 'OSL', 'Senftenberger See', 'Großkoschen', '',  1, '');
insert into gewaesser values (176, 'OSL', 'Senftenberger See', 'Niemtsch',  '',  1, '');
insert into gewaesser values (177, 'OSL', 'Senftenberger See', 'Senftenberg-Stadt', '',  1, '');
insert into gewaesser values (178, 'OSL', 'Senftenberger See', 'Senftenberg/Buchwalde', '',  1, '');
insert into gewaesser values (179, 'P', 'Havel Templiner See', 'Templin',  'Waldbad',  1, '');
insert into gewaesser values (180, 'P', 'Havel Tiefer See', 'Babelsberg', 'Stadtbad Park Babelsberg', 1, '');
insert into gewaesser values (181, 'PM', 'Beetzsee', 'Butzow,', 'Campingplatz', 1, '');
insert into gewaesser values (182, 'PM', 'Beetzsee', 'Gortz,',  'Campingplatz', 1, '');
insert into gewaesser values (183, 'PM', 'Beetzsee', 'Päwesin,', 'KiEZ Bollmannsruh', 1, '');
insert into gewaesser values (184, 'PM', 'Glindower See', 'Glindow', 'Strandbad',  1, '');
insert into gewaesser values (185, 'PM', 'Glindower See', 'Werder,', 'Blütencamping „Riegelspitze“', NULL, 'Veränderung Bewirtschaftungsmaßnahmen');
insert into gewaesser values (186, 'PM', 'Plessower See', 'Werder',  'Strandbad',  1, '');
insert into gewaesser values (187, 'PM', 'Schwielowsee', 'Caputh',  'Strandbad',  1, '');
insert into gewaesser values (188, 'PM', 'Schwielowsee', 'Ferch', 'Strandbad',  1, '');
insert into gewaesser values (190, 'SPN', 'Deulowitzer See', 'Atterwasch', '',  1, '');
insert into gewaesser values (191, 'SPN', 'Großsee',  'Tauer', '',  1, '');
insert into gewaesser values (194, 'TF', 'Glieniksee', 'Dobbrikow', 'Camp',  1, '');
insert into gewaesser values (195, 'TF', 'Gottower See', 'Gottow,', 'Strand',  1, '');
insert into gewaesser values (196, 'TF', 'Großer Wünsdorfer See','Wünsdorf', 'Strand Neuhof',  1, '');
insert into gewaesser values (197, 'TF', 'Großer Wünsdorfer See','Wünsdorf', 'Strandbad',  1, '');
insert into gewaesser values (198, 'TF', 'Großer Zeschsee', 'Lindenbrück OT Zesch', '',  1, '');
insert into gewaesser values (199, 'TF', 'Kiessee', 'Horstfelde,', 'Wasserskianlage',  1, '');
insert into gewaesser values (200, 'TF', 'Kiessee', 'Rangsdorf', 'Strand',  1, '');
insert into gewaesser values (201, 'TF', 'Kliestower See', 'Kliestow', 'Strand',  1, '');
insert into gewaesser values (202, 'TF', 'Körbaer See', 'Erholungsgebiet', 'Körbaer Teich',  1, '');
insert into gewaesser values (203, 'TF', 'Krummer See', 'Sperenberg', 'Strandbad',  1, '');
insert into gewaesser values (204, 'TF', 'Mahlower See', 'Mahlow',  'Strand',  1, '');
insert into gewaesser values (205, 'TF', 'Mellensee', 'Klausdorf', 'Strandbad',  1, '');
insert into gewaesser values (206, 'TF', 'Mellensee', 'Mellensee', 'Strandbad',  1, '');
insert into gewaesser values (207, 'TF', 'Motzener See', 'Kallinchen', 'Campingplatz', 1, '');
insert into gewaesser values (208, 'TF', 'Motzener See', 'Kallinchen', 'Campingplatz AKK', 1, '');
insert into gewaesser values (209, 'TF', 'Motzener See', 'Kallinchen,', 'Strandbad',  1, '');
insert into gewaesser values (210, 'TF', 'Rangsdorfer See', 'Rangsdorf', 'Seebad',  1, '');
insert into gewaesser values (211, 'TF', 'Siethener See', 'Siethen', 'Strand Potsdamer Chaussee, Ortsausgang', 1, '');
insert into gewaesser values (212, 'TF', 'Vordersee', 'Dobbrikow,', 'Strand',  1, '');
insert into gewaesser values (213, 'UM', 'Brüssower See', 'Brüssow', 'Seebad',  1, '');
insert into gewaesser values (214, 'UM', 'Carwitzer See', 'Thomsdorf', '',  1, '');
insert into gewaesser values (215, 'UM', 'Dreetzsee', 'Thomsdorf', 'Campingplatz', 1, '');
insert into gewaesser values (216, 'UM', 'Fährsee', 'Templin', 'Campingplatz', 1, '');
insert into gewaesser values (217, 'UM', 'Gleuensee', 'Klosterwalde', 'Zeltplatz',  1, '');
insert into gewaesser values (218, 'UM', 'Gollinsee', 'Gollin',  '',  1, '');
insert into gewaesser values (267, 'UM', 'Großer Krinertsee', 'Temmen',  '',  NULL, '2015 Neuausweisung');
insert into gewaesser values (219, 'UM', 'Großer Kronsee', 'Rutenberg', '',  1, '');
insert into gewaesser values (220, 'UM', 'Großer Kuhsee', 'Gramzow', '',  1, '');
insert into gewaesser values (221, 'UM', 'Großer Lychensee', 'Lychen',  'Stadtbad', 1, '');
insert into gewaesser values (222, 'UM', 'Großer See',  'Hohengüstow', '',  1, '');
insert into gewaesser values (223, 'UM', 'Großer See',  'Fürstenwerder', '',  1, '');
insert into gewaesser values (224, 'UM', 'Großer Väter-See', 'Groß Väter', '',  1, '');
insert into gewaesser values (225, 'UM', 'Großer Warthesee', 'Warthe',  '',  1, '');
insert into gewaesser values (226, 'UM', 'Haussee', 'Hardenbeck', '',  1, '');
insert into gewaesser values (227, 'UM', 'Kastavensee', 'Retzow,', 'Kastaven', 1, '');
insert into gewaesser values (228, 'UM', 'Kleinowsee',  'Falkenwalde OT Neu Kleinow', '',  1, '');
insert into gewaesser values (229, 'UM', 'Lübbesee',  'Milmersdorf OT Petersdorf', '', 1, '');
insert into gewaesser values (230, 'UM', 'Lübbesee',  'Templin', 'Seehotel', 1, '');
insert into gewaesser values (231, 'UM', 'Lützlower See', 'Lützlow', '',  1, '');
insert into gewaesser values (269, 'UM', 'Mühlensee', 'Schwaneberg', '',  NULL, '2016 Neuausweisung');
insert into gewaesser values (232, 'UM', 'Naugartener See', 'Naugarten', '',  1, '');
insert into gewaesser values (233, 'UM', 'Oberuckersee', 'Fergitz', '',  1, '');
insert into gewaesser values (234, 'UM', 'Oberuckersee', 'Warnitz', 'Quast ',  1, '');
insert into gewaesser values (235, 'UM', 'Oberuckersee', 'Stegelitz', 'Schifferhof',  1, '');
insert into gewaesser values (236, 'UM', 'Oberuckersee', 'Warnitz', 'Campingplatz', 1, '');
insert into gewaesser values (237, 'UM', 'Oberuckersee', 'Warnitz,', 'Ferienhaussiedlung', 1, '');
insert into gewaesser values (268, 'UM', 'Randowtal', 'Schmöllner See', '', NULL, '2016 Neuausweisung');
insert into gewaesser values (238, 'UM', 'Röddelinsee', 'Röddelin', 'Zeltplatz',  1, '');
insert into gewaesser values (239, 'UM', 'Röddelinsee', 'Templin OT Hindenburg', '',  1, '');
insert into gewaesser values (240, 'UM', 'Sabinensee',  'Willmine', '',  1, '');
insert into gewaesser values (241, 'UM', 'Schumellensee', 'Boitzenburg', '',  1, '');
insert into gewaesser values (242, 'UM', 'Templiner See', 'Templin', 'Freibad',  1, '');
insert into gewaesser values (243, 'UM', 'Templiner See', 'Templin', 'Schinderkuhle',  1, '');
insert into gewaesser values (244, 'UM', 'Unteruckersee', 'Prenzlau', 'Am Kap',  1, '');
insert into gewaesser values (245, 'UM', 'Unteruckersee', 'Prenzlau', 'Seebadeanstalt', 1, '');
insert into gewaesser values (246, 'UM', 'Unteruckersee', 'Röpersdorf', '',  1, '');
insert into gewaesser values (247, 'UM', 'Wolletzsee',  'Angermünde', 'Strandbad',  1, '');
insert into gewaesser values (248, 'UM', 'Wurlsee', 'Lychen',  'Zeltplatz 79', 1, '');
insert into gewaesser values (249, 'UM', 'Wurlsee', 'Retzow',  'Wurlgrund',  1, '');
insert into gewaesser values (251, 'UM', 'Zaarsee', 'Templin OT Ahrensdorf', '',  1, '');
insert into gewaesser values (252, 'UM', 'Zenssee', 'Lychen,', 'Wuppgarten', 1, '');
insert into gewaesser values (253, 'UM', 'Zenssee', 'Lychen,', 'Heilstätten',  1, '');
