:ref:`« Fragen-Übersicht BKZFRAU <bkzfraustart>`

.. _selfjoinII:

.. index:: selfjoin (sqlite/bkzfrau)

================
Demo -- selfjoin
================

.. image:: ./images/unkraut.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/unkraut.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/unkraut.jpg">
   </a>

.. sidebar:: Unkraut

   |b|

|a|

     
Geben Sie alle Datensätze mit einer laufenden Nummerierung aus.

- Wobei die Ausgabe auf die ersten 10 Tupel begrenzt sein soll,
- und die Überschrift als "Zeilennummer" auszugeben ist.

:Siehe auch:  `Muster für Nummerierung`_  

oder hier der Lösungsansatz aus der Online-Diskussion:
::
   
   sqlite_version():3.6.11 
   CREATE TABLE test1(b TEXT); 
   INSERT INTO test1(b) VALUES('hello A'); 
   INSERT INTO test1(b) VALUES('hello B'); 
   INSERT INTO test1(b) VALUES('hello C'); 
   SELECT * FROM test1; 
   
   b 
   hello A 
   hello B 
   hello C 
   
   select (select COUNT(0) 
   from test1 t1 
   where t1.b <= t2.b) as 'Row Number', b from test1 t2 ORDER BY b; 
   
   Row Number|b 
   1|hello A 
   2|hello B 
   3|hello C 
   
   select (select COUNT(0) 
   from test1 t1 
   where t1.b >= t2.b) as 'Row Number', b from test1 t2 ORDER by b DESC; 

   Row Number|b 
   1|hello C 
   2|hello B 
   3|hello A 

Antwort?
========

HINWEIS: Das funktioniert, wie hier gezeigt, nur mit der SQLite-Datenbank.

::
      
   ???

Das gesuchte Ergebnis:
::

     
   Zeilennumner     beruf                                             
   ---------------  --------------------------------------------------
   1                Aalbrutzüchterin                                 
   2                Aalkorbmacherin                                   
   3                Aalräucherin                                     
   4                Aalschnurfischerin                                
   5                Aalstickerin                                      
   6                Abbauhauerin                                      
   7                Abbeizerin (Dekapierer)                           
   8                Abbollerin                                        
   9                Abbrecherin (Druckerhelfer)                       
   10               Abbrecherin (Flachglasmacher)
   

.. _Muster für Nummerierung: http://sqlite.1065341.n5.nabble.com/sequential-row-numbers-from-query-td47370.html
