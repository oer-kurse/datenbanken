Subqueries
==========
.. toctree::
   :maxdepth: 1
	      
   aufgabe-4-1
   aufgabe-4-2
   aufgabe-4-3
   aufgabe-4-4
   aufgabe-4-5
   aufgabe-4-6
   aufgabe-4-7
   aufgabe-4-8
   aufgabe-4-9
   aufgabe-4-10
