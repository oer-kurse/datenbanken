===========
Aufgabe 4.6
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 4.6 (Unterabfragen/Funktionen)

.. image:: ./images/kriechband-bei-honigsteinen.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/kriechband-bei-honigsteinen.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/kriechband-bei-honigsteinen.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`

Geben Sie die Angestellten mit dem geringsten Gehalt aus.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    persnr | vname | nname | projnr | telefonnr | gehalt 
   --------+-------+-------+--------+-----------+--------
         7 | Gitta | Gans  |     23 |      2202 |      0

   (1 row)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
