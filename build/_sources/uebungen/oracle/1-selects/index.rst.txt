Einfache Selects
================

.. toctree::
   :maxdepth: 1

   aufgabe-1-1
   aufgabe-1-2
   aufgabe-1-3
   aufgabe-1-4
   aufgabe-1-5
   aufgabe-1-6
   aufgabe-1-7
   aufgabe-1-8
   aufgabe-1-9
   aufgabe-1-10
