.. _create_tabele_sqlite:

:ref:`« Management SQLite <bkzfrau-start>`

.. index:: sqlite; tables

========
Tabellen
========

.. image:: ./images/frost002.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/frost002.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/frost002.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Pflanzen

|a|
            

Die Relation (Tabelle) ist der wichtigste Objekt-Typ
in relationalen Datenbanken.
	   

Liste der Tabellen:
===================

::

   .tables

.. index:: sqlite; Tabellenstruktur
	   
Struktur einer existierenden Tabelle:
=====================================
::

   .schema input
   CREATE TABLE input (id INTEGER PRIMARY KEY   AUTOINCREMENT,
            name TEXT,
            data TEXT,
            desc TEXT);

Etwas hübschere Ausgabe:
::

   .schema --indent
   CREATE TABLE input(
     id INTEGER PRIMARY KEY AUTOINCREMENT,
     name TEXT,
     data TEXT,
     desc TEXT
   );

	    
.. index:: sqlite; Skript einlesen
	   
Tabelle anlegen
===============

::

   CREATE TABLE...

Die möglichen Datentypen werden hier beschrieben:

https://www.sqlite.org/datatype3.html

Tabelle löschen
===============
::

   DROP TABLE...
   
