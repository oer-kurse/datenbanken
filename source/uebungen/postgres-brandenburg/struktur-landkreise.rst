:ref:`« Start Gewässer <startgewaesser>`
     
=====================
 Struktur Landkreise
=====================

.. image:: ./images/schattenspiel.jpg
   :width: 0

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/schattenspiel.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/schattenspiel.jpg">
   </a>


.. sidebar:: Serie: Schattenspiel

   |b|


|a|

.. index:: Übung; Import von Daten (Landkreise)
.. index:: Übung; CREATE TABLE

Die Daten dieser Relation wurden der folgenden Wikipedia-Website
[#]_. entnommen, umstrukturiert und wie folgt in die Relation
*landkreise* eingefügt:

:Aufgabe(n):

   - erstellen Sie die Relation *landkreise* mit der folgenden
     SQL-Anweisung
   - lesen Sie die Daten in die Relation *landkreise* ein
   - verwenden Sie das Skript *data-landkreise.sql*
   - :download:`Download: Daten der Landkreise <data-landkreise.sql>`.

     
.. code:: sql
   

   CREATE TABLE landkreise (
     kuerzel_lk    VARCHAR(3),
     landkreis     VARCHAR(30),
     kreisstadt    VARCHAR(30),
     kfz           VARCHAR(20),
     einwohner     INT,
     flaeche        FLOAT,
     anmerkungen   TEXT);



.. [#] https://de.wikipedia.org/wiki/Liste\_der\_Landkreise\_und\_kreisfreien\_St%C3%A4dte\_in\_Brandenburg

