=========================
Fragen zu den Landkreisen
=========================



:ref:`« Start Gewässer <startgewaesser>`



.. image:: ./images/faehre-caputh.jpg
   :width: 0


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/faehre-caputh.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/faehre-caputh.jpg">
   </a>


.. sidebar:: Serie: Technik

   |b|

   Fähre Caputh
   

|a|

Fragen -- Hinweise -- Lösungen
------------------------------

Wieviele Einwohner hat jeder Landkreis?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Ändern Sie im zweiten Schritt die Spaltenüberschrift 
  in **Einwohner** bzw. **Landkreis** um!

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende die »alias«-Funktion.
    <br/>

`Postgres-Doku: »AS/ALIAS« <https://www.postgresql.org/docs/8.1/static/sql-select.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
    src="../../_static/casts/pg-brb-landkreise-1.cast"
    theme="solarized-light"
    font-size="medium"
    rows="50"
    poster="data:text/plain,\e[5;5H Umbenennungen für die Ausgabe \e[1;33m">
    </asciinema-player>

    </div>
    </div>

Geben Sie nur den Landkreis mit der größten Einwohnerzahl aus:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es gibt zwei Lösungsansätze, finden Sie wenigstens eine Lösung.

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende die folgenden Funktionen:

[Postgres-Doku: »MAX (Aggregat-Funktionen)«][MAX (Aggregat-Funktionen)]]

`Postgres-Doku »SUBQUERY (DML)« <https://www.postgresql.org/docs/current/functions-aggregate.html>`_

`Postgres-Doku: »LIMIT (DML)« <https://www.postgresql.org/docs/current/queries-limit.html>`_

`Postgres-Doku: »ORDER BY (DML)« <https://www.postgresql.org/docs/current/sql-select.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
    src="../../_static/casts/pg-brb-gewaesser-2.cast"
    theme="solarized-light"
    font-size="medium"
    rows="25"
    poster="data:text/plain,\e[5;5H größte Einwohnerzahl \e[1;33m">
    </asciinema-player>

    </div>
    </div>
