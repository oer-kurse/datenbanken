

DROP TABLE landkreise;

CREATE TABLE landkreise (
  kuerzel_lk varchar(3),
  landkreis VARCHAR(30),
  kreisstadt VARCHAR(30),
  kfz VARCHAR(20),
  einwohner INT,
  flaeche FLOAT,
  anmerkungen TEXT);

  
INSERT INTO landkreise VALUES(
 'BAR',
 'Barnim',
 'Eberswalde',
 'BAR, BER, EW',
 177411,
 1479.67,
 'grenzt an Berlin und Polen; entstanden aus den Kreisen Bernau, Eberswalde und Teilen von Bad Freienwalde');


INSERT INTO landkreise VALUES(
  'BRB',
  'Brandenburg an der Havel',
  'Brandenburg an der Havel',
  'BRB',
  71574,
  229.71,
  'flächengrößte kreisfreie Stadt Brandenburgs');


-- Wappen der Stadt Brandenburg an der Havel
-- Lage der Stadt Brandenburg an der Havel im Land Brandenburg
-- Dom St. Peter und Paul

INSERT INTO landkreise VALUES(
  'CB',
  'Cottbus',
  'Cottbus',
  'CB',
  '99687',
  '165.15',
  '(Chóśebuz) -- kreisfreie Stadt');


INSERT INTO landkreise VALUES(
  'LDS',
  'Dahme-Spreewald',
  'Lübben',
  'LDS, KW, LC, LN',
  '164528',
  '2274.48',
  '(Damna-Błota) grenzt an Berlin; größter natürlicher See Brandenburgs (Schwielochsee; 13,3 km²); entstanden aus den Kreisen Königs Wusterhausen, Lübben und Luckau Schlossinsel in Lübben');

INSERT INTO landkreise VALUES(
  'EE',
  'Elbe-Elster',
  'Herzberg',
  'EE, FI, LIB',
  '104673',
  '1899.54',
  'höchster Punkt Brandenburgs (Heidehöhe, 201,4 m); grenzt an Sachsen und Sachsen-Anhalt; entstanden aus den Kreisen Finsterwalde, Herzberg und Teilen von Bad Liebenwerda Elstermühle in Plessa');

INSERT INTO landkreise VALUES(
  'FF',
  'Frankfurt/Oder',
  'Frankfurt/Oder',
  'FF',
  '58092',
  '147.85',
  'kleinste kreisfreie Stadt Brandenburgs; grenzt an Polen');

INSERT INTO landkreise VALUES(
  'HVL',
  'Havelland',
  'Rathenow',
  'HVL, NAU, RN',
  '158236',
  '1727.3',
  'grenzt an Berlin und Sachsen-Anhalt; entstanden aus den Kreisen Nauen und Rathenow Großer Havelländischer Hauptkanal');

INSERT INTO landkreise VALUES(
  'MOL',
  'Märkisch-Oderland',
  'Seelow',
  'MOL, FRW, SEE, SRB',
  '190714',
  '2158.67',
  'grenzt an Polen und Berlin; entstanden aus den Kreisen Seelow, Strausberg und Teilen von Bad Freienwalde');
  
INSERT INTO landkreise VALUES(
  'OHV',  
  'Oberhavel',
  'Oranienburg',
  'OHV',
  '207524',
  '1808.18',
  'grenzt an Berlin und Mecklenburg-Vorpommern; entstanden aus den Kreisen Gransee und Oranienburg');
  
INSERT INTO landkreise VALUES(
  'OSL',
  'Oberspreewald-Lausitz',
  'Senftenberg',
  'OSL, CA, SFB',
  '112450',
  '1223.08',
  '(Górne Błota-Łužyca) flächenmäßig kleinster Landkreis Brandenburgs; grenzt an Sachsen; größter künstlicher See Brandenburgs (Sedlitzer See; 13,3 km²); entstanden aus den Kreisen Calau, Senftenberg und Teilen von Bad Liebenwerda');

INSERT INTO landkreise VALUES(
  'LOS',
  'Oder-Spree',
  'Beeskow',
  'LOS',
  '182397',
  '2256.78',
  'grenzt an Polen und Berlin; entstanden aus den Kreisen Beeskow, Eisenhüttenstadt-Land und Fürstenwalde, sowie der kreisfreien Stadt Eisenhüttenstadt');

INSERT INTO landkreise VALUES(
  'OPR',
  'Ostprignitz-Ruppin',
  'Neuruppin',
  'OPR, KY, NP, WK',
  '99110',
  '2526.55',
  'grenzt an Mecklenburg-Vorpommern und Sachsen-Anhalt; entstanden aus den Kreisen Neuruppin, Wittstock und Teilen der Kreise Kyritz und Pritzwalk');

INSERT INTO landkreise VALUES(
  'P',
  'Potsdam',
  'Potsdam',
  'P',
  '167745',
  '188.25',
  'Landeshauptstadt; bevölkerungsreichste Stadt Brandenburgs; grenzt an Berlin');

INSERT INTO landkreise VALUES(
  'PM',
  'Potsdam-Mittelmark',
  'Bad Belzig',
  'PM',
  '210910',
  '2591.97',
  'bevölkerungsreichster Landkreis Brandenburgs; Kreisgebiet durch die Stadt Brandenburg an der Havel geteilt, grenzt an Berlin und Sachsen-Anhalt; entstanden aus den Kreisen Brandenburg, Belzig und Potsdam');

INSERT INTO landkreise VALUES(
  'PR',
  'Prignitz',
  'Perleberg',
  'PR',
  '77573',
  '2138.61',
  'bevölkerungsärmster Landkreis Brandenburgs; grenzt an Mecklenburg-Vorpommern, Niedersachsen und Sachsen-Anhalt; entstanden aus dem Kreis Perleberg und Teilen der Kreise Kyritz und Pritzwalk');

INSERT INTO landkreise VALUES(
  'SPN',
  'Spree-Neiße',
  'Forst',
  'SPN, FOR, GUB, SPB',
  '117635',
  '1657.45',
  '(Sprjewja-Nysa) grenzt an Polen und Sachsen; entstanden aus den Kreisen Cottbus-Land, Forst, Guben und Spremberg');

INSERT INTO landkreise VALUES(
  'TF',
  'Teltow-Fläming',
  'Luckenwalde',
  'TF',
  '163553',
  '2104.19',
  'grenzt an Berlin und Sachsen-Anhalt; entstanden aus den Kreisen Jüterbog, Luckenwalde und Zossen	Kloster Zinna');
  
INSERT INTO landkreise VALUES(
  'UM',
  'Uckermark',
  'Prenzlau',
  'UM, ANG, PZ, SDT, TP',
  '121014',
  '3076.93',
  'flächengrößter Landkreis Brandenburgs; grenzt an Polen und Mecklenburg-Vorpommern; entstanden aus den Kreisen Angermünde, Prenzlau und Templin, sowie der kreisfreien Stadt Schwedt/Oder');

DROP TABLE images;

CREATE TABLE images (
  id SERIAL,
  kuerzel_lk character varying(3),
  bild VARCHAR(100),
  alt varchar(50),
  kategorie character varying(20), 
  bildquelle text,
  tags character varying(50), 
  datum date,
  lizenz character varying(10)); 

INSERT INTO images (bild, alt, kategorie, bildquelle, kuerzel_lk)
VALUES(
 'Wappen_Landkreis_Barnim.svg',
 'Wappen des Landkreises Barnim',
 'wappen',
 'https://de.wikipedia.org/wiki/Datei:Wappen_Landkreis_Barnim.svg',
 'BAR');

INSERT INTO images (bild, alt, kategorie, bildquelle, kuerzel_lk)
values(
  'Chorin_abbey2.JPG', 
  'Kloster Chorin',
  'gebauede',
  'https://de.wikipedia.org/wiki/Datei:Chorin_abbey2.JPG',
  'BAR');

INSERT INTO images (bild, alt, kategorie, bildquelle, kuerzel_lk)
VALUES(
 'Brandenburg_BAR.svg', 
 'Lage des Landkreises Barnim im Land Brandenburg',
 'karte',
 'https://de.wikipedia.org/wiki/Datei:Brandenburg_BAR.svg',
 'BAR');

