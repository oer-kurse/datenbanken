:ref:`« Start Gewässer <startgewaesser>`

.. meta::

   :description lang=de: Landkreise Brandenburg
   :keywords: Datenbanken, PostgreSQL, Modellierung, Landkreise, Brandenburg

     
======================
Struktur: Badegewässer
======================
.. image:: ./images/schattenspiel2.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/schattenspiel2.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/schattenspiel2.jpg">
   </a>



.. sidebar:: Serie: Schattenspiel

   |b|

   | Sächsische Schweiz
   | auf dem Weg zur
   | »Lokomotive«.

|a|


- verwenden Sie die folgenden Daten:

| :download:`Download: Daten zu den Gewässern (SQL) <data-gewaesser.sql>`.

Tabelle anlegen:
================

.. code:: sql


   CREATE TABLE  gewaesser (
     id          INTEGER,
     kuerzel_lk  VARCHAR(3),
     bezeichnung VARCHAR(100),
     ort         VARCHAR(100),
     zusatz      VARCHAR(100),
     praedikat   INTEGER,
     anmerkung   VARCHAR(50)
   );

Siehe auch: `CREATE TABLE (DDL)`_

.. index:: Übung; Import von Daten (gewaesser)
.. index:: Übung; SQL-Scripte ausführen (gewaesser)
	   
.. code:: bash


   \i /absoluter/pfad/zur/datei/data-gewaesser-ansi.sql
   

Wurden alle Datensätze korrekt eingelesen?
==========================================

- Vergleichen Sie das Ergebnis der *count*-Funktion
  mit der Anzahl Zeilen im Script...
- Welche Datensätze fehlen und welche Fehler wurden gemacht?
- Korrigieren Sie die Fehler und wiederholen Sie die Einleseprozedur...

siehe auch: `COUNT (Aggregate-Funktion)`_

.. code:: sql


   select count(*) from gewaesser;

Welche Felder sind zu groß dimensioniert?
=========================================
- Mit den Funktionen *max* und *length* können die größten Werte ermittelt werden.
- Ändern Sie die Größe der anderen Attribute, wenn es Sinn macht.
- Erschaffen sie die Relation neu und verwenden Sie die ermittelten
  Werte inklusive eines Puffers.
  
  - Löschen Sie die Tabelle als Ganzes 
  - alternativ kann die Eigenschaft mit *alter table* geändert werden.
    
     
.. code:: sql
   
   select max(length(ort)) from gewaesser;

siehe auch:


- `MAX (Aggregate-Funktion)`_
- `LENGTH (String-Funktion)`_
- `DROP TABLE (DDL)`_
- `ALTER TABLE (DDL)`_
  
..
   create table gewaesser (
     id integer,
     kuerzel_lk varchar(3),
     bezeichnung varchar(50),
     ort  varchar(50),
     zusatz varchar(50),
     praedikat integer,
     anmerkung varchar(50)
   );


.. _Amtsblatt-2018-16.pdf: /downloads/Amtsblatt-2018-16.pdf
.. _CREATE TABLE (DDL): https://www.postgresql.org/docs/current/static/sql-createtable.html
.. _CREATE DATABASE (DDL): https://www.postgresql.org/docs/current/static/sql-createdatabase.html
.. _LENGTH (String-Funktion): https://www.postgresql.org/docs/9.6/static/functions-aggregate.html
.. _MAX (Aggregate-Funktion): https://www.postgresql.org/docs/9.6/static/functions-aggregate.html
.. _COUNT (Aggregate-Funktion): https://www.postgresql.org/docs/9.6/static/functions-aggregate.html
.. _DROP TABLE (DDL): https://www.postgresql.org/docs/9.6/static/sql-droptable.html
.. _ALTER TABLE (DDL): https://www.postgresql.org/docs/9.6/static/sql-altertable.html
.. _INSERT INTO (DML): https://www.postgresql.org/docs/9.6/static/sql-insert.html
.. _COPY (postgres): https://www.postgresql.org/docs/9.6/static/sql-copy.html
