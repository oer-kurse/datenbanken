DROP TABLE images;

CREATE TABLE images (
  id         SERIAL,
  kuerzel_lk VARCHAR(3),
  bild       VARCHAR(100),
  alt        TEXT,
  kategorie  VARCHAR(20),
  bildquelle TEXT,
  tags       VARCHAR(50),
  datum      DATE,
  lizenz     VARCHAR(10));


INSERT INTO images (kuerzel_lk, bild, alt, kategorie, bildquelle, tags, datum, lizenz)
VALUES(
 'BAR',
 'wappen-landkreis-barnim.svg',
 'Wappen des Landkreises Barnim',
 'wappen',
 'https://de.wikipedia.org/wiki/Datei:Wappen_Landkreis_Barnim.svg',
 'Barnin, wappen, svg',
 NULL,
 'CC0');
 
INSERT INTO images (kuerzel_lk, bild, alt, kategorie, bildquelle, tags, datum, lizenz)
values(
  'BAR',
  'chorin-abbey2.jpg', 
  'Kloster Chorin',
  'gebauede',
  'https://de.wikipedia.org/wiki/Datei:Chorin_abbey2.JPG',
  'Kolster Chorin, kloster, jpg',
  NULL,
  'CC0');

INSERT INTO images (kuerzel_lk, bild, alt, kategorie, bildquelle, tags, datum, lizenz)
VALUES(
  'BRB',
  'Brandenburg-bar.svg', 
  'Lage des Landkreises Barnim im Land Brandenburg',
  'karte',
  'https://de.wikipedia.org/wiki/Datei:Brandenburg_BAR.svg',
  'karte, Barninm, svg',
  NULL,
  'CC0');

INSERT INTO images (kuerzel_lk, bild, alt, kategorie, bildquelle, tags, datum, lizenz)
VALUES(
  'BRB',
  'Brandenburg-an-der-havel.png',
  'Wappen der Stadt Brandenburg an der Havel',
  'wappen',
  '',
  'wappen, Brandenburg, svg',
  NULL,
  'CC0');

INSERT INTO images (kuerzel_lk, bild, alt, kategorie, bildquelle, tags, datum, lizenz)
VALUES(
  'CB',
  '',
  'Wappen der Stadt Cottbus',
  'wappen',
  'https://upload.wikimedia.org/wikipedia/commons/5/5e/Wappen_Cottbus.svg',
  'wappen, Cottbus, svg',
  NULL,
  'CC0');




