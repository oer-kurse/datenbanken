


Fragen zu den Gewässern
-----------------------



:ref:`« Start Gewässer <startgewaesser>`

..  image:: ./images/spiegelungen.jpg
    :width: 0	    


.. |a| raw:: html
   
       <!-- lightbox container hidden with CSS -->
       <a href="#" class="lightbox" id="img1">
       <span style="background-image: url('../../_images/spiegelungen.jpg')"></span>
       </a>

.. |b| raw:: html

       <a href="#img1">
          <img width="250px" src="../../_images/spiegelungen.jpg">
       </a>

      
.. sidebar:: Serie: Natur

   |b|
   
   Spiegelung
	     
|a|

.. index:: COUNT

Fragen -- Hinweise -- Lösungen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Bitte erst eigene Überlegungen anstellen und danach zur Kontrolle mit der Lösung vergleichen.

Wie viele Datensätze befinden sich in der Relation gewaesser?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende die Funktion »count«
    <br/>

`Postgres-Doku: »count« <https://www.postgresql.org/docs/11/static/functions-aggregate.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/pg-brb-gewaesser-1.cast"
      theme="solarized-light"
      font-size="medium"
      rows="14"
      poster="data:text/plain,\e[5;5H Anzahl der Gewässer: \e[1;33m">
    </asciinema-player>

    </div>
    </div>

Welche Landkreise (Kürzel) wurden in der Relation gespeichert?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Siehe auch: DISTINCT
    <br/> 

`Postgres: DISTINCT <https://www.postgresql.org/docs/11/sql-select.html>`_


.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/pg-brb-gewaesser-2.cast"
      theme="solarized-light"
      font-size="medium"
      rows="24"
      poster="data:text/plain,\e[5;5H Alle Kürzel \e[1;33m">
    </asciinema-player>


    </div>
    </div>

Ordnen Sie die Ausgabe der Kürzel auf- bzw. absteigend
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende das Schlüsselwort »order by«...

`Postgres-Doku: »order by« <https://www.postgresql.org/docs/11/sql-select.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/pg-brb-gewaesser-3.cast"
      theme="solarized-light"
      font-size="medium"
      rows="27"
      poster="data:text/plain,\e[5;5H Alle Kürzel sortiert \e[1;33m">
    </asciinema-player>

    </div>
    </div>

Wieviele Gewässer gehören zum Landkreis *»EE«*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende die »WHERE«-Klausel ...

`Postgres-Doku: »where« <https://www.postgresql.org/docs/11/static/queries-table-expressions.html#QUERIES-WHERE>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/pg-brb-gewaesser-4.cast"
      theme="solarized-light"
      font-size="medium"
      rows="17"
      poster="data:text/plain,\e[5;5H Landkreis »EE« \e[1;33m">
    </asciinema-player>

    <p>Die gezeigte Lösung für den Landkreis Elbe-Elster stimm zufällig,
    weil es eine 1:1 Beziehung für die Zuordnung Ort-Gewässer gibt. Besser
    und allgemeiner ist die folgende Variante:</p>

    <pre>
    SELECT count(id), kuerzel_lk, bezeichnung
    FROM gewaesser  
    WHERE kuerzel_lk = 'OPR'
    GROUP BY kuerzel_lk, bezeichnung
    ORDER BY bezeichnung;
    </pre>    
    </div>
    </div>

Wie viele Gewässer finden sich im ID-Bereich 50 bis 70?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende die Funktion »between«

`Postgres-Doku: »between« <https://www.postgresql.org/docs/11/functions-comparison.html>`_

.. raw:: html



    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/pg-brb-gewaesser-5.cast"
      theme="solarized-light"
      font-size="medium"
      rows="30"
      poster="data:text/plain,\e[5;5H ID-Bereich 50 bis 70 \e[1;33m">
    </asciinema-player>

    </div>
    </div>

Welches Gewässer hat die längste Bezeichnung für den Landkreis *»HVL«*?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende die Funktion »length«
    <br />
    <br />- ermittle ein Zwischenergebnis
    <br />- setze das Zwischenergebnis in die erweiterte Abfrage ein
    <br />- kombiniere die zweite Abfrage mit der Ersten als Unterabfrage

`Postgres-Doku: »length« (String-Funktion) <https://www.postgresql.org/docs/11/functions-string.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

         <asciinema-player 
           src="../../_static/casts/pg-brb-gewaesser-6.cast"
           theme="solarized-light"
           font-size="medium"
           rows="33"
           poster="data:text/plain,\e[5;5H längste Bezeichnung \e[1;33m">
         </asciinema-player>
    </div>
    </div>

Welche Gewässer haben die Bezeichnung *»teich«* im Namen?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende die Funktion »like«

`Postgres-Doku: »LIKE« (pattern matching) <https://www.postgresql.org/docs/11//functions-matching.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/pg-brb-gewaesser-7.cast"
      theme="solarized-light"
      font-size="medium"
      rows="13"
      poster="data:text/plain,\e[5;5H Textvergleich \e[1;33m">
    </asciinema-player>

    </div>
    </div>

Welche Gewässer haben die Bezeichnung *»bad«* im Namen?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- zwei Varianten

- Verwenden Sie »like«!

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende die Funktion »count«...

`Postgres-Doku: »LIKE« (pattern matching) <https://www.postgresql.org/docs/11//functions-matching.html>`_
`Postgres-Doku: »OR« (Logical Operators) <https://www.postgresql.org/docs/current//functions-logical.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/pg-brb-gewaesser-8.cast"
      theme="solarized-light"
      font-size="medium"
      rows="33"
      poster="data:text/plain,\e[5;5H Bad und bad \e[1;33m">
    </asciinema-player>

    </div>
    </div>

Wie viele Gewässer haben *einen* Eintrag im Feld *»praedikat«*?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende die Funktion »count«...

`Postgres-Doku: »COUNT (Aggregat-Funktionen)« <https://www.postgresql.org/docs/11//functions-aggregate.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/pg-brb-gewaesser-9.cast"
      theme="solarized-light"
      font-size="medium"
      rows="11"
      poster="data:text/plain,\e[5;5H Anzahl ermitteln \e[1;33m">
    </asciinema-player>

    </div>
    </div>

Wie viele Gewässer haben *keinen* Eintrag im Feld *»praedikat«*?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Prüfe auf »NULL«-Werte

`Postgres-Doku: »IS NULL | IS NOT NULL (Comparison Functions and Operators)« <https://www.postgresql.org/docs/11//functions-comparison.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">

    <br>

    <asciinema-player 
    src="../../_static/casts/pg-brb-gewaesser-10.cast"
    theme="solarized-light"
    font-size="medium"
    rows="11"
    poster="data:text/plain,\e[5;5H leer \e[1;33m">
    </asciinema-player>

    </div>
    </div>

Welche Gewässer haben *keinen* Eintrag im Feld *»praedikat«*?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende die Funktion »count«...

`Postgres-Doku: »IS NULL | IS NOT NULL (Comparison Functions and Operators)« <https://www.postgresql.org/docs/11//functions-comparison.html>`_

.. raw:: html

      </div>
      Lösung zeigen: <input type="checkbox" value="solution"/>
      <div class="togglesol">
    <br>

    <asciinema-player 
    src="../../_static/casts/pg-brb-gewaesser-11.cast"
    theme="solarized-light"
    font-size="medium"
    rows="12"
    poster="data:text/plain,\e[5;5H NULL-Werte \e[1;33m">
    </asciinema-player>

    </div>
    </div>
