.. _manageposgres:
.. _startgewaesser:

:ref:`« Startseite zum Kurs <kursstart>`

============================
Übung + Lösungen -- Gewässer
============================


.. image:: ./images/bobbycar.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bobbycar.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bobbycar.jpg">
   </a>



.. sidebar:: Serie: Dinge

   |b|
   

|a|

Dies ist eine Übungsserie mit:

- Beispielen
- Hinweisen zur Dokumentation
- Aufgaben

Ausgangspunkt für die Übung, ist eine Veröffentlichung
im Amtsblatt für das Land Brandenburg.

| ( :download:`Amtsblatt 18-16.pdf <amtsblatt-18-16.pdf>` )
|

.. toctree::
   :maxdepth: 1

   struktur-gewaesser
   fragen-zu-gewaesser
   struktur-landkreise
   fragen-zu-landkreisen

   fragen-werte-nachschlagen

