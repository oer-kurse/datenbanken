======================
Mehr als eine Relation
======================



:ref:`« Start Gewässer <startgewaesser>`

.. image:: ./images/schattenspiel3.jpg
   :width: 0

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/schattenspiel3.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/schattenspiel3.jpg">
   </a>



.. sidebar:: Serie: Schattenspiel

   |b|


|a|

Liste der Landkreis-Kürzel?
---------------------------

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende die Funktion »DISTINCT«

`Postgres-Doku: »DISTINCT« <https://www.postgresql.org/docs/9.6/static/sql-select.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
    src="../../_static/casts/pg-brb-landkreise-3.cast"
    theme="solarized-light"
    font-size="medium"
    rows="25"
    poster="data:text/plain,\e[5;5H Alle Landkreise \e[1;33m">
    </asciinema-player>

    </div>
    </div>

Wie zuvor + Bezeichnung?
------------------------

Beispiel:

.. code:: text


     kuerzel_lk |        landkreis         
    ------------+--------------------------
     BAR        | Barnim
     BRB        | Brandenburg an der Havel
     EE         | Elbe-Elster
     FF         | Frankfurt/Oder
     HVL        | Havelland
     LDS        | Dahme-Spreewald
     LOS        | Oder-Spree
     MOL        | Märkisch-Oderland
     OHV        | Oberhavel
     OPR        | Ostprignitz-Ruppin
     OSL        | Oberspreewald-Lausitz
     P          | Potsdam
     PM         | Potsdam-Mittelmark
     SPN        | Spree-Neiße
     TF         | Teltow-Fläming
     UM         | Uckermark
                | Cottbus
                | Prignitz
     (18 rows)


- verwenden Sie zum Nachschlagen die Relation **landkreise**.

- verwenden Sie die Schlüsselworte **AS/ALIAS** um beide Realationen 
  eindeutig unterscheiden zu können und eine kompaktere Abfrage 
  zu erhalten

- verknüpfen Sie beide Relationen über einen **left join**

- experimentieren/variieren Sie mit den Schlüsselworten

- Welches Ergebnis erhalten Sie, wenn sie **left join** durch einen
  **right join** oder einen **full join** austauschen?

.. raw:: html



    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende die Funktion »count«...

- `Postgres-Tutorial: »Join« <https://www.postgresql.org/docs/9.6/tutorial-join.html>`_

- `Postgres-Doku: »Join« <https://www.postgresql.org/docs/9.6/queries-table-expressions.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">

    <br>

    <asciinema-player 
    src="../../_static/casts/pg-brb-landkreise-4.cast"
    theme="solarized-light"
    font-size="medium"
    rows="25"
    poster="data:text/plain,\e[5;5H Alle Landkreise \e[1;33m">
    </asciinema-player>

    </div>
    </div>

Daten aus diversen Relationen?
------------------------------

:Aufgabe: Fügen Sie, wie unten gezeigt, alle Daten aus den 
          verfügbaren Relationen (gewaesser, landkreise, orte und images) 
          zu einer neuen Ergebnis-Relation zusammen.

Ergebnis:

.. code:: text


     bezeichnung  |     ort      | landkreis |            bild             
    --------------+--------------+-----------+-----------------------------
     Grimnitzsee  | Joachimsthal | Barnim    | wappen-landkreis-barnim.svg
     Werbellinsee | Joachimsthal | Barnim    | wappen-landkreis-barnim.svg
    (2 rows)

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint"> Lösen Sie erst die Abfragen für einzelne Tabellen.
    <br />
    Aus den Teilergebnissen bauen Sie abschließend das fertige SQL-Statement.

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br />

    <asciinema-player 
    src="../../_static/casts/pg-brb-landkreise-5.cast"
    theme="solarized-light"
    font-size="medium"
    rows="60"
    poster="data:text/plain,\e[5;5H Alle Angaben aus allen Tabellen \e[1;33m">
    </asciinema-player>

    </div>
    </div>
    <br />
    <br />
