=============
Demo -- JOINS
=============


.. image:: ./images/hochsitz.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/hochsitz.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/hochsitz.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Natur

|a|

Hier werden alle Join-Varianten vortestellt. Führen Sie alle
Anweisungen aus und überlegen Sie, was die Besonderheit einer jeden
Abfrage ist.


Quelle(n) und weiterführende Links:

- https://www.postgresql.org/docs/9.6/static/queries-table-expressions.html
- http://wiki.selfhtml.org/wiki/Datenbank/Einführung_in_Joins#Joins_allgemein
- https://antonz.org/sql-join/
  
.. index:: Übungen; 002 -- Join-Varianten (postgresql)
	   
Datenbank
---------

- Übungsdatenbank anlegen und/oder nutzen.
    

Tabellen anlegen
----------------

.. code:: sql

   CREATE TABLE T1(
   NUM INT PRIMARY KEY     NOT NULL,
   NAME            TEXT    NOT NULL);

   CREATE TABLE T2(
   NUM INT PRIMARY KEY     NOT NULL,
   VALUE           TEXT);

   
Datensätze einfügen
-------------------

.. code:: sql

   INSERT INTO T1 VALUES(1,'a');
   INSERT INTO T1 VALUES(2,'b');
   INSERT INTO T1 VALUES(3,'c');
   INSERT INTO T1 VALUES(4,'d');
   INSERT INTO T1 VALUES(5,'e');
   INSERT INTO T1 VALUES(6,'e');


.. code:: sql

   INSERT INTO T2 VALUES(1,NULL);
   INSERT INTO T2 VALUES(2,'xxx');
   INSERT INTO T2 VALUES(3,'yyy');
   INSERT INTO T2 VALUES(4,'zzz');

.. raw:: html

   <section>
   <input type="checkbox" value="full-join"/> Kurzfilm
   <div class="togglefilm">

     <asciinema-player 
       src="../../_static/casts/alljoins-create-table.cast"
       theme="solarized-light"
       font-size="medium"
       rows="20"
       loop="0" poster="data:text/plain,\e[5;5H Tabellen: \e[1;33m">
     </asciinema-player>

   </div></section>

.. index:: Join; cross join
	   
CROSS JOIN
----------
- Jedes Tupel aus t1 wird mit jedem Tupel aus t2 verknüpft
- Synonym: Kreuztabelle

.. image:: ./images/crossjoin.png
   :width: 250px
	   
.. code:: sql

   # ohne where...
   # und ANSI SQL-89 implicit Syntax 

   SELECT * FROM t1, t2;

   # explizit ...
   # und ANSI SQL-92 explicit join Syntax
   
   SELECT * FROM t1 CROSS JOIN t2;

.. raw:: html

   <section>
   <input type="checkbox" value="full-join"/> Kurzfilm
   <div class="togglefilm">

     <asciinema-player 
       src="../../_static/casts/alljoins-crossjoin.cast"
       theme="solarized-light"
       font-size="medium"
       rows="30"
       poster="data:text/plain,\e[5;5Hcross join \e[1;33m:">
     </asciinema-player>

   </div></section>

.. index:: Join; inner join
	   
INNER JOIN
----------
- Es werden alle Tupel selektiert die in t1 und t2 im gleichnamigen
  Attribut eine Übereinstimmung haben.

.. image:: ./images/inner-natural-join.png
   :width: 250px
	   
.. code:: sql

   SELECT * FROM t1, t2 where t1.num = t2.num;
   SELECT * FROM t1 INNER JOIN t2 ON t1.num = t2.num;
   SELECT * FROM t1 INNER JOIN t2 USING (num);


.. raw:: html

   <section>
   <input type="checkbox" value="full-join"/> Kurzfilm
   <div class="togglefilm">

     <asciinema-player 
       src="../../_static/casts/alljoins-innerjoin.cast"
       theme="solarized-light"
       font-size="medium"
       rows="17"
       poster="data:text/plain,\e[5;5Hinner join: \e[1;33m">
     </asciinema-player>

   </div></section>
   
.. index:: Join; natural inner join
	   
NATURAL INNER JOIN
------------------
- Spezialfall des Inner Join
- die passenden Spalten werden automatisch zugeordnet
  
.. image:: ./images/inner-natural-join.png
   :width: 250px
	   
.. code:: sql

   SELECT * FROM t1 NATURAL INNER JOIN t2;


.. raw:: html

   <section>
   <input type="checkbox" value="full-join"/> Kurzfilm
   <div class="togglefilm">

     <asciinema-player 
       src="../../_static/casts/alljoins-natural-inner-join.cast"
       theme="solarized-light"
       font-size="medium"
       rows="11"
       poster="data:text/plain,\e[5;5Hnatural inner join: \e[1;33m">
     </asciinema-player>

     </div></section>
     
.. _leftjoin:

.. index:: Join; left join
	   
LEFT (OUTER) JOIN
-----------------
- OUTER kann weggelassen werden

  1. werden alle Werte der erstgenannten Relation ausgewählt
  2. wenn dann noch der gleiche Wert in der zweiten Tabelle gefunden
     wird, kommen auch alle anderen Attribute in die Ergebnis-Relation

.. image:: ./images/left-outer-join.png
   :width: 250px    

.. code:: sql

   SELECT * FROM t1 LEFT JOIN t2 ON t1.num = t2.num;
   SELECT * FROM t1 LEFT JOIN t2 USING (num);
   SELECT * FROM t1 LEFT JOIN t2 ON t1.num = t2.num AND t2.value = 'xxx';
   SELECT * FROM t1 LEFT JOIN t2 ON t1.num = t2.num WHERE t2.value = 'xxx';

.. raw:: html

   <section>
   <input type="checkbox" value="full-join"/> Kurzfilm
   <div class="togglefilm">

     <asciinema-player 
       src="../../_static/casts/alljoins-left-outer-join.cast"
       theme="solarized-light"
       font-size="medium"
       rows="15"
       poster="data:text/plain,\e[5;5Hleft [outer] join: \e[1;33m">
     </asciinema-player>

   </div></section>
   
.. index:: Join; right join
	      
RIGHT JOIN
----------
- OUTER kann weggelassen werden

  1. werden alle Werte der zweiten Relation ausgewählt
  2. wenn dann noch der gleiche Wert in der ersten Tabelle gefunden
     wird, kommen auch alle anderen Attribute in die Ergebnis-Relation

.. image:: ./images/right-outer-join.png
   :width: 250px    
     
.. code:: sql

   SELECT * FROM t1 RIGHT JOIN t2 ON t1.num = t2.num;

.. raw:: html

   <section>
   <input type="checkbox" value="full-join"/> Kurzfilm
   <div class="togglefilm">

     <asciinema-player 
       src="../../_static/casts/alljoins-right-outer-join.cast"
       theme="solarized-light"
       font-size="medium"
       rows="25"
       poster="data:text/plain,\e[5;5Hright [outer] join: \e[1;33m">
     </asciinema-player>

   </div></section>
	 
.. index:: Join; full join
	   
FULL JOIN
---------
- realisiert einen *left outer join* und einen *right outer join*
- für fehlende Werte in der einen oder anderen Relation werden
  NULL-Values eingesetzt
- Wird nicht von allen DB-Managementsystemen unterstützt

.. code:: sql

   SELECT * FROM t1 FULL JOIN t2 ON t1.num = t2.num;

   SELECT * FROM t1 NATURAL FULL JOIN t2;

   
.. raw:: html

   <section>
   <input type="checkbox" value="full-join"/> Kurzfilm
   <div class="togglefilm">


     <asciinema-player 
       src="../../_static/casts/alljoins-full-join.cast"
       theme="solarized-light"
       font-size="medium"
       rows="12"
       poster="data:text/plain,\e[5;5Hfull join: \e[1;33m">
     </asciinema-player>

   </div></section>
   
.. index:: Join; self join
	      
SELF JOIN
---------

- Mit zwei Alias-Definitionen die gleiche Tabelle befragen.

.. code:: sql

   select * from t1 as a, t1 as b where a.name=b.name;   

  
.. index:: Join; self join (Beispiel)

.. _self-join:

   
Wozu ist ein Self-Join gut ist?
===============================

Damit lassen sich hierarchische Strukturen (Eltern/Kind-Beziehungen)
in einer Tabelle abbilden [#f1]_.

Beachte die Nachteile:

- Änderungen sind schwierig zu realisieren
- verschachtelte Hierarchien lassen sich so nicht einfach abfragen
- Viele DB-Management-System bieten dafür eine Unterstützung durch *recursive CTEs* [#f2]_ 
  
.. code:: sql

   DROP TABLE kategorie;
   CREATE TABLE kategorie (
     id INTEGER,
     bezeichnung VARCHAR(45) NOT NULL,
     parent INTEGER NOT NULL
   );

   INSERT INTO kategorie (id, bezeichnung, parent) VALUES (1,'Opensource',0);
   INSERT INTO kategorie (id, bezeichnung, parent) VALUES (2,'Windows',0);
   INSERT INTO kategorie (id, bezeichnung, parent) VALUES (3, 'Linux',1);
   INSERT INTO kategorie (id, bezeichnung, parent) VALUES (4,'SUSE',3);
   INSERT INTO kategorie (id, bezeichnung, parent) VALUES (5,'Knoppix',3);
   INSERT INTO kategorie (id, bezeichnung, parent) VALUES (6,'Win XP',2);
   INSERT INTO kategorie (id, bezeichnung, parent) VALUES (7,'Vista',2);
   INSERT INTO kategorie (id, bezeichnung, parent) VALUES (8,'Ubuntu',3);
   INSERT INTO kategorie (id, bezeichnung, parent) VALUES (9,'Macintosh',0);
   INSERT INTO kategorie (id, bezeichnung, parent) VALUES (10,'Windows 10',2);
   INSERT INTO kategorie (id, bezeichnung, parent) VALUES (11,'CentOS',3);
   
   select distinct t2.bezeichnung as Betriebssystem, t1.bezeichnung as Kategorie
   from kategorie t1 join kategorie t2 on t1.id = t2.parent order by Kategorie;

   -- Ausgabe
    betriebssystem | kategorie  
   ----------------+------------
    CentOS         | Linux
    Knoppix        | Linux
    SUSE           | Linux
    Ubuntu         | Linux
    Linux          | Opensource
    Vista          | Windows
    Win XP         | Windows
    Windows 10     | Windows
    (8 rows)

Lateral Join
============

Ein lateral Join erlaubt den Zugriff auf Attribute eines vorherigen
select, was mit einem normalen join nicht erlaubt ist. Ein Beispiel
finden Sie in der Demo für die :ref:`Datentypen JSON/JSONB <20231122T210327>`.


