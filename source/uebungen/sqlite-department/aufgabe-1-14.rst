===================
1.14 -- Gruppierung
===================

.. index:: Übung: Department (sqlite); Aufgabe 1.14 (Group By)

.. image:: ./images/rote-blume-rohrbeck.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/rote-blume-rohrbeck.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/rote-blume-rohrbeck.jpg">
   </a>

.. sidebar:: SQL-Kurs

   |b|
   Serie: Pflanzen

|a|

Vorbereitung: Drei weitere Datensätze einfügen:

.. code:: sql

   INSERT INTO COMPANY VALUES (8, 'Paul', 24, 'Houston', 20000.00 );
   INSERT INTO COMPANY VALUES (9, 'James', 44, 'Norway', 5000.00 );
   INSERT INTO COMPANY VALUES (10, 'James', 45, 'Texas', 5000.00 );

:ref:`« Fragen-Übersicht Department <departmentstart>`
     
Wenn eine Aggregat-Funktion zur Ausgabe hinzugefügt wird,
ist die Verwendung von GROUP BY zwingend erforderlich?

:Siehe auch:  `GROUP-BY`_


Antwort?
========
Ohne GROUP BY...

.. code:: sql

   select name, sum(salary) from company;


.. error::

   ERROR:  column "company.name" must appear in
   the GROUP BY clause or be used in an aggregate function

Deshalb erweitern mit:

.. code:: sql

   select name, sum(salary)
   from company group by name;

.. code:: sql

     name  |  sum  
   -------+-------
    Teddy | 20000
    David | 85000
    Paul  | 40000
    Kim   | 45000
    Mark  | 65000
    Allen | 15000
    James | 20000
   (7 rows)

.. _GROUP-BY: https://www.tutorialspoint.com/sqlite/sqlite_group_by.htm
