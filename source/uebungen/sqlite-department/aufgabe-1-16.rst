=====================
1.16 -- Concatenation
=====================

.. index:: Übung: Department (sqlite); Aufgabe 1.12 (Concat)
.. INDEX:: Concatenieren; Zeichenkettenverknüpfung 
.. INDEX:: Zeichenkettenverknüpfung; Concatenieren


.. image:: ./images/rote-blume-rohrbeck.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/rote-blume-rohrbeck.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/rote-blume-rohrbeck.jpg">
   </a>

.. sidebar:: SQL-Kurs

   |b|
   Serie: Pflanzen

|a|	   

:ref:`« Fragen-Übersicht Department <departmentstart>`
     
Zeichenketten können wieder zu einer neuen Zeichenkette verknüpft werden.
Dafür werden zwei senkrechte Striche verwendet: »||«

	      
Verknüpfen Sie Name und Adresse zu folgender Aussage:

::
   
   <name> lebt in <adress>

Beispiel
========

.. code:: sql

   select
     name as "Mitarbeiter" ,
     ' lebt in ' || address as  "Lebt wo?"
   from
     company;

Das gesuchte Ergebnis:

.. code:: sql

    Mitarbeiter |      Lebt wo?       
   -------------+---------------------
    Paul        |  lebt in California
    Allen       |  lebt in Texas
    Teddy       |  lebt in Norway
    Mark        |  lebt in Rich-Mond
    David       |  lebt in Texas
    Kim         |  lebt in South-Hall
    James       |  lebt in Houston
    Paul        |  lebt in Houston
    James       |  lebt in Norway
    James       |  lebt in Texas
   (10 rows)

Aufgabe:
========

- Geben Sie für einen Serienbrief die folgende(n) Aussage(n) aus.
- Erstellen Sie dafür die Relation in einer **Postgres-DB**.

.. code:: text

		    Serienbrief                 
   ---------------------------------------------
    Das Jahresgehalt von Paul  beträgt 20000 €.
    Das Jahresgehalt von Allen beträgt 15000 €.
    Das Jahresgehalt von Teddy beträgt 20000 €.
    Das Jahresgehalt von Mark  beträgt 65000 €.
    Das Jahresgehalt von David beträgt 85000 €.
    Das Jahresgehalt von Kim   beträgt 45000 €.
    Das Jahresgehalt von James beträgt 10000 €.
    Das Jahresgehalt von Paul  beträgt 20000 €.
    Das Jahresgehalt von James beträgt  5000 €.
    Das Jahresgehalt von James beträgt  5000 €.

    (10 rows)


	  
   
.. _ORDER BY: https://www.tutorialspoint.com/sqlite/sqlite_order_by.htm
