:hide-toc:
   
:ref:`« Fragen-Übersicht Department <departmentstart>`

.. index:: Übung: Department (sqlite); Aufgabe 1.1 (Auswahl)
     
==================================
1.1 -- Projektion - ganze Relation
==================================

.. image:: ./images/alf.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/alf.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/alf.jpg">
   </a>



.. sidebar:: SQL-Kurs

   |b|
   Serie: Plüschtiere

|a|
	   

Erläuterungen zu SELECT
=======================
   
Diese Anweisung wird verwendet, um die Daten aus einer Relation zu
holen und gibt eine Projektion in Form einer Ergebnistabelle
zurück. Diese Ergebnistabellen werden auch als Ergebnismengen
(result sets) bezeichnet.   


Für die Ausgabe aller Attribute dient der * (Stern) als Platzhalter:

.. image:: ./images/pikchr-select-stern.svg
   :width: 400px	   

Frage: Zeige alle Datensätze (Tuple) der Relation COMPANY?


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

     
    id | name  | age |       address               | salary 
   ----+-------+-----+-----------------------------+--------
     1 | Paul  |  32 | California                  |  20000
     2 | Allen |  25 | Texas                       |  15000
     3 | Teddy |  23 | Norway                      |  20000
     4 | Mark  |  25 | Rich-Mond                   |  65000
     5 | David |  27 | Texas                       |  85000
     6 | Kim   |  22 | South-Hall                  |  45000
     7 | James |  24 | Houston                     |  10000
   (7 rows)

