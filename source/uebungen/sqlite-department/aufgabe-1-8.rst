:ref:`« Fragen-Übersicht Department <departmentstart>`

.. index:: Übung: Department (sqlite); Aufgabe 1.8 (Auswahl)

====================
1.8 -- Wertebereiche
====================

.. image:: ./images/rote-blume-rohrbeck.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/rote-blume-rohrbeck.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/rote-blume-rohrbeck.jpg">
   </a>


.. sidebar:: SQL-Kurs

   |b|
   Serie: Pflanzen

|a|

     
Geben Sie alle Tuple aus, wenn der Benutzer 22, 23 oder 24 Jahre alt ist.

Es gibt zwei Lösungen mit dem gleichen Ergebnis.

.. image:: ./images/pik-operator-in.svg
   :width: 400px

----

.. image:: ./images/pik-operator-between.svg
   :width: 400px	   

:Siehe auch:  `IN/BETWEEN`_

Antwort?
========

.. code:: sql

      
   ???

Das gesuchte Ergebnis:

.. code:: sql


   ID          NAME        AGE         ADDRESS     SALARY    
   ----------  ----------  ----------  ----------  ----------
   3           Teddy       23          Norway      20000.0   
   6           Kim         22          South-Hall  45000.0   
   7           James       24          Houston     10000.0   
     

.. _IN/BETWEEN: https://www.tutorialspoint.com/sqlite/sqlite_operators.htm

