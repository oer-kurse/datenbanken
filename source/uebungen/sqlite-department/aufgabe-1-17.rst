:ref:`« Fragen-Übersicht Department <departmentstart>`

.. _nachkommastellen:

.. index:: Übung: Department (sqlite); Aufgabe 1.17 (Unterabfragen/Aggregatfunktionen)

========================
1.18 -- Nachkommastellen
========================

.. image:: ./images/bonsai001.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bonsai001.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bonsai001.jpg">
   </a>

.. sidebar:: SQL-Kurs

   |b|
   »Japanischer Garten« in Ferch

|a|

  
     
Die Ausgabe soll mit zwei Nachkommastellen erfolgen. Befragen Sie Ihre
Lieblingssuchmaschine  nach einer möglichen Lösung.


Antwort?
========

.. code:: sql

      
   ???

Das gesuchte Ergebnis:

.. code:: sql

   Durchschnittsgehalt  Gesamtgehalt
   -------------------  ------------ 
   30000.00              270000.00    
