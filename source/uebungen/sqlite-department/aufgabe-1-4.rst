:ref:`« Fragen-Übersicht Department <departmentstart>`

.. index:: Übung: Department (sqlite); Aufgabe 1.4 (Vergleiche)
	   
==============================
1.4 -- Filtern -- Ungleichheit
==============================

.. image:: ./images/bluete002.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bluete002.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bluete002.jpg">
   </a>


.. sidebar:: SQL-Kurs

   |b|
   Serie: Pflanzen

|a|
     
 
Wer verdient mehr als 20000?

:Siehe auch:  `Operatoren`_

Antwort?
========

.. code:: sql
 
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

     
    id | name  | age |                address       | salary 
   ----+-------+-----+------------------------------+--------
     4 | Mark  |  25 | Rich-Mond                    |  65000
     5 | David |  27 | Texas                        |  85000
     6 | Kim   |  22 | South-Hall                   |  45000

     (3 rows)


.. _Operatoren: https://www.tutorialspoint.com/sqlite/sqlite_operators.htm
     
