================================
1.13 -- Unterabfragen/Subqueries
================================

.. index:: Übung: Department (sqlite); Aufgabe 1.13 (Subqueries)

.. image:: ./images/rote-blume-rohrbeck.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/rote-blume-rohrbeck.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/rote-blume-rohrbeck.jpg">
   </a>

.. sidebar:: SQL-Kurs

   |b|
   Serie: Pflanzen

|a|	   

:ref:`« Fragen-Übersicht Department <departmentstart>`


Subqueries/Unterabfragen können mit einem Vergleich kombiniert werden.

Dazu gehören, die Operatoren: =, <, >, >=, <=, IN, BETWEEN

     
:Siehe auch: 

   - `SUBQUERY`_
      
     
Wessen Gehalt liegt über 60000?

:Siehe auch:  `SUBQUERY`_


Antwort?
========

Erste Version:

.. code:: sql

   select * from company where salary > 45000;

Das gesuchte Ergebnis:

.. code:: sql


    id | name  | age |                 address       | salary 
   ----+-------+-----+-------------------------------+--------
     4 | Mark  |  25 | Rich-Mond                     |  65000
     5 | David |  27 | Texas                         |  85000
   (2 rows)

 
   
Alternativ mit Unterabfrage:

.. code:: sql

   select * 
	from company 
	  where id in (select id 
	    from company 
	     where salary > 45000) ;

   -- anderes Format:

   select
     * 
   from
     company 
   where
     id in (select id 
            from company 
            where salary > 45000);

	     

Das gesuchte Ergebnis:

.. code:: sql


    id | name  | age |                 address       | salary 
   ----+-------+-----+-------------------------------+--------
     4 | Mark  |  25 | Rich-Mond                     |  65000
     5 | David |  27 | Texas                         |  85000
   (2 rows)

  
.. _SUBQUERY: https://www.tutorialspoint.com/sqlite/sqlite_sub_queries.htm
