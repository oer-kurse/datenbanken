============================
1.11 -- Länge und Umwandlung
============================

.. index:: Übung: Department (sqlite); Aufgabe 1.11 (Text-Funktionen)


.. image:: ./images/buddha.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/buddha.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/buddha.jpg">
   </a>

.. sidebar:: SQL-Kurs

   |b|
   Serie: Skulpturen

|a|	   

:ref:`« Fragen-Übersicht Department <departmentstart>`
     
Geben Sie alle Namen in Kleinbuchstaben und dazu die Länge der Namen aus.
Benennen Sie die ausgewählten Spalten als »Name« bzw. »Länge«.

:Siehe auch:  `Funktionen`_


Antwort?
========

.. code:: sql

      
   ???

Das gesuchte Ergebnis:

.. code:: sql


   Name         Länge
   -----------  ------------
   paul         4           
   allen        5           
   teddy        5           
   mark         4           
   david        5           
   kim          3           
   james        5
   

Erweiterte Fragestellung: das Attribut »name« kann mehrfach, auf
unterschiedliche Art, für die folgende Ausgabe manipuliert
werden. Verwenden Sie die entspechenden Funktionen und den Alias für
die Umbenennung der Überschriften in der Ausgabe.


.. code:: sql

    original | alles groß | alles klein | Länge 
   ----------+------------+-------------+-------
    Paul     | PAUL       | paul        |     4
    Allen    | ALLEN      | allen       |     5
    Teddy    | TEDDY      | teddy       |     5
    Mark     | MARK       | mark        |     4
    David    | DAVID      | david       |     5
    Kim      | KIM        | kim         |     3
    James    | JAMES      | james       |     5
    Paul     | PAUL       | paul        |     4
    James    | JAMES      | james       |     5
    James    | JAMES      | james       |     5
   (10 rows)

.. _Funktionen: https://www.tutorialspoint.com/sqlite/sqlite_useful_functions.htm
