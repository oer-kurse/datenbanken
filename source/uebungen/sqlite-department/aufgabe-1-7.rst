:ref:`« Fragen-Übersicht Department <departmentstart>`

.. index:: Übung: Department (sqlite); Aufgabe 1.7 (Vergleich)

==================
1.7 -- Platzhalter
==================

.. image:: ./images/bluete005.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bluete005.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bluete005.jpg">
   </a>


.. sidebar:: SQL-Kurs

   |b|
   Serie: Pflanzen

|a|
     
Welche Adressen beginnen mit der Zeichenfolge »Te«?

- Geben Sie die ID als Nummer
- und ADDRESS mit der Überschrift »Orte mit Te« aus.
- Platzhalter spielen hier eine Rolle, vieleicht kennen Sie diese aus
  der Windowswelt? Hier eine Gegenüberstellung:


.. code:: sql

   Platzhalter

   ======================== ===== ==========
   Anzahl Zeichen           SQL   Windows
   ======================== ===== ==========			  
   beliebig viele Zeichen    %       *        
   einzelnes Zeichen         _       ?
   ======================== ===== ==========

.. image:: ./images/pik-like-pattern.svg
   :width: 400px

   
Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:
::

   Nummer      Orte mit Te
   ----------  -----------
   2           Texas      
   5           Texas  
     
