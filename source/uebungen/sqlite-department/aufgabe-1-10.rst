===========================
1.10 -- Aggregat-Funktionen
===========================

.. index:: Übung: Department (sqlite); Aufgabe 1.10 (Aggregatfunktionen)
	   

.. image:: ./images/bonsai002.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bonsai002.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bonsai002.jpg">
   </a>

.. sidebar:: SQL-Kurs

   |b|
   »Japanischer Garten« in Ferch

|a|


:ref:`« Fragen-Übersicht Department <departmentstart>`
     
Wieviel Lohn wird im Durchschnitt und in der Summe gezahlt?

:Siehe auch:  `Aggregat-Funktionen`_

Zusatz: Wie kann die Ausgabe auf eine Genauigkeit von zwei Stellen
hinter dem Komma gekürzt werden? Siehe auch: :ref:`Aufgabe 1.18 <nachkommastellen>`


Antwort?
========

.. code:: sql

      
   ???

Das gesuchte Ergebnis:

.. code:: sql

   
   avg(salary)       sum(salary)
   ----------------  -----------
   37142.8571428571  260000.0   

.. _Aggregat-Funktionen: https://www.tutorialspoint.com/sqlite/sqlite_useful_functions.htm
