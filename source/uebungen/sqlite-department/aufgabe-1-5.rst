:ref:`« Fragen-Übersicht Department <departmentstart>`

.. index:: Übung: Department (sqlite); Aufgabe 1.5 (Vergleiche)

======================
1.5 -- UND-Verknüpfung
======================


.. image:: ./images/bluete003.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bluete003.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bluete003.jpg">
   </a>


.. sidebar:: SQL-Kurs

   |b|
   Serie: Pflanzen

|a|

      
Verknüpfen Sie zwei Bedingungen in einer Abfrage. 

- Wer ist jünger oder gleich  25 Jahre alt,
- *und* verdient weniger als 65000?

	      
Antwort?
========

.. code:: sql

      
   ???

Das gesuchte Ergebnis:

.. code:: sql

   ID     NAME     AGE    ADDRESS     SALARY 
   -----  -------  -----  ----------  -------
   2      Allen    25     Texas       15000.0
   3      Teddy    23     Norway      20000.0
   6      Kim      22     South-Hall  45000.0
   7      James    24     Houston     10000.0


.. image:: ./images/pikchr-and-or-verknuepfung.svg
   :width: 400px
   
:Siehe auch:  `AND/OR`_



.. _AND/OR: https://www.tutorialspoint.com/sqlite/sqlite_and_or_clauses.htm  



