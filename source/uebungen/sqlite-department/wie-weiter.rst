:ref:`« Fragen-Übersicht Department <departmentstart>`

.. index:: Übung: Department (sqlite); Aufgabe 1.9 (NOT)

==========
Wie weiter
==========

.. image:: ./images/pusteblume.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/pusteblume.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/pusteblume.webp">
   </a>

.. sidebar:: Blüte

   |b|

   Pusteblume

|a|


- Widerholung mit anderen Daten und Sqlite3
  :ref:`Übungsserie zu den Grundlagen (BKZ-Frau) <bkzfraustart>` 

- Wiederholung mit anderen Daten und Postgres
  :ref:`Übungsserie zu den Grundlagen (BKZ-Frau) <bkzfrau-psql-start>` 
