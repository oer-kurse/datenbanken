:ref:`« Fragen-Übersicht Department <departmentstart>`

.. index:: Übung: Department (sqlite); Aufgabe 1.9 (NOT)

=================
1.9 -- Verneinung
=================

.. image:: ./images/pusteblume.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/pusteblume.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/pusteblume.webp">
   </a>

.. sidebar:: Blüte

   |b|

   Pusteblume

|a|

Verneinen Sie die Aussage aus Aufgabe 1.8 mit dem NOT-Operator.     


.. image:: ./images/pik-operator-not-in.svg
   :width: 400px

----

.. image:: ./images/pik-operator-not-between.svg
   :width: 400px


:Siehe auch:

   - `IN/BETWEEN`_
   - `OPERATOREN`_

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

   
   ID  NAME   AGE  ADDRESS     SALARY 
   --  -----  ---  ----------  -------
   1   Paul   32   California  20000.0
   2   Allen  25   Texas       15000.0
   4   Mark   25   Rich-Mond   65000.0
   5   David  27   Texas       85000.0

.. _OPERATOREN: https://www.tutorialspoint.com/sqlite/sqlite_operators.htm
.. _IN/BETWEEN: https://www.tutorialspoint.com/sqlite/sqlite_operators.htm
