:hide-toc:
   
:ref:`« Fragen-Übersicht Department <departmentstart>`

.. index:: Übung: Department (sqlite); Aufgabe 1.3 (Operatoren)
   
=============================
1.3 -- Filtern auf Gleichheit
=============================
.. image:: ./images/bluete001.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bluete001.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bluete001.jpg">
   </a>


.. sidebar:: SQL-Kurs

   |b|
   Serie: Pflanzen

|a|


Erläuterung zu den Operatoren
=============================

Die gefundenen Daten können direkt weiterverarbeitet werden oder
die Ergebnismenge durch Filter und Bedingungen verkleinert werden.
Die verfügbaren Operatoren lassen sich in vier Klassen einteilen:

- mathematische Operatoren
- Vergleichs-Operatoren
- Logische Operatoren
- Bit-Operatoren

Diese Operatoren finden sich immer hinter einer WHERE-Klausel.

   
.. image:: ./images/pikchr-filtern-with-eq.svg
   :width: 400px

:Siehe auch:  `Vergleichs-Operatoren`_
     
Frage: Zeige alle Tuple, die als Gehalt (SALARY) den Wert 10000 enthalten!

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

     
   id | name  | age |               address        | salary 
  ----+-------+-----+------------------------------+--------
    7 | James |  24 | Houston                      |  10000

  (1 row)

   
.. _Vergleichs-Operatoren: https://www.tutorialspoint.com/sqlite/sqlite_operators.htm

