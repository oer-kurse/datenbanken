.. _departmentstart:

==========
Department
==========



.. image:: ./images/indianer.jpg
   :width: 0


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/indianer.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/indianer.jpg">
   </a>

.. sidebar:: Kendall Old Elk

   |b|
   
   http://www.eldorado-templin.de/Stuntcrew--Indianer


|a|


   
.. Kendall Old Elk stammt aus Wyola, Montana. Er ist Mitglied der
   Apsaalooke Nation or The Crow Tribe. Die Crow, in ihrer Sprache
   übersetzt die „Kinder des langschnäbeligen Vogels“, sind ein
   Volksstamm der Indianer Nordamerikas. Kendall ist Sänger, Tänzer
   und gibt Einblicke in seine Kultur durch Konferenzen und
   Workshops. Zu diesem Zweck ist er in ganz Europa unterwegs
   gewesen. Er hofft, dass die Menschen somit ein besseres Verständnis
   dessen bekommen, wer sie sind und woher sie als Native American
   people kommen.
   

.. Sehr geehrter Herr Koppatz,

   vielen Dank für Ihre Anfrage in unserer Westernstadt.

   Gern können Sie die selbstgeschossenen Fotos auf Ihrer Homepage veröffentlichen.
   Vielen Dank für die Einbindung auf ihrer Homepage.
   
   Für weitere Fragen stehe ich Ihnen gern zur Verfügung.
   
   Mit freundlichen Grüßen aus der Westernstadt
   ELDORADO Abenteuer GmbH
   
   Michaela Junghans
   Leitung Special Events
   
   ELDORADO Abenteuer GmbH
   Am Röddelinsee 1
   D - 17268 Templin
   www.eldorado-templin.de   

:ref:`« Startseite zum Kurs <kursstart>`

Erweiterte Aufgabenstellung:

1. erstellen Sie alle Relationen
2. füllen Sie die Relationen mit den entsprechenden Daten
3. beantworten Sie alle gestellten Fragen (folgen Sie auch
   den Links, die bei der Lösung der jeweiligen Aufgabe helfen
   können)

Daten
=====

.. toctree::
   :maxdepth: 1

   daten-und-struktur-department
	      
Fragen
======
.. toctree::
   :maxdepth: 1
	      
   aufgabe-1-1
   aufgabe-1-2
   aufgabe-1-3
   aufgabe-1-4
   aufgabe-1-5
   aufgabe-1-6
   aufgabe-1-7
   aufgabe-1-8
   aufgabe-1-9
   aufgabe-1-10
   aufgabe-1-11
   aufgabe-1-12
   aufgabe-1-13
   aufgabe-1-14
   aufgabe-1-15
   aufgabe-1-16
   aufgabe-1-17

   aufgabe-2-1-mengenoperatoren
   
   wie-weiter

	      

   
