:hide-toc:
   
:ref:`« Fragen-Übersicht Department <departmentstart>`

.. index:: Übung: Department (sqlite); Aufgabe 1.2 (Auswahl)

=========================
1.2 -- einzelne Attribute
=========================

.. image:: ./images/baum.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/baum.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/baum.jpg">
   </a>



.. sidebar:: SQL-Kurs

   |b|
   Serie: Pflanzen

|a|

.. image:: ./images/pikchr-select-attributes.svg
   :width: 400px	   

     
Gib einzelne Attribute der Relation zurück.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql
     
    id | name  | salary 
   ----+-------+--------
     1 | Paul  |  20000
     2 | Allen |  15000
     3 | Teddy |  20000
     4 | Mark  |  65000
     5 | David |  85000
     6 | Kim   |  45000
     7 | James |  10000
   (7 rows)
