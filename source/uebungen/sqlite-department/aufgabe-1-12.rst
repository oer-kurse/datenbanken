==================
1.12 -- Sortierung
==================

.. index:: Übung: Department (sqlite); Aufgabe 1.12 (Sortieren)

.. image:: ./images/rote-blume-rohrbeck.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/rote-blume-rohrbeck.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/rote-blume-rohrbeck.jpg">
   </a>

.. sidebar:: SQL-Kurs

   |b|
   Serie: Pflanzen

|a|	   

:ref:`« Fragen-Übersicht Department <departmentstart>`
     
Geben Sie die Namen der Abteilungen in aufsteigender Reihenfolge aus.

:Siehe auch:  `ORDER BY`_


Antwort?
========

.. code:: sql

      
   ???

Das gesuchte Ergebnis:

.. code:: sql

   
   ID          DEPT         EMP_ID    
   ----------  -----------  ----------
   2           Engineering  2         
   3           Finance      7         
   1           IT Billing   1  

.. _ORDER BY: https://www.tutorialspoint.com/sqlite/sqlite_order_by.htm
