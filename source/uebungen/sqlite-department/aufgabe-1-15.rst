===========================
1.15 -- GROUP BY und HAVING
===========================
.. _departmentcte:

.. index:: Übung: Department (sqlite); Aufgabe 1.15 (Having)
.. index:: HAVING
	   
.. image:: ./images/rote-blume-rohrbeck.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/rote-blume-rohrbeck.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/rote-blume-rohrbeck.jpg">
   </a>

.. sidebar:: SQL-Kurs

   |b|
   Serie: Pflanzen

|a|	   

:ref:`« Fragen-Übersicht Department <departmentstart>`
     
Nach der Zusammenfassung von Datensätzen mit GROUP BY kann auf
die Gruppen ein weiterer Filter eingesetzt werden, um das Endergebnis
weiter zu verfeinern. Dafür wird HAVING (sozusagen das WHERE für
GROUP BY.

:Siehe auch:  `HAVING`_


Nur mit GROUP BY:

.. code:: sql

   select name, sum(salary) from company group by name;

.. code:: sql
	  
    name  |  sum  
   -------+-------
    Teddy | 20000
    David | 85000
    Paul  | 40000
    Kim   | 45000
    Mark  | 65000
    Allen | 15000
    James | 20000
   (7 rows)

   
Mit HAVING, wird klar, welche Namen einen
zusammengesetzten Wert repräsentieren:

.. code:: sql

   select name, sum(salary) from company
   group by name having count(name) >1;
   
Antwort?
========

.. code:: sql

   name  |  sum  
  -------+-------
  Paul  | 40000
  James | 20000
  (2 rows)
	  
.. _HAVING: https://www.tutorialspoint.com/sqlite/sqlite_having_clause.htm
