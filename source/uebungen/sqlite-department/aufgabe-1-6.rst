:ref:`« Fragen-Übersicht Department <departmentstart>`

.. index:: Übung: Department (sqlite); Aufgabe 1.6 (Alias)
	   
=======================
1.6 -- Alias definieren
=======================

.. image:: ./images/bluete004.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bluete004.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bluete004.jpg">
   </a>


.. sidebar:: SQL-Kurs

   |b|
   Serie: Pflanzen

|a|
     
Geben Sie den Spalten, für die Anzeige einen neuen Namen.

- für das Attribut »ADDRESS« die Bezeichnng »Ort« und
- statt »SALARY« die Bezeichnung »Gehalt« 

.. image:: ./images/pikchr-attribute-alias.svg
   :width: 400px
	   
:Siehe auch: `Alias`_

Antwort?
========

.. code:: sql

      
   ???

Das gesuchte Ergebnis:

.. code:: sql


   Ort         Gehalt    
   ----------  ----------
   California  20000.0   
   Texas       15000.0   
   Norway      20000.0   
   Rich-Mond   65000.0   
   Texas       85000.0   
   South-Hall  45000.0   
   Houston     10000.0


.. _Alias: https://www.tutorialspoint.com/sqlite/sqlite_alias_syntax.htm
