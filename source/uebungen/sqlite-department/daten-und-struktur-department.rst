==================
Struktur und Daten
==================

.. image:: ./images/rote-blume-rohrbeck.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/rote-blume-rohrbeck.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/rote-blume-rohrbeck.jpg">
   </a>


.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Pflanzen

|a|


.. index:: Übungen; 001 -- COMPANY & DEPARTMENT (sqlite)

- Die folgenden Tabellen und Daten werden für das Tutorial auf
  https://www.tutorialspoint.com/sqlite benötigt. Damit können dann alle
  im Tutorial gezeigten Beispiele und auch eigene Variaten ausgeführt
  werden.
- Führen Sie die hier gezeigte vorbereitenden Arbeiten durch.


Datenbank öffnen
----------------
.. code:: sql

   sqlite3 my-department.db

Tabellen anlegen
----------------
.. code:: sql

   CREATE TABLE COMPANY(
   ID INT PRIMARY KEY     NOT NULL,
   NAME           TEXT    NOT NULL,
   AGE            INT     NOT NULL,
   ADDRESS        CHAR(50),
   SALARY         REAL );

   CREATE TABLE DEPARTMENT(
   ID INT PRIMARY KEY      NOT NULL,
   DEPT           CHAR(50) NOT NULL,
   EMP_ID         INT      NOT NULL);

Datensätze einfügen
-------------------
.. code:: sql

   INSERT INTO COMPANY VALUES(1,'Paul',32,'California',20000.0);
   INSERT INTO COMPANY VALUES(2,'Allen',25,'Texas',15000.0);
   INSERT INTO COMPANY VALUES(3,'Teddy',23,'Norway',20000.0);
   INSERT INTO COMPANY VALUES(4,'Mark',25,'Rich-Mond ',65000.0);
   INSERT INTO COMPANY VALUES(5,'David',27,'Texas',85000.0);
   INSERT INTO COMPANY VALUES(6,'Kim',22,'South-Hall',45000.0);
   INSERT INTO COMPANY VALUES(7,'James',24,'Houston',10000.0);

.. code:: sql

   INSERT INTO DEPARTMENT (ID, DEPT, EMP_ID)
   VALUES (1, 'IT Billing', 1 );
   
   INSERT INTO DEPARTMENT (ID, DEPT, EMP_ID)
   VALUES (2, 'Engineering', 2 );
   
   INSERT INTO DEPARTMENT (ID, DEPT, EMP_ID)
   VALUES (3, 'Finance', 7 );
   
Ausgabeformat anpassen
----------------------
.. code:: sql

   .header on
   .mode column
   SELECT * FROM COMPANY;


