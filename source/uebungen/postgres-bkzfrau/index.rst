:ref:`« Startseite <kursstart>`

.. _bkzfrau-psql-start:

===============================
Übung -- bkzfrau (Wiederholung)
===============================

.. image:: ./images/tannentrieb2.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/tannentrieb2.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/tannentrieb2.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Pflanzen

|a|


- Dies ist eine Wiederholung der Übung *SQLite-bkzfrau*. Verwenden Sie die dort
  bereitgestellten Dateien :ref:`Download zu den Übungen bkzfrau <download-bkzfrau>`
  
  Siehe auch: :ref:`Daten importieren (postgres) <postgres-import>` 

- Danach beantworten Sie bitte die gleichen Fragen, die für die SQLite-Datenbank formuliert wurden.
- FRAGE: Wo stellen Sie Unterschiede zwischen beiden Datenbanksystemen fest?


Sie finden die Fragen unter: :ref:`Übungsserie mit der SQLite-Datenbank <bkzfraustart>`
