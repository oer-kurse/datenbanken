Aggregatfunktionen
==================
.. toctree::
   :maxdepth: 1

	      
   aufgabe-2-1
   aufgabe-2-2
   aufgabe-2-3
   aufgabe-2-4
   aufgabe-2-5
   aufgabe-2-6a
   aufgabe-2-7
   aufgabe-2-8
   aufgabe-2-9
