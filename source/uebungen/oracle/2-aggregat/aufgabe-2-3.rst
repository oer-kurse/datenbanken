===========
Aufgabe 2.3
===========
	 
.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 2.3 (Summen/Sortierung)

.. image:: ./images/blick-zum-weifberg.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/blick-zum-weifberg.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/blick-zum-weifberg.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|
	   

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`

Bestimmen Sie die Anzahl der Mitarbeiter pro Projekt
absteigend sortiert nach der Mitarbeiterzahl!
 

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


   projnr | anzahl 
  --------+--------
        1 |      6
        4 |      5
        3 |      4
        7 |      3
       23 |      2
        6 |      2
        2 |      1

  (7 rows)
 
   
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
