===========
Aufgabe 2.8
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 2.8 (Aggregatfunktionen/Vergleiche)

.. image:: ./images/carolafelsen.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/carolafelsen.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/carolafelsen.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Geben Sie für alle Projekte, die mehr als 5 Mitarbeiter haben
ODER für ihre Mitarbeiter zusammen mehr als 3000 Euro Gehalt zahlen
müssen, die projnr, das zu zahlende Gehalt und die Anzahl der
Mitarbeiter aus. Sortierung nach projnr.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    projnr | gehaelter | anz_mitarbeiter 
   --------+-----------+-----------------
         1 |      1400 |               6
	 2 |      5000 |               1
	 3 |      8000 |               4
	 4 |     15000 |               5
	 7 |      5000 |               3

   (5 rows)

   
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
