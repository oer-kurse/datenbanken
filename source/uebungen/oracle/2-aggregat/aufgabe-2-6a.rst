=========================
Aufgabe 2.6 (siehe Joins)
=========================
.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 2.6 (Join/Gruppierung)

.. image:: ./images/brosinvariante.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/brosinvariante.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/brosinvariante.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Für die Lösung dieser Aufgabe ist ein Join zwingend notwendig,
deshalb ist die Frage im Block »Join Queries« zu finden.

