===========
Aufgabe 2.4
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 2.4 (Mittelwert/Alias)

.. image:: ./images/blockhaus-daubaer-schweiz.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/blockhaus-daubaer-schweiz.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/blockhaus-daubaer-schweiz.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|
   

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`

Wie viele Projekte führen die Abteilungen im Mittel (Alias:
mittl_projzahl) aus?
   
Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

      mittl_projzahl   
   --------------------
    1.8333333333333333

   (1 row)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
