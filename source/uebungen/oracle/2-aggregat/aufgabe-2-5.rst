===========
Aufgabe 2.5
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 2.5 (Doppelte Einträge)

.. image:: ./images/brosinnadel-vom-bauernloch-aus.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/brosinnadel-vom-bauernloch-aus.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/brosinnadel-vom-bauernloch-aus.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|


:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`

Finden Sie alle Telefonnummern, 
die mehr als ein mal vergeben wurden!
 

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    telefonnr | anzahl 
   -----------+--------
         4201 |      3
	 5201 |      2
	 3201 |      2
	 1203 |      3

   (4 rows)	 

   
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
