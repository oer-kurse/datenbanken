===========
Aufgabe 2.9
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 2.9 (Zeichenketten)

.. image:: ./images/domerker.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/domerker.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/domerker.jpg">
   </a>


.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|


:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Ermitteln Sie für jedes Jahr die Anzahl der Einträge in Tabelle
Akte. Sortierung soll absteigend nach dem Jahr erfolgen.

:Anmerkung: Bei Oracle erhalten Sie das Jahr über TO_CHAR(datum, 'YYYY')
   

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    jahr | count 
   ------+-------
    2009 |     4
    2008 |     9
    2007 |     8
    2006 |     1
    2004 |     1
    2003 |     5
    2002 |     2
    2001 |     1
    1999 |     1
    1997 |     1
    1988 |     1
    1983 |     1
    1982 |     2
    
   (13 rows)
  
   
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
