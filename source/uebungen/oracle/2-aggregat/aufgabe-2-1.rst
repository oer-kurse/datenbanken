===========
Aufgabe 2.1
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 2.1 (Summen)

.. image:: ./images/blick-vom-kleinen-baerenstein.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/blick-vom-kleinen-baerenstein.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/blick-vom-kleinen-baerenstein.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|


:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Wieviel Gehalt wird für alle Angestellten zusammen 
in Tabelle Personal (gehaltssumme) ausbezahlt?
 

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

    gehaltssumme 
   --------------
        36600

   (1 row)


Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
