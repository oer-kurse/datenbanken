.. oracle-data::
=====
Daten
=====

Die Tabellen und Datensätze, die in einer lokalen Postgres-Datenbank
verwendet werden können.

Hier nochmal der Link:

http://sqlcoach.informatik.hs-kl.de/sqlcoach/training.do

In wenige Fällen verwendet Postgres andere Schlüsselworte und
Funktionen, beachten Sie die zusätzlichen Hinweise der hier
wiedergegebenen Aufgaben. 
 
.. literalinclude:: data-personal-oracle.sql
   :language: sql
