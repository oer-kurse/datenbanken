===========
Aufgabe 5.4
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 5.4 (Mengenoperationen)

.. image:: ./images/schrammsteinkette-von-schmilkaer-bastei.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/schrammsteinkette-von-schmilkaer-bastei.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/schrammsteinkette-von-schmilkaer-bastei.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Finden Sie alle Projekte, für die kein Mitarbeiter mit einem
Gehalt über 500 arbeitet. Ausgabe nach projnr aufsteigend sortiert.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

    projnr 
   --------
	 5
	 6
	 8
	 9
	10
   (5 rows)


Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
