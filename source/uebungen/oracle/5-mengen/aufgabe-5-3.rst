===========
Aufgabe 5.3
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 5.3 (Mengenoperationen)

.. image:: ./images/schneeberg-zschirnsteine-vom-prebischtor.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/schneeberg-zschirnsteine-vom-prebischtor.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/schneeberg-zschirnsteine-vom-prebischtor.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Finden Sie alle Projekte die zur Abteilung 3 gehören und für die
mindestens 2 Personen arbeiten.
Das ist nicht korrekt, wird aber als gültig berwertet!


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    projnr 
   --------
       4
      23

   (2 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
