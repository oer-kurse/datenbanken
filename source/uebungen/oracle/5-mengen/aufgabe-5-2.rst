===========
Aufgabe 5.2
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 5.2 (Mengenoperationen)

.. image:: ./images/rauensteine-baerengrund.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/rauensteine-baerengrund.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/rauensteine-baerengrund.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|


:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Finden Sie alle Projekte für die mehr als 3 Personen
arbeiten und die nicht zur Abteilung 3 gehören.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    projnr 
   --------
      1
      3

   (2 rows)

   
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
