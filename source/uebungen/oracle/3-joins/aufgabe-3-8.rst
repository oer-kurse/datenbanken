===========
Aufgabe 3.8
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 3.8 (Join)

.. image:: ./images/felsensteig-khaatal.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/felsensteig-khaatal.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/felsensteig-khaatal.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Geben Sie für jeden Angestellten mit einem 'a' im Nachnamen
aus: persnr, vname, nname sowie die Positionen seiner Akteneinträge,
sofern Einträge vorhanden sind. Sortierung nach persnr und position.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    persnr | vname  | nname  |       position        
   --------+--------+--------+-----------------------
         5 | Anna   | Bolika | Hilfsbuchhalter
         6 | Gustav | Gans   | Angestellter
         7 | Gitta  | Gans   | Praktikant
        52 | Rainer | Zufall | 
	71 | Micky  | Maus   | Berater
	71 | Micky  | Maus   | Junior Berater
	71 | Micky  | Maus   | Senior Berater
	72 | Anna   | Nass   | 
	73 | Minni  | Maus   | 
	83 | Franz  | Gans   | 
	94 | Heinz  | Elmann | Abteilungsleiter
	94 | Heinz  | Elmann | Angestellter
	94 | Heinz  | Elmann | Hauptabteilungsleiter
	94 | Heinz  | Elmann | Küchenchef
	94 | Heinz  | Elmann | Schuhputzer
	94 | Heinz  | Elmann | Tellerstapler
	94 | Heinz  | Elmann | Tellerwäscher

   (17 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
    
