===========
Aufgabe 3.9
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 3.9 (Join)

.. image:: ./images/frienstein-idagrotte.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/frienstein-idagrotte.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/frienstein-idagrotte.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`

Geben Sie alle 12 Abteilungen aus, mit der Anzahl der ihnen
zugeordneten Projekte! Sortierung nach der abtnr.
   

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

    abtnr |   abtname   | proj_anz 
   -------+-------------+----------
        1 | Buchhaltung |        3
	2 | FuE         |        0
	3 | Immobilien  |        2
	4 | Marketing   |        3
	5 | Controling  |        0
	6 | Hausmeister |        0
	7 | Kantine     |        1
	8 | Vorstand    |        1
	9 | Fuhrpark    |        0
       10 | Service     |        1
       11 | Einkauf     |        0
       12 | Verkauf     |        0

   (12 rows)



Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
