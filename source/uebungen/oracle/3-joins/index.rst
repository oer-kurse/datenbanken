Join Queries
============
.. toctree::
   :maxdepth: 1
	      
   aufgabe-3-1
   aufgabe-3-2
   aufgabe-3-3
   aufgabe-3-4
   aufgabe-3-5
   aufgabe-3-6
   aufgabe-3-7
   aufgabe-3-8
   aufgabe-3-9
   aufgabe-3-10
   aufgabe-3-11
   aufgabe-2-6
