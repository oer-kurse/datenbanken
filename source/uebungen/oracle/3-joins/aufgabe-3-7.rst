===========
Aufgabe 3.7
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 3.7 (Join)

.. image:: ./images/felsensteig-khaatal.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/felsensteig-khaatal.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/felsensteig-khaatal.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Geben Sie alle Angestellten aus, für die ein anderer mit dem gleichen
Vornamen existiert. Geben Sie für beide aus: persnr, vname, nname
Sortierung nach persnr.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


   persnr | vname  |   nname   | persnr | vname  |   nname   
  --------+--------+-----------+--------+--------+-----------
        4 | Axel   | Nässe     |     91 | Axel   | Schweiss
        5 | Anna   | Bolika    |     72 | Anna   | Nass
       21 | Klaus  | Trophobie |     90 | Klaus  | Trophobie
       51 | Rainer | Wein      |     52 | Rainer | Zufall
       52 | Rainer | Zufall    |     51 | Rainer | Wein
       72 | Anna   | Nass      |      5 | Anna   | Bolika
       90 | Klaus  | Trophobie |     21 | Klaus  | Trophobie
       91 | Axel   | Schweiss  |      4 | Axel   | Nässe

   (8 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
    

