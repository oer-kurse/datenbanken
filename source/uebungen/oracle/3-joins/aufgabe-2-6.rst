:orphane:
   
===========
Aufgabe 2.6
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 2.6 (Join/Gruppierung)

.. image:: ./images/brosinvariante.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/brosinvariante.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/brosinvariante.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|


:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Wieviel Gehalt wird pro Abteilung ausbezahlt? - Sortieren Sie
nach der AbtNr - Es brauchen nur die Abteilungen, die Mitarbeiter
haben, ausgegeben werden.

:Hinweis: Sie benötigen einen Join auf den Tabellen Projekt und Personal.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

    abtnr | gehaltssumme 
   -------+--------------
        1 |         6600
	3 |        17000
	4 |         5000
       10 |         8000

   (4 rows)
   
   
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
