===========
Aufgabe 3.1
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 3.1 (Sortierung)

.. image:: ./images/edmunds-klamm.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/edmunds-klamm.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/edmunds-klamm.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Erstellen Sie eine Telefonliste der Abteilungsleiter mit
Abteilungsname, VName, NNamen und Telefonnummer, sortiert nach dem
Abteilungsnamen.   

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

      abtname   | vname  |  nname   | telefonnr 
   -------------+--------+----------+-----------
    Buchhaltung | Donald | Duck     |      1201
    Controling  | Heinz  | Elmann   |      4204
    Einkauf     | Donald | Duck     |      1201
    FuE         | Micky  | Maus     |      3201
    Fuhrpark    | Heinz  | Elmann   |      4204
    Hausmeister | Heinz  | Elmann   |      4204
    Immobilien  | Axel   | Schweiss |      4201
    Kantine     | Heinz  | Elmann   |      4204
    Marketing   | Tick   |          |      5201
    Service     | Micky  | Maus     |      3201
    Verkauf     | Donald | Duck     |      1201
    Vorstand    | Heinz  | Elmann   |      4204

   (12 rows)


Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
