============
Aufgabe 3.11
============

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 3.1 (Sortierung)


.. image:: ./images/ein-elefant-auf-dem-bamrig.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/ein-elefant-auf-dem-bamrig.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/ein-elefant-auf-dem-bamrig.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Geben Sie für jede Abteilung aus, wie viel Gehalt sie für
die zugeordneten Mitarbeiter zahlen muss.
Sortieren Sie nach der AbtNr.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


  ABTNR	| GEHALTSSUMME
  ------+-------------
   1    |   6600
   2    |  - 
   3    |  17000
   4    |  5000
   5    |  -
   6    |  - 
   7    |  -
   8    |  -
   9    |  -
  10    |  8000
  11    |  -
  12    |  -

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
    
