===========
Aufgabe 3.4
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 3.4 (Join)

.. image:: ./images/elbe-wartturm.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/elbe-wartturm.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/elbe-wartturm.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     

Ermitteln Sie die Daten aller Mitarbeiter, die für ein Projekt
arbeiten, das zur Abteilung des Abteilungsleiters Donald gehört.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    persnr |  vname   |    nname    | projnr | telefonnr | gehalt 
   --------+----------+-------------+--------+-----------+--------
         1 | Donald   | Duck        |      1 |      1201 |   1000
	 2 | Dagobert | Duck        |      1 |      1202 |    200
	 3 | Zita     | Delle       |      1 |      1203 |     50
	 4 | Axel     | Nässe       |      1 |      1203 |     50
	 5 | Anna     | Bolika      |      1 |      1203 |     50
	 8 | Daniel   | Duesentrieb |      2 |      2203 |   5000
	10 | Klaas    | Klever      |      1 |      1204 |     50
	51 | Rainer   | Wein        |      6 |      6202 |    100
	52 | Rainer   | Zufall      |      6 |      6203 |    100

   (9 rows)


Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
    

