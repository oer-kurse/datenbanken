===========
Aufgabe 3.3
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 3.3 (Join)

.. image:: ./images/elbdampfer.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../..../../../_images/elbdampfer.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../..../../../_images/elbdampfer.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`

Ermitteln Sie den Abteilungsleiter von Heinz.
    
Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    persnr | vname |  nname   | projnr | telefonnr | gehalt 
   --------+-------+----------+--------+-----------+--------
        91 | Axel  | Schweiss |      4 |      4201 |   2000

   (1 row)
   

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
    
