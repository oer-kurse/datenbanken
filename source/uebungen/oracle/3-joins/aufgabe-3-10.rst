============
Aufgabe 3.10
============

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 3.10 (Join)

.. image:: ./images/gabrielensteig.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/gabrielensteig.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/gabrielensteig.jpg">
   </a>


.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Geben Sie alle Projekte aus und zu jedem Projekt den Namen des
Abteilungsleiters, falls dieser dem Projekt zugeordnet ist! Die
Sortierung soll aufsteigend nach der projnr erfolgen.
 

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

   projnr | abtnr | vname  |  nname   
  --------+-------+--------+----------
        1 |     1 | Donald | Duck
        2 |     1 |        | 
        3 |    10 | Micky  | Maus
	4 |     3 | Axel   | Schweiss
	5 |     4 |        | 
	6 |     1 |        | 
	7 |     4 | Tick   | 
	8 |     8 |        | 
	9 |     4 |        | 
       10 |     7 |        | 
       23 |     3 |        | 

    (11 rows)
   

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
    
