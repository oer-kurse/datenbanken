===========
Aufgabe 3.2
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 3.2 (Join)

.. image:: ./images/eine-der-vielen-leitern.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/eine-der-vielen-leitern.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/eine-der-vielen-leitern.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Geben Sie für jeden Angestellten (persnr, nname, vname) den
Namen der Abteilung aus, zu der das Projekt gehört, für das er
arbeitet. Die Sortierung soll nach der persnr erfolgen.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    persnr |    nname    |  vname   |   abtname   
   --------+-------------+----------+-------------
         1 | Duck        | Donald   | Buchhaltung
	 2 | Duck        | Dagobert | Buchhaltung
	 3 | Delle       | Zita     | Buchhaltung
	 4 | Nässe       | Axel     | Buchhaltung
	 5 | Bolika      | Anna     | Buchhaltung
	 6 | Gans        | Gustav   | Immobilien
	 7 | Gans        | Gitta    | Immobilien
	 8 | Duesentrieb | Daniel   | Buchhaltung
	10 | Klever      | Klaas    | Buchhaltung
	21 | Trophobie   | Klaus    | Service
	51 | Wein        | Rainer   | Buchhaltung
	52 | Zufall      | Rainer   | Buchhaltung
	71 | Maus        | Micky    | Service
	72 | Nass        | Anna     | Service
	73 | Maus        | Minni    | Service
	81 |             | Tick     | Marketing
	82 |             | Trick    | Marketing
	83 | Gans        | Franz    | Marketing
	84 |             | Track    | Immobilien
	90 | Trophobie   | Klaus    | Immobilien
	91 | Schweiss    | Axel     | Immobilien
	93 | Sterputz    | Fenn     | Immobilien
	94 | Elmann      | Heinz    | Immobilien

   (23 rows)


Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
    
