===========
Aufgabe 6.6
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 6.6 (Komplexe Abfragen)


.. image:: ./images/aufstieg-rudolfstein.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/aufstieg-rudolfstein.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/aufstieg-rudolfstein.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Aufstieg zum Rudolfstein
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Geben Sie für den Angestellten mit der persnr 94 alle
Gehaltsänderungen zeitlich aufsteigend sortiert mit dem
jeweiligen Datum aus.
      
:HINWEIS: Hinweis: Um die Differenz zwischen 2 Gehältern zu
	  berechnen, müssen Sie einen Join zwischen 2
	  Akteneinträge durchführen. Achten sie darauf, nur
	  direkt aufeinanderfolgende Einträge zu vergleichen.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql
      
   ???
   
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
   
