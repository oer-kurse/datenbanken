===========
Aufgabe 6.2
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 6.2 (Komplexe Abfragen)

.. image:: ./images/wehlnadel.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/wehlnadel.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/wehlnadel.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Wehlnadel
   |b|
   Serie: Elbsandsteingebirge

|a|
 

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`


Ermitteln Sie die durchschnittliche Anzahl von
Projekten pro Abteilung.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


        AVG(PANZ)      
   --------------------
    1.8333333333333333

    (1 row)

