===========
Aufgabe 6.4
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 6.4 (Komplexe Abfragen)

.. image:: ./images/blick-zur-bastei.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/blick-zur-bastei.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/blick-zur-bastei.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Blick zur Bastei
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Ermitteln Sie alle Angestellte, die Akteneinträge vor dem
01.01.2003 haben, sowie die Anzahl dieser Einträge! Sortierung
absteigend nach der Anzahl der Einträge. Anmerkung: Datumsformat
kann bestimmt werden durch TO_DATE('01.01.2003','DD.MM.YYYY')


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    persnr |   nname   | vname | anzahl 
   --------+-----------+-------+--------
        94 | Elmann    | Heinz |      5
        21 | Trophobie | Klaus |      2
        71 | Maus      | Micky |      1
        91 | Schweiss  | Axel  |      1

   (4 rows)


Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
   
