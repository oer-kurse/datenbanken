===========
Aufgabe 6.8
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 6.8 (Komplexe Abfragen)


.. image:: ./images/tyssaer-waende.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/tyssaer-waende.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/tyssaer-waende.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Tyssaer Waende
   |b|
   Serie: Elbsandsteingebirge

|a|


:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Ermitteln Sie für jedes in Akte vorhandene Jahr, wie viele Einträge
für den Angestellten mit der persnr 94 gemacht wurden. Hinweis: Das
Jahr aus dem Datum erhalten Sie mit: TO_CHAR(datum, 'YYYY').
Sortierung sollte absteigend nach dem Jahr erfolgen.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

   JAHR | COUNT 
   ------+-------
    2009 |     0
    2008 |     1
    2007 |     0
    2006 |     0
    2004 |     1
    2003 |     0
    2002 |     0
    2001 |     0
    1999 |     1
    1997 |     0
    1988 |     1
    1983 |     1
    1982 |     2
   (13 rows)

   
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
   
