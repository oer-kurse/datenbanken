===========
Aufgabe 6.9
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 6.9 (Komplexe Abfragen)

.. image:: ./images/tyssaer-waende.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/tyssaer-waende.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/tyssaer-waende.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Tyssaer Waende
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Geben Sie für jeden Angestellten mit einem aktuellen Gehalt von
mehr als 2000 aus, wie viele Einträge er in allen, in Tabelle
Akte vorkommenden Jahren ab 2002, jeweils hat. Sortierung nach
persnr, jahr. Hinweis: Das Jahr erhält man bei Oracle über:
TO_CHAR(datum,'YYYY')


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

   ???

   
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
