============
Aufgabe 6.10
============

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 6.10 (Komplexe Abfragen)

.. image:: ./images/tyssaer-waende2.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/tyssaer-waende2.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/tyssaer-waende2.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Tyssaer Wände
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Ermitteln Sie für jede Abteilung die Anzahl der zugeordneten
Projekte und der Mitarbeiter. Die Sortierung soll nach dem
Abteilungsnamen erfolgen.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    abtnr |   abtname   | anz_projekte | anz_mitarbeiter 
   -------+-------------+--------------+-----------------
        1 | Buchhaltung |            3 |               9
	2 | FuE         |            0 |               0
	3 | Immobilien  |            2 |               7
	4 | Marketing   |            3 |               3
	5 | Controling  |            0 |               0
	6 | Hausmeister |            0 |               0
	7 | Kantine     |            1 |               0
	8 | Vorstand    |            1 |               0
	9 | Fuhrpark    |            0 |               0
       10 | Service     |            1 |               4
       11 | Einkauf     |            0 |               0
       12 | Verkauf     |            0 |               0

   (12 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
   
