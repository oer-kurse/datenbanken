Komplexe Queries
================
.. toctree::
   :maxdepth: 1
	      
   aufgabe-6-1
   aufgabe-6-2
   aufgabe-6-3
   aufgabe-6-4
   aufgabe-6-5
   aufgabe-6-6
   aufgabe-6-7
   aufgabe-6-8
   aufgabe-6-9
   aufgabe-6-10
