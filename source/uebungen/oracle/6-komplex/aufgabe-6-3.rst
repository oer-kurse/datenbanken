===========
Aufgabe 6.3
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 6.3 (Komplexe Abfragen)

.. image:: ./images/wilde-klamm-zweite-bootsfahrt.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/wilde-klamm-zweite-bootsfahrt.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/wilde-klamm-zweite-bootsfahrt.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Wilde Klamm. 2. Bootsfahrt
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Geben Sie ALLE Angestellten sowie die Anzahl
ihrer Akteneinträge aus (Sortiert nach persnr).
ACHTUNG: Nicht alle haben Akteneinträge!


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    persnr |    nname    |  vname   | anzahl 
   --------+-------------+----------+--------
         1 | Duck        | Donald   |      3
	 2 | Duck        | Dagobert |      2
	 3 | Delle       | Zita     |      1
	 4 | Nässe       | Axel     |      1
	 5 | Bolika      | Anna     |      1
	 6 | Gans        | Gustav   |      1
	 7 | Gans        | Gitta    |      1
	 8 | Duesentrieb | Daniel   |      3
        10 | Klever      | Klaas    |      1
	21 | Trophobie   | Klaus    |      3
	51 | Wein        | Rainer   |      0
	52 | Zufall      | Rainer   |      0
	71 | Maus        | Micky    |      3
	72 | Nass        | Anna     |      0
	73 | Maus        | Minni    |      0
	81 |             | Tick     |      1
	82 |             | Trick    |      1
	83 | Gans        | Franz    |      0
	84 |             | Track    |      2
	90 | Trophobie   | Klaus    |      1
	91 | Schweiss    | Axel     |      4
	93 | Sterputz    | Fenn     |      1
	94 | Elmann      | Heinz    |      7
	
   (23 rows)


Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
