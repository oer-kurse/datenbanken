===========
Aufgabe 6.5
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 6.5 (Komplexe Abfragen)

.. image:: ./images/wilhelminenwand-blick-auf-marinaskala.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/wilhelminenwand-blick-auf-marinaskala.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/wilhelminenwand-blick-auf-marinaskala.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Wilhelminenwand mit Blick auf Marinaskala
   |b|
   Serie: Elbsandsteingebirge

|a|
	   

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`

Ermitteln Sie die Projekte mit dem höchsten Durchschnittsgehalt der beteiligten Personen!

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

   projnr |          avg          
  --------+-----------------------
        2 | 5000.0000000000000000
   (1 row)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
   
