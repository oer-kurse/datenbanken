===========
Aufgabe 6.1
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 6.1 (Komplexe Abfragen)

.. image:: ./images/torstein-schrammsteinkette-vom-OAP.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/torstein-schrammsteinkette-vom-OAP.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/torstein-schrammsteinkette-vom-OAP.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Bestimmen Sie die Abteilungen mit den meisten
Projekten, sowie die Anzahl der zugehörigen Projekte (proj_anz)
Hinweis: Benutzen Sie geschachtelte SELECT-Statements. Denken
Sie daran, dass Sie auch in der FROM-Klausel eine Tabelle mit
SELECT konstruieren können.

Quelle:
      
http://www.w3resource.com/sql/aggregate-functions/max-count.php
   

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

    abtnr | anz_proj 
   -------+----------
        1 |        3
        4 |        3

   (2 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
