===========
Aufgabe 6.7
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 6.7 (Komplexe Abfragen)

.. image:: ./images/felstuerme.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/felstuerme.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/felstuerme.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Fels-Türme
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Ermitteln Sie für jeden Angestellten alle Projekte, die sich
sein Gehalt nicht leisten können. Geben Sie für diese Projekte
die Differenz zwischen Gehalt und Projektbudget sortiert nach
persnr und projnr aus.
 

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:
.. code:: sql

   ???

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
   
