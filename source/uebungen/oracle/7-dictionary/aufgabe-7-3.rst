===========
Aufgabe 7.3
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 7.3 (Data Dictionary)

.. image:: ./images/solitaer.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/solitaer.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/solitaer.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`

Finden Sie alle Spaltennamen, die länger als 6 Zeichen sind. 
Geben Sie diese zusammen mit dem Namen ihrer Tabelle sortiert aus.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

  TABLE_NAME              COLUMN_NAME
  -----------------------------------
  ABTEILUNG               ABTNAME
  AKTE                    POSITION
  BUDGET_UEBERSICHT_V     GEHALTSUMME
  BUDGET_UEBERSICHT_V     PROJBUDGET
  PERSONAL                TELEFONNR
  PRODUKTE                KATEGORIE
  PRODUKTE                PRODUKT
  PRODUKTE                WGRUPPE
  REGIONEN                VETRIEBSGEBIET
  VERKAEUFE               PRODUKT
