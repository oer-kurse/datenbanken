===========
Aufgabe 7.1
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 7.1 (Data Dictionary)

.. image:: ./images/tyssaer-waende-afrika.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/tyssaer-waende-afrika.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/tyssaer-waende-afrika.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Tyssaer Wände: Afrika
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Finden Sie alle Spaltennamen, die länger als 6 Zeichen sind.
Geben Sie diese zusammen mit dem Namen ihrer Tabelle sortiert aus.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


   COLUMN_NAME
   -----------
   ABTNR
   ABTNAME
   BUDGET
   CHEFNR
   

   
