===========
Aufgabe 7.2
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 7.2 (Data Dictionary)

.. image:: ./images/lilienstein.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/lilienstein.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/lilienstein.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Lilienstein
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     

Finden Sie die Tabellen mit den kürzesten Namen.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

   COLUMN_NAME
   -----------
   ABTNR
   ABTNAME
   BUDGET
   CHEFNR
   
