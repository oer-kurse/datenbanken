============
Aufgabe 4.10
============

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 4.10 (Unterabfragen/Funktionen)

.. image:: ./images/gohrisch-aussicht.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/gohrisch-aussicht.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/gohrisch-aussicht.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Ermitteln Sie für jedes Projekt das Durchschnittsgehalt
innerhalb des Projektes und dessen Abweichung vom
Durchschnittsgehalt im Gesamtunternehmen jeweils auf 1 Stelle hinter
dem Komma genau.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

       projnr | d_gehalt | abweichung 
      --------+----------+------------
            4 |   3000.0 |     1408.7
           23 |   1000.0 |     -591.3
            1 |    233.3 |    -1358.0
	    3 |   2000.0 |      408.7
	    6 |    100.0 |    -1491.3
	    2 |   5000.0 |     3408.7
	    7 |   1666.7 |       75.4

      (7 rows)
   
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
