===========
Aufgabe 4.7
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 4.7 (Unterabfragen/Funktionen)

.. image:: ./images/kriechband-bei-honigsteinen2.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/kriechband-bei-honigsteinen2.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/kriechband-bei-honigsteinen2.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     

Geben Sie den neuesten Akteneintrag aus.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

    persnr |   datum    |    position    | gehalt 
   --------+------------+----------------+--------
         8 | 2009-04-01 | Cheferfinder   |   5000
	 1 | 2009-04-01 | Oberbuchhalter |   1000

   (2 rows)


Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
