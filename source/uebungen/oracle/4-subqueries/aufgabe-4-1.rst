.. _ora41:
===========
Aufgabe 4.1
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 4.1 (Unterabfragen/Logik)

.. image:: ./images/gans-und-wehlgrund.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/gans-und-wehlgrund.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/gans-und-wehlgrund.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     

Geben Sie die Personaldaten aller Angestellten aus, die keinen
Akteneintrag haben.
 
Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    persnr | vname  | nname  | projnr | telefonnr | gehalt 
   --------+--------+--------+--------+-----------+--------
        72 | Anna   | Nass   |      3 |      3202 |   1000
	73 | Minni  | Maus   |      3 |      3203 |   2000
	51 | Rainer | Wein   |      6 |      6202 |    100
	52 | Rainer | Zufall |      6 |      6203 |    100
	83 | Franz  | Gans   |      7 |      5205 |   2000

   (5 rows)


Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
