===========
Aufgabe 4.2
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 4.2 (Unterabfragen/Funktionen)

.. image:: ./images/grundmuehle-relikte.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/grundmuehle-relikte.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/grundmuehle-relikte.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Geben Sie alle Abteilungen (abtname,budget) nach dem Budget 
und dem abtname sortiert aus, für die es noch mindestens eine
weitere gibt, die über das gleiche Budget verfügt.


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


      abtname   | budget 
   -------------+--------
    Hausmeister |   1000
    Kantine     |   1000
    Einkauf     |  15000
    FuE         |  15000
    Service     |  30000
    Verkauf     |  30000

   (6 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
