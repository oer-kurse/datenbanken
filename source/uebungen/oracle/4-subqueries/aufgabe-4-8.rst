===========
Aufgabe 4.8
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 4.8 (Unterabfragen/Funktionen)

.. image:: ./images/prebischtor-mit-rosenberg.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/prebischtor-mit-rosenberg.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/prebischtor-mit-rosenberg.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|


:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Finden Sie alle Angestellten, die nicht am oberen oder unteren
Ende mit ihrem Gehalt liegen. Sortierung nach pernr.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    persnr |  vname   |    nname    | projnr | telefonnr | gehalt 
   --------+----------+-------------+--------+-----------+--------
         1 | Donald   | Duck        |      1 |      1201 |   1000
	 2 | Dagobert | Duck        |      1 |      1202 |    200
	 3 | Zita     | Delle       |      1 |      1203 |     50
	 4 | Axel     | Nässe       |      1 |      1203 |     50
	 5 | Anna     | Bolika      |      1 |      1203 |     50
	 6 | Gustav   | Gans        |     23 |      2201 |   2000
	 8 | Daniel   | Duesentrieb |      2 |      2203 |   5000
	10 | Klaas    | Klever      |      1 |      1204 |     50
	21 | Klaus    | Trophobie   |      3 |      3201 |   2000
	51 | Rainer   | Wein        |      6 |      6202 |    100
	52 | Rainer   | Zufall      |      6 |      6203 |    100
	71 | Micky    | Maus        |      3 |      3201 |   3000
	72 | Anna     | Nass        |      3 |      3202 |   1000
	73 | Minni    | Maus        |      3 |      3203 |   2000
	81 | Tick     |             |      7 |      5201 |   1500
	82 | Trick    |             |      7 |      5201 |   1500
	83 | Franz    | Gans        |      7 |      5205 |   2000
	84 | Track    |             |      4 |      4201 |   2000
	90 | Klaus    | Trophobie   |      4 |      4201 |   2000
	91 | Axel     | Schweiss    |      4 |      4201 |   2000
	93 | Fenn     | Sterputz    |      4 |      4203 |   2000

   (21 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
