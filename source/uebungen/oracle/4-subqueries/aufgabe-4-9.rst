===========
Aufgabe 4.9
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 4.9 (Unterabfragen/Funktionen)

.. image:: ./images/prebischtor-von-weiteroben.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/prebischtor-von-weiteroben.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/prebischtor-von-weiteroben.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Finden Sie alle Angestellten, die überdurchschnittlich viel verdienen.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    persnr | vname  |    nname    | projnr | telefonnr | gehalt 
   --------+--------+-------------+--------+-----------+--------
         6 | Gustav | Gans        |     23 |      2201 |   2000
         8 | Daniel | Duesentrieb |      2 |      2203 |   5000
        21 | Klaus  | Trophobie   |      3 |      3201 |   2000
	71 | Micky  | Maus        |      3 |      3201 |   3000
	73 | Minni  | Maus        |      3 |      3203 |   2000
	83 | Franz  | Gans        |      7 |      5205 |   2000
	84 | Track  |             |      4 |      4201 |   2000
	90 | Klaus  | Trophobie   |      4 |      4201 |   2000
	91 | Axel   | Schweiss    |      4 |      4201 |   2000
	93 | Fenn   | Sterputz    |      4 |      4203 |   2000
	94 | Heinz  | Elmann      |      4 |      4204 |   7000

   (11 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
