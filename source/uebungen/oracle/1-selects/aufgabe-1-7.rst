===========
Aufgabe 1.7
===========

.. image:: ./images/blick-auf-kleine-und-grosse-gans.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/blick-auf-kleine-und-grosse-gans.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/blick-auf-kleine-und-grosse-gans.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|


:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     

Finden sie alle Akteneinträge, die ab 1998 mit einem Gehalt 
von über 2800 gemacht wurden. 


:HINWEIS:

  Beachten Sie bitte Die unterschieliche Behandlung des Datums
  in Oracle und Postgres

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

    persnr |   datum    |       position        | gehalt 
   --------+------------+-----------------------+--------
         8 | 2008-06-01 | Erfinder              |   3000
         8 | 2009-04-01 | Cheferfinder          |   5000
        94 | 1999-05-01 | Abteilungsleiter      |   3000
        94 | 2008-05-01 | Hauptabteilungsleiter |   7000
        71 | 2008-04-01 | Senior Berater        |   3000

   (5 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
