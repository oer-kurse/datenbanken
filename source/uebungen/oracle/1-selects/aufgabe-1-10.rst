============
Aufgabe 1.10
============

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 1.10 (Namen/Muster)

.. image:: ./images/blick-auf-pfaffenstein-mit-barbarine.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/blick-auf-pfaffenstein-mit-barbarine.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/blick-auf-pfaffenstein-mit-barbarine.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Finden Sie alle Angestellten, bei denen ein 'a' als zweiter Buchstabe
im Nachnamen vorkommt. Sortierung nach persnr.
 

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

    persnr | vname  | nname | projnr | telefonnr | gehalt 
   --------+--------+-------+--------+-----------+--------
         6 | Gustav | Gans  |     23 |      2201 |   2000
         7 | Gitta  | Gans  |     23 |      2202 |      0
        71 | Micky  | Maus  |      3 |      3201 |   3000
	72 | Anna   | Nass  |      3 |      3202 |   1000
	73 | Minni  | Maus  |      3 |      3203 |   2000
	83 | Franz  | Gans  |      7 |      5205 |   2000
   (6 rows)

   
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
