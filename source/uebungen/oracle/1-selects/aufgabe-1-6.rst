===========
Aufgabe 1.6
===========

.. image:: ./images/blick-auf-hohnstein.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/blick-auf-hohnstein.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/blick-auf-hohnstein.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
 
Finden Sie alle Angestellten, die keinen Nachnamen haben.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

    persnr | vname | nname | projnr | telefonnr | gehalt 
   --------+-------+-------+--------+-----------+--------
        81 | Tick  |       |      7 |      5201 |   1500
        82 | Trick |       |      7 |      5201 |   1500
        84 | Track |       |      4 |      4201 |   2000

   (3 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
