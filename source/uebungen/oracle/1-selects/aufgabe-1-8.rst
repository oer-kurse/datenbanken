===========
Aufgabe 1.8
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 1.8 (Namen/Längen)

.. image:: ./images/blick-auf-pfaffenstein-mit-barbarine.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/blick-auf-pfaffenstein-mit-barbarine.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/blick-auf-pfaffenstein-mit-barbarine.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|
	   

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
 
Geben Sie für alle Angestellten, deren Nachnamen mit einem
Buchstaben vor M beginnt, ihren Namen sowie die Länge ihres
Vornamens aus. Geben Sie die Angestellten so aus, dass diejenigen
mit einem längeren Vornamen vor denjenigen mit einem kürzeren
ausgegeben werden.
 

Antwort?
========

.. code:: sql

   
   ???

Das gesuchte Ergebnis:

.. code:: sql


       nname    |  vname   | laenge 
   -------------+----------+--------
    Duck        | Dagobert |      8
    Gans        | Gustav   |      6
    Duck        | Donald   |      6
    Duesentrieb | Daniel   |      6
    Elmann      | Heinz    |      5
    Gans        | Gitta    |      5
    Klever      | Klaas    |      5
    Gans        | Franz    |      5
    Bolika      | Anna     |      4
    Delle       | Zita     |      4
    (10 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
 
