===========
Aufgabe 1.1
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 1.1 (Namen/Sortierung)

.. image:: ./images/bastei-felsen-am-wehlgrund.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/bastei-felsen-am-wehlgrund.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/bastei-felsen-am-wehlgrund.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Geben Sie den Nach- und Vornamen aller Angestellten aufsteigend
sortiert aus.



Antwort?
========

.. code:: sql
      
   ???
   

Das gesuchte Ergebnis:

.. code:: sql

      nname    |  vname   
   ------------+----------
   Bolika      | Anna
   Delle       | Zita
   Duck        | Dagobert
   Duck        | Donald
   Duesentrieb | Daniel
   Elmann      | Heinz
   Gans        | Franz
   Gans        | Gitta
   Gans        | Gustav
   Klever      | Klaas
   Maus        | Micky
   Maus        | Minni
   Nass        | Anna
   Nässe       | Axel
   Schweiss    | Axel
   Sterputz    | Fenn
   Trophobie   | Klaus
   Trophobie   | Klaus
   Wein        | Rainer
   Zufall      | Rainer
               | Tick
               | Track
               | Trick
   (23 rows)
     
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
