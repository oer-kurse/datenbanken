===========
Aufgabe 1.3
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 1.3 (Namen/Sortierung)

.. image:: ./images/bastei3.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/bastei3.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/bastei3.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: 

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`

 
Finden Sie alle Angestellen mit 'ei' im Nachnamen, 
sortiert nach ihrer Personalnummer.


Antwort?
========

::
      
   ???

Das gesuchte Ergebnis:
::

    persnr | vname  |  nname   | projnr | telefonnr | gehalt 
   --------+--------+----------+--------+-----------+--------
        51 | Rainer | Wein     |      6 |      6202 |    100
        91 | Axel   | Schweiss |      4 |      4201 |   2000

   (2 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
   
