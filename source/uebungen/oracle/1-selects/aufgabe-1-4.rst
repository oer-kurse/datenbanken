===========
Aufgabe 1.4
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 1.4 (Namen/Sortierung)

.. image:: ./images/bielatal2.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/bielatal2.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/bielatal2.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Finde alle Angestellten mit 'ma' im Nachnamen, sortiert nach
ihrem vollständigen Namen.
 
Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

    persnr | vname | nname  | projnr | telefonnr | gehalt 
   --------+-------+--------+--------+-----------+--------
        94 | Heinz | Elmann |      4 |      4204 |   7000
        71 | Micky | Maus   |      3 |      3201 |   3000
        73 | Minni | Maus   |      3 |      3203 |   2000
	
   (3 rows)
     
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   
