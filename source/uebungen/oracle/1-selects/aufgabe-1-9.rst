===========
Aufgabe 1.9
===========

.. index:: Übung: Personal (Oracle/Postgres); Aufgabe 1.9 (Namen/Längen/Concatenation)

.. image:: ./images/blick-vom-carolafelsen.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/blick-vom-carolafelsen.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/blick-vom-carolafelsen.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|
	   
	   
..

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
Geben Sie für alle Angestellten Persnr, Vor- und Nachnamen und
die Gesamtlänge ihres Namens aus. Die Sortierung soll dabei gemäß
der Gesamtlänge des Namens erfolgen.
Für Postgres verwenden Sie die *coalesce*-Funktion um Null-Werte
sichtbar zu machen, bzw. Zwischenergebnisse weiterverarbeiten zu
könnnen.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql


    persnr |  vname   |    nname    | laenge 
   --------+----------+-------------+--------
        81 | Tick     |             |      4
	84 | Track    |             |      5
	82 | Trick    |             |      5
	72 | Anna     | Nass        |      8
	 7 | Gitta    | Gans        |      9
        83 | Franz    | Gans        |      9
         3 | Zita     | Delle       |      9
        73 | Minni    | Maus        |      9
         4 | Axel     | Nässe       |      9
        71 | Micky    | Maus        |      9
         1 | Donald   | Duck        |     10
	 5 | Anna     | Bolika      |     10
	 6 | Gustav   | Gans        |     10
        51 | Rainer   | Wein        |     10
	10 | Klaas    | Klever      |     11
	94 | Heinz    | Elmann      |     11
         2 | Dagobert | Duck        |     12
        52 | Rainer   | Zufall      |     12
	91 | Axel     | Schweiss    |     12
	93 | Fenn     | Sterputz    |     12
	21 | Klaus    | Trophobie   |     14
	90 | Klaus    | Trophobie   |     14
         8 | Daniel   | Duesentrieb |     17

   (23 rows)

   
Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
