===========
Aufgabe 1.5
===========

.. image:: ./images/blick-auf-blossstock.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/blick-auf-blossstock.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/blick-auf-blossstock.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Elbsandsteingebirge

|a|

:ref:`« Fragen-Übersicht zum SQL-Coach <oraclestart>`
     
 
Finden Sie alle Angestellten, deren Nachnamen mehr als 4
Buchstaben hat! Sortieren Sie nach der persnr.

Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql

    persnr | vname  |    nname    | projnr | telefonnr | gehalt 
   --------+--------+-------------+--------+-----------+--------
         3 | Zita   | Delle       |      1 |      1203 |     50
         4 | Axel   | Nässe       |      1 |      1203 |     50
         5 | Anna   | Bolika      |      1 |      1203 |     50
         8 | Daniel | Duesentrieb |      2 |      2203 |   5000
	10 | Klaas  | Klever      |      1 |      1204 |     50
        21 | Klaus  | Trophobie   |      3 |      3201 |   2000
        52 | Rainer | Zufall      |      6 |      6203 |    100
        90 | Klaus  | Trophobie   |      4 |      4201 |   2000
        91 | Axel   | Schweiss    |      4 |      4201 |   2000
        93 | Fenn   | Sterputz    |      4 |      4203 |   2000
        94 | Heinz  | Elmann      |      4 |      4204 |   7000

   (11 rows)

Die Struktur der Datenbank
==========================

.. code:: bash

   ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
   AKTE	        ( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
   PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                  TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
   PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
