.. _oraclestart:

=================
Übung -- Personal
=================

.. raw:: html

   <script  src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.0.3/vue.js"></script>
   <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
   <div id="app">

.. image:: ./images/bobbycar.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bobbycar.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bobbycar.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Dinge

|a|

- Übungen auf:

  http://sqlcoach.informatik.hs-kl.de/sqlcoach/training.do

  *(mit freundlicher Genehmigung von Prof. Dr. Bernhard Schiefer)*

  Wenn Sie das SQL-Statement auch auf dem Server der TH Kaiserslautern
  testen wollen, lassen Sie das Semikolon am Ende der Abfrage bitte weg.
  
- Erweiterte Aufgabenstellung:

  1. verwenden Sie Postgres statt der Oracle-DB
  2. erstellen Sie alle Relationen
  3. füllen Sie die Relationen mit den entsprechenden Daten
  4. beantworten Sie alle gestellten Fragen
   
Personal-Datenbank
==================

Aufgaben für Oracle und/oder Postgres.


.. toctree::
   :maxdepth: 1

   0-rohdaten/daten
   1-selects/index
   2-aggregat/index
   3-joins/index
   4-subqueries/index
   5-mengen/index
   6-komplex/index
   7-dictionary/index
   


	     
