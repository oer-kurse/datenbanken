:ref:`« zurück zur Startseite <kursstart>`
     
===============================
SQLite -- Festplatten-Statistik
===============================


	     
Auch wenn immer mehr Festplatten durch SSD ersetzt werden,
die Firma **Backblaze** stellt seit 2013 Daten und die daraus 
generierten Statistiken für die Allgemeinheit zur Verfügung. Mit diesen
Zahlen zum Einsatz der Festplatten in den Rechenzentren wird ein
transparenter Einblick zu technischen Parametern der eingesetzten
Hardware gegeben.

:Aufgaben:

   - Laden Sie die Daten für eines der Quartale herunter.
   - Entpacken Sie `Daten und Dokumentation zur Festplattenstatistik`_.
   - Führen Sie die Anweisungen, wie in der Dokumentation beschrieben, aus. 

:Fragen:

   - Warum gibt die letzte Anweisung keine Ergebnisse?
   - Was müssen Sie ändern, um zu solch einem Ergebnis zu kommen, wie
     in der folgenden Tabelle gezeigt?
     
   | 

   ::
   
	select * from failure_rates order by model;
	model                 drive_days  failures    annual_failure_rate
	--------------------  ----------  ----------  -------------------
	HGST HMS5C4040ALE640  134209      4           1.087855508945     
	HGST HMS5C4040BLE640  161203      3           0.679267755562862  
	Hitachi HDS5C3030ALA  85347       1           0.427665881636144  
	Hitachi HDS723030ALA  18744       1           1.94728979940248   
	ST4000DM000           660152      45          2.4880633551061    
	ST4000DX000           3631        1           10.0523271825943   
	ST6000DX000           35859       2           2.03575113639533   
	ST8000DM002           112699      5           1.61935775827647   
	WDC WD30EFRX          20530       1           1.77788602045787  


Denkpause
=========


.. image:: ./images/festplatte.png
   :width: 0

.. raw:: html
	 
   <div style='text-align:center;'>
     <img src='../../_images/festplatte.png' 
          alt='Aufbau einer Festplatte' width='400px' />
     </div>

Aufbau einer Festplatte (Bildquelle: `Wikipedia; I, Surachit, CC BY-SA 3.0`_ ) 
	
.. _Daten und Dokumentation zur Festplattenstatistik:
   https://www.backblaze.com/b2/hard-drive-test-data.html

.. _Wikipedia; I, Surachit, CC BY-SA 3.0: https://commons.wikimedia.org/w/index.php?curid=2693333
