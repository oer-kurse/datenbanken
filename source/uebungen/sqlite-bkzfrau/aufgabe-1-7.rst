:ref:`« Fragen-Übersicht BKZFRAU <bkzfraustart>`

.. index:: BKZFRAU (sqlite); Aufgabe 7
		
===========
Aufgabe 1.7
===========

.. image:: ./images/vorsicht-kinder.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/vorsicht-kinder.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/vorsicht-kinder.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Schilder

|a|
     
 
 
Welcher Beruf hat die längste Bezeichnung?
 

:Siehe auch:
   
   - `Aggregat-Funktionen`_
   - `Subqueries`_


Antwort?
========

.. code:: sql

      
   ???
   
Das gesuchte Ergebnis:

.. code:: sql


   gnum        beruf                                   
   ----------  ----------------------------------------
   4331        ????
   8615        ????

.. _Aggregat-Funktionen: https://www.tutorialspoint.com/sqlite/sqlite_useful_functions.htm

.. _Subqueries: https://www.tutorialspoint.com/sqlite/sqlite_sub_queries.htm
