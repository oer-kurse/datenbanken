:ref:`« Fragen-Übersicht BKZFRAU <bkzfraustart>`

.. index:: BKZFRAU (sqlite); Aufgabe 2
     
===========
Aufgabe 1.2
===========

.. image:: ./images/flugverbot.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/flugverbot.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/flugverbot.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Schilder

|a|

     
Geben Sie nun die Tuple sortiert für das Attribut »gnum« aus.


:Siehe auch:  `ORDER BY`_

Antwort?
========

.. code:: sql

      
   ???

Das gesuchte Ergebnis:

.. code:: sql

   
    gnum |                beruf                
  ------+-------------------------------------
    215 | Aalbrutzüchterin
    222 | Aalschnurfischerin
    222 | Aalstickerin 
   1841 | Aalkorbmacherin
   4033 | Aalräucherinin
   6290 | Saalaufseherin, Industriemeisterin
   6290 | Saalmeisterinldienerin
   7056 | Saalchefin (Croupiere)
   7744 | Leiterin des Maschinensaals, EDV 
   7744 | Maschinensaalleiterin (EDV)
   7744 | EDV-Saalaufseherin
   7833 | Lochkartensaalleiterin
   7833 | Leiterin des Lochkartensaals
   7924 | Lesesaalaufseherin
   7925 | Saalaufseherin (Wächterin Ordnerin)
   7944 | Theater-, Saaldienerin
   7944 | Saaldienerin
   9111 | Saalbesitzerin
   9121 | Obersaaltochter
   9122 | Saalkellnerin
   9127 | Saalmädchen
   9127 | Saaltochter
   9331 | Saalreinigerin
  (23 rows)

.. raw:: html

   <section>
   <input type="checkbox" value="bkzfrau-1.2"/> Kurzfilm
   <div class="togglefilm">

   
	 
   <p>Lösung... </p>
   <asciinema-player src="../../_static/casts/bkzfrau-1-2.cast"></asciinema-player>
   <p>...zur Aufgabe</p>

   </div></section>
   
.. _ORDER BY: https://www.tutorialspoint.com/sqlite/sqlite_order_by.htm
