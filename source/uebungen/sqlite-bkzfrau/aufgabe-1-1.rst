:ref:`« Fragen-Übersicht BKZFRAU <bkzfraustart>`

.. index:: BKZFRAU (sqlite); Aufgabe 1 
     
============
Aufgabe 1.1a
============

.. image:: ./images/diverse-verbote.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/diverse-verbote.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/diverse-verbote.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Schilder

|a|

    
Welche Berufe enthalten die Zeichenkette 'aal'?
Gibt es einen Unterschied zu 'Aal'?
Wie ignoriert man Klein- und Großschreibung?

Antwort?
========

.. code:: sql

      
   ???

Das gesuchte Ergebnis:

.. code:: sql

    
      gnum        beruf            
   ----------  -----------------
   215         Aalbrutzüchterin
   1841        Aalkorbmacherin  
   4033        Aalräucherin    
   222         Aalschnurfischeri
   222         Aalstickerin     
   7744        EDV-Saalaufseheri
   7833        Leiterin des Loch
   7744        Leiterin des Masc
   7924        Lesesaalaufseheri
   7833        Lochkartensaallei
   7744        Maschinensaalleit
   9121        Obersaaltochter  
   7925        Saalaufseherin (W
   6290        Saalaufseherin, I
   9111        Saalbesitzerin   
   7056        Saalchefin (Croup
   7944        Saaldienerin     
   9122        Saalkellnerin    
   9127        Saalmädchen     
   6290        Saalmeisterin    
   9331        Saalreinigerin   
   9127        Saaltochter      
   7944        Theater-, Saaldie

.. raw:: html

   <section>
   <input type="checkbox" value="full-join"/> Kurzfilm
   <div class="togglefilm">

   
	 
   <p>Lösung... </p>
   <asciinema-player src="../../_static/casts/bkzfrau-1-1.cast"></asciinema-player>
   <p>...zur Aufgabe</p>

   </div></section>
   


============
Aufgabe 1.1b
============

1. Welche Berufe enthalten die Zeichenkette 'rota'?
2. Gibt es einen Unterschied zu 'Rota'? 
3. Wie ignoriert man Klein- und Großschreibung?


Antwort?
========

.. code:: sql
      
   ???

Das gesuchte Ergebnis:

.. code:: sql
     
    num  |              beruf              
   ------+---------------------------------
    3914 | Brotausrichterin    
    7442 | Brotausträgerin    
    1739 | Illustrationsrotationerin    
    1741 | Rollenrotationsdruckerin    
    1742 | Tiefdruckrotationsdruckerin    
    1732 | Zeitungsrotationsdruckerin    

    (6 rows)
    
