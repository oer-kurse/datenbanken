.. _bkzfraustart:

:ref:`« Startseite zum Kurs <kursstart>`
     
=======
bkzfrau
=======

.. image:: ./images/polnisch-russische-grenze.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/polnisch-russische-grenze.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/polnisch-russische-grenze.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Schilder

|a|


1. Erstellen Sie bitte alle Relationen
2. Füllen Sie die Relationen mit den entsprechenden Daten.
3. Beantworten Sie alle gestellten Fragen.
4. Legen Sie sich eine Scriptsammlung an, um das gleiche SQL-Statement
   auch ein zweites Mal ausführen zu können.
   
Erweiterte Aufgabenstellung:

- Verwenden Sie die Postgres-DB statt der sqlite-DB.

  (:ref:`siehe auch Hauptmenu <bkzfrau-psql-start>`)
  
  
Daten
=====

.. _download-bkzfrau:

Laden Sie die Daten herunter.

:download:`Download: bkzfrau-daten.zip <bkzfrau-daten.zip>`

Nach dem Entpacken finden Sie drei Dateien:

- bkzfrau.csv
- bkzfrau-utf8.sql
- bkzfrau-buchstabe-a.sql
  
Importieren Sie die Daten auf die eine oder andere Art:


:ref:`Siehe auch: Daten importieren <sqlite-import>`
	  
Fragen
======

.. toctree::
   :maxdepth: 1
	      
   aufgabe-1-1
   aufgabe-1-2
   aufgabe-1-3
   aufgabe-1-4
   aufgabe-1-5
   aufgabe-1-6
   aufgabe-1-7
   aufgabe-1-8
   aufgabe-1-9
   

