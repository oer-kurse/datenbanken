:ref:`« Fragen-Übersicht BKZFRAU <bkzfraustart>`

.. index:: BKZFRAU (sqlite); Aufgabe 4
     
===========
Aufgabe 1.4
===========

.. image:: ./images/nachbar.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/nachbar.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/nachbar.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Schilder

|a|

     
Wie viele Datensätze gibt es im Nummernbereich 1500 bis 1600?

:Siehe auch:  `IN/BETWEEN`_

Antwort?
========

.. code:: sql

      
   ???

Das gesuchte Ergebnis:

.. code:: sql

     
   1500-1600 
   ----------
   146  

.. Lösung

   select count(*) as "1500-1600" from bkzfrau where gnum between 1500 and 1600;


.. _IN/BETWEEN: https://www.tutorialspoint.com/sqlite/sqlite_where_clause.htm
