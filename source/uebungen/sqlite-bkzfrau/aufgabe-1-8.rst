:ref:`« Fragen-Übersicht BKZFRAU <bkzfraustart>`

.. index:: BKZFRAU (sqlite); Aufgabe 8
		
===========
Aufgabe 1.8
===========

.. image:: ./images/vorsicht-kinder2.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/vorsicht-kinder2.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/vorsicht-kinder2.webp">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Schilder

|a|
     
Setzen Sie alle Berufe mit der ID **1111** auf NULL!

:Siehe auch:

- `Update`_

  - um einen Wert komplett zu entfernen (also auf NICHTS zu setzen) wird
  das Schlüsselwort **NULL** verwendet! Siehe auch `NULL`_

Frage
=====

Wieviele Datensätze besitzen nun den Wert »NULL« im Attribut »beruf« ?

Antwort?
========

.. code:: sql
      
   ???
   
Das gesuchte Ergebnis:

.. code:: sql


   Datensätze ohne einen Wert
   --------------------------
   26      
     
.. _Update: https://www.tutorialspoint.com/sqlite/sqlite_update_query.htm
.. _NULL: https://www.tutorialspoint.com/sqlite/sqlite_null_values.htm
