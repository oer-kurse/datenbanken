:ref:`« Fragen-Übersicht BKZFRAU <bkzfraustart>`

.. index:: BKZFRAU (sqlite); Aufgabe 5

===========
Aufgabe 1.5
===========

.. image:: ./images/schild-beschwerde.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/schild-beschwerde.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/schild-beschwerde.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Schilder

|a|
 
Wie viele Berufe fangen mit dem Buchstaben U an?

:Siehe auch:  `Aggregat-Funktionen`_


Antwort?
========

.. code:: sql

      
   ???

Das gesuchte Ergebnis:

.. code:: sql


   Berufe mit U am Anfang
   ----------------------
   169  

.. _Aggregat-Funktionen: https://www.tutorialspoint.com/sqlite/sqlite_useful_functions.htm
