:ref:`« Fragen-Übersicht BKZFRAU <bkzfraustart>`

.. index:: BKZFRAU (sqlite); Aufgabe 3

===========
Aufgabe 1.3
===========

.. image:: ./images/kraftwagenanlage.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/kraftwagenanlage.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/kraftwagenanlage.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Schilder

|a|
     
Geben Sie nur die ersten 5 Tuple der vorherigen Abfrage aus! 

:Siehe auch:  `LIMIT`_

Antwort?
========

.. code:: sql

      
   ???

Das gesuchte Ergebnis:

.. code:: sql

     
      gnum        beruf            
   ----------  -----------------
   215         Aalbrutzüchterin
   1841        Aalkorbmacherin  
   4033        Aalräucherin    
   222         Aalschnurfischeri
   222         Aalstickerin     

.. _LIMIT: https://www.tutorialspoint.com/sqlite/sqlite_limit_clause.htm
