:ref:`« Fragen-Übersicht BKZFRAU <bkzfraustart>`

.. index:: BKZFRAU (sqlite); Aufgabe 9
		
===========
Aufgabe 1.9
===========

.. image:: ./images/unser-potsdam.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/unser-potsdam.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/unser-potsdam.webp">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Schilder

|a|
     
 
In wievielen Berufsbezeichnungen steckt das Wort »mann«? 
 
  
Antwort?
========

.. code:: sql

      
   ???
   
Das gesuchte Ergebnis:

.. code:: sql


   beruf
   --------
   ???
