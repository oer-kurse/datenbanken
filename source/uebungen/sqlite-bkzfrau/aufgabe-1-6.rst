:ref:`« Fragen-Übersicht BKZFRAU <bkzfraustart>`

.. index:: BKZFRAU (sqlite); Aufgabe 6
		
===========
Aufgabe 1.6
===========

.. image:: ./images/schild-fasse-dich-kurz.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/schild-fasse-dich-kurz.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/schild-fasse-dich-kurz.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Schilder

|a|
 
Starten Sie eine Suche mit like, die mehr als 30 Tupel im Result-Set ergibt.
Geben Sie danach nur die ersten drei Tupel aus.

:Siehe auch:  `Limit`_


Antwort?
========

.. code:: sql

      
   ???
   
Das gesuchte Ergebnis:

.. code:: sql

   gnum        beruf                                   
   ----------  ----------------------------------------
   ???        ????
   ???        ????
   ???        ????
     
.. _Limit: https://www.tutorialspoint.com/sqlite/sqlite_limit_clause.htm
