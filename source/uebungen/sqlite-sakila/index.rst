===========================
SQLite --  Sakila-Datenbank
===========================

.. meta::

   :description lang=de: Datenbank, Sakila zum üben mit SQLite3 
   :keywords: SQL, Sakila, SQLite3 


Die Sakila-Beispieldatenbank ist eine fiktive Datenbank, die einen
DVD-Verleih darstellt. Die Tabellen der Datenbank umfassen unter
anderem Film, film_category, actor, customer, rental, payment und
inventory.  Ausführliche Informationen über die Datenbank finden Sie
auf der MySQL-Website:

- https://dev.mysql.com/doc/sakila/en/


- Download: https://github.com/jOOQ/sakila
  

Datenbankstruktur
=================

.. image:: images/sqlite3-sakila-datenbank-erd.png

	   
