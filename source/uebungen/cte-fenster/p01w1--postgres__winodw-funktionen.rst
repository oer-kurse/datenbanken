==================
Windows-Funktionen
==================


.. _20231121T124634:

.. INDEX:: Fensterfunktionen; row_numger


Fensterfunktionen bieten die Möglichkeit, Berechnungen über Gruppen
von Zeilen durchzuführen, die mit der aktuellen Abfrage in Beziehung stehen.

Zeilen durchnumerieren
----------------------

Ausgangspunkt ist die Abfrage :ref:`hierachischer Strukturen <self-join>`.
Möchte man dem Resultset noch eine laufende Nummer zuordnen, kann die Abfrage mit
einer Windowfunktion erweitert werden.

.. sourcecode:: sql

   select
     ROW_NUMBER() OVER (ORDER BY t1.bezeichnung) AS "Lfd.-Nr.", 
     t2.bezeichnung as "Betriebssystem",
     t1.bezeichnung as "Kategorie"
   from
     kategorie t1 join kategorie t2 on t1.id = t2.parent
   order by
     "Kategorie";

.. INDEX:: Fensterfunktionen; sum

Aufsummieren von Werten
-----------------------

Möchten man  bei einem Zahlenwert den Zwischenstand mitverfolgen,
bietet sich die Summenfunktion an. Ausgangspunkt ist die :ref:`Personal-Datenbank <oraclestart>`.
Die folgende Abfrage liefert eine Liste der Gehälter.



.. code:: sql

    SELECT

      PERSNR, VNAME, NNAME, GEHALT

    FROM 
        PERSONAL;

Nun kann mit jeder Zeile eine Zwischensumme gebidet werden. Der Letzte
Eintrag zeigt dann den Gesamtbetrag aller Löhne und Gehälter an.

.. code:: sql

    SELECT

      PERSNR, VNAME, NNAME, GEHALT,
      SUM(GEHALT) OVER (ORDER BY PERSNR) AS ZWISCHENSUMME

    FROM 
        PERSONAL;

mit \`ORDER BY\` können Sie auch ein anderes Attribut für die Sortierung
wählen. Die Reihenfolge ändert sich, das Erbebnis bleibt unverändert.

Ranking
-------

.. INDEX:: Fensterfunktionen; rank

Die Gehaltunterschiede lassen sich auch nochmal als Ranking darstellen.

.. code:: sql


    SELECT

      PERSNR, VNAME, NNAME, GEHALT,
      RANK() OVER (ORDER BY gehalt) AS Ranking

    FROM

      PERSONAL;

Durchschnitt
------------

.. INDEX:: Fensterfunktionen; avg


Um das durchschnittliche Budget pro Abteilung zu berechnen, kann
die folgende Fenster-Funktion eingesetzt werden:

.. code:: sql


    SELECT
         distinct on (abtnr),
         avg(budget) OVER (PARTITION BY abtnr)
      FROM
         projekt;

Weiterführende Links
--------------------

- `Syntax in der Postgres-Dokumentation: 4.2.8. Window Function Calls <https://www.postgresql.org/docs/current/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS>`_

- `Liste der Windowsfunktionen <https://www.postgresql.org/docs/current/functions-window.html>`_

- `https://notso.boringsql.com/posts/window-functions-introduction/ <https://notso.boringsql.com/posts/window-functions-introduction/>`_
