=============================
Common Table Expression (CTE)
=============================


.. _20231121T141212:

.. INDEX:: CTE; Common Table Expression 
.. INDEX:: Common Table Expression; CTE

- CTE's vereinfachen komplexe Joins oder Unterabfragen.

- Sie werden mit dem Schlüsselwort \`WITH\` eingeleitet.

- Es entsteht eine temporäre Tabelle, die wiederum in die folgende Abfrage eingebunden werden kann.

- Die temporäre Tabelle existiert nur für die Dauer der Abfrage.

Eine Unterabfrage als CTE
-------------------------

Das folgende Beispiel verwendet die :ref:`Aufgabe 1.15 aus der Department-Übung <departmentcte>`.

Gehaltssumme gruppiert nach Namen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In dieser Version ist nicht erkennbar, aus welchen Zeilen sich die
Summe zusammensetzt.

.. code:: sql


    select name, sum(salary) from company
    group by name having count(name) >1;

Lösung I: Einzelpositionen für jede Zeile ausgeben?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    SELECT c1.name, c1.age, c1.address, c1.salary, total_salaries.total_salary
    FROM company c1
    JOIN (
        SELECT name, SUM(salary) AS total_salary
        FROM company
        GROUP BY name
        HAVING COUNT(name) > 1
    ) total_salaries
    ON c1.name = total_salaries.name
    ORDER BY name;

Lösung II: Mit CTE und CASE
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    WITH summed_salaries AS (
        SELECT name, age, address, salary,
               SUM(salary) OVER (PARTITION BY name) AS total_salary,
               ROW_NUMBER() OVER (PARTITION BY name ORDER BY id) AS row_num
        FROM company
    )
    SELECT name, age, address, salary,
           CASE WHEN row_num = 1 THEN total_salary ELSE NULL END AS total_salary
    FROM summed_salaries
    WHERE name IN (
        SELECT name
        FROM company
        GROUP BY name
        HAVING COUNT(name) > 1
    )
    ORDER BY name, row_num;
