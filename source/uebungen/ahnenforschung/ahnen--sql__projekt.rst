===================================
Ahnenfroschung -- als -- DB-Projekt
===================================

.. _46767adf-a5df-48ac-9a68-a41195526f66:
.. INDEX:: Ahnenfroschung; DB-Projekt
.. INDEX:: DB-Projekt; Ahnenfroschung

Irgendwann kommen die Fragen nach den Vorfahren, wer weiß noch was
über die Ahnen?  Die Geneanologie beschäftigt sich mit diesen
Fragen. Warum die ersten Erkenntisse nicht als Datenbankprojekt
implementieren?

