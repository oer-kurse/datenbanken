================
Personen + Infos
================


.. org::

.. _26fc491c-ecc8-4db7-b60a-4125224d4ffc:
.. INDEX:: Person; Infos
.. INDEX:: Infos; Person

**Tabellenstruktur**

.. code:: text

    ahnen  (id, name, mutter_id, vater_id, geburtstag, sterbetag, referenz)
    infos  (id  ahnen_id, status, datum, anmerkung)
    ehen   (id, ehepartner1_id, ehepartner2_id, hochzeitsdatum, ort, trenngsdatum)
    assets (id, ahnen_id, pfad, beschreibung)

Gib alle Personen und alle Infos aus.
-------------------------------------

.. raw:: html



    <div>

    SQL: <input type="checkbox" value="hint" />
    <div class="togglehint">Siehe auch: LEFT JOIN
    <br/> 

.. code:: sql

      SELECT	
        ahnen.*, infos.*
      FROM 
        ahnen
      LEFT JOIN
        infos
      ON  ahnen.id = infos.ahnen_id
      ;

     -- Alternativ mit Alias-Definitionen 

    SELECT	
        a.*, s.*
      FROM 
        ahnen a
      LEFT JOIN
        infos s
      ON  a.id = s.ahnen_id;

.. raw:: html

    </div>

    Als Film: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>


.. image:: ./images/ahnen-select__personen-und-infos.avif
   :width: 550px



.. raw:: html

    </div>
    </div>
