-- Person

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (1, 'Taust, Dorothea', 'Dieskau', '08/02/1651', NULL, NULL, '27/12/1730','Hä');

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (2, 'Händel, Georg', '', '24/09/1622', NULL, NULL, '11/02/1697','Hä');

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (3, 'Händel, Georg Friedrich', 'Halle', '23/02/1685', 1, 2, '14/04/1759','Hä.');

-- Person

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (4, 'Kathe, Anna', NULL, NULL, NULL, NULL, NULL,'Hä');

-- Eheschließung

INSERT INTO ehen (ehepartner1_id, ehepartner2_id, ort, heiratsdatum) 
VALUES (4, 2, NULL, '20/02/1643');

INSERT INTO ehen (ehepartner1_id, ehepartner2_id, ort, heiratsdatum) 
VALUES (1, 2, '', '24/04/1683');

--info




INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', '08/01/1705', 'Der in Kronen erlangte Glückswechsel, oder: Almira, Königin von Kastilien');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', '08/01/1705', 'Oper am Gänsemarkt, Hamburg');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', '25/02/1705', 'Die durch Blut und Mord erlangte Liebe, oder: Nero');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', '25/02/1705', 'Oper am Gänsemarkt, Hamburg');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', NULL, 'Januar 1708, Der beglückte Florindo');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', NULL, 'Januar 1708, Oper am Gänsemarkt, Hamburg');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', NULL, 'Februar 1708, Der beglückte Florindo');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', NULL, 'Februar 1708, Oper am Gänsemarkt, Hamburg');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', NULL, 'Februar 1708, Die verwandelte Daphne');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', NULL, 'Februar 1708, Oper am Gänsemarkt, Hamburg');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', NULL, 'Herbst 1707, Vincer se stesso è la maggior vittoria (“Rodrigo”) ');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', NULL, 'Herbst 1707, Teatro del Cocomero, Florenz');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', '26/12/1709', 'Agrippina');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', '26/12/1709', 'Teatro San Giovanni Grisostomo, Venedig');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', '24/02/1711', 'Rinaldo');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'Erstaufführung', '24/02/1711', 'Queen’s Theatre, Haymarket, London');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (3, 'gestorben in', NULL, 'London');

-- Kind

INSERT INTO ahnen (id, name, mutter_id, vater_id, geburtstag, geburtsort, sterbetag, referenz)
VALUES (5, 'Händel, Dorothea Elisabeth', NULL , 3, '13/02/1644', 'Halle', '21/11/1690', 'Hä.');

-- Kind

INSERT INTO ahnen (id, name, mutter_id, vater_id, geburtstag, geburtsort, sterbetag, referenz)
VALUES ( 6, 'Händel, Gottfried', 4 , 3, '14/04/1645', 'Halle', '07/04/1682', 'Hä.');


INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (6, 'gestorben in', NULL, 'Barby');

-- Kind

INSERT INTO ahnen (id, name, mutter_id, vater_id, geburtstag, geburtsort, sterbetag, referenz)
VALUES (7, 'Händel, Anna Barbara', 4, 3, '30/08/1646', 'Halle', '12/09/1680', 'Hä.');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (7, 'gestorben in', NULL, 'Weißenfels');


-- Person

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (8, 'Händel, Christoph', 'Halle', '28/01/1648', 4, 3, '13/10/1648','Hä.');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (8, 'gestorben in', NULL, 'Halle');


-- Person

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (9, 'Händel, Karl', 'Halle', '30/10/1649', 4, 3, NULL,'Hä.');


-- Person

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (10, 'Frankenberger, Justine Margarethe', 'Langensalza', '28/08/1672', NULL, NULL, NULL,'Hä.');

-- Eheschließung

INSERT INTO ehen (ehepartner1_id, ehepartner2_id, ort, heiratsdatum) 
VALUES (10, 9, 'Langensalza', '28/08/1672');


INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (10, 'gestorben am', NULL, 'nach 1679');

-- Kind

INSERT INTO ahnen (id, name, mutter_id, vater_id, geburtstag, geburtsort, sterbetag, referenz)
VALUES (11, 'Händel, Sophie Rosine', 4, 3, ' 11/04/1652', 'Halle', NULL, 'Hä.');


-- Person

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (12, 'Pferstorff, Philipp', NULL, NULL, NULL, NULL, NULL,'Hä.');

-- Eheschließung

INSERT INTO ehen (ehepartner1_id, ehepartner2_id, ort, heiratsdatum) 
VALUES (11, 12, NULL, '22/06/1668');


-- Person

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (13, 'Händel, Dorothea Sophie', 'Hallo', '08/10/1687', 1	, 3 , NULL,'Hä.');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (13, 'gestorben am', NULL, 'etwa 1720');


-- Person

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (14, 'Händel, Johanne Christiane', 'Halle', '12/01/1690', 1, 3, '16/07/1709','Hä.');


-- Person

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (15, 'Taust, Georg "Georgius"', 'Halle', '/11/06/1606', NULL, NULL, '08/04/1685','Hä.');



-- Person

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (16, 'Cuno, Dorothea', NULL, NULL, NULL, NULL, NULL,'Hä.');

-- Eheschließung

INSERT INTO ehen (ehepartner1_id, ehepartner2_id, ort, heiratsdatum) 
VALUES (16, 15, NULL, NULL);

update ahnen set mutter_id=16, vater_id=15 where id=1;


-- Person

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (17, 'Händel, Valentin', 'Breslau', NULL, NULL, NULL, '20/08/1636','Hä.');


INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (17, 'gestorben in', NULL, 'Halle');

INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (17, 'geboren', NULL, '1582');


-- Person

INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz)
VALUES (18, 'Beichling, Anna', 'Eisleben', NULL, NULL, NULL,'05/01/1670','Hä.');

-- Eheschließung

INSERT INTO ehen (ehepartner1_id, ehepartner2_id, ort, heiratsdatum) 
VALUES (18, 17, NULL, NULL);


INSERT INTO infos (ahnen_id, status, datum, anmerkung)
VALUES (17, 'Anmerkung', NULL, 'geheiratet 1608');

update ahnen set mutter_id=17, vater_id=18 where id=2;
