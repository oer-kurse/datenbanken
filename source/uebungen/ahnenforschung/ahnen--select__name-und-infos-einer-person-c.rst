=======================
Person -- und Infos III
=======================


.. org::

.. _9decbe16-38a7-4949-ae57-dded3a9c46ef:
.. INDEX:: Person; Infos III
.. INDEX:: Infos III; Person

Zeilen anordnen
---------------

Um die Zeilen untereinander anzuordnen, wird eine Escape-Sequenz für
den Zeilenumbruch »'\n'« verwendet. Zur Geltung kommt der
Zeilenumbruch erst, wenn Ein großes E vorangestellt wird »E'\n'«.


.. code:: sql

     WITH StatusAggregated AS (
        SELECT 
            a.name,
            array_agg(s.status || ': ' || s.anmerkung ORDER BY s.status) AS status_infos
        FROM 
            ahnen a
        LEFT JOIN 
            infos s
        ON 
            a.id = s.ahnen_id
        WHERE 
            a.id = 17
        GROUP BY 
            a.name
    )
    SELECT 
        name,
        array_to_string(status_infos, E'\n') AS status_info
    FROM 
        StatusAggregated;

Tabellenstruktur
~~~~~~~~~~~~~~~~

.. code:: text

    ahnen  (id, name, mutter_id, vater_id, geburtstag, sterbetag, referenz)
    infos  (id  ahnen_id, status, datum, anmerkung)
    ehen   (id, ehepartner1_id, ehepartner2_id, hochzeitsdatum, ort, trenngsdatum)
    assets (id, ahnen_id, pfad, beschreibung)
