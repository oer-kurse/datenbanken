======================
Person -- und Infos IV
======================


.. org::

.. _bfd72511-f8ba-439b-9920-d0074394668a:
.. INDEX:: Person; Infos IV
.. INDEX:: Infos IV; Person

Datum, wenn vorhanden
---------------------

Einige Ereignisse lassen sich einem konkreten Termin zurodnen, andere wieder nicht.

.. code:: sql

    WITH StatusAggregated AS (
        SELECT 
            a.name,
            array_agg(s.status || ': ' || s.datum || ' -- ' || s.anmerkung ORDER BY s.status) AS status_infos
        FROM 
            ahnen a
        LEFT JOIN 
            infos s
        ON 
            a.id = s.ahnen_id
        WHERE 
            a.id = 17
        GROUP BY 
            a.name
    )
    SELECT 
        name,
        array_to_string(status_infos, E'\n') AS status_info
    FROM 
        StatusAggregated;

Tabellenstruktur
~~~~~~~~~~~~~~~~

.. code:: text

    ahnen  (id, name, mutter_id, vater_id, geburtstag, sterbetag, referenz)
    infos  (id  ahnen_id, status, datum, anmerkung)
    ehen   (id, ehepartner1_id, ehepartner2_id, hochzeitsdatum, ort, trenngsdatum)
    assets (id, ahnen_id, pfad, beschreibung)
