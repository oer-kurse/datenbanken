==============================
Demo -- JSON -- Ahnenforschung
==============================


.. org::

.. _1720bee7-4a0c-4777-a7f8-eeaed8c5da57:
.. INDEX:: Ahnenforschung; Demo
.. INDEX:: Demo; Ahnenforschung

Genealogen oder Familienforscher befassen sich mit menschlichen
Verwandtschaftsbeziehungen und ihrer Darstellung. Irgendwann kommen
die Fragen nach den Vorfahren und jeder wird mehr oder weniger zum
Genealogen.

Warum die ersten Erkenntisse nicht als Datenbankprojekt
implementieren?

.. toctree::
   :maxdepth: 1
   :caption: 📘
   :glob:
   
   ahnen--sql__projekt
   ahnen--strukur__db
   ahnen--data__insert

.. toctree::
   :maxdepth: 1
   :caption: JOINS
   
   ahnen--select__personen
   ahnen--select__personen-und-infos
   ahnen--sql__ehen
   ahnen--sql__eltern-zum-kind

.. toctree::
   :maxdepth: 1
   :caption: CTE
   
   ahnen--select__name-und-infos-einer-person-a
   ahnen--select__name-und-infos-einer-person-b
   ahnen--select__name-und-infos-einer-person-c
   ahnen--select__name-und-infos-einer-person-d
   ahnen--select__name-und-infos-einer-person-e
