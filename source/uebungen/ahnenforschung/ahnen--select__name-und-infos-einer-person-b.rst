======================
Person -- und Infos II
======================

.. _07c8efcb-d916-44a7-a92b-57a6c9cbc6db:
.. INDEX:: Person; und Infos II
.. INDEX:: und Infos II; Person

CTE I (Array/Aggregat)
----------------------

Die Zusatzinfos werden im ersten Schritt in einem Array
zusammengefasst, auf die temporäre Tabelle »StatusAggregated«
wird abschließend zugegriffen und das Array in eine string
umgewandelt.

.. code:: sql


    WITH StatusAggregated AS (
        SELECT 
            a.name,
            array_agg(s.status || ': ' || s.anmerkung ORDER BY s.status) AS status_infos
        FROM 
            ahnen a
        LEFT JOIN 
            infos s
        ON 
            a.id = s.ahnen_id
        WHERE 
            a.id = 17
        GROUP BY 
            a.name
    )
    SELECT 
        name,
        array_to_string(status_infos, '; ') AS status_info
    FROM 
        StatusAggregated;

Tabellenstruktur
~~~~~~~~~~~~~~~~

.. code:: text

    ahnen  (id, name, mutter_id, vater_id, geburtstag, sterbetag, referenz)
    infos  (id  ahnen_id, status, datum, anmerkung)
    ehen   (id, ehepartner1_id, ehepartner2_id, hochzeitsdatum, ort, trenngsdatum)
    assets (id, ahnen_id, pfad, beschreibung)
