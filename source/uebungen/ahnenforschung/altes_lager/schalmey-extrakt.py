from PIL import Image
import pytesseract

# Pfad zur Tesseract-Installation (unter Windows, falls nicht im Systempfad)
# pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

def extract_text_from_image(image_path):
    # Bild öffnen
    img = Image.open(image_path)
    
    # OCR auf das Bild anwenden
    text = pytesseract.image_to_string(img, lang='deu')  # 'deu' für deutsche Texte
    
    return text

# Beispielbild
image_path = './images/schalmey-seite-0261-sy-10A.jpg'

# Texterkennung durchführen
extracted_text = extract_text_from_image(image_path)

print("Erkannter Text:")
print(extracted_text)
