import re

def transform_text(text):
    # Regex Patterns
    name_pattern = re.compile(r"^(\S+),\s*(\S+)")
    birth_pattern = re.compile(r"\*\s*(\w+)\s+(\d{1,2}\.\d{1,2}\.\d{4})")
    marriage_pattern = re.compile(r"⚭\s*(\w+)\s+(\d{1,2}\.\d{1,2}\.\d{4})")
    children_pattern = re.compile(r"(\d\.)\s*(\S+),\s*(\S+)\s*\*\s*(\w+)\s+(\d{1,2}\.\d{1,2}\.\d{4})")
    info_pattern = re.compile(r"\(([^)]+)\)")
    end_pattern = re.compile(r"\.")

    # Variables to hold SQL statements
    sql_statements = []

    # Split text into sentences
    sentences = end_pattern.split(text)

    # Counter for IDs
    id_counter = 1

    for sentence in sentences:
        if not sentence.strip():
            continue

        # Extract main person's name
        name_match = name_pattern.search(sentence)
        if name_match:
            last_name, first_name = name_match.groups()
            full_name = f"{last_name.capitalize()}, {first_name.capitalize()}"

            # Extract birth info
            birth_match = birth_pattern.search(sentence)
            if birth_match:
                birth_place, birth_date = birth_match.groups()
            else:
                birth_place, birth_date = None, None

            # Insert main person into ahnen table
            sql_statements.append(f"INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz) VALUES ({id_counter}, '{full_name}', '{birth_place}', '{birth_date}', NULL, NULL, NULL, 'Sy.5B');")
            main_person_id = id_counter
            id_counter += 1

            # Extract additional info
            info_matches = info_pattern.findall(sentence)
            for info in info_matches:
                sql_statements.append(f"INSERT INTO infos (ahnen_id, status, datum, anmerkung) VALUES ({main_person_id}, 'Anmerkung', NULL, '{info.strip()}');")

            # Extract marriage info
            marriage_match = marriage_pattern.search(sentence)
            if marriage_match:
                marriage_place, marriage_date = marriage_match.groups()
                # Extract spouse info
                spouse_match = name_pattern.search(sentence[marriage_match.end():])
                if spouse_match:
                    spouse_last_name, spouse_first_name = spouse_match.groups()
                    spouse_full_name = f"{spouse_last_name.capitalize()}, {spouse_first_name.capitalize()}"

                    # Extract spouse birth info
                    spouse_birth_match = birth_pattern.search(sentence[marriage_match.end():])
                    if spouse_birth_match:
                        spouse_birth_place, spouse_birth_date = spouse_birth_match.groups()
                    else:
                        spouse_birth_place, spouse_birth_date = None, None

                    # Insert spouse into ahnen table
                    sql_statements.append(f"INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz) VALUES ({id_counter}, '{spouse_full_name}', '{spouse_birth_place}', '{spouse_birth_date}', NULL, NULL, NULL, 'Sy.5B');")
                    spouse_person_id = id_counter
                    id_counter += 1

                    # Insert marriage info
                    sql_statements.append(f"INSERT INTO ehen (ehepartner1_id, ehepartner2_id, ort, heiratsdatum) VALUES ({main_person_id}, {spouse_person_id}, '{marriage_place}', '{marriage_date}');")

            # Extract children info
            children_matches = children_pattern.findall(sentence)
            for child_match in children_matches:
                _, child_last_name, child_first_name, child_birth_place, child_birth_date = child_match
                child_full_name = f"{child_last_name.capitalize()}, {child_first_name.capitalize()}"

                # Insert child into ahnen table
                sql_statements.append(f"INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz) VALUES ({id_counter}, '{child_full_name}', '{child_birth_place}', '{child_birth_date}', NULL, NULL, NULL, 'Sy.5B');")
                child_person_id = id_counter
                id_counter += 1

    return sql_statements

# Beispieltext
text = """
S C H N E I D E R, HERMANN, * Berlin 11.6.1897. Lehrer (in Sy. seit 6.4.1932), ⚭ Berlin Neukölln 17.11.1925
Gertrud Pommerening, * Neidenburg 27.09.1899, kinderlos, 1 Berlin-Neukölln, Innstr. 41.
"""

# Transformation
sql_statements = transform_text(text)

# Ausgabe der SQL-Statements
for stmt in sql_statements:
    print(stmt)
