====================
Personen -- ausgeben
====================


.. org::

.. _cdda5396-419d-40b1-84b9-058d237b47fd:
.. INDEX:: Personen; ausgeben
.. INDEX:: ausgeben; Personen

Alle Personen ausgeben
----------------------

**Tabellenstruktur**

.. code:: text

    ahnen  (id, name, mutter_id, vater_id, geburtstag, sterbetag, referenz)
    infos  (id  ahnen_id, status, datum, anmerkung)
    ehen   (id, ehepartner1_id, ehepartner2_id, hochzeitsdatum, ort, trenngsdatum)
    assets (id, ahnen_id, pfad, beschreibung)

Gib alle Daten zu allen Personen aus.

.. raw:: html



    <div>

    SQL: <input type="checkbox" value="hint" />
    <div class="togglehint">Siehe auch: DISTINCT
    <br/> 

.. code:: sql

    select
      *
    from
      ahnen;


.. raw:: html

    </div>

    Als Film: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>


.. image:: ./images/ahnen--select__all.avif
   :width: 550px



.. raw:: html

    </div>
    </div>
