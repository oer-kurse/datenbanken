import re

def transform_text(text):
    # Regex Patterns
    name_pattern = re.compile(r"(\S+),\s*(\S+)")
    birth_pattern = re.compile(r"\*\s*(\w+)\s+(\d{1,2})\.(\d{1,2})\.(\d{4})")
    marriage_pattern = re.compile(r"⚭\s*(\w+)\s+(\d{1,2})\.(\d{1,2})\.(\d{4})")
    children_pattern = re.compile(r"(\d\.)\s*(\S+),\s*(\S+)\s*\*\s*(\w+)\s+(\d{1,2})\.(\d{1,2})\.(\d{4})")
    info_pattern = re.compile(r"\(([^)]+)\)")
    end_pattern = re.compile(r"\.")

    # Variables to hold SQL statements
    sql_statements = []

    # Split text into sentences
    sentences = end_pattern.split(text)

    # Counter for IDs
    id_counter = 1

    for sentence in sentences:
        if not sentence.strip():
            continue

        # Extract main person's name
        name_match = name_pattern.search(sentence)
        if name_match:
            last_name, first_name = name_match.groups()
            full_name = f"{last_name.capitalize()}, {first_name.capitalize()}"

            # Extract birth info
            birth_match = birth_pattern.search(sentence)
            if birth_match:
                birth_place = birth_match.group(1)
                birth_date = f"{birth_match.group(2)}/{birth_match.group(3)}/{birth_match.group(4)}"
            else:
                birth_place, birth_date = None, None

            # Insert main person into ahnen table
            sql_statements.append(f"INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz) VALUES ({id_counter}, '{full_name}', '{birth_place}', '{birth_date}', NULL, NULL, NULL, 'Sy.5B');")
            main_person_id = id_counter
            id_counter += 1

            # Extract additional info
            info_matches = info_pattern.findall(sentence)
            for info in info_matches:
                sql_statements.append(f"INSERT INTO infos (ahnen_id, status, datum, anmerkung) VALUES ({main_person_id}, 'Anmerkung', NULL, '{info.strip()}');")

            # Extract marriage info
            marriage_match = marriage_pattern.search(sentence)
            if marriage_match:
                marriage_place = marriage_match.group(1)
                marriage_date = f"{marriage_match.group(2)}/{marriage_match.group(3)}/{marriage_match.group(4)}"
                
                # Extract spouse info
                spouse_part = sentence[marriage_match.end():].strip()
                spouse_name_match = name_pattern.search(spouse_part)
                if spouse_name_match:
                    spouse_last_name, spouse_first_name = spouse_name_match.groups()
                    spouse_full_name = f"{spouse_last_name.capitalize()}, {spouse_first_name.capitalize()}"

                    # Extract spouse birth info
                    spouse_birth_match = birth_pattern.search(spouse_part)
                    if spouse_birth_match:
                        spouse_birth_place = spouse_birth_match.group(1)
                        spouse_birth_date = f"{spouse_birth_match.group(2)}/{spouse_birth_match.group(3)}/{spouse_birth_match.group(4)}"
                    else:
                        spouse_birth_place, spouse_birth_date = None, None

                    # Insert spouse into ahnen table
                    sql_statements.append(f"INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz) VALUES ({id_counter}, '{spouse_full_name}', '{spouse_birth_place}', '{spouse_birth_date}', NULL, NULL, NULL, 'Sy.5B');")
                    spouse_person_id = id_counter
                    id_counter += 1

                    # Insert marriage info
                    sql_statements.append(f"INSERT INTO ehen (ehepartner1_id, ehepartner2_id, ort, heiratsdatum) VALUES ({main_person_id}, {spouse_person_id}, '{marriage_place}', '{marriage_date}');")

            # Extract children info
            children_matches = children_pattern.findall(sentence)
            for child_match in children_matches:
                _, child_last_name, child_first_name, child_birth_place, child_birth_day, child_birth_month, child_birth_year = child_match
                child_full_name = f"{child_last_name.capitalize()}, {child_first_name.capitalize()}"
                child_birth_date = f"{child_birth_day}/{child_birth_month}/{child_birth_year}"

                # Insert child into ahnen table
                sql_statements.append(f"INSERT INTO ahnen (id, name, geburtsort, geburtstag, mutter_id, vater_id, sterbetag, referenz) VALUES ({id_counter}, '{child_full_name}', '{child_birth_place}', '{child_birth_date}', NULL, NULL, NULL, 'Sy.5B');")
                child_person_id = id_counter
                id_counter += 1

    return sql_statements

# Beispieltext
text = """
S C H N E I D E R, HERMANN, * Berlin 11.6.1897. Lehrer (in Sy. seit 6.4.1932), ⚭ Berlin Neukölln 17.11.1925
Gertrud Pommerening, * Neidenburg 27.09.1899, kinderlos, 1 Berlin-Neukölln, Innstr. 41.
"""


text1= """
Klein‚ Bernhard * L1ebenau 6.10.1907 (s. Sy.10B.3) Straßenwärter
⚭ Sy. 4.10.1935 Luzia Stern, * Gr. 20.2.1915 (s. Gr.11.4) Werneuchen
Kr.Bernau, Hagebuttenweg 51. Ki. zu Sy. geboren: 1, Kurt, * 26.6.1939
 ebd, 27.4,1942. - 2. Annemarie, * 3.12.1941, - 3. Helga‚ * 19.2.1944
"""

Der obige Text soll mit Python in eine Liste konvertiert werden wobei vor und hinter jedem Datum ein Trennung erfolgen soll.

for text1.split 


text = """
10A   Klein, Bernhard,
*
Liebenau 6.10.1907
(s.Sy.10B.3), Straßenwärter
⚭
Sy. 4.10.1933 Luzia Stern,
*
Gr. 20.2.1915 (s.Gr.11.4), Werneuchen
Kr.Bernau, Hagebuttenweg 51. Ki. zu Sy. geboren:
1. Kurt,
*
26.6.193
+ ebd. 27.4.1942,
* 2. Annemarie, 3.12.1941.
3. Helga, * 19.2.1944
"""

# Transformation
sql_statements = transform_text(text)

# Ausgabe der SQL-Statements
for stmt in sql_statements:
    print(stmt)
