import re

def split_text_by_dates_and_keywords(text):
    # Muster für Datumsangaben im Format Tag.Monat.Jahr
    date_pattern = re.compile(r'(\d{1,2}\.\d{1,2}\.\d{4})')
    
    # Zuerst den Text anhand von "Ki." und "geboren:" aufteilen
    keyword_pattern = re.compile(r'(Ki\..*?geboren:)')
    segments = re.split(keyword_pattern, text)
    
    # Liste für das Endergebnis
    result = []
    
    for segment in segments:
        # Wenn der aktuelle Segment ein "Ki. ... geboren:"-Block ist, fügen wir ihn direkt zur Ergebnisliste hinzu
        if segment.startswith("Ki.") and segment.endswith("geboren:"):
            result.append(segment.strip())
        else:
            # Ansonsten weiter nach Datum aufteilen
            split_text = re.split(date_pattern, segment)
            split_text = [s.strip() for s in split_text if s.strip()]
            result.extend(split_text)
    
    return result

text1 = """
Klein‚ Bernhard * L1ebenau 6.10.1907 (s. Sy.10B.3) Straßenwärter
# Sy. 4.10.1935 Luzia Stern, * Gr. 20.2.1915 (s. Gr.11.4) Werneuchen
Kr.Bernau, Hagebuttenweg 51. Ki. zu Sy. geboren: 1, Kurt, * 26.6.1939
+ ebd, 27.4,1942. - 2. Annemarie, * 3.12.1941, - 3. Helga‚ * 19.2.1944
"""

split_text = split_text_by_dates_and_keywords(text1)
for segment in split_text:
    print(segment)

print("\nResulting List:\n", split_text)

