===============
Beispieldaten I
===============


.. org::

.. _647ab740-4a15-4c13-9250-f1b4cf6bedb2:
.. INDEX:: Beispieldaten; 
.. INDEX:: ; Beispieldaten

Der Übungsdatensatz enthält einige Daten zur Musikerfamilie Händel.


:download:`Download: Daten für die Ahnenforschung <files/haendel-ahnen.sql>`.

Quelle: `WikiTree: Georg Friedrich Händel (1685 - 1759) <https://www.wikitree.com/wiki/H%C3%A4ndel-100>`_
