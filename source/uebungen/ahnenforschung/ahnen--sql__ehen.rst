=================
Ehegemeinschaften
=================


.. org::

.. _351e1016-4f6b-4543-bb1f-a75694d4d0f7:
.. INDEX:: Ehegemeinschaften; 
.. INDEX:: ; Ehegemeinschaften

Ehen (Self-Join)
----------------

.. code:: sql

    SELECT 
        e.id AS ehe_id,
        e.heiratsdatum,
        e.trennungsdatum,
        partner1.name AS ehepartner1_name,
        partner2.name AS ehepartner2_name
    FROM 
        ehen e
    JOIN 
        ahnen partner1 ON e.ehepartner1_id = partner1.id
    JOIN 
        ahnen partner2 ON e.ehepartner2_id = partner2.id;

Tabellenstruktur
~~~~~~~~~~~~~~~~

.. code:: text

    ahnen  (id, name, mutter_id, vater_id, geburtstag, sterbetag, referenz)
    infos  (id  ahnen_id, status, datum, anmerkung)
    ehen   (id, ehepartner1_id, ehepartner2_id, hochzeitsdatum, ort, trenngsdatum)
    assets (id, ahnen_id, pfad, beschreibung)
