=====================
Person -- und Infos I
=====================


.. org::

.. _cec35147-338a-4af8-8fe9-c4bfc6d39ea5:
.. INDEX:: Person;  und Infos II
.. INDEX::  und Infos II; Person

Formatierte Ausgabe
-------------------

Zu einer Person können unterschiedlichste Informationen zur Verfügung stehen.
Was eventuell stört, ist die wiederholte Ausgabe des Namens.

**Problem (nicht wirklich):**

Die folgende Abfrage erzeugt 4 Zeilen.
Wie kann der Name auf eine einmalige Ausgabe reduziert werden?
Vielleicht mit einer CTE?

.. code:: sql


    SELECT      
        a.name,
        s.status,
        s.datum,
        s.anmerkung
      FROM 
        ahnen a
      LEFT JOIN 
        infos s
      ON 
        a.id = s.ahnen_id
      WHERE 
        a.id = 43;

Tabellenstruktur
~~~~~~~~~~~~~~~~

.. code:: text

    ahnen  (id, name, mutter_id, vater_id, geburtstag, sterbetag, referenz)
    infos  (id  ahnen_id, status, datum, anmerkung)
    ehen   (id, ehepartner1_id, ehepartner2_id, hochzeitsdatum, ort, trenngsdatum)
    assets (id, ahnen_id, pfad, beschreibung)
