================
Tabellenstruktur
================


.. org::

.. _46767adf-a5df-48ac-9a68-a41195526f66:
.. INDEX:: Ahnenfroschung; DB-Projekt
.. INDEX:: DB-Projekt; Ahnenfroschung



.. table::

    +------------+----------------------------------------------------------+
    | **ahnen**  | Enthält Informationen über die Personen.                 |
    +------------+----------------------------------------------------------+
    | **ehen**   | Speichert Eheinformationen.                              |
    +------------+----------------------------------------------------------+
    | **status** | Speichert verschiedene Statusinformationen.              |
    +------------+----------------------------------------------------------+
    | **assets** | Speichert Pfade zu Medien (Fotos, PDF ...( der Personen. |
    +------------+----------------------------------------------------------+
    | \          | \                                                        |
    +------------+----------------------------------------------------------+

.. code:: sql


    CREATE TABLE ahnen (
        id SERIAL PRIMARY KEY,
        name TEXT NOT NULL,
        mutter_id INTEGER REFERENCES ahnen(id),
        vater_id INTEGER REFERENCES ahnen(id),
        geburtstag DATE,
        geburtsort TEXT,
        sterbetag DATE,
        referenz varchar(10)
    );

    CREATE TABLE ehen (
        id SERIAL PRIMARY KEY,
        ehepartner1_id INTEGER REFERENCES ahnen(id),
        ehepartner2_id INTEGER REFERENCES ahnen(id),
        heiratsdatum DATE,
        ort TEXT,
        trennungsdatum DATE
    );

    CREATE TABLE infos (
        id SERIAL PRIMARY KEY,
        ahnen_id INTEGER REFERENCES ahnen(id),
        status TEXT NOT NULL,
        datum DATE,
        anmerkung TEXT
    );

    CREATE TABLE assets (
        id SERIAL PRIMARY KEY,
        ahnen_id INTEGER REFERENCES ahnen(id),
        pfad TEXT NOT NULL,
        beschreibung TEXT
    );

Tabellenstruktur
----------------

.. code:: text

    ahnen  (id, name, mutter_id, vater_id, geburtstag, sterbetag, referenz)
    infos  (id  ahnen_id, status, datum, anmerkung)
    ehen   (id, ehepartner1_id, ehepartner2_id, hochzeitsdatum, ort, trenngsdatum)
    assets (id, ahnen_id, pfad, beschreibung)
