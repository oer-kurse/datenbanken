======================
Person  -- und Infos V
======================


.. org::

.. _e6b38f15-2477-45e7-9d43-a0474c506fdf:
.. INDEX:: Person ; und Infos V
.. INDEX:: und Infos V; Person 

Fallunterscheidung
------------------

Um sowohl Zeilen mit Dataum, als auch Zeilen ohne Datum in die Ausgabe
zu bringen, wird mit einer Fallunterscheidung gearbeitet (CASE/WHEN/ELSE).


.. code:: sql

    WITH StatusAggregated AS (
        SELECT 
            a.name,
            array_agg(CASE 
                        WHEN s.datum IS NOT NULL THEN s.status || ':'  || s.anmerkung || ' (' || s.datum || ')' 
                        ELSE s.status || ': ' || s.anmerkung
                      END ORDER BY s.status) AS status_infos
        FROM 
            ahnen a
        LEFT JOIN 
            infos s
        ON 
            a.id = s.ahnen_id
        WHERE 
            a.id = 17
        GROUP BY 
            a.name
    )
    SELECT 
        name,
        array_to_string(status_infos, E'\n') AS status_info
    FROM 
        StatusAggregated;

Tabellenstruktur
~~~~~~~~~~~~~~~~

.. code:: text

    ahnen  (id, name, mutter_id, vater_id, geburtstag, sterbetag, referenz)
    infos  (id  ahnen_id, status, datum, anmerkung)
    ehen   (id, ehepartner1_id, ehepartner2_id, hochzeitsdatum, ort, trenngsdatum)
    assets (id, ahnen_id, pfad, beschreibung)
