=============================
Eltern -- Kind -- Beziehungen
=============================


.. org::

.. _75e7b5e0-a541-4993-b403-2dfe379ec094:
.. INDEX:: Eltern; Beziehungen
.. INDEX:: Beziehungen; Eltern

Mutter und Vater zum Kind ermitteln
-----------------------------------

.. code:: sql


    SELECT 
      kind.id AS kind_id,
      kind.name AS kind_name,
      mutter.name AS mutter_name,
      vater.name AS vater_name
    FROM 
      ahnen kind
    LEFT JOIN 
      ahnen mutter ON kind.mutter_id = mutter.id
    LEFT JOIN 
      ahnen vater ON kind.vater_id = vater.id
    WHERE 
      kind.name = 'Johann Schmidt'; -- Name (Wert) anpassen 

Tabellenstruktur
~~~~~~~~~~~~~~~~

.. code:: text

    ahnen  (id, name, mutter_id, vater_id, geburtstag, sterbetag, referenz)
    infos  (id  ahnen_id, status, datum, anmerkung)
    ehen   (id, ehepartner1_id, ehepartner2_id, hochzeitsdatum, ort, trenngsdatum)
    assets (id, ahnen_id, pfad, beschreibung)
