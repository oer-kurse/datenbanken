=======================
Gemeinden -- AGS -- BEM
=======================


.. org::

.. _82e7965a-5d9a-423d-91a7-5f8e153014e9:
.. INDEX:: Gemeinden; BEM
.. INDEX:: BEM; Gemeinden

Fragen
------

1. Gib die AGS und die Bezeichnung für alle bzw. die ersten 20 Gemeinden aus.

2. Gib den Namen der Gemeinde/Stadt aus und zusätzlich den Status/die Kategorie.

3. Wieviele Städte gibt es?

Antworten zu 1 und 2..
----------------------

Antwort I: Arrow-Notation
~~~~~~~~~~~~~~~~~~~~~~~~~

- Inklusive JSON-Verpackung

- beachte die einfache Pfeilspitze

.. code:: sql

    SELECT            
             ags,
             data->'properties' -> 'GEN' as Name,
             data->'properties' -> 'BEZ' as Status
         from 
            gemeinden;

Antwort II: Arrow-Notation
~~~~~~~~~~~~~~~~~~~~~~~~~~

- Ohne "JSON-Verpackung"

- beachte die doppelte Pfeilspitze für den Zugriff auf den Wert

.. code:: sql

    SELECT            
       ags,
       data->'properties' ->> 'GEN' as Name,
       data->'properties' ->> 'BEZ' as Status
    FROM 
      gemeinden;

Antwort III: Hash-Notation
~~~~~~~~~~~~~~~~~~~~~~~~~~

- Pfadnotation mit der Hashmarke (#)

- wieder eine Pfeilspitze um ein JSON-Objekt zu erhalten

- durch Kommata getrenn der Pfad von der Wurzel (**data**),über den Key
  (**properties**) bis zum Ziel-Schlüssel (**GEN** bzw. **BEZ**)

.. code:: sql


    SELECT            
      ags,
      data#>'{properties, GEN}' as Name,
      data#>'{properties, BEZ}' as Status
    FROM 
       gemeinden;

Antwort IV: Hash-Notation
~~~~~~~~~~~~~~~~~~~~~~~~~

- wie Antwort II

- nur die Werte werden zurückgeliefert

.. code:: sql


     SELECT                
        ags,
        data#>>'{properties, GEN}' as Name,
        data#>>'{properties, BEZ}' as Status
    FROM 
        gemeinden;

Antwort V: Funktion
~~~~~~~~~~~~~~~~~~~

.. code:: sql


    SELECT 
       ags,
       jsonb_extract_path(data,'properties', 'GEN') as Name,
       jsonb_extract_path(data,'properties', 'BEZ') as Status
    FROM 
       gemeinden;

Antwort zu 3.
-------------

.. code:: sql


    SELECT                
          count(*)                           
      from                                       
         gemeinden
    where data#>>'{properties, BEZ}' = 'Stadt';
     count 
    -------
        84
    (1 Zeile)
