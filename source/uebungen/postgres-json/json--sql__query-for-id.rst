====================
Quiz -- Frage zur ID
====================


.. org::

.. _5aaa1e51-ca3e-4e57-a95a-821dd7417577:
.. INDEX:: Quiz; Frage zur ID
.. INDEX:: Frage zur ID; Quiz

Wie lautet die Frage für die ID 112?
------------------------------------

**SQL:**

.. code:: text

    ???          

**Ausgabe:**

.. code:: text


              Frage           
    --------------------------
     "Was ist ein Keylogger?"
    (1 Zeile)


.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende den »->«-Operator «
    <br/>

`Postgres-Doku: »json« <https://www.postgresql.org/docs/current/functions-json.html>`_


.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/json/json--fragen__id-112.cast"
      theme="solarized-light"
      font-size="small"
      rows="25"
      poster="data:text/plain,\e[5;5H Frage & Antwort \e[1;33m">
    </asciinema-player>

    </div>
    </div>
