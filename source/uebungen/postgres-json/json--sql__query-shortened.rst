=========================
Quiz -- Fragetext gekürzt
=========================


.. org::

.. _a039b92c-2f2e-4be6-a77e-6a9b2903d0d2:
.. INDEX:: Quiz; Fragetext gekürzt
.. INDEX:: Fragetext gekürzt; Quiz

Gib nur die ersten 20 Zeichen der Frage aus.
--------------------------------------------

**SQL:**

.. code:: text

    ???          

**Ausgabe:**

.. code:: text


           Frage         
    ----------------------
     Wenn man auch nicht 
     Benutzen Sie private
     Logins und Fernzugri
     Wie viele unterschie
     Wie oft führen Sie U
     Prüfen und ändern Si
     Glauben Sie, dass di
     Dürfen Sie private E
     Sie arbeiten im Home
     Sie sind auf Geschäf
     Welchen dieser E-Mai
     Kriminelle verschlüs
     Was ist ein Keylogge
     Ein bei Sicherheitsd
     Wie kann man einen C
     Auf fast allen Syste
    (16 Zeilen)

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende den »->«-Operator «
    <br/>

`Postgres-Doku: »json« <https://www.postgresql.org/docs/current/functions-json.html>`_
`Diskussion auf StackOverflow <https://stackoverflow.com/questions/40944760/how-to-get-substring-in-sql-query>`_


.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/json/json--fragen__erste-bis-20-zeichen.cast"
      theme="solarized-light"
      font-size="small"
      rows="40"
      poster="data:text/plain,\e[5;5H Frage & Antwort \e[1;33m">
    </asciinema-player>

    </div>
    </div>
