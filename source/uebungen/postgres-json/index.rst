============
Demo -- JSON
============


.. org::

.. _da430aa9-e2e7-4845-b057-5c6600e81833:
.. INDEX:: Postgres; JSON
.. INDEX:: JSON; Postgres

:ref:`« Übersicht Übungen <uebungen>`

.. image:: ./images/hagebuttenwein.avif
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/hagebuttenwein.avif')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/hagebuttenwein.avif">
   </a>


.. sidebar:: Serie: Nahrung

   |b|

   Hagebuttenwein

|a|


| :download:`Download: Daten für die Gemeinden <files/gemeinden-brb.sql>`.
| :download:`Download: Daten für den Quiz <files/quiz.sql>`.
| :download:`Download: Daten für die Demo Relation »karte« <files/import-orte.sql>`.

.. toctree::
   :maxdepth: 1

   json--sql__operatoren
   json

Fragen zu den Gemeinden
=======================

.. toctree::
   :maxdepth: 1

   json--sql__gemeinde-datensatz
   json--sql__gemeinde-ags-und-bem
   json--sql__gemeinde-pretty-print
   json--sql__gemeinde-update

Beispiel Karte
==============
.. toctree::
   :maxdepth: 1
	      
   json--sql__karte-aggregat
   json--sql__karte-row

   
Fragen zum Quiz
===============

.. toctree::
   :maxdepth: 1
      
   json--sql__all_queries
   json--sql__query-for-id
   json--sql__query-shortened
   json--sql__query-short-points
   json--sql__all-answers
   json--sql__first-answer
