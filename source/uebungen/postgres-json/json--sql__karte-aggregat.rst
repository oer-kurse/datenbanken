=================
Karte -- Aggregat
=================


.. _20230919T094638:
.. INDEX:: json; Aggregat
.. INDEX:: Aggregat; json

.. code:: sql


    SELECT json_agg(x)
    FROM karte AS X WHERE id < 10;

.. code:: text


                                                       json_agg                                                    
    ---------------------------------------------------------------------------------------------------------------
     [{"id":1,"plz":14959,"ort":"Trebbin OT Kleinbeuthen","langitude":13.193184,"latitude":52.258174},            +
      {"id":2,"plz":14959,"ort":"Trebbin OT Christinendorf","langitude":13.281649,"latitude":52.211374},          +
      {"id":3,"plz":15517,"ort":"Fürstenwalde/Spree","langitude":14.020111,"latitude":52.354694},                 +
      {"id":4,"plz":14624,"ort":"Dallgow-Döberitz","langitude":13.135222,"latitude":52.513167},                   +
      {"id":5,"plz":1945,"ort":"Ruhland Gemeindeteil Arnsdorf","langitude":13.866661,"latitude":51.430356},       +
      {"id":6,"plz":1945,"ort":"Ruhland Gemeindeteil Arnsdorf","langitude":13.866628,"latitude":51.430383},       +
      {"id":7,"plz":1968,"ort":"Senftenberg OT Brieske","langitude":13.967871,"latitude":51.496972},              +
      {"id":8,"plz":13127,"ort":"Berlin Pankow, Französisch Buchholz","langitude":13.43373,"latitude":52.604356}, +
      {"id":9,"plz":3222,"ort":"Lübbenau/Spreewald","langitude":13.941668,"latitude":51.86813}]
    (1 Zeile)
