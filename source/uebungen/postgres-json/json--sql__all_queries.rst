===================
Quiz -- Alle Fragen
===================


.. org::

.. _f5fcd90b-24e8-46a2-afe7-61a1efc9161a:
.. INDEX:: Quiz; Alle Fragen
.. INDEX:: Alle Fragen; Quiz

Gib eine Liste aller Fragen aus.
--------------------------------

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende den »->«-Operator «
    <br/>

Beispiel:
::

   '{"a": {"b":"foo"}}'::json -> 'a' → {"b":"foo"}

Siehe auch: `Postgres-Doku: »json« <https://www.postgresql.org/docs/current/functions-json.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/json/json--fragen__als-liste.cast"
      theme="solarized-light"
      font-size="small"
      rows="40"
      poster="data:text/plain,\e[5;5H Frage & Antwort \e[1;33m">
    </asciinema-player>

    </div>
    </div>
