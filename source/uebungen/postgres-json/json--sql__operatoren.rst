===========================
JSON --  Zugriffsoperatoren
===========================


.. org::

.. _c12a88d6-970e-405d-b901-c422d4ccd4b3:
.. INDEX:: JSON;  Zugriffsoperatoren
.. INDEX::  Zugriffsoperatoren; JSON

Die Zugriffsoperatoren
----------------------

Für den Zugriff auf einzelne Werte existiert eine spezielle Syntax. 

.. table::

    +-----+--------------------------------------------+
    | ->  | JSON Objekt                                |
    +-----+--------------------------------------------+
    | ->> | JSON Objekt als Text                       |
    +-----+--------------------------------------------+
    | #>  | JSON Objekt mit specifischem Pfad          |
    +-----+--------------------------------------------+
    | #>> | JSON Objekt mit spezifischen Pfad als Text |
    +-----+--------------------------------------------+
