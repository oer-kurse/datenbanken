=========================
Gemeinde -- Datenstruktur
=========================


.. org::

.. _5fac5c94-fca8-4d33-af76-ddedf04d1e7c:
.. INDEX:: Gemeinde; Datenstruktur
.. INDEX:: Datenstruktur; Gemeinde

Ein Datensatz
-------------

Zum Datensatz gehören diverse Metadaten und die Koordinaten für die
grenzen der jeweilingen Gemarkung (Gemeinde, Stadt). Interssant für
das Beispiel sind:

.. table::

    +-------------------------------------+
    | AGS = Allgemeiner Gemeindeschlüssel |
    +-------------------------------------+
    | GEN = Name der Gemeinde/Stadt       |
    +-------------------------------------+
    | BEZ = Kategorie                     |
    +-------------------------------------+

.. code:: sql

    ags  | 12051000
    gen  | Brandenburg an der Havel
    data | {"type": "Feature", "geometry": {"type": "MultiPolygon", "coordinates": [[[[12.715248966197441, 52.54484226978378, 0], viele weitere Datenpunkte]]]]}, "properties": {"GF": 9, "ADE": 6, "AGS": "12051000", "ARS": "120510000000", "BEM": "kreisfrei", "BEZ": "Stadt", "BSG": 1, "GEN
    ": "Brandenburg an der Havel", "IBZ": 60, "NBD": "ja", "WSK": "2012-10-01", "NUTS": "DE401", "SN_G": "000", "SN_K": "51", "SN_L": "12", "SN_R": "0", "AGS_0": "12051000", "ARS_0": "120510000000", "FK_S3": "R", "O
    BJID": "DEBKGVG500000001", "SN_V1": "00", "SN_V2": "00", "BEGINN": "2019-10-04", "SDV_ARS": "120510000000"}}

Das Attribut data nochmal formatiert, in der Datenbank sind die
Angaben nahtlos aneinander gereiht.

.. code:: json


    {
      "type": "Feature",
      "geometry": {
        "type": "MultiPolygon",
        "coordinates": [
          [
            [
              [12.715248966197441, 52.54484226978378, 0], "viele weitere Datenpunkte"
            ]
          ]
        ]]
    }, "properties": {
      "GF": 9,
      "ADE": 6,
      "AGS": "12051000",
      "ARS": "120510000000",
      "BEM": "kreisfrei",
      "BEZ": "Stadt",
      "BSG": 1,
      "GEN": "Brandenburg an der Havel ",
      "IBZ ": 60,
      "NBD": "ja",
      "WSK ": "2012-10-01 ",
      "NUTS ": "DE401",
      "SN_G ": "000",
      "SN_K ": "51",
      "SN_L ": "12",
      "SN_R ": "0",
      "AGS_0 ": "12051000",
      "ARS_0 ": "120510000000 ",
      "FK_S3 ": "R",
      "OBJID ": "DEBKGVG500000001 ",
      "SN_V1 ": "00",
      "SN_V2 ": "00",
      "BEGINN ": "2019-10-04 ",
      "SDV_ARS ": "120510000000"}}
