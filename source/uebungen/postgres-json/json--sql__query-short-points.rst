=================
Quiz -- Frage ...
=================


.. org::

.. _281daf36-5e91-4972-90b1-da6e6eb13d6d:
.. INDEX:: Quiz; Frage ...
.. INDEX:: Frage ...; Quiz

Aufgabe
-------

Gib für die ID 110 die ersten 20 Zeichen aus,
gefolgt von drei Punkten (...)

**SQL:**

.. code:: text

    ???          

**Ausgabe:**

.. code:: text


              Frage           
    --------------------------
     Wenn man auch nicht  ...
    (1 Zeile)

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Concatenation
    <br/>

`Diskussion auf Stack Overflow <https://stackoverflow.com/questions/40944760/how-to-get-substring-in-sql-query>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/json/json--fragen__id-100-20-zeichen-und-punkte.cast"
      theme="solarized-light"
      font-size="small"
      rows="40"
      poster="data:text/plain,\e[5;5H Frage & Antwort \e[1;33m">
    </asciinema-player>

    </div>
    </div>
