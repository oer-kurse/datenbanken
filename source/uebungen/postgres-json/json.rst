=======================
JSON -- erste Beispiele
=======================


.. INDEX:: Datentyp; JSON
.. INDEX:: JSON; Datentyp


.. meta::

   :description lang=de: Postgres: Datenbyp json.

   :keywords: PostgreSQL, json

Testdatensatz anlegen
---------------------

.. code:: sql


    create table jsonstart ("query" json);

    INSERT INTO jsonstart (query)
    VALUES ('{
      "id": "43",
      "cat": "6",
      "qry_type": "rb",
      "qry": {
         "question": "Benutzen Sie private Messenger für organisatorischer Absprachen mit Kollege und Kunden?",
         "answers": ["ja ", "nein "],
         "feedback": "Messenger speichern die Daten auf Servern, deren Standort wie auch die Zugriffsrechte unbekannt sind.Identisch mit Frage 42 (pk)"
      }
    }');

Struktur eines Datensatzes
--------------------------

JSON-Stuktur im Attribut »query« (Zeilen wegen Überlänge gekürzt).


.. code:: python


    select * from jsonstart;

                              query                                                                         
    ---------------------------------------------------------------------
     {                                                                  +
        "id": "43",                                                     +
        "cat": "6",                                                     +
        "qry_type": "rb",                                               +
        "qry": {                                                        +
           "question": "Benutzen Sie private Messenger ...?",           +
           "answers": ["ja ", "nein "],                                 +
           "feedback": "Messenger speichern die Daten au unbekannt sind"+
        }                                                                                                                                                +
     }
    (1 Zeile)

Zugriff auf Keys
----------------

.. code:: sql


    select query->'id' from jsonstart;

Beachte doppeltes größer als Zeichen »->>« Zeichen:

.. code:: sql


    select query->>'id' from jsonstart;


Datentyp ändern

.. code:: sql


    ALTER TABLE jsonstart
    ALTER COLUMN query TYPE jsonb;

    select  query->'cat' as kategorie
    from jsonstart;
