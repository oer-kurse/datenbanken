
--
-- Name: queries; Type: TABLE;
--

CREATE TABLE queries (
    query_id integer NOT NULL,
    cat_id integer,
    query json,
    correct_answer integer
);


--
-- Name: queries_query_id_seq; Type: SEQUENCE;
--

CREATE SEQUENCE queries_query_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: queries_query_id_seq; Type: SEQUENCE OWNED BY;
--

ALTER SEQUENCE queries_query_id_seq OWNED BY queries.query_id;


--
-- Name: queries query_id; Type: DEFAULT;
--

ALTER TABLE ONLY queries ALTER COLUMN query_id SET DEFAULT nextval('queries_query_id_seq'::regclass);


--
-- Data for Name: queries; Type: TABLE DATA;
--

COPY queries (query_id, cat_id, query, correct_answer) FROM stdin;
100	6	{"question": "Wenn man auch nicht direkt betroffen ist, in den Medien wird immer wieder \\u00fcber spektakul\\u00e4re Vorf\\u00e4lle berichtet. Je gr\\u00f6\\u00dfer der Schaden, um so intensiver die Berichterstattung. <br> Wie werden diese Vorf\\u00e4lle f\\u00fcr den eigenen Betrieb ausgewertet?", "answers": [["Es findet keine Auswertung statt.", "Es gibt eine interne Information, wenn es einen Bezug zur Branche gibt.", "Der Datenschutz- bzw. Sicherheitsbeauftragte informiert die Mitarbeitenden dar\\u00fcber.", "Die Firmenleitung diskutiert solche Meldungen und teilt der <br> Belegschaft mit, welche Schlussfolgerungen gezogen wurden."]], "feedback": "\\u00dcber aktuelle Sicherheitsvorf\\u00e4lle sollte berichtet werden, um die Awareness/das Sicherheitsbewusstsein der Mitarbeitenden hoch zu halten."}	99
101	6	{"question": "Benutzen Sie private Messenger f\\u00fcr Absprachen mit Kolleginnen und Kollegen oder Kundinnen und Kunden?", "answers": [["ja", "nein"]], "feedback": "Messenger speichern die Daten auf Servern, deren Standorte wie auch die Zugriffsrechte oftmals unbekannt sind."}	99
102	99	{"question": "Logins und Fernzugriffe zum Firmennetzwerk sind ...", "answers": [["... sicher, wenn die Mitarbeitenden nur gesicherte WLAN-Netze nutzen und keine \\u00f6ffentlichen Zugangsm\\u00f6glichkeiten verwenden, um z.&#x2009;B. aus dem Hotel oder Zug auf das Firmennetzwerk zu zugreifen.", "... grunds\\u00e4tzlich immer sicher.", "... sicher, wenn alle Nutzerinnen und Nutzer eine Zwei-Faktor-Authentifizierung f\\u00fcr Logins verwenden."]], "feedback": "Eine Zwei-Faktor-Authentifizierung bietet den besten Schutz. Siehe auch: <a target='_blank' href='https://de.wikipedia.org/wiki/Zwei-Faktor-Authentisierung'>https://de.wikipedia.org/wiki/Zwei-Faktor-Authentisierung</a>"}	2
103	6	{"question": "Wie viele unterschiedliche Passw\\u00f6rter verwenden Sie? ! (private und dienstliche zusammengez\\u00e4hlt)?", "answers": [["weniger als 3", "4 bis 10", "10 bis 20", "mehr als 20"]], "feedback": "Sich Passw\\u00f6rter zu merken ist schwierig, wenn die Anzahl genutzter Dienste steigt. Der Einsatz eines Passwortmanagers ist empfehlenswert."}	99
104	6	{"question": "Wie oft f\\u00fchren Sie Updates von Softwareprogrammen durch? Bitte w\\u00e4hlen Sie die f\\u00fcr Sie am h\\u00e4ufigsten zutreffende Antwort.", "answers": [["w\\u00f6chentlich", "monatlich", "halbj\\u00e4hrlich", "j\\u00e4hrlich", "nach Bedarf", "gar nicht", "Ich verlasse mich auf automatische Updates."]], "feedback": "Software sollte stets auf dem neusten und damit aktuellsten Stand gehalten werden, denn f\\u00fcr veraltete Software enth\\u00e4lt h\\u00e4ufig Sicherheitsl\\u00fccken, die von Kriminellen aktiv ausgenutzt werden."}	99
105	6	{"question": "Pr\\u00fcfen und \\u00e4ndern Sie gegebenenfalls die Einstellungen Ihres Browsers?", "answers": [["Ich behalte die Standardeinstellungen bei.", "Ich schaue mir die Browsereinstellungen an, wenn es z.&#x2009;B. in einem Zeitungsartikel thematisiert wird.", "Nach einem Update schau ich mal rein."]], "feedback": "Es lohnt sich, die Einstellungen auf die eigenen Bed\\u00fcrfnisse anzupassen. Nehmen Sie sich also die Zeit und deaktivieren nicht ben\\u00f6tigte Einstellungen."}	99
106	6	{"question": "Glauben Sie, dass die Daten Ihres Unternehmens sicher sind?", "answers": [["ja", "nein", "wei\\u00df nicht"]], "feedback": "Fragen Sie Ihre/n Datenschutzbeauftragte/n nach der Firmenstrategie."}	99
107	6	{"question": "D\\u00fcrfen Sie private E-Mails an Ihrem Arbeitsplatzrechner abrufen und versenden?", "answers": [["ja", "nein"]], "feedback": "Beispiel einer Sicherheitswarnung vom 29.01.2021 (leicht angepasster Wortlaut): Aus gegebenen Anl\\u00e4ssen, unabh\\u00e4ngig von den Medienberichten \\u00fcber die Ausschaltung des Trojaners Emotet, weist der IT-Dienstleister alle Mitarbeiterinnen und Mitarbeiter darauf hin, auf den Abruf privater E-Mails von dienstlichen Systemen zu verzichten. Das Abrufen privater E-Mails aus den Webportalen der E-Mail-Provider kann durch beigef\\u00fcgten Schadcode schwerwiegende Auswirkungen auf die Funktionalit\\u00e4t und IT-Sicherheit von IT-Systemen, IT-Infrastrukturen und IT-Verfahren haben. (leicht angepasster Wortlaut)"}	3
108	3	{"question": "Sie arbeiten im Homeoffice und m\\u00fcssen dieses f\\u00fcr einen kurzen Zeitraum verlassen. Was sollten Sie beachten?", "answers": [["Ich lasse meinen Arbeitsplatz so, wie er ist, da ich die T\\u00fcr verschlie\\u00dfen kann.", "Lediglich das Sperren meines PCs ist notwendig, da ich nur kurz den Raum verlasse.", "Ich lasse alles so, wie es ist. Auch meine T\\u00fcr muss ich nicht versperren, da ich nur kurz abwesend bin.", "Ich sperre meinen PC, r\\u00e4ume alle offenen Akten/Dokumente weg und verschlie\\u00dfe den Raum."]], "feedback": "Um bestm\\u00f6gliche Informationssicherheit zu gew\\u00e4hrleisten, sollten Sie den PC sperren und sensible Dokumente wegr\\u00e4umen sowie den Raum verschlie\\u00dfen."}	3
109	7	{"question": "Sie sind auf Gesch\\u00e4ftsreise, sitzen in einem Caf\\u00e9 und m\\u00f6chten sich mit Ihren Kolleginnen und Kollegen per E-Mail \\u00fcber gewisse Aufgaben austauschen. Um die Kommunikation in \\u00f6ffentlichen Netzwerken zu sch\\u00fctzen, gilt das Folgende:", "answers": [["Suchen Sie immer das st\\u00e4rkste WLAN-Signal in Ihrer N\\u00e4he.", "Schalten Sie Ihr Filesharing aus.", "Benutzen Sie ein virtuelles privates Netzwerk (VPN)."]], "feedback": "Nur eine VPN-Verbindung ist sicher, wenn Sie im \\u00f6ffentlichen Netzwerk unterwegs sind."}	2
110	3	{"question": "Welchen dieser E-Mail-Anh\\u00e4nge sollten Sie nicht \\u00f6ffnen?", "answers": [["Datei mit der Endung .jpg", "Datei mit der Endung .png", "Datei mit der Endung .exe", "Datei mit der Endung .gif", "Datei mit der Endung .docx"]], "feedback": "Ausf\\u00fchrbare Programme (exe) sollten nicht ge\\u00f6ffnet werden. Vorsicht ist generell bei allen Arten von Anh\\u00e4ngen geboten, besonders bei .gif und Office Dateien ggf. mit Makros."}	2
111	5	{"question": "Kriminelle verschl\\u00fcsseln Dateien und fordern f\\u00fcr die Freigabe/Entschl\\u00fcsselung der Daten ein L\\u00f6segeld. Wie nennt man diese Art von Angriff?", "answers": [["Browser-Hijacking", "Ransomware-Angriff", "Brute-Force-Attacke"]], "feedback": "Ransomware-Angriff ist der korrekte Begriff f\\u00fcr diese Angriffsart."}	1
112	5	{"question": "Was ist ein Keylogger?", "answers": [["Keylogger sind Programme zur Speicherung von Anmeldenamen und Kennw\\u00f6rtern. Die Nutzenden m\\u00fcssen sich nur noch das Login f\\u00fcr den Keylogger merken und k\\u00f6nnen auf die verschl\\u00fcsselt gespeicherten Informationen zugreifen.", "Als Keylogger bezeichnet man Management-Software f\\u00fcr digitale Zertifikate in einem Unternehmen.", "Ein Keylogger ist eine Soft- oder Hardware, die Eingaben \\u00fcber die Tastatur mitprotokolliert, ohne dass die nutzenden Personen dies bemerken."]], "feedback": "Keylogger sind Programme, die Eingaben \\u00fcber die Tastatur weitergeben und so auch Passw\\u00f6rter abfangen k\\u00f6nnen."}	2
113	10	{"question": "Ein bei Sicherheitsdiskussionen h\\u00e4ufig erw\\u00e4hnter Begriff ist das \\"Spoofing\\". Doch was ist das eigentlich?", "answers": [["Beim so genannten Spoofing (Englisch f\\u00fcr \\"Vort\\u00e4uschen\\") verkaufen kriminelle H\\u00e4ndlerinnen und H\\u00e4ndler geknackte Produkt-Seriennummern f\\u00fcr teure Software. Die T\\u00e4uschung f\\u00e4llt erst dann auf, wenn die kaufende Person das Produkt versucht zu registrieren.", "Spoofing beschreibt eine Methode, bei der sich Kriminelle mit einer speziellen Software (dem Spoofer) illegal Zugriff zu einem Netzwerk verschaffen. Der Spoofer erscheint im Netzwerk wie ein herk\\u00f6mmliches Ger\\u00e4t. Er ist so kaum zu entdecken.", "Spoofing umfasst alle Methoden, mit denen sich Authentifizierungs- und Identifikationsverfahren untergraben lassen, die auf den Einsatz vertrauensw\\u00fcrdiger Adressen oder Hostnamen in Netzwerkprotokollen beruhen."]], "feedback": "L\\u00f6sung: Spoofing bezeichnet eine Angriffstechnik, bei der Cyberkriminelle in Compter oder Netzwerke eindringen, indem sie eine vertrauensw\\u00fcrdige Identit\\u00e4t vort\\u00e4uschen. Siehe auch: https://de.wikipedia.org/wiki/Spoofing"}	2
114	5	{"question": "Wie kann man einen Computer vor Malware sch\\u00fctzen?", "answers": [["Antiviren-Programm nutzen", "nur kostenpflichtige Programme nutzen", "Firewall nutzen"]], "feedback": "Malware ist Schadsoftware, die darauf abzielt, Computer und/oder Computersysteme zu besch\\u00e4digen und zu zerst\\u00f6ren. Malware ist eine Abk\\u00fcrzung f\\u00fcr \\"b\\u00f6sartige Software\\". Beispiele f\\u00fcr g\\u00e4ngige Malware sind Viren, W\\u00fcrmer, Trojaner, Spyware, Adware und Ransomware."}	0
115	10	{"question": "Auf fast allen Systemen k\\u00f6nnen Sie eine Datei mit dem Namen hosts (oder .hosts) finden. Ist diese Datei im Hinblick auf die Sicherheit wichtig?", "answers": [["Die hosts-Datei ist ein \\u00dcberbleibsel aus den Zeiten vor Einf\\u00fchrung des DNS (Domain Name Service). Sie hat heute f\\u00fcr die Sicherheit keine Bedeutung mehr.", "Die hosts-Datei ist sicherheitsrelevant, da Betriebssysteme bei der Namensaufl\\u00f6sung immer zuerst auf sie zugreifen. Wurde diese Datei manipuliert, so k\\u00f6nne angreifende Cyberkriminelle Zugriffe gezielt f\\u00fcr ihre Zwecke umlenken.", "Eine hosts-Datei existiert sowieso nur auf \\u00e4lteren Unix-Betriebssystemen und w\\u00e4re somit auch nur da sicherheitsrelevant."]], "feedback": "Die hosts-Datei wird von allen System benutzt, um gezielt Computernamen aufzul\\u00f6sen, in erster Linie localhost bzw. die IP 127.0.0.1"}	1
\.


--
-- Name: queries_query_id_seq; Type: SEQUENCE SET;
--

SELECT pg_catalog.setval('queries_query_id_seq', 1, false);


--
-- Name: queries pk_queries; Type: CONSTRAINT;
--

ALTER TABLE ONLY queries
    ADD CONSTRAINT pk_queries PRIMARY KEY (query_id);


