========================
Gemeinde -- Pretty-Print
========================


.. org::

.. _a49c6857-bd2d-4457-baba-df8106f34572:
.. INDEX:: Gemeinde; Pretty Print
.. INDEX:: Pretty Print; Gemeinde

Anzeige I
---------

- sehr unübersichtlich

- aber kompakt

.. code:: sql


    select * from gemeinden where ags = 12066052;             

Anzeige II
----------

- etwas übersichtlicher

- aber viele Zeilen

Dafür kann die Funktion **jsonb_pretty** verwendet werden.

.. code:: sql


    SELECT      

      ags,
      data ->'properties' ->>  'GEN' as Name,
      jsonb_pretty(
        data 
      ) as Daten
    FROM  
       gemeinden;
