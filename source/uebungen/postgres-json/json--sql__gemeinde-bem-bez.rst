=======================
Gemeinden -- AGS -- BEM
=======================


.. org::

.. _82e7965a-5d9a-423d-91a7-5f8e153014e9:
.. INDEX:: Gemeinden; BEM
.. INDEX:: BEM; Gemeinden

Frage
-----

1. Gib den Namen und die Bezeichnung für alle bzw. die ersten 20 Gemeinden aus.

2. Gib den Namen der Gemeinde/Stadt aus und zusätzlich den Status.

3. Wieviele Städte gibt es?

Antwort
-------

Eine zweite Varinante mit anderne Zugriffsoperatoren.

.. code:: sql


    SELECT                
         ags,
         data ->'properties' ->>  'GEN' as Name,
         data ->'properties' -> 'BEZ' as Status
     from 
        gemeinden;

**Alternativ**


 SELECT                
      data#>>'{properties, GEN}' as Name,
      data#>>'{properties, BEZ}' as Kategorie
  from 
     gemeinden
where data -> 'properties' ->> 'BEZ' = 'Stadt';

Eregebnis
---------

.. code:: text


       ags    |           name           
    ----------+--------------------------
     12051000 | Brandenburg an der Havel
     12052000 | Cottbus
     12053000 | Frankfurt (Oder)
     12054000 | Potsdam
     12060005 | Ahrensfelde
     12060020 | Bernau bei Berlin
     12060052 | Eberswalde
     12060181 | Panketal
     12060198 | Schorfheide
     12060269 | Wandlitz
    (10 Zeilen)
