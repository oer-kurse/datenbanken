=================
Quiz -- Antworten
=================


.. org::

.. _b276989c-9ce3-4f7f-8ac5-3670bad6c18a:
.. INDEX:: Quiz; Antworten
.. INDEX:: Antworten; Quiz

Gib für die ID 106 alle Antworten aus.
--------------------------------------

**SQL:**

.. code:: text

    ???          

**Ausgabe:**

.. code:: text


           Antwort-Optionen       
    ------------------------------
     ["ja", "nein", "weiß nicht"]
    (1 Zeile)

.. raw:: html



    <div>
    Hinweis zeigen: <input type="checkbox" value="hint" />
    <div class="togglehint">Verwende den »->«-Operator «
    <br/>

`Dokumentation zu JSON (josn_array_elements/jasonb_array_elements) <https://www.postgresql.org/docs/current/functions-json.html>`_

.. raw:: html

    </div>
    Lösung zeigen: <input type="checkbox" value="solution"/>
    <div class="togglesol">
    <br>

    <asciinema-player 
      src="../../_static/casts/json/json--fragen__id-106-als-array.cast"
      theme="solarized-light"
      font-size="small"
      rows="35"
      poster="data:text/plain,\e[5;5H Frage & Antwort \e[1;33m">
    </asciinema-player>

    </div>
    </div>
