=============
Übungen/Demos
=============


.. _471e71af-2d85-4e25-b1c6-0469449559d6:
.. INDEX:: Übungen/Demos

.. _uebungen:

.. image:: ./images/Datenbankkurs-Planung-Teil-1-und-2.drawio.svg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/Datenbankkurs-Planung-Teil-1-und-2.drawio.svg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/Datenbankkurs-Planung-Teil-1-und-2.drawio.svg">
   </a>

.. sidebar:: SQL-Kurs

   
.. sidebar:: Kursplan

   |b|
   
|a|

SQLite
======

.. toctree::
   :maxdepth: 1

   sqlite-department/index
   sqlite-bkzfrau/index

PostgreSQL
==========

.. toctree::
   :maxdepth: 1

   mengen/mengenopertoren
   joins/join-varianten
   joins/demo-self-join
   postgres-bkzfrau/index
   oracle/index
   postgres-brandenburg/index
   postgres-json/index
   ahnenforschung/index
   cte-fenster/index

Extras
======

.. toctree::
   :maxdepth: 1

   konsolidieren/index
   sqlite-festplatten/festplatten-sqlite
   sqlite-sakila/index
