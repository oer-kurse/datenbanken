===================
Daten Konsolidieren
===================

.. image:: ./images/kunst-am-bau-olympia.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/kunst-am-bau-olympia.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/kunst-am-bau-olympia.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Kunst am Bau

|a|
            
:ref:`« Übersicht: Manage PostgreSQL <managepostgres>`
     

Importierte Daten sind nicht immer in der optimal strukturiert. Wenn eine
nachträgliche Umstrukturierung und Konsolidierung notwendig ist, kann
das mit externen Werkzeugen geschehen. Einge Aktionen sind aber auch
in der Datenbank möglich und werden hier vorgestellt.

- Tabellen ändern
- Datentypen ändern  
- Constraints löschen/hinzufügen
- Redundante Daten entfernen

  
Im- und Export von Daten
========================

.. hint:: für **Windows-Benutzer**

   der Backslash (\\) muss durch einen
   Schrägstrich (/) ersetzt werden.

   .. table::

      +--------+--------------------------------------+
      | aus:   | \\i C:\\home\\sql\\data-gewaesser.sql|
      +--------+--------------------------------------+
      | wird:  | \\i C:/home/sql/data-gewaesser.sql   |
      +--------+--------------------------------------+
      | sonst: | C\:\: Permission denied              |
      +--------+--------------------------------------+


Import der Testdaten
--------------------

.. index:: import; copy
.. index:: import; COPY
	   
Die Beispieldatei enthält Mitschriften von Zuhörerern, die Vorlesungen
von Alexander von Humboldt gehört haben. Diese Mitschriften wurden in
einzelne Worte zerlegt. Nach dem Einlesen soll die Wortlste reduziert
werden, indem doppelte Einträge entfernt werden.


- :download:`viele-worte-csv.zip <viele-worte-csv.zip>`
- :download:`viele-worte-sql.zip <public-worte-sql.zip>`

	  
.. code-block:: bash

   -- Das doppelte Minus leitet in SQL-Scripten einen Kommentar ein
   -- \copy (klein geschreiben) wird auf dem Client ausgeführt
   -- die Realationen müssen bereits existieren
   -- die Reihenfolge der Daten muss der Reihenfolge der Attribute
   -- entsprechen...

   CREATE TABLE worte (wort VARCHAR(100));
   
   \copy worte from 'c:\home\viele_worte.txt';

   -- COPY (groß geschrieben) wird auf dem Server ausgeführt
   
   COPY worte from 'c:\home\viele_worte.txt';

.. index:: psql-shell; SQL-Skritpe ausführen
.. index:: manage; SQL-Skritpe ausführen
.. index:: Skripte; Postgres
.. index:: SQL-Skripte; Postgres

Tabelle für den import der Rohdaten
-----------------------------------

.. code:: bash
	  
    CREATE TABLE worte (
       wort VARCHAR(100) NULL
    );

    \COPY worte FROM  '/home//viele_worte.csv' WITH (FORMAT csv)

Tabelle für die konsolidierten Daten
------------------------------------
Hier soll jedes Wort nur einmal und zusätzlich die
ursprüngliche Anzahl erfasst werden.


.. code:: sql

   CREATE TABLE worte_ohne_wiederholung (                                                                 
     wort VARCHAR(100) NULL,
     anzahl integer
   );


Transfer und Konsolidierung
---------------------------

.. code:: SQL

	  insert
	    into worte_ohne_wiederholung
	      select
	        wort, count(word)
	      from
	        worte 
	      group by
	        wort;

Erster Analyse
--------------

.. code:: sql
	  
   select
     *
   from
     worte_ohne_wiederholung
   limit 10;


   
	   
