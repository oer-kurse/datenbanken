:orphan:
   
.. _downloadliste:
   
=========
Downloads
=========

:HINWEIS: Wenn der Download nicht startet, bitte den rechten Mausklick
	  versuchen und anschließend »öffnen mit...« oder eine ähnliche
	  Möglichkeit nutzen.

.. list-table:: Frozen Delights!
   :widths: 20 70
   :header-rows: 1

   * - Zweck
     - Download-Link

       
   * - Mindeswortschatz
     - :download:`Übersicht mit kurzen Beispielen (pdf/90kb) <./anhang/files/sql-mindestwortschatz.pdf>`

   * - Übungsdaten für
     - 
       
       - Index
       - Konsolieren

       :download:`Konsolidieren/Viele Worte (zip/csv 1,1 MB) <./uebungen/konsolidieren/viele-worte-csv.zip>`
		 
       :download:`Konsolidieren/Viele Worte (zip/sql 1,6 MB) <./uebungen/konsolidieren/public-worte-sql.zip>`
       

   * - Übungsdaten
     - SQLite/Postgres :download:`BKZ-Frau (zip/412 kB) <./uebungen/sqlite-bkzfrau/bkzfrau-daten.zip>`
