:orphan:
   
.. _linkliste:
  
===========
 Linkliste
===========

===========================  ===============
Stichwort                    Link
===========================  ===============
Datenbank für Datenbanken    https://dbdb.io/browse
Doku Postgres                https://www.postgresql.org/docs/
Tipps: psql                  https://psql-tips.org/psql_tips_all.html
Entwicklunsgeschichte        https://www.crunchydata.com/blog/when-did-postgres-become-cool
Stolperfallen
- Postgres                   https://sql-info.de/postgresql/postgres-gotchas.html
- MySQL                      https://sql-info.de/mysql/gotchas.html
SQL-Map (Security-Tests)     https://highon.coffee/blog/sqlmap-cheat-sheet/
Anwendungen für Microsoft
SQL Server mit Postgres
betreiben                    https://wiltondb.com/
Security (Anleitung)         https://www.crunchydata.com/files/stig/PGSQL_16_STIG_V1R1.pdf
Wiki                         https://pgpedia.info/
===========================  ===============

