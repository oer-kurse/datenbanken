==========================
psycopg2: Einfaches select
==========================

.. index:: psycopg2; Python
.. index:: Python; psycopg2	   

Eine Abfrage der Datenbank, was wird benötigt?

Eine virtuelle Umgebung + psycopg2

Python installieren und eine virutelle Umgebung einrichten, ist im Web
schon viele male beschrieben worden und wird hier nicht wiederholt.

Installation von psycopg2
=========================
::

   
   pip install psycopg2


Ein erstes Skript
=================
Inhalt der Datei *pg-simple-select.py*

.. literalinclude:: files/pg-simple-select.py
   :language: python

Aufruf und Ausgabe
==================

.. code:: python

   (py36) python pgtest001.py 
   ##################################
   Die Antwort auf Ihre Frage lautet:
   ##################################
   (7744, 'EDV-Saalaufseherin    ')
   (7833, 'Leiterin des Lochkartensaals  ')
   (7744, 'Leiterin des Maschinensaals, EDV ')
   (7924, 'Lesesaalaufseherin    ')
   (7833, 'Lochkartensaalleiterin    ')
   (7744, 'Maschinensaalleiterin (EDV)   ')
   (9121, 'Obersaaltochter    ')
   (7925, 'Saalaufseherin (Wächterin Ordnerin)  ')
   (6290, 'Saalaufseherin, Industriemeisterin   ')
   (9111, 'Saalbesitzerin    ')
   (7056, 'Saalchefin (Croupiere)   ')
   (7944, 'Saaldienerin    ')
   (9122, 'Saalkellnerin    ')
   (9127, 'Saalmädchen    ')
   (6290, 'Saalmeisterin    ')
   (9331, 'Saalreinigerin    ')
   (9127, 'Saaltochter    ')
   (7944, 'Theater-, Saaldienerin   ')
   

Weitere Beipiele
----------------

- `Beispiele mit Erläuterungen <https://techbeamers.com/python-connect-postgresql/#h-1-installing-required-libraries>`_
