================
 Programmierung
================
Python ist dafür bekannt, für alles und jedes noch so kleine oder große
Software-Projekt ein passendes Werkzeug anzubieten.
Falls es das noch nicht gibt, schreibt man sein eigenes Script!

Nachfolgend ein paar Beispiele zu den bekannten Datenenbanken im
OpenSource-Umfeld:

SQLite
======

.. toctree::
   :maxdepth: 1

   pysqlite

PostgreSQL
==========

.. toctree::
   :maxdepth: 1

   postgres
   postgres--pool
   postgres-binaere-daten
  

Weitere Beipiele
----------------

- `Beispiele mit Erläuterungen <https://techbeamers.com/python-connect-postgresql/#h-1-installing-required-libraries>`_
