====================
Python: binäre Daten
====================

.. INDEX:: Binäre Daten; Speichern
.. INDEX:: Speichern; Binäre Daten
.. INDEX:: Python; Simple select
.. INDEX:: Simple select; Python


Wie sollten binäre Daten gespeichert werden?

Das Wiki zu PostgreSQL gibt einen Überblick und nennt Vor- bzw. Nachteile [#f1]_

Das Skript
==========

Das hier vorgestellte Skript [#f2]_ kann binäre Dateien speichern und auch
wieder herunterladen. Am Beispiel einer PDF wird das Prinzip demonstriert.
- Anzupassen sind der Datenbankname und der Benutzer.
- Die Nutzung mit anderen binären Datenformaten ist auch möglich.
  

Hilfe zum Script
================

.. code:: bash

   	  
   (env) python gp-handle--pdf.py -h

   pg-handle-pdfs.py [-h] (--store | --fetch xx) filename

   positional arguments:
      filename    Name der Datei

   options:
     -h, --help  show this help message and exit
     --store     Speichere ein pdf mit dem Dateinamen in der DB
     --fetch xx  Ein PDF im Dateisystem ablegen.
                 Für den Parameter xx die passende ID angeben.


PDF vom Dateisystem in die DB
=============================

.. code:: bash

   python pg-handle-pdfs.py --store sql-mindestwortschatz.pdf



PDF von  DB zum Dateisystem
===========================

.. code:: bash

   python pg-handle-pdfs.py --fetch 1 test.pdf

   ID: 1 als Datei: »test.pdf« gespeichert.
   Der original Dateiname ist »sql-mindestwortschatz.pdf«.


Quellcode zum Script
====================

.. literalinclude:: files/pg-handle-pdfs.py
   :language: python

   
.. [#f1] https://wiki.postgresql.org/wiki/BinaryFilesInDB
	 
.. [#f2] https://stackoverflow.com/questions/16763904/how-to-save-a-image-file-on-a-postgres-database
