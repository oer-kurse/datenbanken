"""
Vorarbeiten

1. Datenbank "basics" angelgen
   create database basics
2. Relation anlegen
   \c basics
   \i bkzfrau-utf8.sql
2. Benutzer "basics_user" anlegen
   CREATE USER basics_user WITH PASSWORD 'basics';
3. Rechte
   GRANT SELECT ON gewaesser TO basics_user;

"""

import psycopg2

connect_str = """dbname='basics'
user='basics_user' host='localhost'
password='basics'"""

conn = psycopg2.connect(connect_str)

with conn:

    try:
        cursor = conn.cursor()
        sql = "SELECT * from bkzfrau where beruf like '%aal%'"
        cursor.execute(sql)
        rows = cursor.fetchall()
        antwort = "Die Antwort auf Ihre Frage lautet:"
        print(len(antwort) * '#')
        print(antwort)
        print(len(antwort) * '#')
        for row in rows:
            print(row)
    
    except Exception as e:
        # Rollback the transaction in case of an error
        conn.rollback()
        print(f"Error: {e}")
