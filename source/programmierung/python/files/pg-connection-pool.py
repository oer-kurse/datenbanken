"""
Vorarbeiten

1. Datenbank "basics" angelgen
   create database basics
2. Relation anlegen
   \c basics
   \i bkzfrau-utf8.sql
2. Benutzer "basics_user" anlegen
   CREATE USER basics_user WITH PASSWORD 'basics';
3. Rechte
   GRANT SELECT ON gewaesser TO basics_user;

"""

from psycopg2 import pool

conn_pool = pool.SimpleConnectionPool(
    1,  # Minmale Anzahl an Verbindungen
    5,  # Maximle Anzahl an Verbindungen
    database="basics",
    user="basics_user",
    password="basics",
    host="localhost",
    port="5432"
)
try:
    conn = conn_pool.getconn()
    cursor = conn.cursor()
    sql = "SELECT * from bkzfrau where beruf like '%aal%'"
    cursor.execute(sql)
    rows = cursor.fetchall()
    antwort = "Die Antwort auf Ihre Frage lautet:"
    print(len(antwort) * '#')
    print(antwort)
    print(len(antwort) * '#')
    for row in rows:
        print(row)

except SomeException:
    print("Funktioniert nicht! Datenbankname, Benutzer und Password sind ok?")
    print(e)
# Connection freigeben
conn_pool.putconn(conn)
