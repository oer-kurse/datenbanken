
.. _20221117T201235:

.. INDEX:: Programmierung; 
.. INDEX:: SQLite/Python;Programmierung 

SQLite3/Python
--------------

Installation
~~~~~~~~~~~~

.. code:: bash


    python3 -m venv pysqlite
    cd pysqlite
    source ./bin/activate
    pip install --upgrade pip

Versionstest
~~~~~~~~~~~~

Python starten ...

.. code:: python


    import sqlite3
    sqlite3.connect(":memory:").execute("select sqlite_version()").fetchall()

Bulkimport
~~~~~~~~~~

.. code:: python


    import sqlite3
    persons =[
        ('Peter','Koppatz'),
        ('Erhard','Koppatz')
    ]

    con = sqlite3.connect(":memory:")
    con.execute('create table person(vorname, nachname)')
    con. executemany('insert into person(vorname, nachname) values (?,?)', persons)

Tansaktionen
~~~~~~~~~~~~

.. code:: python


    import sqlite3
    import time

    t= time.time()
    persons =[
        ('Peter','Koppatz'),
        ('Erhard','Koppatz')
    ]

    con = sqlite3.connect(":memory:")
    cur = con.cursor()
    con.execute('create table person(vorname, nachname)')

    cur.execute('BEGIN TRANSACTION')
    for chunk in persons:
        con. execute('insert or ignore into person(vorname, nachname) values (?,?)', chunk)
    cur.execute('COMMIT')
    print(f'\nZeitmessung: {time.time()-t:.3f} Sekunden')
