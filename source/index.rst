.. meta::

   :description lang=de: Kurs: Datenbanken und SQL mit SQLite und PostgreSQL
   :keywords: Kurs, Kursmaterial, OER-Kurs, Datenbanken, relationale Datenbanken, PostgreSQL, SQLite, SQL

.. _kursstart:

.. image:: ./baum-caputh-weide.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('./_images/baum-caputh-weide.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="./_images/baum-caputh-weide.webp">
   </a>

.. sidebar:: SQL-Kurs

   :ref:`genindex`
	
   :ref:`Linkliste <linkliste>`
	
   :ref:`Download-Liste <downloadliste>`

   
.. sidebar:: Serie: Natur

   |b|
   
|a|

Inhalte
=======

.. toctree::
   :maxdepth: 1

   fachbegriffe/index
   modellierung/index
   manage/index
   uebungen/index
   programmierung/python/index
   anhang/index
