:orphan:
   
===================================
Datenbankspezifische Besonderheiten
===================================

Bezeichnungen, Funktionen, ...

coalese
=======

.. index:: coalesce


Die Funktion »coalesce« gibt für zwei oder mehr Parameter, den ersten
Wert zurück, der verschieden von NULL ist. Damit lassen sich fehlende
Werte durch einen Standard-Wert ersetzen. 
	   
::
   
   select coalesce(NULL, NULL, 'Treffer');

   -- Null durch eine leere Zeichenkette ersetzen

   select coalesce(NULL,'');
   
   -- besser

   select coalesce(NULL,'Kein Wert');
   

limit
=====




Limit ist ein gutes Beispiel für Fälle in denen es keinen
einheitlichen Stadard gibt.

:ref:`Beispiele zu limit <limit>`

CTE
===

.. _cte:

.. INDEX:: CTE; Postgres
.. INDEX:: Postgres; CTE

Common Table Expressions sind temporäre Abfrageergebnisse (result
sets) und existieren nur für die Laufzeit einer umfangreicheren Frage,
in der sie integriert sind.
Sie sind eine Vereinfachung und Ergänzung  für Views und Unterabfragen.

Siehe auch:

https://lernsql.com/blog/difference-between-sql-cte-and-view



PGDATA
======

.. INDEX:: PGDATA; Postgres
.. INDEX:: Postgres; PGDATA

Ist der Ordner in dem die Daetn einer Postgresinstanz gespeichert werden.

WAL
===

.. INDEX:: Postgres; WAL
.. INDEX:: WAL; Postgres


Write-Ahaed Logs (WALs), sichert nach einem Datenbankabsturz die
Wiederherstellung des letzten stabielen Zustands.
Diese Log-Dateien sollten idealerweise auch auf ein anderes
physikalisches System repliziert werden.


