=============
SQL Developer
=============


.. _20240302T081251:

.. INDEX:: SQLDeveloper; Werkzeuge 
.. INDEX:: Werkzeuge; SQLDeveloper 
.. INDEX:: SQLDeveloper; Tools 
.. INDEX:: Tools; SQLDeveloper 

Der SQL Developer ist ein Werkzeug, um die Migration externer Daten in
eine Oracle-Datenbank zu vereinfachen. Über externe Treiber ist die
Anbindung anderer Datenbanksystem möglich und damit können Abfragen
z.B. für eine Postgres-Datenbank realisiert werden.

Installationsschritte für Postgres
----------------------------------

1. Download des »SQL Developer« (Account ist notwendig).

2. Prüfen ob Java installiert ist, gegebenenfalls die neueste
   JRE herunterladen und installieren.

3. Entpacken und in einen lokalen Ordner verschieben.

4. JDBC-Treiber für Postges herunterladen.

5. Treiber in SQL Develper einbinden.

6. Verbindung zur Datenbank herstellen.

Bildschirmfotos
---------------

.. tab:: 1

   Einbinden des JDBC-Treibers über das Menü:
   Extras, Voreinstellungen, Datenbank, JDBC-Treiber...
   
   .. image:: ./images/sql-developer-001.png

.. tab:: 2

   Datenbankverbindung erstellen ...
   
   .. image:: ./images/sql-developer-002.png


.. tab:: 3

   Wenn Datenbank und Benutzername nicht übereinstimmen,
   muss an den Hostname noch ein Datenbankname und ein
   Fragezeichen angehängt werden!
    
   .. image:: ./images/sql--developer__connect.avif

.. tab:: 4

   Abfrage nach erfolgreichem Verbindungsaufbau ...
   
   .. image:: ./images/sql-developer-003.png
