============
Modellierung
============

.. image:: ./images/katktusbluete08.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/katktusbluete08.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/katktusbluete08.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Pflanzen

|a|

Es muss nicht gleich ein Programm sein, meistens reichen Papier und
Stifte für den ersten Schritt.

- `Liste von Werkzeugen für die Modellierung (Wikipedia)`_
- `Liste der Werkzeuge für PostgreSQL (Wiki)`_
- `Workbench MySQL`_
- `DatabaseSpy -- Altova`_
- `Datenbank Designer -- Microsoft`_
- `SQL Developer -- Oracle`_


.. Linkliste
  
.. _Liste von Werkzeugen für die Modellierung (Wikipedia): https://de.wikipedia.org/wiki/Liste_von_Datenmodellierungswerkzeugen
.. _Liste der Werkzeuge für PostgreSQL (Wiki): https://wiki.postgresql.org/wiki/Community_Guide_to_PostgreSQL_GUI_Tools
.. _Workbench MySQL: https://dev.mysql.com/downloads/file/?id=468289
.. _DatabaseSpy -- Altova: https://www.altova.com/databasespy
.. _Datenbank Designer -- Microsoft: https://msdn.microsoft.com/de-de/library/aa292883(v=vs.71).aspx
.. _SQL Developer -- Oracle: http://www.oracle.com/technetwork/developer-tools/sql-developer/downloads/index.html

