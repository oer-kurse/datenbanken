========
Squirrel
========


.. INDEX:: Squirrel; Werkzeuge
.. INDEX:: Werkzeuge; Squirrel
.. INDEX:: Tools; Squirrel
.. INDEX:: Squirrel; Tools

GUI: Squirrel
-------------

Ein Java-Tool mit dem viele Datenbanken über eine grafische
Benutzeroberfläche verwaltet werden können. Alle wichtigen 
Links sind unter Installation/Vorarbeiten gelistet.

Konfiguration I: Treiber einbinden
----------------------------------

Starte das Programm:  SQuirrel SQL Client:
------------------------------------------

.. code:: bash


    java -jar squirrel-sql-<version>-install.jar 

Driver list
-----------

- »Create a New Driver«

- Einen beschreibenden Namen erfinden »Meine Verbindung«

- »org.postgresql.Driver« im Feld »Class Name«

- »Extra Class Path« auswählen.

  - »Add«  JDBC jar-Datei für postgresql
    z.B: postgresql-42.2.5.jar

- alle Einstellungen bestätigen

Hier noch ein Bild:

.. image:: ./images/squirrel-treiber.png

Konfiguration II: Alias definieren
----------------------------------

.. image:: ./images/squirrel-alias-definieren.png

Konfiguration III: Verbinden
----------------------------

.. image:: ./images/squirrel-alias.png

Vorarbeiten: Installation
-------------------------

Java installieren

`https://www.java.com/de/download/win10.jsp <https://www.java.com/de/download/win10.jsp>`_

Download SQuirrel SQL Client

`http://squirrel-sql.sourceforge.net/#installation <http://squirrel-sql.sourceforge.net/#installation>`_

Download Postgres-Treiber

`https://jdbc.postgresql.org/download.html <https://jdbc.postgresql.org/download.html>`_
