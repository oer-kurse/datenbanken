================
BeeKeeper-Studio
================



.. INDEX:: BeeKeeper; Tools
.. INDEX:: Tools; BeeKeeper
.. INDEX:: BeeKeeper; Werkzeuge
.. INDEX:: Werkzeuge; BeeKeeper

.. image:: ./images/beekeeper-logo-yellow-icon.svg
   :width: 150px

Download
--------

`https://www.beekeeperstudio.io/ <https://www.beekeeperstudio.io/>`_

Beispiel-Bildschirmfoto
-----------------------

.. image:: ./images/beekeeper-beispiel.png

FAQ
---

Wo speichert BeeKeeper die Abfragen?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Wenn man auf Abfrage speichern klickt und einen Namen vergibt,
findet man die SQL-Anweisungen in einer SQLite-Datenbank **»App.db«**.

Windows: C:\\\Users\\\user600\\\AppData\\\Roaming\\\beekeeper-studio\\\App.db
