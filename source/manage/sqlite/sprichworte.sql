-- Demonstration für das Einlesen von Daten
-- über ein SQL-Script

-- Relation löschen
-- Erzeugt beim ersten Aufruf einen Fehler!

DROP TABLE sprichworte;

-- Neuanlage der Relation
CREATE TABLE sprichworte (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  spruch TEXT
);

insert into sprichworte values(NULL, 'Alle Wege führen nach Rom.');
insert into sprichworte values(NULL, 'Aller guten Dinge sind drei.');
insert into sprichworte values(NULL, 'Allzu klug ist dumm.');
insert into sprichworte values(NULL, 'Alte Liebe rostet nicht.');
insert into sprichworte values(NULL, 'Alter schützt vor Torheit nicht.');
insert into sprichworte values(NULL, 'Am Abend gekammert, am Morgen gejammert.');
insert into sprichworte values(NULL, 'Am Schilde erkennt man die Gilde.');
insert into sprichworte values(NULL, 'Angriff ist die beste Verteidigung.');
insert into sprichworte values(NULL, 'April, April, der weiß nicht, was er will.');
insert into sprichworte values(NULL, 'Auch ein blindes Huhn findet mal ein Korn.');
insert into sprichworte values(NULL, 'Auch Morgenland ist Sorgenland.');
insert into sprichworte values(NULL, 'Aus den Augen, aus dem Sinn.');
insert into sprichworte values(NULL, 'Besser stumm als dumm.');
insert into sprichworte values(NULL, 'Blut ist dicker als Wasser.');
insert into sprichworte values(NULL, 'Das Auge isst mit.');
insert into sprichworte values(NULL, 'Das Blatt hat sich gewendet.');
insert into sprichworte values(NULL, 'Das Leben ist kein Ponyhof.');
insert into sprichworte values(NULL, 'Der Apfel fällt nicht weit vom Stamm.');
insert into sprichworte values(NULL, 'Der Appetit kommt beim Essen.');
insert into sprichworte values(NULL, 'Der Fisch will schwimmen.');
insert into sprichworte values(NULL, 'Der frühe Vogel fängt den Wurm.');
insert into sprichworte values(NULL, 'Der Krug geht so lange zum Brunnen, bis er bricht.');
insert into sprichworte values(NULL, 'Der Teufel ist ein Eichhörnchen.');
insert into sprichworte values(NULL, 'Der Ton macht die Musik.');
insert into sprichworte values(NULL, 'Der Zweck heiligt die Mittel.');
insert into sprichworte values(NULL, 'Die Axt im Haus erspart den Zimmermann.');
insert into sprichworte values(NULL, 'Die Hoffnung stirbt zuletzt.');
insert into sprichworte values(NULL, 'Die Katze beißt sich in den Schwanz.');
insert into sprichworte values(NULL, 'Die Katze lässt das Mausen nicht.');
insert into sprichworte values(NULL, 'Die Letzten werden die Ersten sein.');
insert into sprichworte values(NULL, 'Die Schönheit liegt im Auge des Betrachters.');
insert into sprichworte values(NULL, 'Die Welt ist ein Dorf.');
insert into sprichworte values(NULL, 'Die Würfel sind gefallen.');
insert into sprichworte values(NULL, 'Dienst ist Dienst und Schnaps ist Schnaps.');
insert into sprichworte values(NULL, 'Ein Unglück kommt selten allein.');
insert into sprichworte values(NULL, 'Eine Hand wäscht die andere.');
insert into sprichworte values(NULL, 'Eine Schwalbe macht noch keinen Sommer.');
insert into sprichworte values(NULL, 'Einem geschenkten Gaul schaut man nicht ins Maul.');
insert into sprichworte values(NULL, 'Ende gut, alles gut.');
insert into sprichworte values(NULL, 'Es gibt sone und solche.');
insert into sprichworte values(NULL, 'Es ist nicht alles Gold, was glänzt.');
insert into sprichworte values(NULL, 'Es wird nichts so heiß gegessen, wie es gekocht wird.');
insert into sprichworte values(NULL, 'Essen und Trinken hält Leib und Seele zusammen.');
insert into sprichworte values(NULL, 'Früh übt sich, was ein Meister werden will.');
insert into sprichworte values(NULL, 'Degen Dummheit kämpfen Götter selbst vergebens.');
insert into sprichworte values(NULL, 'Gleich und Gleich gesellt sich gern.');
insert into sprichworte values(NULL, 'Glück und Glas, wie leicht bricht das.');
insert into sprichworte values(NULL, 'Grüner wird es nicht.');
insert into sprichworte values(NULL, 'Gut Ding will Weile haben.');
insert into sprichworte values(NULL, 'Gut gekaut ist halb verdaut.');
insert into sprichworte values(NULL, 'Hochmut kommt vor dem Fall.');
insert into sprichworte values(NULL, 'Hunde, die bellen, beißen nicht.');
insert into sprichworte values(NULL, 'Ihr seid das Salz der Erde.');
insert into sprichworte values(NULL, 'In der Not frisst der Teufel Fliegen.');
insert into sprichworte values(NULL, 'Ist der Ruf erst ruiniert, lebt es sich ganz ungeniert.');
insert into sprichworte values(NULL, 'Jeder ist seines Glückes Schmied.');
insert into sprichworte values(NULL, 'Kindermund tut Wahrheit kund.');
insert into sprichworte values(NULL, 'Kleider machen Leute.');
insert into sprichworte values(NULL, 'Käse schließt den Magen.');
insert into sprichworte values(NULL, 'Liebe geht durch den Magen.');
insert into sprichworte values(NULL, 'lieber den Magen verrenken als dem Wirt was schenken.');
insert into sprichworte values(NULL, 'lieber den Magen verrenkt als dem Wirt was geschenkt.');
insert into sprichworte values(NULL, 'Lügen haben kurze Beine.');
insert into sprichworte values(NULL, 'Ich hat schon Pferde kotzen sehen und das vor der Apotheke.');
insert into sprichworte values(NULL, 'Man muss das Eisen schmieden, solange es heiß ist.');
insert into sprichworte values(NULL, 'morgen, morgen, nur nicht heute, sagen alle faulen Leute.');
insert into sprichworte values(NULL, 'Morgenstund hat Gold im Mund.');
insert into sprichworte values(NULL, 'Mühsam ernährt sich das Eichhörnchen.');
insert into sprichworte values(NULL, 'Mühsam nährt sich das Eichhörnchen.');
insert into sprichworte values(NULL, 'Nachts sind alle Katzen grau.');
insert into sprichworte values(NULL, 'Ohne Fleiß kein Preis.');
insert into sprichworte values(NULL, 'Ordnung muss sein.');
insert into sprichworte values(NULL, 'Pünktlichkeit ist die Höflichkeit der Könige.');
insert into sprichworte values(NULL, 'Ratschläge sind auch Schläge.');
insert into sprichworte values(NULL, 'Reden ist Silber, Schweigen ist Gold.');
insert into sprichworte values(NULL, 'Reisende soll man nicht aufhalten.');
insert into sprichworte values(NULL, 'Schuster, bleib bei deinem Leisten.');
insert into sprichworte values(NULL, 'Schönheit liegt im Auge des Betrachters.');
insert into sprichworte values(NULL, 'Schönheit vergeht, Tugend besteht.');
insert into sprichworte values(NULL, 'Steter Tropfen höhlt den Stein.');
insert into sprichworte values(NULL, 'Stille Wasser gründen tief.');
insert into sprichworte values(NULL, 'Stille Wasser sind tief.');
insert into sprichworte values(NULL, 'Umsonst ist der Tod, und der kostet das Leben.');
insert into sprichworte values(NULL, 'Undank ist der Welten Lohn.');
insert into sprichworte values(NULL, 'Unter den Blinden ist der Einäugige König.');
insert into sprichworte values(NULL, 'Unverhofft kommt oft.');
insert into sprichworte values(NULL, 'Unwissenheit schützt vor Strafe nicht.');
insert into sprichworte values(NULL, 'Viele Köche verderben den Brei.');
insert into sprichworte values(NULL, 'Was der Bauer nicht kennt, frisst er nicht.');
insert into sprichworte values(NULL, 'Was Hänschen nicht lernt, lernt Hans nimmermehr.');
insert into sprichworte values(NULL, 'Was sich liebt, das neckt sich.');
insert into sprichworte values(NULL, 'Was sich neckt, das liebt sich.');
insert into sprichworte values(NULL, 'Wasser hat keine Balken.');
insert into sprichworte values(NULL, 'Wasser hat keinen Balken.');
insert into sprichworte values(NULL, 'wenn man vom Teufel spricht.');
insert into sprichworte values(NULL, 'wenn man vom Teufel spricht, dann kommt er gegangen.');
insert into sprichworte values(NULL, 'Wenn man vom Teufel spricht, kommt er.');
insert into sprichworte values(NULL, 'Wer A sagt, muss auch B sagen.');
insert into sprichworte values(NULL, 'Wer anderen eine Grube gräbt, fällt selbst hinein.');
insert into sprichworte values(NULL, 'Wer die Wahl hat, hat die Qual.');
insert into sprichworte values(NULL, 'Wer im Glashaus sitzt, soll nicht mit Steinen werfen.');
insert into sprichworte values(NULL, 'Wer nicht wagt, der nicht gewinnt.');
insert into sprichworte values(NULL, 'Wer Wind sät, wird Sturm ernten.');
insert into sprichworte values(NULL, 'Eer zuerst kommt, mahlt zuerst.');
insert into sprichworte values(NULL, 'Ees Brot ich ess, des Lied ich sing.');
insert into sprichworte values(NULL, 'Eie der Herr, so’s Gescherr.');
insert into sprichworte values(NULL, 'Eie gewonnen, so zerronnen.');
insert into sprichworte values(NULL, 'Eie kommt Saul unter die Propheten.');
insert into sprichworte values(NULL, 'Eie man in den Wald hineinruft, so schallt es heraus.');
insert into sprichworte values(NULL, 'Wo Aas ist, da sammeln sich die Geier.');
insert into sprichworte values(NULL, 'Wo aber ein Aas ist, da sammeln sich die Geier.');
insert into sprichworte values(NULL, 'Wo das Aas ist, da sammeln sich die Geier.');
insert into sprichworte values(NULL, 'Wo ein Wille ist, ist auch ein Weg.');
insert into sprichworte values(NULL, 'Wo gehobelt wird, fallen Späne.');
insert into sprichworte values(NULL, 'Wo kein Kläger, da kein Richter.');
insert into sprichworte values(NULL, 'Zeit ist Geld.');
insert into sprichworte values(NULL, 'Übung macht den Meister.');

