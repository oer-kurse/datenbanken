===================================
Konfiguration -- "default settings"
===================================


.. _20231012T193449:

.. INDEX:: SQLite; Konfiguration
.. INDEX:: Konfiguration; SQLite


Wer regelmäßig mit SQLite arbeitet, möchte nach dem öffnen einer
Datenbank die Grundeinstellungen nicht immer wieder manuell ändern.

Deshalb können die wichtigsten Einstellungen in einer Datei hinterlegt
werden. Diese werden dann immer automatisch gesetzt

Eine Datei anlegen:

.. code:: bash


    .sqliterc


darin die Steuerbefehle (beginnen mit Punkt) ablegen wie z.B:

.. code:: bash


    .header on
    .mode column
    .timer on
    .width 10, 20, 10



.. tab:: Windows


   BEACHTE -- Windows:

   Datei im User-Folder speichern:

   ::

      C:\Users\user200


.. tab:: Linux

   Hier muss die Datei im gleichen Ordner liegen, wie die Datenbank-Datei.
