:ref:`« Management SQLite <bkzfrau-start>`

.. _sqlite-export:
   
.. index:: sqlite; Export
.. index:: Export; sqlite

   
=================
Daten exportieren
=================

.. image:: ./images//lucas-cranach-adam-eva.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images//lucas-cranach-adam-eva.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images//lucas-cranach-adam-eva.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   
   | Lucas Cranach d. Ä.: Adam und Eva,
   | Herzog-Anton-Ulrich-Museum
   | Serie: Gemälde
            

|a|


Export durch Ausgabeumleitung
=============================


Den Inhalt einer Datenbank:

.. code-block:: sql

   sqlite> .output company-db.sql
   sqlite> .dump
   sqlite> .output

SQL-Skript mit Insertanweisungen
--------------------------------

.. index:: Export; sqlite -- insert
.. index:: sqlite; Export -- insert
	   
.. code-block:: sql

   sqlite> .mode insert
   sqlite> .output company-inserts.sql
   sqlite> select * from categories;
   sqlite> .output
   
