:ref:`« Management SQLite <bkzfrau-start>`

.. index:: sqlite; Ausgabe anpassen
	   
================
Ausgabe anpassen
================

.. image:: ./images/apfel.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/apfel.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/apfel.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Pflanzen

|a|
            
Die Ausgaben können, abweichend von den Standardeinstellungen angepasst werden.

- Überschriften ein(on)/aus(off)
- Spaltentrenner
- Laufzeitverhalten anzeigen
- Spaltenbreite (neu definieren)
- ...
  
.. index:: sqlite; Ausgaben steuern, formatieren
	   
Gut lesbare Ausgaben:
::

   .header on
   .mode column
   .timer on
   .width 10, 20, 10

Weitere Infos dazu liefert der Befehl:
::

   .help
