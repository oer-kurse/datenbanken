:ref:`« Management SQLite <bkzfrau-start>`
     
.. index:: sqlite; Programm start
	   
=======
Starten
=======

.. image:: ./images/bruchrechnung-apfel.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bruchrechnung-apfel.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bruchrechnung-apfel.jpg">
   </a>



.. sidebar:: SQL-Kurs

   |b|
   Serie: Dinge

|a|

Die Arbeit mit der Datenbank beginnt mit dem Aufruf des Programmes.
Alternativ kann sofort eine Datenbankdatei als Parameter übergeben werden.


Programmstart mit Datenbankname (Linux)
=======================================

Existiert die Datei nicht, wird eine neue Datei angelegt.
::

   sqlite3 datenbankname.db

Programmstart mit Datenbankname (Windows)
=========================================
Bildserie zum Öffnen einer Conmandline im Ordner,
in dem sich die sqlite3.exeund eine Datenbank befinden.


.. tab:: Ordner

   Im Datei-Explorer zu dem Ordner wechseln, in dem sich die sqlite3.exe befindet.
   
   .. image:: ./images/sqlite-start01.png

.. tab:: CMD

   In die Pfadzeile klicken, der Pfad wird als Zeichenkette angzeigt und markiert.
   Die Markierung nicht aufheben!
   
   .. image:: ./images/sqlite-start02.png

.. tab:: Start

   Die Zeichenfolge cmd tippen, gefolgt von einem ENTER, ein Terminal öffnet sich
   und wechselt automatisch in den vorher ausgewählten Ordner. Dort kann nun
   SQLite3 mit einer Datenbank geöffnet werden.
   
   .. image:: ./images/sqlite-start03.png


Existiert die Datei nicht, wird eine neue Datenbank angelegt.
::

   sqlite3 datenbankname.db

Start ohne Datenbank
====================
Anschließend öffnen:

::

   sqlite3
   .open /pfad/zur/db/mit/datenbankname.db
   
.. index:: sqlite; Datenbanken

Wo liegt die Datenbank?
=======================

::
   
   .databases

   seq  name   file                                                      
   ---  -----  ---------------------------
   0    main   /Users/mis36/src/pyhasse.db                  

Datenbank sichern
=================

::

   .save /pfad/zum/ordner/datenbankname.db

Datenbank reparieren
====================

Ab Version 3.40 gibt es eine Recovery-Methode.

::

   sqlite3 defekt.db .recover > repariert.sql
   
