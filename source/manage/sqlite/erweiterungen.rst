=============
Erweiterungen
=============


SQLite wird von Richard Hipp entwickelt und ist die weltweit meist
genutzte Datenbank und eine der wenigen, die ohne einen
Datenbankserver auskommt.

Ein Interview mit dem Entwickler kann als Podcast oder als
Transkription angehört bzw. gelesen werden. Es ist ein Gang durch die
Geschichte der Software- und Hardware-Giganten und wichtigen Aussagen
wie Testdriven Development und Freiheit. Ein Muß für jeden der mit
Datenbanken arbeitet.

`https://corecursive.com/066-sqlite-with-richard-hipp/ <https://corecursive.com/066-sqlite-with-richard-hipp/>`_


Auf SQLite basierend gibt es Erweiterungen, die das Datenbanksystem
nicht nur für die Entwicklung, sondern auch für Produktivsystene
interssant machen.

**Litestream -- Replication**
-----------------------------

*URL*: `Litestream -- Replication <https://litestream.io/>`_
*letzter Zugriff*: 2021-02-13

**Anmerkung** \:\: Litestream

Damit lassen sich Datenbanken in die Amzon-Cloud oder im
Dateisystem replizieren und so Sicherungskopien anlegen.

**Fossil -- CMS/Git**
---------------------

*URL*: `Fossil -- CMS/gitclone <https://fossil-scm.org/home/doc/trunk/www/index.wiki>`_
*letzter Zugriff*: 2021-02-13

**Anmerkung** \:\: Fossil

- `Git verglichen mit Fossil <https://fossil-scm.org/home/doc/trunk/www/index.wiki>`_
