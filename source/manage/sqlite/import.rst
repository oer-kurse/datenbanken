:ref:`« Management SQLite <bkzfrau-start>`

.. _sqlite-import:
   
.. index:: sqlite; import von Daten
	   
=================
Daten importieren
=================

.. image:: ./images//lucas-cranach-adam-eva.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images//lucas-cranach-adam-eva.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images//lucas-cranach-adam-eva.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   
   | Lucas Cranach d. Ä.: Adam und Eva,
   | Herzog-Anton-Ulrich-Museum
   | Serie: Gemälde
            

|a|

Der Import von Daten erfolgt durch:

- einlesen in eine vorhandene Tabelle aus einer CSV/Text-Datei
- durch abarbeiten eines SQL-Scriptes

Import über ein SQL-Script
==========================

.. code-block:: sql

   -- Relation löschen, was beim ersten Aufruf einen
   -- Fehler erzeugt.
   
   DROP TABLE sprichworte;

   -- Neuanlage der Relation
   CREATE TABLE sprichworte (
     id INTEGER PRIMARY KEY AUTOINCREMENT,
     spruch TEXT
   );

   insert into sprichworte values(NULL, 'Alle Wege führen nach Rom.');
   insert into sprichworte values(NULL, 'Aller guten Dinge sind drei.');
   insert into sprichworte values(NULL, 'Allzu klug ist dumm.');
   insert into sprichworte values(NULL, 'Alte Liebe rostet nicht.');
   insert into sprichworte values(NULL, 'Alter schützt vor Torheit nicht.');
   insert into sprichworte values(NULL, 'Am Abend gekammert, am Morgen gejammert.');

.. index:: sqlite; Skript einlesen

Das Ausführen aller Anweisungen erfolgt über den folgenden Aufruf:
::

   .read /pfad/zum/sql-script.sql

.. hint::

   **Windows** und Pfade

   Hier sind die Pfade mit einem \\ (Backslash) getrennt, diese
   müssen wie unter Unix üblich durch einen / (Schrägstrich) ersetzt
   werden!

.. index:: CSV; import (sqlite)
.. index:: import; csv (sqlite)
	   
Import aus einer CSV-Datei
==========================
Folgende Bedingungen sind zu beachten:

- alle Attribute müssen in der CSV einen Wert zugeordnet bekommen 
- leere Einträge werden durch zwei aufeinander folgende Komma dargestellt
- ein automatisches Füllen mit einem autoincrement-Attribut ist nicht möglich,
  aber es gibt einen Umweg (siehe Import ohne ID) ...
- umschalten auf cvs-mode
  
::

   .mode csv
   .import /pfad/zur/csv-datei.csv sprichworte
   .import --csv --skip 1 /pfad/zur/csv-datei.csv sprichworte

1. Die Tabelle anlegen:

   ::

      CREATE TABLE sprichworte (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	spruch TEXT
      );

2. Jede Spalte muss einen passenden Wert aufweisen:
   
   ::

      1,"Alle Wege führen nach Rom."
      2,
      3,"Allzu klug ist dumm."
      4,"Alte Liebe rostet nicht."
      5,"Alter schützt vor Torheit nicht."

Import ohne ID
==============

1. Tabelle ohne ID anlegen

   ::

      CREATE TABLE TempTable (
         spruch TEXT
      );

2. Inhalt der CSV-Datei

   ::

      "Alle Wege führen nach Rom."

      "Allzu klug ist dumm."
      "Alte Liebe rostet nicht."
      "Alter schützt vor Torheit nicht."

3. Import

   ::

      .import /pfad/zur/csv-datei.csv TempTable

   :Windows: Hier sind die Pfade mit einem \\ (Backslash) getrennt, diese
             müssen wie unter Unix üblich durch einen / (Schrägstrich) ersetzt
 	     werden.

4. Tranfer von TempTable zur Zieltabelle (sprichworte)

   ::

      INSERT INTO sprichworte (spruch) SELECT * FROM TempTable;
      DROP TABLE TempTable;

.. index:: Duplikate löschen; sqlite
   
Duplikate entfernen
===================
Struktur der ersten Tabelle:

.. code-block:: sql
		
   CREATE TABLE words (
     'word_id' INTEGER PRIMARY KEY AUTOINCREMENT,
     'listener_id' INT NULL,
     'word' VARCHAR(45) NULL
   );

Anzahl Datensätze:

.. code-block:: sql
		
   select count(*) from words;
   44299
		
1. Zweite Tabelle mit der Struktur der ersten Tabelle anlegen
2. Danach den Tranfer mit folgender Anweisung:

   .. code-block:: sql

      drop table worte;
      CREATE TABLE worte (
        'word_id' INTEGER PRIMARY KEY AUTOINCREMENT,
        'listener_id' INT NULL,
        'word' VARCHAR(45) NULL
      );

      INSERT INTO worte (listener_id, word)
        SELECT DISTINCT listener_id, word FROM words;
   
Anzahl Datensätze:

.. code-block:: sql
		
   select count(*) from worte;
   13112
   
