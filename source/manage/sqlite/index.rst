:ref:`« Startseite zum Kurs <kursstart>`
     
.. _bkzfrau-start:

======
SQLite
======

.. image:: ./images/bobbycar.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bobbycar.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bobbycar.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Dinge

|a|

Download & Entpacken
--------------------

`https://www.sqlite.org/download.html <https://www.sqlite.org/download.html>`_

.. toctree::
   :maxdepth: 1
   :glob:

   *   
   
Wie weiter?
-----------

Einstieg :ref:`Übungsserie zu den Grundlagen (Department-Beispiel) <departmentstart>` 
