=============
Transaktionen
=============
.. index:: Transaktion; BEGIN
.. index:: Transaktion; COMMIT
.. index:: Transaktion; ROLLBACK
.. index:: BEGIN
.. index:: COMMIT
.. index:: ROLLBACK

	   
.. image:: ./images/regenbogen.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/regenbogen.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/regenbogen.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Natur

|a|
            
:ref:`« Übersicht: Manage PostgreSQL <managepostgres>`
     

Transaktionen gruppieren eine Folge von Arbeitsschritten und
garantieren die vollständige Ausführung aller Einzelschritte. 
Scheitert eine Aktion, werden alle Arbeitschritte der Transaktion 
rückgängig gemacht.


Start der Transaktion mit Bestätigung durch COMMIT
==================================================
Verwedent wird die Übungstabelle :ref:`BKZFRAU... <bkzfrau-psql-start>`


::

   BEGIN;

   SELECT COUNT(*) FROM bkzfrau;
   DELETE FROM bkzfrau 
   WHERE name like 'Aal%';
   SELECT COUNT(*) FROM bkzfrau;
   
   COMMIT;

   SELECT COUNT(*) FROM bkzfrau;
   

Start der Transaktion mit ROLLBACK
==================================
Verwedent wird die Übungstabelle :ref:`BKZFRAU... <bkzfrau-psql-start>`

::

   BEGIN;

   SELECT COUNT(*) FROM bkzfrau;
   DELETE FROM bkzfrau 
   WHERE name like '%blume%';
   SELECT COUNT(*) FROM bkzfrau;
   
   ROLLBACK;
   
   SELECT COUNT(*) FROM bkzfrau;

   
