.. _managepostgres:

==========
PostgreSQL
==========

.. INDEX:: Manage; Postgres
.. INDEX:: Postgres; Manage

.. image:: ./images/kunst-am-bau-wasserabweiser.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/kunst-am-bau-wasserabweiser.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/kunst-am-bau-wasserabweiser.jpg">
   </a>


.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Kunst am Bau

|a|

:ref:`« Startseite zum Kurs <kursstart>`

Management
==========


.. toctree::
   :maxdepth: 1

   db
   interna/db

Tabellen
========

.. toctree::
   :maxdepth: 1

   tabellen
   constraints
   indices
   datentypen/index

Standard-Erweiterungen
======================

.. toctree::
   :maxdepth: 1

   views
   views-materialized
   trigger
   schemas
   
Spezial-Funktionen
==================

.. toctree::
   :maxdepth: 1

   nummerierung
   import
   transaktionen
   benutzer
   autovacuum
   optimierungen
   logging

.. toctree::
   :maxdepth: 1
	      
   installation
   migration
   postgres--datensicherung__pg-dump
   
	      
Weitere Infos zum Programm PSQL:

- `psql --  PostgreSQL interactive terminal <https://www.postgresql.org/docs/current/app-psql.html>`_
- `Spickzettel -- Kommados zu sql/psql <https://quickref.me/postgres>`_
  
