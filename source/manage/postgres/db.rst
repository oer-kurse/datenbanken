==================
Kommandozeile/psql
==================

.. image:: ./images/kunst-am-bau-streetart-florenz.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/kunst-am-bau-streetart-florenz.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/kunst-am-bau-streetart-florenz.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Graffiti

|a|
            
:ref:`« Übersicht: Manage PostgreSQL <managepostgres>`
     
.. index:: psql; verbinden

   
Die Arbeit mit der Datenbank beginnt mit dem Aufruf des Programmes.
Alternativ kann sofort eine Datenbankdatei als Parameter übergeben werden.


.. index:: psql-shell; Verbinden zur DB
.. index:: Manage; Verbinden zur DB (postgres)

	   
Verbinden mit dem Server über die Kommandozeile (psql)
======================================================
::

   /pfad/zu/psql -h localhost -p 5432 -U postgres
   /pfad/zu/psql -h localhost -p 5432 -U postgres -d meineDatenbank   

.. index:: psql; Hilfe
.. index:: Manage; Hilfe (postgres)
.. INDEX:: Encoding; Windows
.. INDEX:: Windows; Encoding

Das Encoding unter Windows
==========================
Leider ist es mit der Anzeige von dt. Umlauten unter Windows etwas
schwierig. Nur besondere Einstellunng führen zu einer exakten
Darstellung. Der Start erfolgt an der Konsole:

.. tab:: Schritt 1

   ::

      cmd

.. tab:: Schritt 2

   ::

      chcp 65001
      "C:\Program Files\PostgreSQL\16\bin\psql.exe" -U postgres -d
      postgres

.. tab:: Schritt 3

   ::
      
      set client_encoding='utf8';

.. todo:: Encoding

   Vergleichbare Einstellungen für pgAdmin finden.

Besser ist es, Postgres auf einem Linux-System zu betreiben.
	   
Hilfe zu den SQL-Kommandos
==========================
::

   \h

Hilfe zu den PSQL-Kommandos
::

   \?

Auflistung verfügbarer Datenbanken:
===================================

.. index:: Liste; aller Datenbanken (postgres)
.. index:: Datenbanken; Liste aller Namen (postgres)
.. index:: Postgres; Liste aller Datenbanken
	   
.. tab:: psql

   ::

      \l
      
.. tab:: sql

   ::

      SELECT datname FROM pg_database;
 
.. _create_database_postgres:

.. index:: create; database
.. index:: DB; anlegen (postgres)
	   
Neue Datenbank erstellen
========================
::

   create database meinedb;

   -- Neue als Kopie einer vorhandnene Dabenbank
   
   create database grundlagen template basics;
   

Kann nicht Teil einer Transaktion sein.

.. index:: DB; wechseln

Datenbank verwenden/wechseln (connect):
=======================================
::

   \c <dbname>


.. index:: Anzeige: pager an/aus
  
Pager deaktivieren
==================

Ich will alle Zeilen sehen 
::

   \pset pager false
   \pset pager true

.. index:: timing (Zeitmessung)
.. index:: Zeitmessungen (timing)
	   
Zeitmessung an- und ausschalten
===============================
::

   \timing

.. index:: Ansicht; umschalten

Umschalten zw. Tabellen- und Record-Ansicht
===========================================
Für überlange Spalten ist die Ausgabe im Record-Modus eventuell etwas
leichter zu lesen. Funktioniert wie ein an/aus-Schalter.

::

   \x
	   
Anzeige von NULL-Werten
=======================

.. index:: NULL-Wert anzeigen

::

   \pset null NULL


   
