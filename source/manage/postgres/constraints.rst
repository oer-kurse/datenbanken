===========
Constraints
===========


Einschränkungen, Beschränkung, Zwang

..  image:: ./images/alf2.jpg
    :width: 0	    


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/alf2.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/alf2.jpg">
   </a>


.. sidebar:: Alf 

   
   |b|

   Serie: Kuscheltiere

|a|


.. table::

    +-------------+---------------------------------------------------------------------------------------------------------+
    | Constraint  | Aufgabe/Funktion                                                                                        |
    +=============+=========================================================================================================+
    | NOT NULL    | keine leeren Attribute                                                                                  |
    +-------------+---------------------------------------------------------------------------------------------------------+
    | UNIQUE      | kein Attribut mit dem gleichen Wert                                                                     |
    +-------------+---------------------------------------------------------------------------------------------------------+
    | PRIMARY KEY | Eine Kombination aus NOT NULL und UNIQUE. Jedes Tuple besitzt eine eindeutige Kennung                   |
    +-------------+---------------------------------------------------------------------------------------------------------+
    | FOREIGN KEY | Ein eindeutiger Schlüssel aus einer anderen Relation                                                    |
    +-------------+---------------------------------------------------------------------------------------------------------+
    | CHECK       | Überprüfen einer Bedingung, Regel, Einschränkung die erfüllt sein muss, bevor der Wert gespeichert wird |
    +-------------+---------------------------------------------------------------------------------------------------------+
    | DEFAULT     | Stadardwert für ein Attribut                                                                            |
    +-------------+---------------------------------------------------------------------------------------------------------+



.. _create-tables-constraint:

Beispiel
--------

Neue Relationen (Produkte, Verkäufe) mit Constaints
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    DROP TABLE produkte;
    CREATE TABLE produkte(
      id                      SERIAL,
      art_nr                  VARCHAR(100),
      bezeichnung             VARCHAR(200) NOT NULL,
      beschreibung            TEXT,
      preis DECIMAL(10,2)     DEFAULT '0.00' CHECK (preis > 0),
      steuersatz DECIMAL(5,3) DEFAULT '0.190' CHECK (steuersatz > 0),
      PRIMARY KEY (id)
    );

    DROP TABLE verkaeufe;
    CREATE TABLE verkaeufe (
      verkdatum varchar(30),
      filiale varchar(20),
      produkt int REFERENCES produkte(id) ON DELETE CASCADE,
      anzahl int,
      kunde int,
      verkaeufer int);

Neue Relationen ohne Constraints
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    DROP TABLE produkte;
    CREATE TABLE produkte(
      id            INTEGER,
      art_nr        VARCHAR(100),
      bezeichnung   VARCHAR(200),
      beschreibung  TEXT,
      preis         DECIMAL(10,2),
      steuersatz    DECIMAL(5,3)
    );

    DROP TABLE verkaeufe;
    CREATE TABLE verkaeufe (
      verkdatum varchar(30),
      filiale varchar(20),
      produkt int,
      anzahl int,
      kunde int,
      verkaeufer int);

**Hinweis:** DECIMAL(5,3) bedeutet fünfstellig mit 3 Nachkommastellen

Constraints ergänzen, wenn Relation schon existiert:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    ALTER TABLE produkte
    ADD CONSTRAINT produkte_pk PRIMARY KEY (id);

    ALTER TABLE produkte
    ALTER COLUMN bezeichnung SET NOT NULL;

    ALTER TABLE produkte
    ADD CONSTRAINT produkte_art_nr_unique UNIQUE (art_nr);

    ALTER TABLE produkte
    ADD CONSTRAINT produkte_preis_check CHECK (preis > 0);

    ALTER TABLE produkte
    ADD CONSTRAINT produkte_steuersatz_check CHECK (steuersatz > 0);

    ALTER TABLE verkaeufe
    ADD CONSTRAINT fk_produkte FOREIGN KEY (produkt) REFERENCES produkte (id);

Welche Constraints gibt es?
---------------------------

.. code:: sql


    SELECT
       *
    FROM
      information_schema.constraint_table_usage
    WHERE
      table_name like 'produkte';

    -- alternativ

    SELECT
      *
    FROM
      information_schema.constraint_column_usage
     WHERE
       table_name like 'produkte';

**Alternative Anzeige in psql:** \d <tabellenname>

.. code:: text


      \d verkaeufe
    		   Table "public.verkaeufe"
       Column   |         Type          | Collation | Nullable | Default 
    ------------+-----------------------+-----------+----------+---------
     verkdatum  | character varying(30) |           |          | 
     filiale    | character varying(20) |           |          | 
     produkt    | integer               |           |          | 
     anzahl     | integer               |           |          | 
     kunde      | integer               |           |          | 
     verkaeufer | integer               |           |          | 
     Foreign-key constraints:
       "fk_produkte" FOREIGN KEY (produkt) REFERENCES produkte(id)

Constraint entfernen
--------------------

.. code:: sql


    ALTER TABLE produkte DROP CONSTRAINT produkte_steuersatz_check;

Neuer Constraint mit RegExp
---------------------------

Über einen regulären Ausdruck können Suchmuster als Einschränkungen
defiert werden. Im folgende Beispiel sind nur Großbuchstaben und
auch nicht mehr als drei und nicht weniger als zwei erlaubt.

.. code:: sql


    ALTER TABLE images ADD CONSTRAINT
    nur_buchstaben check (kuerzel_lk ~ '^[A-Z]{2,3}$');
