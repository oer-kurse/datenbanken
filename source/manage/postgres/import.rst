===============
 Im- und Export
===============

.. image:: ./images/kunst-am-bau-olympia.jpg
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/kunst-am-bau-olympia.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/kunst-am-bau-olympia.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Kunst am Bau

|a|
            
:ref:`« Übersicht: Manage PostgreSQL <managepostgres>`
     

Der Import von Daten erfolgt durch:


- einlesen in eine vorhandene Tabelle aus einer CSV/Text-Datei
- durch abarbeiten eines SQL-Scripts

.. _postgres-import:
  
Im- und Export von Daten
========================

.. hint:: für **Windows-Benutzer**

   der Backslash (\\) muss durch einen
   Schrägstrich (/) ersetzt werden.

   .. table::

      +--------+--------------------------------------------+
      | aus:   | \\i C:\\home\\sql\\data-gewaesser.sql      |
      +--------+--------------------------------------------+
      | wird:  | \\i C:/home/sql/data-gewaesser.sql         |
      +--------+--------------------------------------------+
      | sonst: | Fehlermeldung -->  C\:\\ Permission denied |
      +--------+--------------------------------------------+


Import
------

.. index:: import; copy
.. index:: import; COPY
	   
Die Beispieldateien:


:download:`bkzfrau-utf8.txt <bkzfrau-utf8.txt>`

:download:`bkzfrau-utf8.sql <bkzfrau-utf8.sql>`

	  
.. code-block:: bash

   -- Das doppelte Minus leitet in SQL-Scripten einen Kommentar ein
   -- \copy (klein geschreiben) wird auf dem Client ausgeführt
   -- die Realationen müssen bereits existieren
   -- die Reihenfolge der Daten muss der Reihenfolge der Attribute
   -- entsprechen...

   CREATE TABLE bkzfrau (gnum INTEGER, beruf VARCHAR(150));
   
   \copy bkzfrau from 'c:\home\bkzfrau-utf8.txt' using delimiters '#';

   -- COPY (groß geschrieben) wird auf dem Server ausgeführt
   
   COPY bkzfrau from 'c:\home\bkzfrau-utf8.txt' using delimiters '#';


.. index:: Übung; SQL-Scripte ausführen (bkzfrau)
.. index:: Übung; SQL-Scripte ausführen (bkzfrau)
.. index:: psql-shell; SQL-Skritpe ausführen
.. index:: manage; SQL-Skritpe ausführen
.. index:: Skripte; Postgres
.. index:: SQL-Skripte; Postgres

Import von Daten mit SQL-Anweisungen
====================================
.. index:: Windows; Pfade für import
.. index:: Fehler; Permission denied

   
.. index:: import; SQL-Script

.. code-block:: bash

   \i /absoluter/pfad/zum/bkzfrau.sql  

.. index:: psql-shell; Ausgabe in Datei umleiten
.. index:: manage; Ausgabe in Datei umleiten


Import von Daten aus einer CSV-Datei
====================================

.. INDEX:: import (postgres); csv
.. INDEX:: csv; import (postgres)

.. code-block:: bash

   \COPY worte FROM  'C:\home\viele_worte.csv' WITH (FORMAT csv);

	   
Export durch Ausgabeumleitung in eine Datei
===========================================
.. code-block:: bash

   \o ausgabe-datei.txt

   -- und die Ausgabe wieder umschalten in Richtung Konsole
   -- durch weglassen des Parameters
   
   \o

.. index:: psql-shell; Header unterdrücken
	   
Ausgabe ohne Header
-------------------
Ein- bzw. Ausschalten:
::

   \t

.. index:: Export; COPY und copy
   
Export
------
.. code-block:: bash

   -- Export der Daten in eine Datei ...
   
   COPY bkzfrau TO '/home/peter/export.txt';
   \copy bkzfrau TO '/home/peter/export.txt';

.. index:: Postgres; Export als CSV
.. index:: CSV; export (postgres)
.. index:: Export; CSV (postgres)
   
Export als CSV
==============
Das Beispiel bezieht sich auf das "Hidden-Cosmos" Beispiel für Neo4j.

Hinweis: Dort wird der Name *hico2* als Schema verwendet!
Vergessen Sie nicht, auf dieses Schema umzuschalten:

Export als CSV-Datei, am Beispiel der Autoren
::

   \c hiddencosmos
   set search_path to hico2;
   \copy authors to 'hico3-authors.csv' csv header;
   COPY 9

	   
