
.. index:: Trigger

:ref:\`« Übersicht: Manage PostgreSQL <managepostgres>\`

=========
 Trigger
=========

Regeln für Trigger
~~~~~~~~~~~~~~~~~~

- insert, update und delete-Aktionen

- können vor oder nach der Aktion ausgelöst werden

- können auf ein Statement reagieren

- können auf Tupel reagieren

- sind Funktionen, die keine Parameter kennen

Beispieltabelle erstellen
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    CREATE TABLE COMPANY(
       ID INT PRIMARY KEY     NOT NULL,
       NAME           TEXT    NOT NULL,
       AGE            INT     NOT NULL,
       ADDRESS        CHAR(50),
       SALARY         REAL
    );

Tabelle für das Protokoll
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    CREATE TABLE AUDIT(
       EMP_ID INT NOT NULL,
       ENTRY_DATE TEXT NOT NULL
    ); 

Die Trigger-Funktion
~~~~~~~~~~~~~~~~~~~~

.. code:: bash


    CREATE OR REPLACE FUNCTION auditlogfunc() RETURNS TRIGGER AS $example_table$
       BEGIN
          INSERT INTO AUDIT(EMP_ID, ENTRY_DATE) VALUES (new.ID, current_timestamp);
          RETURN NEW;
       END;
    $example_table$ LANGUAGE plpgsql;

Trigger an Tabelle binden
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    CREATE TRIGGER example_trigger AFTER INSERT ON COMPANY
    FOR EACH ROW EXECUTE PROCEDURE auditlogfunc();

Änderungsaktion mit Protokoll
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)
    VALUES (3, 'Paul', 32, 'California', 20000.00 );

Trigger wieder entfernen
~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    DROP TRIGGER example_trigger on company;

welche Trigger hat die Tabelle?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    \d company

Welche Trigger befinden sich im System?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    SELECT * FROM pg_trigger;

Weiterführende Links
~~~~~~~~~~~~~~~~~~~~

`https://www.postgresql.org/docs/11/plpgsql-trigger.html <https://www.postgresql.org/docs/11/plpgsql-trigger.html>`_

Änderungen in Tabellen protokollieren
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- `https://wiki.postgresql.org/wiki/Audit_trigger_91plus <https://wiki.postgresql.org/wiki/Audit_trigger_91plus>`_

- `https://github.com/2ndQuadrant/audit-trigger <https://github.com/2ndQuadrant/audit-trigger>`_
