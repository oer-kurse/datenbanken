.. index:: Schema (Postgres)
	   
==========
 Schemata
==========

.. image:: ./images/schnecke.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/schnecke.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/schnecke.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Tiere

|a|
            
:ref:`« Übersicht: Manage PostgreSQL <managepostgres>`
     
Ein Schemata dienen der Abgrenzung und Gruppierung von Objekten.
In andren Kontexten spricht man auch von Namensräumen, um Namenskonflikte
zu vermeiden. In XML und anderen Sprachen, wird der Begriff *namespace*
verwendet.

https://www.postgresql.org/docs/9.1/static/ddl-schemas.html

.. index:: Schema (Postgres); anlegen
	   
Alle Schemata anzeigen
======================

.. code-block:: sql

   create schema mein_schema;

.. index:: Schema (Postgres); auflisten
	   
Alle Schemata anzeigen
======================

.. code-block:: sql

   select nspname
   from pg_catalog.pg_namespace;
   
.. index:: Schema (Postgres); aktueller Suchpfad
	   
Suchpfad für Objekte
====================

.. code-block:: sql
		
   SHOW search_path;

     search_path   
   -----------------
   "$user", public
   
.. index:: Schema (Postgres); Suchpfad ändern
	   
Suchpfad ändern
===============
.. code-block:: sql
		
   SET search_path TO myschema;

.. index:: Schema (Postgres); drop
	   
Schema mit allen Objekten löschen
=================================
.. code-block:: sql

   DROP SCHEMA myschema CASCADE;


.. index:: grant; SCHEMA
.. index:: Schema (Postgres); grant

Zugriff auf Schema erlauben
===========================

.. code-block:: sql
		
   GRANT SELECT ON ALL TABLES IN SCHEMA myschema TO PUBLIC;


.. index:: Schema (Postgres); Zugriffsrechte
   
Zu einem späteren Zeitpunkt hinzugefügte Relationen haben nicht die
gleichen Zugriffsrechte, wie die vorhanden Relationen.

Dies lässt sich wie folgt korrigieren:


.. code-block:: sql
   
   ALTER DEFAULT PRIVILEGES IN SCHEMA myschema
   GRANT SELECT ON TABLES TO PUBLIC;

Schemen-Name ändern
===================

.. code-block:: sql

.. index:: Tabelle; verschieben (in anderes Schema)
		
Relation verschieben
====================

.. code-block:: sql
   
   ALTER TABLE cust
   SET SCHEMA anotherschema;		
   
