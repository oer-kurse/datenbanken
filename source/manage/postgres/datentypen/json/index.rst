=================
JSON --  -- NoSQL
=================


.. org::

.. _6a07cb2e-edf8-4cda-bf5c-d89006b8f2b6:
.. _startjson:

.. INDEX:: JSON; NoSQL
.. INDEX:: NoSQL; JSON
.. INDEX:: json; Postgres
.. INDEX:: Postgres; Datentyp -- json

.. image:: ./images/mantis.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../../_images/mantis.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../../_images/mantis.webp">
   </a>


.. sidebar:: SQL-Kurs

   Mantis Religiosa
   |b|
   Serie: Natur

|a|

:ref:`« Startseite zum Kurs <kursstart>`

:download:`Download: Cheat Sheet zu JSON-Funktionen <files/postgresql_jsonb_cheatsheet_en.pdf>`. [#f1]_

	  
.. toctree::
   :maxdepth: 1
   :glob:     

   *   

.. [#f1] https://aiven.io/developer/postgresql-jsonb-cheatsheet
