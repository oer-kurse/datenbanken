=========
JSON-Demo
=========


.. INDEX:: Datentyp; JSON
.. INDEX:: JSON; Datentyp
.. _20231122T210327:


.. meta::

   :description lang=de: Postgres: Datenbyp json.

   :keywords: PostgreSQL, json

Vorbereitung
------------

Anlegen einer Mini-Relation mit einem Datensatz.«

.. code:: sql


    create table jsondemo (id int, query json);
    insert into jsondemo (id, query)
    values (1, ' {
       "id": 43,
       "cat": "6",
       "qry_type": "rb", "qry": {
          "question": ["Benutzen Sie private Messenger fuer ",
                       "organisatorische Absprachen mit Kollegen und Kunden?"],
          "answers": ["ja", "nein"],
          "feedback": ["Messenger speichern die  Daten auf Servern, ",
                       "deren Standort wie auch die Zugriffsrechte",
                       "unbekannt sind. Identisch mit Frage 42 (pk)"]
        }
    }');

Für eine übersichtliche Darstellung, wurden Frage und Feedback als
Array angelegt. Weiter unten wird gezeigt, wie die Array-Elemente
wieder zu einer Zeichenkette zusammengefügt werden.

Abfragen
--------

Der Zugriff auf einen Key (hier die ID):

.. code:: sql

    select query->'id' from jsondemo;

Beachte doppeltes größer als Zeichen »->>« Zeichen:

.. code:: sql

    select query->>'id' from jsondemo;


Der Datentyp läßt sich noch ändern, beachte die Unterschiede
zwische beiden Datentypten.[1]

.. code:: sql

    ALTER TABLE jsondemo ALTER COLUMN query TYPE jsonb;

Noch einmal der Zugriff auf einen Key (hier die Katalog-ID).

.. code:: sql

    select
      query->'cat' as kategorie
    from
      jsondemo;

Lateral Join
~~~~~~~~~~~~

.. index:: lateral; Join
.. index:: Join; lateral


Die Frage und das Feedback wurden als Array formatiert, um eine
kompakte Darstellung zu erhalten (siehe oben die Vorbereitungen).
Das JSON-Format  erlaubt keine Zeilenumbrüche für lange Texte!
In der Ausgabe können die Array-Elemente wieder zu einer einzigen
Zeile zusammengefügt werden. Dies gelingt hier mit einem `LATERAL JOIN`.
Das LATERAL-Schlüsselwort ist besonders nützlich, wenn man auf Spalten
aus vorherigen Tabellenreferenzen in der FROM-Klausel zugreifen möchte.
Im folgenden Fall kann das Schlüsselwort `LATERAL` auch weggelassen werden.

.. code:: sql

    SELECT
      string_agg(element, ' ') AS result_string
    FROM
      jsondemo
    JOIN LATERAL
        json_array_elements_text(query->'qry'->'question') AS element ON true;

Weiterführende Links
~~~~~~~~~~~~~~~~~~~~

| [1] :ref:`Unterschiede zw. json/jsonb <20221216T143903>`
| [2] :ref:`Weitere Beispiele zum JSON-Datentyp<da430aa9-e2e7-4845-b057-5c6600e81833>`
