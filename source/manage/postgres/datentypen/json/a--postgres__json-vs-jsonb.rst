==============
JSON vs. JSONB
==============

.. _20221216T143903:

.. INDEX:: Postgres; JSON/JSONB
.. INDEX:: JSON/JSONB; Postgres

.. code:: bash


    SELECT '{"c":0,   "a":2,"a":1}'::json, '{"c":0,   "a":2,"a":1}'::jsonb;

              json          |        jsonb
    ------------------------+------------------
     {"c":0,   "a":2,"a":1} | {"a": 1, "c": 0}
    (1 row)


- json: text gespeichert »wie er ist«

- jsonb: keine Leerzeichen

- jsonb: keine Schlüssel doppelt, der letzte Schlüssel gewinnt

- jsonb: Schlüssel sind sortiert


**Quelle** \:\: `Diskussion auf Stackoverflow <https://stackoverflow.com/questions/22654170/explanation-of-jsonb-introduced-by-postgresql>`_
