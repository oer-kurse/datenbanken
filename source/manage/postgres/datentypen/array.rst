=====
Array
=====



.. meta::

   :description lang=de: Postgres: Datenbyp array.  
   
   :keywords: PostgreSQL, array


.. image:: ./images/suprematismus02.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/suprematismus02.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/suprematismus02.webp">
   </a>

.. sidebar:: Supremalismus

   |b|

   Serie: Kunst

|a|

   
Download: :download:`Lottozahlen 2021 (sql-Datei) <./files/lotto-2021.sql>`

Fragen und Antworten
~~~~~~~~~~~~~~~~~~~~

- Wie werden Array-Werte gespeichert?

- Wie auf die Werte eines Array zugreifen?

- Siehe auch Station »Meßwerte« dort wird der Array-Typ mit JSON
  und XML verglichen.

- Laden Sie die Beispiel-Daten herunter und befüllen Sie die
  folgende Tabelle mit den Lottozahlen.

Tabelle anlegen und befüllen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~


   .. code:: sql
   
      drop table if exists lotto;
      create table lotto ( 
	id int,
	datum text, 
	ziehung integer[],
	zusatzzahl int
      );

Zugriff über den Array-Index
~~~~~~~~~~~~~~~~~~~~~~~~~~~~


   .. code:: sql


      select
        id,
        ziehung[1] as "1. Zahl",
        ziehung[2] as "2. Zahl",
        ziehung[3] as "3. Zahl",
        ziehung[4] as "4. Zahl",
        ziehung[5] as "5. Zahl",
        ziehung[6] as "6. Zahl",
        zusatzzahl as "ZZ"
      from lotto
      where id < 10;

Testausgabe
-----------


.. code:: sql

    id | 1. Zahl | 2. Zahl | 3. Zahl | 4. Zahl | 5. Zahl | 6. Zahl | ZZ 
   ----+---------+---------+---------+---------+---------+---------+----
     1 |       7 |       8 |      16 |      28 |      30 |      34 |  6
     2 |      14 |      21 |      32 |      37 |      38 |      44 |  6
     3 |       7 |      16 |      19 |      20 |      30 |      41 |  5
     4 |       7 |      23 |      26 |      38 |      40 |      43 |  6
     5 |       1 |       5 |      15 |      35 |      37 |      45 |  3
     6 |       5 |       6 |      11 |      31 |      35 |      42 |  0
     7 |      12 |      13 |      16 |      18 |      32 |      49 |  7
     8 |       1 |       4 |      15 |      16 |      44 |      45 |  3
     9 |       6 |      30 |      33 |      36 |      40 |      43 |  1
   (9 rows)
