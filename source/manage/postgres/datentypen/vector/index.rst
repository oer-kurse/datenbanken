.. _managepostgres:

======
Vector
======

.. INDEX:: Vector; Postgres
.. INDEX:: Postgres; Datentyp -- Vector

.. image:: ./images/zackeneule.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../../../../_images/zackeneule.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../../../../_images/zackeneule.webp">
   </a>


.. sidebar:: SQL-Kurs

   Zackeneule
   |b|
   Serie: Natur

|a|

:ref:`« Startseite zum Kurs <kursstart>`

.. toctree::
   :maxdepth: 1
   :glob:     

   *   
