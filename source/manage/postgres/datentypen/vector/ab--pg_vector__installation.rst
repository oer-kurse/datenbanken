=========================
pg_vector -- Installation
=========================


.. _20231003T201130:
.. INDEX:: pg_vector (postgres);Installation 
.. INDEX:: Installation;pg_vector (postgres) 

Dokumentation  [1]_ 

.. code:: bash


    cd /tmp
    git clone --branch v0.5.0 https://github.com/pgvector/pgvector.git
    cd pgvector
    make
    make install # may need sudo

.. code:: sql


    CREATE EXTENSION vector;

Quellen
-------


.. [1] `https://github.com/pgvector/pgvector <https://github.com/pgvector/pgvector>`_
