// https://github.com/pgvector/pgvector

create table vectorkarte (
  id SERIAL PRIMARY KEY,
  plz Integer NOT NULL,
  ort varchar(45) NOT NULL,
  koordinaten vector(2),
  );
  
insert into vectorkarte (plz, ort, koordinaten) values (14959,'Trebbin OT Kleinbeuthen',[13.193184,52.258174]);
insert into vectorkarte (plz, ort, koordinaten) values (14959,'Trebbin OT Christinendorf',[13.281649,52.211374]);
insert into vectorkarte (plz, ort, koordinaten) values (15517,'Fürstenwalde/Spree',[14.020111,52.354694]);
insert into vectorkarte (plz, ort, koordinaten) values (14624,'Dallgow-Döberitz',[13.135222,52.513167]);
insert into vectorkarte (plz, ort, koordinaten) values (01945,'Ruhland Gemeindeteil Arnsdorf',[13.866661,51.430356]);
insert into vectorkarte (plz, ort, koordinaten) values (01945,'Ruhland Gemeindeteil Arnsdorf',[13.866628,51.430383]);
insert into vectorkarte (plz, ort, koordinaten) values (01968,'Senftenberg OT Brieske',[13.967871,51.496972]);
insert into vectorkarte (plz, ort, koordinaten) values [13127,'Berlin Pankow, Französisch Buchholz',[13.43373,52.604356]);
insert into vectorkarte (plz, ort, koordinaten) values (03222,'Lübbenau/Spreewald',[13.941668,51.86813]);
insert into vectorkarte (plz, ort, koordinaten) values (14959,'Trebbin',[13.247587,52.248657]);
insert into vectorkarte (plz, ort, koordinaten) values (10827,'Berlin Schöneberg',[13.347364,52.479256]);
insert into vectorkarte (plz, ort, koordinaten) values (56263,'Treis-Karden',[7.304659,50.176501]);
insert into vectorkarte (plz, ort, koordinaten) values (15746,'Klein Köris',[13.698789,52.147244]);
insert into vectorkarte (plz, ort, koordinaten) values (14959,'Trebbin OT Glau',[13.151111,52.240833]);
insert into vectorkarte (plz, ort, koordinaten) values (01968,'Senftenberg-Sedlitz',[14.0554,51.550947]);
insert into vectorkarte (plz, ort, koordinaten) values (14552,'Michendorf',[13.083815,52.330021]);
insert into vectorkarte (plz, ort, koordinaten) values (14089,'Spandau Kladow',[13.162384,52.458894]);
insert into vectorkarte (plz, ort, koordinaten) values (06925,'Annaburg',[13.131653,51.748028]);
insert into vectorkarte (plz, ort, koordinaten) values (15838,'Wünsdorf',[13.458034,52.171408]);
insert into vectorkarte (plz, ort, koordinaten) values (99518,'Großheringen',[11.663859,51.102381]);
insert into vectorkarte (plz, ort, koordinaten) values (15806,'Zossen OT Dabendorf',[13.421144,52.245192]);
insert into vectorkarte (plz, ort, koordinaten) values (67292,'Kirchheimbolanden',[8.007653,49.658985]);
insert into vectorkarte (plz, ort, koordinaten) values (14193,'Berlin Grunewald',[13.22356,52.47906]);
insert into vectorkarte (plz, ort, koordinaten) values (12623,'Berlin Mahlsdorf',[13.63127,52.517351]);
insert into vectorkarte (plz, ort, koordinaten) values (14949,'Trebbin OT Kliestow',[13.219337,52.19136]);
insert into vectorkarte (plz, ort, koordinaten) values (15838,'Am Mellensee OT Rehagen',[13.376386,52.1667485]);
insert into vectorkarte (plz, ort, koordinaten) values (03249,'Sonnewalde',[13.725718,51.687464]);
insert into vectorkarte (plz, ort, koordinaten) values (03249,'Sonnewalde OT Pahlsdorf',[13.673845,51.72936]);
insert into vectorkarte (plz, ort, koordinaten) values (14943,'Luckenwalde',[13.175685,52.088379]);
insert into vectorkarte (plz, ort, koordinaten) values (14959,'Trebbin OT Blankensee',[13.130584,52.241872]);
insert into vectorkarte (plz, ort, koordinaten) values (14822,'Linthe',[12.783632,52.154288]);
insert into vectorkarte (plz, ort, koordinaten) values (14554,'Seddin OT Seddin',[13.016042,52.268504]);
insert into vectorkarte (plz, ort, koordinaten) values (15517,'Fürstenwalde/Spree',[14.068292,52.357071]);
insert into vectorkarte (plz, ort, koordinaten) values (14959,'Trebbin OT Kleinbeuthen',[13.193184,52.258174]);
insert into vectorkarte (plz, ort, koordinaten) values (06132,'Halle (Saale)',[11.976434,51.448151]);
insert into vectorkarte (plz, ort, koordinaten) values (06132,'Halle (Saale)',[11.976434,51.448151]);
insert into vectorkarte (plz, ort, koordinaten) values (12524,'Berlin Treptow-Köpenick',[13.550909,52.407538]);
insert into vectorkarte (plz, ort, koordinaten) values (03226,'Vetschau OT Raddusch',[14.037161,51.820303]);
insert into vectorkarte (plz, ort, koordinaten) values (03205,'Calau',[13.946183,51.746977]);
insert into vectorkarte (plz, ort, koordinaten) values (15838,'Am Mellensee',[13.350443,52.155722]);
insert into vectorkarte (plz, ort, koordinaten) values (14548,'Schwielowsee OT Caputh',[13.015612,52.343977]);
insert into vectorkarte (plz, ort, koordinaten) values (14959,'Trebbin OT Klein Schulzendorf',[13.242753,52.20275]);
insert into vectorkarte (plz, ort, koordinaten) values (66679,'Losheim am See',[6.742028,49.518006]);
insert into vectorkarte (plz, ort, koordinaten) values (14974,'Ludwigsfelde',[13.25046,52.32007]);
insert into vectorkarte (plz, ort, koordinaten) values (15859,'Storkow',[13.934985,52.258236]);
