==================
Vector -- Datentyp
==================


.. _20231003T194434:
.. INDEX:: Vector; Datentyp
.. INDEX:: Datentyp; Vector

`https://theblue.ai/blog-de/vektor-datenbanken/ <https://theblue.ai/blog-de/vektor-datenbanken/>`_
`https://de.wikipedia.org/wiki/Support_Vector_Machine <https://de.wikipedia.org/wiki/Support_Vector_Machine>`_

Was ist ein Vector?
-------------------

Ich bin kein Mathematikexperte, aber ich sehe das so, dass es
eine Reihe von Dingen gibt, die in irgendeiner Weise zusammenhängen,
zum Beispiel eine Liste von Städten. Anhand dieser Liste können Sie
jede Stadt in verschiedenen Dimensionen beschriften. Beispielsweise
könnte eine Dimension die Bevölkerung sein, eine andere könnte das
Land sein, eine andere könnte die durchschnittliche
Jahrestemperatur sein. Die Beschriftung ist der wichtigste Teil, aber
auch der mühsamste Teil beim Codieren. Normalerweise bedeutet das,
dass Sie nur Beschriftungen und Bemaßungen erstellen, die für Ihre
Arbeit von Bedeutung sind.
