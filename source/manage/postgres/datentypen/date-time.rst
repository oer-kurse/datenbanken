==============
Datum und Zeit
==============

.. image:: ./images/teddy.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/teddy.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/teddy.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Meditation
   |b|
   Serie: Spass

|a|

:ref:`« Übersicht: Manage PostgreSQL <managepostgres>`
	     
.. index:: date, time interval, timestamp
.. index:: Datentyp; date und time

An zwei Tabellen werden unterschiedlichste Speicher- und
Manipulationsvarianten für Zeit- und Datums-Angaben gezeigt.

- Legen Sie die Tabellen an und führen Sie die gezeigten Beispiele aus.
- Lassen Sie mit dem select-Statment die gespeicherten Werte zeigen.
- Gibt es weitere Varianten? Suchen Sie im Netz nach Beispielen...
  
.. code:: bash

   create table zeit (
     zeit time,
     datum date,
     stempel timestamp,
     zeitspanne interval);


     \d zeit
                 Table "public.zeit"
      Column   |            Type             | Modifiers 
   ------------+-----------------------------+-----------
    zeit       | time without time zone      | 
    datum      | date                        | 
    stempel    | timestamp without time zone | 
    zeitspanne | interval                    | 

    create table zeit_tz (
     zeit time with time zone,
     datum date,
     stempel timestamp with time zone,
     zeitspanne interval);

     \d zeit_tz 
              Table "public.zeit_tz"
      Column   |           Type           | Modifiers 
   ------------+--------------------------+-----------
    zeit       | time with time zone      | 
    datum      | date                     | 
    stempel    | timestamp with time zone | 
    zeitspanne | interval                 | 

.. index:: Jahr (aus Datum); extrahieren
.. index:: extrahieren; Jahr (aus Datum)

Einfügen/Selektieren ohne Zeitzone
==================================

.. code:: sql

   insert into zeit (zeit) values (now());
   insert into zeit (datum) values (now());
   insert into zeit (stempel) values (now());
   insert into zeit (zeitspanne) values (AGE('1957-03-02', now()));
   insert into zeit (zeitspanne) values (AGE(now(), '1957-03-02'));

   
   
   select to_char(datum, 'DD.MM.YYYY HH24:MI') from zeit;
   select extract(year from datum) from zeit;

   --  HINWEIS: für Berechnungen ohne Tabellenwerte wird eine
   --           virtuelle Tabelle generiert.
   	      
   select now() + interval '1 day 3 hour' as Abgabetermin;

   -- In Oracle gibt es für Berechnungen ohne Tabellenwerte
   -- die virtuelle Tabelle »dual«.

   select date_trunc('hour', now()  + interval '1 day 3 hour') as Abgabetermin;

Einfügen/Selektieren mit Zeitzone
=================================

.. code:: sql

   insert into zeit_tz (zeit) values (now());
   insert into zeit_tz (datum) values (now());
   insert into zeit_tz (stempel) values (now());
   insert into zeit_tz (zeitspanne) values (AGE('1957-03-02', now()));
   insert into zeit_tz (zeitspanne) values (AGE(now(), '1957-03-02'));


   -- Achtung: Ausgabe als virtuelle Tabelle
   
   select now() + interval '1 day 3 hour' as Abgabetermin;
   select date_trunc('hour', now()  + interval '1 day 3 hour') as Abgabetermin;

   -- Virtuelle Tabelle in Oracle
   select now() + interval '1 day 3 hour' as Abgabetermin from dual;

   
   select to_char(datum, 'DD.MM.YYYY HH24:MI') from zeit_tz;
   select extract(year from datum) from zeit_tz;
