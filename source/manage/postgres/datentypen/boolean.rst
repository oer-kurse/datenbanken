
:ref:\`« Übersicht: Manage PostgreSQL <managepostgres>\`


.. meta::

   :description lang=de: Postgres: Datenbyp Boolean.  
   
   :keywords: PostgreSQL, Boolean


.. image:: ./images/suprematismus03.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/suprematismus03.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/suprematismus03.webp">
   </a>

.. sidebar:: Supremalismus

   |b|

   Serie: Kunst

|a|

Boolean
-------

Tabelle und Daten
~~~~~~~~~~~~~~~~~

.. code:: sql


    create table lagerbestand (isbn text, auf_lager boolean);
    insert into lagerbestand values ('3897217600',true);
    insert into lagerbestand values ('9783407792303','t');
    insert into lagerbestand values ('9783527815470','true');
    insert into lagerbestand values ('978-3772474019',false);
    insert into lagerbestand values ('9783899052664','0');
    insert into lagerbestand values ('9781484237816','1');
    insert into lagerbestand values ('9781786466143','yes');
    insert into lagerbestand values ('978-3-319-45419-1','f');

Abfragen
~~~~~~~~

.. code:: sql


    select * from lagerbestand where auf_lager;
    select * from lagerbestand where auf_lager ='no';
    select * from lagerbestand where not auf_lager;
