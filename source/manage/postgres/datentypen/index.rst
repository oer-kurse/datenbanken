.. _startdatentypen:

==========
Datentypen
==========

.. INDEX:: Manage; Datentypen (postgres)
.. INDEX:: Postgres; Manage -- Datentypen

.. image:: ./images/suprematismus.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/suprematismus.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/suprematismus.webp">
   </a>


.. sidebar:: SQL-Kurs

   Supremalismus
   |b|
   Serie: Kunst

|a|

:ref:`« Startseite zum Kurs <kursstart>`

.. toctree::
   :maxdepth: 1
	      
   date-time
   boolean
   json/index
   vector/index
   array
   messwerte
