

.. meta::

   :description lang=de: Datenbanken -- Postgres -- Vacuum, Autovacuum 
   :keywords: OER-Kurs, Datenbanken, SQL, Postgres, Vacuum, Autovacuum 

.. INDEX:: vacuum
.. INDEX:: vacuum; auto
.. INDEX:: vacuum; full

Vacuum/Autovacuum
-----------------


..  image:: ./images/kasten-mit-flaschen.jpg
    :width: 0

.. |a| raw:: html

       <div class='hover_img'>
         <a href='#'>Bierkasten
         <span>
           <img src='../../_images/kasten-mit-flaschen.jpg'
                alt='kasten mit Flaschen' />
          </span>
         </a>
        </div>

.. sidebar:: SQL-Kurs

	     | Statt der Werbung...
	     | |a|
	     | Serie: Gegenstände

:ref:`« Übersicht: Manage PostgreSQL <managepostgres>`

Dieser Befehl oder Konfigurationsparameter gibt ungenutzten
Speicherplatz für eine Wiederverwendung frei.

Konfiguriert wird er in *postgresql.conf*:

Vacuum verkleinert die Tabellen nicht automatisch. Dafür muss ein
VACUUM FULL ausgeführt werden. Damit werden die Indices neu erstellt
und das kann bei großen Tabellen etwas dauern.

.. code:: bash


    autovacuum = on 
    track_counts = on 

Die wichtigsten Parameter
~~~~~~~~~~~~~~~~~~~~~~~~~

.. table::

    +-------------------------------------+-----------+
    | Parameter                           | Anmerkung |
    +-------------------------------------+-----------+
    | autovacuum                          | \         |
    +-------------------------------------+-----------+
    | autovacuum_work_mem                 | \         |
    +-------------------------------------+-----------+
    | autovacuum_max_workers              | \         |
    +-------------------------------------+-----------+
    | autovacuum_naptime                  | \         |
    +-------------------------------------+-----------+
    | autovacuum_analyze_scale_factor     | \         |
    +-------------------------------------+-----------+
    | autovacuum_analyze_threshold        | \         |
    +-------------------------------------+-----------+
    | autovacuum_vacuum_cost_delay        | \         |
    +-------------------------------------+-----------+
    | autovacuum_vacuum_cost_limit        | \         |
    +-------------------------------------+-----------+
    | autovacuum_vacuum_scale_factor      | \         |
    +-------------------------------------+-----------+
    | autovacuum_vacuum_threshold         | \         |
    +-------------------------------------+-----------+
    | autovacuum_freeze_max_age           | \         |
    +-------------------------------------+-----------+
    | autovacuum_multixact_freeze_max_age | \         |
    +-------------------------------------+-----------+
    | log_autovacuum_min_duration         | \         |
    +-------------------------------------+-----------+
    | vacuum_cost_page_dirty              | \         |
    +-------------------------------------+-----------+
    | vacuum_cost_page_hit                | \         |
    +-------------------------------------+-----------+
    | vacuum_cost_page_miss               | \         |
    +-------------------------------------+-----------+
    | vacuum_cost_page_dirty              | \         |
    +-------------------------------------+-----------+
    | vacuum_freeze_min_age               | \         |
    +-------------------------------------+-----------+
    | vacuum_freeze_table_age             | \         |
    +-------------------------------------+-----------+
    | vacuum_multixact_freeze_min_age     | \         |
    +-------------------------------------+-----------+
    | vacuum_multixact_freeze_table_age   | \         |
    +-------------------------------------+-----------+

Konfiguration auf Tabellenebene
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    ALTER TABLE meine_tabelle SET (storage_parameter = value);

Parmaeter-Liste:

.. table::

    +---------------------------------------+
    | autovacuum_enabled                    |
    +---------------------------------------+
    | autovacuum_vacuum_cost_delay          |
    +---------------------------------------+
    | autovacuum_vacuum_cost_limit          |
    +---------------------------------------+
    | autovacuum_vacuum_scale_factor        |
    +---------------------------------------+
    | autovacuum_vacuum_threshold           |
    +---------------------------------------+
    | autovacuum_freeze_min_age             |
    +---------------------------------------+
    | autovacuum_freeze_max_age             |
    +---------------------------------------+
    | autovacuum_freeze_table_age           |
    +---------------------------------------+
    | autovacuum_multixact_freeze_min_age   |
    +---------------------------------------+
    | autovacuum_multixact_freeze_max_age   |
    +---------------------------------------+
    | autovacuum_multixact_freeze_table_age |
    +---------------------------------------+
    | autovacuum_analyze_scale_factor       |
    +---------------------------------------+
    | autovacuum_analyze_threshold          |
    +---------------------------------------+
    | log_autovacuum_min_duration           |
    +---------------------------------------+
    | \                                     |
    +---------------------------------------+

Optimierungen deaktivieren
~~~~~~~~~~~~~~~~~~~~~~~~~~

Für große Tabellen ist es vielleicht hilfreich das AUTOVACCUM zu deaktivieren:

.. code:: sql


    ALTER TABLE grosse_tabelle SET (autovacuum_enabled = off);

Behandlung von TOAST-Tabellen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

TOAST  = Abkürzung für oversize attribute storage technique

Damit werden Tabellen verwaltet, die das Limit eines Speicherblocks
(Standard: 8Kbyte) überschreiten.

.. code:: sql


    ALTER TABLE pgbench_accounts
    SET ( toast.autovacuum_enabled = off);


.. code:: sql


    SELECT n.nspname, c.relname,
                          pg_catalog.array_to_string(c.reloptions || array(
                          select 'toast.' || 
    x from pg_catalog.unnest(tc.reloptions) x),', ')
                          as relopts
    FROM pg_catalog.pg_class c
    LEFT JOIN
    pg_catalog.pg_class tc ON (c.reltoastrelid = tc.oid)
    JOIN
    pg_namespace n ON c.relnamespace = n.oid
    WHERE c.relkind = 'r'
    AND nspname NOT IN ('pg_catalog', 'information_schema');

Umschalten der Autovacuum-Parameter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Voraussetzung: autovacucum ist als include in der *postgres.conf*
eingetragen:

.. code:: bash


    autovacuum = on 
    autovacuum_max_workers = 3 
    include 'autovacuum.conf' 


HINWEISE zu den Parametern:

- autovacuum_max_workers größer 2 gesetzt werden

- Zu hoch gesetzt, ist nicht unbedingt nützlich

- vacuum_cost_delay nicht zu hoch setzen.

- maintenance_work_mem sollte über 1GB gesetzt werden.

- Unter welcher PID läuft der VACUUM-Prozess -- pg_stat_activity view


autovacuum.conf.tag für die stark beschäftigte Datenbank

.. code:: bash


    autovacuum_analyze_scale_factor = 0.1 
    autovacuum_analyze_threshold = 50 
    autovacuum_vacuum_cost_delay = 30 
    autovacuum_vacuum_cost_limit = -1 
    autovacuum_vacuum_scale_factor = 0.2 
    autovacuum_vacuum_threshold = 50  

autovacuum.conf.nacht für die etwas ruhigere Zeit

.. code:: bash


    autovacuum_analyze_scale_factor = 0.05 
    autovacuum_analyze_threshold = 50 
    autovacuum_vacuum_cost_delay = 10 
    autovacuum_vacuum_cost_limit = -1 
    autovacuum_vacuum_scale_factor = 0.1 
    autovacuum_vacuum_threshold = 50 


Umschalten für die Nacht (entsprechend für den Tag), das kann durch
einen Cron-Job automatisiert werden.

.. code:: bash


    ln -sf autovacuum.conf.nacht autovacuum.conf
    $ pg_ctl -D datadir reload

Defekte pages beseitigen
~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    SET vacuum_freeze_table_age = 0;
    VACUUM;

Siehe auch: pageinspect - ein Zusatzpaket zur Analyse

Ist VACUUM für eine Tabelle notwendig?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    CREATE OR REPLACE VIEW av_needed AS 
    SELECT N.nspname, C.relname 
    , pg_stat_get_tuples_inserted(C.oid) AS n_tup_ins 
    , pg_stat_get_tuples_updated(C.oid) AS n_tup_upd 
    , pg_stat_get_tuples_deleted(C.oid) AS n_tup_del 
    , CASE WHEN pg_stat_get_tuples_updated(C.oid) > 0 
           THEN pg_stat_get_tuples_hot_updated(C.oid)::real 
              / pg_stat_get_tuples_updated(C.oid) 
           END 
      AS HOT_update_ratio 
    , pg_stat_get_live_tuples(C.oid) AS n_live_tup 
    , pg_stat_get_dead_tuples(C.oid) AS n_dead_tup 
    , C.reltuples AS reltuples 
    , round( current_setting('autovacuum_vacuum_threshold')::integer 
           + current_setting('autovacuum_vacuum_scale_factor')::numeric 
           * C.reltuples) 
      AS av_threshold 
    , date_trunc('minute', 
        greatest(pg_stat_get_last_vacuum_time(C.oid), 
                 pg_stat_get_last_autovacuum_time(C.oid))) 
      AS last_vacuum 
    , date_trunc('minute', 
        greatest(pg_stat_get_last_analyze_time(C.oid), 
                 pg_stat_get_last_analyze_time(C.oid))) 
      AS last_analyze 
    , pg_stat_get_dead_tuples(C.oid) > 
      round( current_setting('autovacuum_vacuum_threshold')::integer 
           + current_setting('autovacuum_vacuum_scale_factor')::numeric 
           * C.reltuples) 
      AS av_needed 
    , CASE WHEN reltuples > 0 
           THEN round(100.0 * pg_stat_get_dead_tuples(C.oid) / reltuples) 
           ELSE 0 END 
      AS pct_dead 
    FROM pg_class C 
    LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace) 
    WHERE C.relkind IN ('r', 't') 
      AND N.nspname NOT IN ('pg_catalog', 'information_schema') 
      AND N.nspname NOT LIKE 'pg_toast%' 
    ORDER BY av_needed DESC, n_dead_tup DESC; 

Danach die Abfrage für eine Tabelle:

.. code:: sql


    SELECT * 
    FROM av_needed 
    WHERE nspname = 'public' 
    AND relname = 'pgbench_accounts';

Informationen über die VACCUM-Prozesse
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Welche PID hat der VACCUM-Prozess?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    SELECT pid, application_name, backend_type FROM pg_stat_activity;

Informationen zum Prozess
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    SELECT pid, application_name, backend_type FROM pg_stat_activity;

Wieviele Datensätze wurden geändert/gelöscht
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Übersicht für alle Tabellen:

.. code:: bash


    select sum(n_dead_tup) from pg_stat_user_tables;

Übersicht für eine Tabelle:

.. code:: bash


    select n_dead_tup from pg_stat_user_tables where relname = '<Name der Tabelle>';

VACUUM für alle DB ausführen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash


    vacuumdb --jobs=4 --all -U postgres

Weiterführende Links
~~~~~~~~~~~~~~~~~~~~

- `Vacuum für große Datenbanken <https://medium.com/helpshift-engineering/auto-vacuum-tuning-in-postgresql-3408f8b62ad8>`_
