===================
Tabellen/Relationen
===================

.. image:: ./images/kunst-am-bau-abstreifer.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/kunst-am-bau-abstreifer.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/kunst-am-bau-abstreifer.jpg">
   </a>

.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Kunst am Bau

|a|
            
:ref:`« Übersicht: Manage PostgreSQL <managepostgres>`

Die Relation (Tabelle) ist der wichtigste Objekt-Typ in relationalen Datenbanken.

.. index:: psql-shell; Liste der Tabellen
.. index:: manage; Liste der Tabellen
	   
Liste der Tabellen:
===================

.. code:: bash

    \dt


Als SQL-Abfrage auf dem Systemkatalog

.. code:: sql

   SELECT *
   FROM pg_catalog.pg_tables
   WHERE schemaname != 'pg_catalog' AND 
   schemaname != 'information_schema';

   
.. index:: Übung; CREATE TABLE

.. _create-table-bkzfrau:
	   
Eine Tabelle anlegen
====================
.. code:: sql

   CREATE TABLE bkzfrau (
     num INTEGER,
     beruf VARCHAR(150)
   );

.. index:: psql-shell; Tabellenstruktur
.. index:: manage; Tabellenstruktur
.. index:: Tabellen; Struktur
   
Tabellenstruktur
================

.. code:: bash

   \d bkzfrau

   \d bkzfrau
           Table "public.bkzfrau"
    Column |          Type          | Modifiers 
   --------+------------------------+-----------
    num    | integer                | 
    beruf  | character varying(150) | 


.. index:: psql-shell; Systemtabelle
.. index:: manage; Systemtabelle
.. index:: Tabellen; Systemtabellen
   
Liste der System-Tabellen
=========================

.. code:: bash

   \dtS
   -- Beispiel mit Teil des Namens
   \d pg_am

.. index:: Tabellen; die zehn größten Tabellen
	   
Die 10 größten Tabellen in der DB
=================================

.. code:: sql

   SELECT table_name
   , pg_relation_size(table_schema || '.' || table_name) as size
   FROM information_schema.tables
   WHERE table_schema NOT IN ('information_schema', 'pg_catalog')
   ORDER BY size DESC
   LIMIT 10;   

Tabelle umbenennen
==================

.. index:: Tabelle; umbenennen
	   
.. code:: sql

   ALTER TABLE leika RENAME TO leikarow;   

Tabellenstruktur mit SQL
========================

.. index:: Tabelle; Struktur

.. code:: sql

   SELECT 
      table_name, 
      column_name, 
      data_type 
   FROM 
      information_schema.columns
   WHERE 
      table_name = 'akte';

  
Tabellenstruktur kopieren
=========================

.. index:: Tabelle; Struktur kopieren

.. code:: sql

   create table dublikat as (select * from original ) with no data;
