============
Installation
============


.. index:: Installation; Postgres/Debian

.. index:: Postgres; Installation

Installation
------------

.. code:: bash


    apt install postgresql postgresql-client

Verbinden als root
------------------

.. code:: bash


    su -s /bin/bash postgres

Verbinden als sudo
------------------

.. code:: bash


    sudo -u postgres bash

Weiter Informationen
--------------------

- `https://wiki.debian.org/PostgreSql <https://wiki.debian.org/PostgreSql>`_
