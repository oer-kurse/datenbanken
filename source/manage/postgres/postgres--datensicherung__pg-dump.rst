=========================
Datensicherung -- pg_dump
=========================


.. _8715e860-c030-4b24-93b2-453454c482f4:
.. INDEX:: Datensicherung; pg_dump
.. INDEX:: pg_dump; Datensicherung

Daten sichern und wieder herstellen ist das A und O des
Datenmanagements. Postgres liefert dafür die Werkzeuge
**pg_dump** und **pg_restore**. Alle Kommandos werdne an der Konsole
ausgeführt. für den Import können Sie alle im Kurs verwendeten
Tabellen als DUMP importieren:


:download:`Download: DUMP mit allen Tabellen <files/oer-kurse--db__alle-tabellen-24-07-04.dump>`.

Version I: als sql-Skript
-------------------------

Datenbank sichern
~~~~~~~~~~~~~~~~~

.. code:: bash

    pg_dump meineDB > /tmp/meine-db-2024-07-03.sql

Sicherung einlesen
~~~~~~~~~~~~~~~~~~

Als Vorarbeit in Postgres die neue Datenbank anlegen:

.. code:: sql

    create database kursdb


.. code:: bash

    psql -U postgres -d kursdb -f  meine-db-2024-07-03.sql

Version II: als Dump-File
-------------------------

Sichern
~~~~~~~

.. code:: bash

    pg_dump -Fc alletabellen > /tmp/oer-kurse--db__alle-tabellen-24-07-04.dump

Rücksichern
~~~~~~~~~~~

.. code:: bash


    pg_restore -U postgres -d kursdb oer-kurse--db__alle-tabellen-24-07-04.dump
    pg_restore -U postgres --no-owner -U postgres -d kursdb oer-kurse--db__alle-tabellen-24-07-04.dump

    -- Den Tabellen einen neuen Benutzer zuweisen
    ALTER DATABASE db_name OWNER TO new_owner_name;

User/Rollen sichern
~~~~~~~~~~~~~~~~~~~

HINWEIS: im Export ist auch der Benutzer Postgres enthalten. Löschen
Sie vor der Ausführung des Skripts den Benutzer **postgres** und alle
anderen, die schon im neuen System vorhanden sind. Es besteht die
Gefahr, dass Sie sich aus der Datenbank aussperren!

.. code:: bash


    pg_dumpall --globals-only -U postgres -f /tmp/roles.sql
