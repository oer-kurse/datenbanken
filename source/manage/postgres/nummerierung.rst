==============
 Nummerierung
==============

.. index:: Laufende Nummer
.. index:: ROW_NUMBER; Funktion
.. index:: Funktion; ROW_Number	   

.. image:: ./images/frosch-meditation.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/frosch-meditation.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/frosch-meditation.webp">
   </a>



.. sidebar:: SQL-Kurs

   Meditation
   |b|
   Serie: Dinge

|a|

	   
In Auswertungen oft praktiziert, die Ausgabe einer laufenden Nummer.

::

   create table runner
   (
      startnr integer
    , runden integer
    , meter integer
    , name varchar(30)
    );

    insert into
       runner
    values
       ( 109 , 12 , 4950 , 'Anna, Nass'),          
       ( 111 ,  8 , 3500 , 'Diggedag'),            
       ( 112 , 13 , 5500 , 'Daniel, Duesentrieb'), 
       ( 113 , 10 , 4250 , 'Dig'),                 
       ( 114 , 17 , 7000 , 'Dagobert, Duck'),      
       ( 115 , 14 , 5950 , 'Anna, Bolika'),        
       ( 116 , 12 , 5100 , 'Micky, Maus'),         
       ( 118 , 12 , 5100 , 'Klaus, Trophobie'),    
       ( 211 , 15 , 6250 , 'Axel, Nässe'),         
       ( 213 , 16 , 6550 , 'Zita, Delle'),         
       ( 214 , 11 , 4800 , 'Rainer, Wein'),        
       ( 215 , 13 , 5600 , 'Gitta, Gans'),         
       ( 216 , 10 , 4150 , 'Dag'),                 
       ( 218 , 10 , 4150 , 'Franz, Gans'),         
       ( 316 , 13 , 5250 , 'Klaas, Klever'),       
       ( 317 , 14 , 5950 , 'Gustav, Gans'),        
       ( 318 , 11 , 4450 , 'Minni, Maus'),         
       ( 319 ,  5 , 2000 , 'Klaus, Trophobie'),    
       ( 427 , 10 , 4350 , 'Rainer, Zufall'),      
       ( 110 , 17 , 7100 , 'Donald, Duck');

ROW_NUMBER
==========
.. code:: sql
	  
    select
        ROW_NUMBER ()
	over(order by meter desc)
	as "Lfd.-Nr."
      , *
    from
      runner;
