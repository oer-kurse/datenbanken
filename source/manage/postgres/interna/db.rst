====================
Infos zur Instanz/DB
====================

.. meta::

   :description lang=de: Informationen zur Postgres-Datenbank
   :keywords:  Version, Datenbank, Postgres, Extensions, Konfiguration


.. image:: ../images/kunst-am-bau-streetart-florenz.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/kunst-am-bau-streetart-florenz.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/kunst-am-bau-streetart-florenz.jpg">
   </a>



.. sidebar:: SQL-Kurs

   |b|

   Serie: Graffiti

|a|
            
:ref:`« Übersicht: Manage PostgreSQL <managepostgres>`
     
.. index:: psql; Infos

   
Datenbanken -- Liste
====================

.. index:: Postgres; Liste der Datenbanken
	   
Auflistung verfügbarer Datenbanken:

.. tab:: psql

   ::

      \l
      
.. tab:: sql

   ::

      SELECT
        datname
      FROM
        pg_database;

Postgres -- Version?
====================

.. index:: Postgres; Version
	   
::
	
   SELECT version();

Datenbank -- aktuelle?
======================

.. index:: Postgres; aktelle DB
	   
Welche ist die aktuelle DB?

::

  select current_database();   

  
Extensions
==========

Welche Extensions sind installiert?

.. index:: Postgres; extensions
.. index:: Extensions; Postgres

.. code:: sql

   select
	  name, comment, default_version, installed_version
   from
	  pg_available_extensions;

Konfigurationsdatei
===================

.. index:: Speicherort; der Daten (postgres)
.. index:: Speicherort; der Logdateien (postgres)
.. index:: Speicherort; der Konfiguration (postgres)

Wo liegen Konfigurationsdatei, Daten-Ordner, Log-Dateien?

::

   SHOW config_file;
   SHOW data_directory;
   SHOW log_destination;
   
   # Das folgende Kommando gibt alle Parameter aus:
   
   show all;   
