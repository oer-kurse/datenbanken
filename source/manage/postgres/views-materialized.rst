.. index:: View; materialized

.. index:: materialized view

.. image:: ./images/spielzeug-auto.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/spielzeug-auto.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/spielzeug-auto.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Dinge

|a|

Views: materialized
-------------------

Eine »materialized view« (materialisierte Sicht) speichert
das Ergebnis einer Abfrage. Die Abfrage wird ausgeführt und verwendet, 
die View zum ****Zeitpunkt**** der Befehlsausgabe um das Ergebnis 
zusammenzustellen und bereit zu halten. Das Ergebnis ändert sich bis
zu einem Refresh nicht, ist also nur für Relationen geeignet, deren
Inhalt sich eher selten ändert.
Zu einem späteren Zeitpunkt kann mit einem Refresh eine Aktualisierung 
erfolgen.

- die Erstellung ist langsam

- die Verwendung ist schnell

- Änderungen werden im *materialized view* nicht sofort wirksam

Tabellen: gerichte und gaeste
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: sql


    CREATE TABLE gerichte
    ( gericht_id SERIAL PRIMARY KEY
      , bezeichnung text
    );

    CREATE TABLE gaeste
    ( gast_id SERIAL
      , besuch_am date
      , gericht_id int REFERENCES gerichte (gericht_id)
    );
    INSERT INTO gerichte (bezeichnung)
    VALUES ('Schnitzel'), ('Spinat mit Ei'), ('Gulasch'), ('Reis'), ('Nudeln'), ('Pommes');

    INSERT INTO gaeste(besuch_am, gericht_id)
    SELECT floor(abs(sin(n)) * 365) :: int + date '2022-01-01'
     , ceil(abs(sin(n :: float * n))*6) :: int
     FROM generate_series(1,500000) AS rand(n);

View (normal)
~~~~~~~~~~~~~

.. code-block:: sql


    CREATE VIEW v_gerichte AS
      SELECT bezeichnung, count(*)
      FROM gerichte JOIN gaeste USING (gericht_id)
      GROUP BY bezeichnung
      ORDER BY 1;

View (materialized)
~~~~~~~~~~~~~~~~~~~

.. code-block:: sql


    CREATE MATERIALIZED VIEW m_gerichte AS
      SELECT bezeichnung, count(*)
      FROM gerichte JOIN gaeste USING (gericht_id)
      GROUP BY bezeichnung
      ORDER BY 1;

View (materialized) aktualisieren
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: text


    REFRESH MATERIALIZED VIEW m_gerichte;

Zeitmessung
~~~~~~~~~~~

Für den Vergleich muss eventuell die Zeitmessung aktiviert werden:

.. code-block:: bash


    \timing
