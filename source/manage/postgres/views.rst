:ref:`« Übersicht: Manage PostgreSQL <managepostgres>`

.. meta::

   :description lang=de: Posgres: Views mit Landkreisen Brandenburg
   
   :keywords: Views, Landkreise
     
=======
 Views
=======

.. image:: ./images/tassen.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/tassen.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/tassen.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Dinge

|a|

.. index:: View; Liste aller Views

:ref:`« Übersicht: Manage PostgreSQL <managepostgres>`
     
Wiederholungen sind bei der Abfrage und Pflege von Datenbeständen
unvermeidlich. Die Routine kann zumindest teilweise in Views, Trigger
und Prozeduren konserviert werden. Die spätere Arbeitsersparnis sollte
den Aufwand der Erstellung wieder wett machen.

- Ab PostgreSQL Version 9.3 sind einfache Views in der Lage insert,
  update und delete-Anweisungen auszuführen.

	   
View zur Auflistung aller Views definieren
==========================================

Für diese Ausgabe kann ein eigener View erstellt werden:

::

   create or replace view show_views as 
   select table_name from INFORMATION_SCHEMA.views 
   WHERE table_schema = ANY (current_schemas(false));


Alle Views anzeigen
===================
Vor der Verwendung den view *show_views* erstellen (siehe oben)!
::

   select * from show_views;
   

.. index:: View; create or replace
	   
View für die Anzeige aller Landkreise aus der Relation *gewaesser*
==================================================================

Download: :download:`SQL-Script für die Gewässer und Landkreise <files/demo-data-views.sql>`

Führen Sie nach dem Downlod das Script aus. Anschließen können Sie den folgenden View erzeugen:

::   

   create or replace view landkreis_liste as
   (select distinct g.kuerzel_lk as "Landkreis (kurz)",
   l.landkreis as "Landkreis (lang)"
   from gewaesser as g right outer join landkreise as l
   on l.kuerzel_lk = g.kuerzel_lk order by g.kuerzel_lk);

   select * from landkreis_liste;

.. index:: manage: View; definition zeigen

Definition zum view zeigen
==========================
::

   -- allgemein definiert

   \d+ <view-name>
   
   -- oben definierter view landkreis_liste
   
   \d+ landkreis_liste

   -- Ausgabe
   
                         View "public.landkreis_liste"
         Column      |         Type          | Modifiers | Storage  | Description 
   ------------------+-----------------------+-----------+----------+-------------
   Landkreis (kurz) | character varying(3)  |           | extended | 
   Landkreis (lang) | character varying(30) |           | extended | 
   View definition:
     SELECT DISTINCT g.kuerzel_lk AS "Landkreis (kurz)",
      l.landkreis AS "Landkreis (lang)"
     FROM gewaesser g
       RIGHT JOIN landkreise l ON l.kuerzel_lk::text = g.kuerzel_lk::text
    ORDER BY g.kuerzel_lk;


Noch eine View zum Beispiel Personaldatenbank:

::


   create or replace view banktransfer as
   SELECT ABTNR, SUM(GEHALT) AS GEHALTSSUMME
   FROM PERSONAL LEFT JOIN PROJEKT ON PERSONAL.PROJNR=PROJEKT.PROJNR
   GROUP BY ABTNR ORDER BY ABTNR ASC;


Definition zum View anzeigen

   
::

   select pg_get_viewdef('banktransfer', true);
