==============
Upgrade/Update
==============


.. INDEX:: Upgrade (PostgreSQL); Update
.. INDEX:: Update (PostgreSQL; Upgrade

.. image:: ./images/ball-und-kugel.webp
   :width: 0px


.. |a| raw:: html

       <!-- lightbox container hidden with CSS -->
       <a href="#" class="lightbox" id="img1">
       <span style="background-image: url('../../_images/ball-und-kugel.webp')"></span>
       </a>

.. |b| raw:: html

       <a href="#img1">
       <img width="250px" src="../../_images/ball-und-kugel.webp">
       </a>

.. sidebar:: SQL-Kurs

   |b|
	     
   Serie: Dinge

|a|
   
Workflow
--------

| Mit dem Werkzeug **»pg_dump«** können PostgreSQL-Datenbanken gesichert werden. 
| Die Rücksicherung erfolgt dann mit **»pg_restore«**.

Linkliste
~~~~~~~~~

- `https://www.postgresql.org/docs/12/app-pgdump.html <https://www.postgresql.org/docs/12/app-pgdump.html>`_
- `https://www.postgresql.org/docs/12/app-pgrestore.html <https://www.postgresql.org/docs/12/app-pgrestore.html>`_
- `https://snapshooter.com/learn/postgresql/pg_dump_pg_restore <https://snapshooter.com/learn/postgresql/pg_dump_pg_restore>`_

Migration
---------

`Ein Bericht zum Upgrade von Version 9.6 to zu Version 13. <https://retool.com/blog/how-we-upgraded-postgresql-database/>`_ 
