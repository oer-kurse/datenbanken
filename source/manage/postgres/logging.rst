=======
Logging
=======


.. index:: Logging

Auswertung
----------

pgBadger
~~~~~~~~

- `http://dalibo.github.io/pgbadger/ <http://dalibo.github.io/pgbadger/>`_

Beispielscript
~~~~~~~~~~~~~~

.. code:: bash


    #!/bin/bash
    outdir=/var/www/reports
    begin=$(date +'%Y-%m-%d %H:00:00' -d '-1 day')
    end=$(date +'%Y-%m-%d %H:00:00')
    outfile="$outdir/daily-$(date +'%H').html"

    pgbadger -q -b "$begin" -e "$end" -o "$outfile " 
      /var/log/postgres.log.1 /var/log/postgres.log

grep
~~~~

.. code:: bash


    egrep "FATAL|ERROR" /var/log/postgres.log

pg_stat_statement -- Echtzeitauswertung
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Weiterführende Informationen:

`https://www.postgresql.org/docs/current/static/pgstatstatements.html <https://www.postgresql.org/docs/current/static/pgstatstatements.html>`_

Adminrechte um das Modul zu aktivieren

In der postgresql.conf:

.. code:: bash


    shared_preload_libraries = 'pg_stat_statements'

Als superuser:

.. code:: sql


    CREATE EXTENSION pg_stat_statements;

Auswertung: meist benutzten Abfragen

.. code:: sql


    SELECT query FROM pg_stat_statements ORDER BY calls DESC;


Welche Abfragen benötigen die meiste Zeit

.. code:: sql


    SELECT query, total_time/calls AS avg, calls
           FROM pg_stat_statements ORDER BY 2 DESC;
