================
 SQL-Injections
================

.. index:: SQL-Injections


.. image:: ./images/bruecke-postdam-westkreuz.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bruecke-postdam-westkreuz.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bruecke-postdam-westkreuz.jpg">
   </a>

.. sidebar:: SQL-Kurs

   |b|

   Serie: Kust am Bau

|a|

:WICHTIGER HINWEIS:
   
   Diese Befehle niemals für  ein fremdes System anwenden, weil
   strafrechtlich relevant!

Schutzmaßnahmen:

- Prüfen, welche Software ich einsetze! 

  `Beispiel Sicherheitslücken in Zimbra`_

  Alternativen prüfen und/oder Sicherheitswarnungen verfolgen und schnell reagieren!

- Eingaben prüfen!

  `Wordpress -- Eingabeprüfung im Plugin`_

  - Maßnahmen wie
    
    - Pingback, Talkback und RPC nicht nutzen
    - REST-API mit JSON
    - Zugang zum Backend nicht über offensichtliche URLs!
  
- streng typisierte Abfragen
- sichere API verwenden
- keine internen ID's der Datenbanken im Webinterface verwenden,
  besser einen Index, indirekte Referenzen oder andere indirekte
  Methoden verwenden
- Fehlermeldungen geben oft zu viele Informationen über das System
  preis (prüfen Sie Fehlerseiten nach Fehleingaben)!
- keine eigenen kryptischen Verfahren anwenden, sondern allgemein
  anerkannte wie  AES, RSA, SHA-​256 oder besser...
  
Die am häufigsten ausgenutzten Schwachstellen sind laut
http://cwe.mitre.org/top25/index.html

.. code-block:: sql

   SELECT userid
   FROM users
   WHERE  user = 'foo' AND password = 'password' OR '1' = '1';

   
   SELECT *
   FROM items
   WHERE user = 'koppatz' AND itemname = 'wichtig' OR 'a'='a';

   
Mit dem angehängten *OR* wird jede Aussage wahr und deshalb
ausgeführt. Sie verkürzt sich dann zu:

.. code-block:: sql

   SELECT *
   FROM items;


Prüfen der ID
=============
- Hier wird die ID in der Abfrage verwendet.
- Wo ist die Prüfung, ob die ID zu einem konkreten Kunden gehört?
- Hier könnte jede ID eingefügt werden, dann auch solche, die nicht zu einem
  Konkreten Kunden gehören.
  
.. code-block:: sql
   
   SELECT * FROM
   invoices WHERE id = parameter1

Besser:

.. code-block:: sql

   SELECT * FROM
   rechnung WHERE id = parameter1
   AND kunde = parameter2


Andere Quellen
==============
Folien:

- https://www.slideshare.net/billkarwin/sql-injection-myths-and-fallacies

.. _Beispiel Sicherheitslücken in Zimbra: http://www.cvedetails.com/vendor/7863/Zimbra.html  
.. _Wordpress -- Eingabeprüfung im Plugin: https://www.heise.de/security/meldung/WordPress-Plug-in-NextGEN-Gallery-kann-sich-an-SQL-Anfragen-verschlucken-3645075.html
  
.. https://www.endpoint.com/blog/2012/06/10/detecting-postgres-sql-injection


.. http://sqlmap.org
   
.. Die jeweiligen manuellen Schritte:
   :Quelle: https://meingottundmeinewelt.de/category/sqli/

   http://testphp.vulnweb.com/artists.php?artist=1
   http://testphp.vulnweb.com/artists.php?artist=1'
   http://testphp.vulnweb.com/artists.php?artist=1 and 1=1
   http://testphp.vulnweb.com/artists.php?artist=1 and 1=2
   http://testphp.vulnweb.com/artists.php?artist=1 oder by 1
   http://testphp.vulnweb.com/artists.php?artist=1 oder by 2
   http://testphp.vulnweb.com/artists.php?artist=1 oder by 3
   http://testphp.vulnweb.com/artists.php?artist=1 oder by 4
   http://testphp.vulnweb.com/artists.php?artist=1 UNION SELECT 1,2,3
   http://testphp.vulnweb.com/artists.php?artist=1999 UNION SELECT 1,2,3
   http://testphp.vulnweb.com/artists.php?artist=1999 UNION SELECT 1,2,CONCAT_WS
   (CHAR(32,58,32),user(),database(),version())


- `SQL Injection Cheat Sheet <https://www.invicti.com/blog/web-security/sql-injection-cheat-sheet/>`_
- `Spielerisch SQL-Injections kennenlernen <https://www.sql-insekten.de/>`_
