===================
Programmiersprachen
===================

.. image:: ./images/zirkus-clowns.jpg
   :width: 0px


.. |a| raw:: html

      <!-- lightbox container hidden with CSS -->
      <a href="#" class="lightbox" id="img1">
      <span style="background-image: url('../../_images/zirkus-clowns.jpg')"></span>
      </a>

.. |b| raw:: html

       <a href="#img1">
       <img width="250px" src="../../_images/zirkus-clowns.jpg">
       </a>

.. sidebar:: SQL-Kurs

   |b|

   Serie: Menschen

|a|

.. index:: C#
.. index:: PHP

	   
	     
In jeder Programmiersprache gibt es Konstrukte, denen ein
Programmierer seien besonder Aufmerksamkeit widmen sollte.
Hier finden Sie eine kleine Auswahl...

C Sharp
=======

Die Fehlerklassen NullReferenceException und ArgumentNullException 
gehören zu den häufigsten Laufzeitfehlern, die .NET-Anwendung produzieren.

Den ganzen Artikel zu diesem Thema finden Sie unter
`C# 8 soll Fehler mit null verhindern`_


.. index:: PHP

PHP
===

register_globals
----------------
Sollte deaktiviert werden, ist heute standardmäßig deaktiviert, aber
eine Kontrolle kann nicht schaden...

prepaerd statements
-------------------
Prepared statements sind zwar praktisch und bieten bei korrekter
Verwendung einen hohen grad an Sicherheit gegen SQL Injections,
schließen aber bei weitem nicht alle Angriffsvektoren gegen die
Datenbank. siehe auch `Dokumentation zu prepare`_ 

Wichtig ist wieder die Überprüfung aller möglichen und unmöglichen
Benutzereingaben!

.. index:: PL/SQL (Oracle)

PL/SQL(Oracle)
--------------

- `Sicheren PLSQL-Code schreiben (YouTube)`_
- `Unibrute is a multi-threaded SQL injection union bruteforcer (Tool)`_

Code-Analyse
------------
- `Graudit`_
- 
  
.. _Dokumentation zu prepare: http://php.net/manual/en/pdo.prepare.php
.. _C# 8 soll Fehler mit null verhindern:
   https://www.heise.de/developer/meldung/Programmiersprachen-C-8-soll-Fehler-mit-null-verhindern-3835949.html

.. Oracle

.. _Sicheren PLSQL-Code schreiben (YouTube): https://www.youtube.com/watch?v=2om5-tYda-E&feature=youtu.be

.. _Unibrute is a multi-threaded SQL injection union bruteforcer (Tool): https://github.com/GDSSecurity/Unibrute
.. Tools

   prüfen
   http://sqlninja.sourceforge.net/download.html
   http://ferruh.mavituna.com/archive/
   http://ferruh.mavituna.com/oracle-sql-injection-cheat-sheet-oku/
   https://sqlmap.org
   https://sourceforge.net/directory/os:mac/?q=bobcat+-+sql+injection+exploitation+tool
   https://labs.portcullis.co.uk/downloads/
   http://infobyte.com.ar
   http://thehackernews.com/2011/04/pangolin-323-automatic-sql-injection.html
   http://www.securiteam.com/tools/6Y00320C0Y.html
   
.. _Graudit: http://www.justanotherhacker.com/projects/graudit.html

