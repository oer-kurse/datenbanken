==============
 Speicherorte
==============

.. index:: Speicherorte
.. INDEX:: Standard-Passworte
.. INDEX:: Passworte

Die Standardspeicherorte wie Standardpassworte vieler Systeme sind bekannt.
Der Zugriff auf diese Bereiche ist streng zu prüfen.

Was passiert, wenn an dieser Stelle nachlässig gearbeitet wird,
zeigt der folgnde Bericht:


`Wer wohnt in Baden? <https://www.heise.de/news/Datenleck-Wie-Baden-in-Oesterreich-33-000-Meldedatensaetze-ins-Netz-stellte-6660890.html>`_


