============
 Allgemein
============

- Siehe auch: :ref:`Aus Fehlern lernen <ausfehlernlernen>`

Management- und Anwendungsfragen
================================

- regelmäßig Backups erstellen
- die Administration über separaten Zugang organisieren
- nur bestimmte IPs zulassen
- zwingend SSL benutzen
- ein Client-Zertifikat voraussetzen
- nur lange, vom System erzeugte Zufalls-Kennwörter zulassen
- ggf. nur per VPN/IP erreichbar sein
- bei zu vielen Fehlversuchen IPs blocken und Alarme senden
- alle Zugriffe und alle Änderungen unveränderlich protokollieren
- nur die minimalsten Rechte haben, ggf. nur lesenden Zugriff über 
  SQL, und nur ganz bestimmte besondere Bereiche ggf. auch schreibende
  (eigener DB-Nutzer).
- von Sicherheitsprofis geprüft worden sein vor der Einrichtung
- von Sicherheitsprofis geprüft worden sein nach der Einrichtung
- von Sicherheitsprofis geprüft worden sein in regelmäßigen Abständen
- überhaupt sollte man superparanoid sein :-)


