===============================
Migration -- Excel zu Datenbank
===============================


.. _20231015T153434:
.. INDEX:: Migration; Landkreise zu Datenbank
.. INDEX:: Excel; Migration Landkreise zu Datenbank

Zuviele Daten werden in Anwendungen gespeichert, die einen
Mehrbenutzerzugriff oder den Datenaustausch nur bedingt unterstützen.
Deshalb sollten möglichst viele Daten zu Datenbanken migriert werden.

Für diesen Schritt sind einige Entscheidungen zu treffen.

- Diskutieren Sie die folgende von Statista herausgegebene Statistik.

- Exportieren Sie die Daten als CSV-Format.

- Importieren Sie die Daten, nachdem Sie die Datenbankstruktur
  erstellt haben.


Download: :download:`Ausgangsdaten (odt-Datei) <statistik-landkreise-brandenburg.ods>`
