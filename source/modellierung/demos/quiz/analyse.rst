=============
Erste Analyse
=============


Die Ausgangslage und Beschreibung der zu speichernden
Daten. Darüberhinaus soll eine Anonyme erfassung an Hand einer
User-/Organisations-ID möglich sein.
Änderungsaktivitäten durch Wiederholung sollen als Historie
gespeichert werden.

Was soll modelliert (gespeichert) werden?
-----------------------------------------

Fragen/Antworten
~~~~~~~~~~~~~~~~

Als Muster zwei Datensätze, die für die Darstellung in der Anwendung
genutzt werden.

.. code:: python


    {
        "question": "Die Schlüssellänge ist eine wichtige Eigenschaft kryptographischer Verfahren. Welche Länge ist hier richtig?",
        "answers": ["130 Bit", "194 Bit", "256 Bit", "110 Bit","1022 Bit", "1033 Bit"],
        "id": "162",
        "correct": 2,
        "kategorie": "12",
        "feedback": "Leider fehlen bei allen Schlüssellängen zwei Bit, nur bei 256 Bit ist die genannte Zahl korrekt. Was bitte ist ein Bit? Siehe auch:
                https://de.wikipedia.org/wiki/Bit"},

    },
    {
        "question": "Teilen Sie bei  einem Anruf durch eine Autorit\u00e4t (Service, Helpdesk, Vorgesetzter etc.) ein notwendiges Passwort mit?",
        "answers": ["Ja.", "Nein."],
        "id": "163",
        "correct": 99,
        "kategorie": "6",
        "feedback": ""
    }

Kategorien
~~~~~~~~~~

Die Kategorien sind über einen Schlüssel definiert.

.. code:: python


    const categories = [
      { id: 3, short: "he", long: "Home Office" },
      { id: 5, short: "me", long: "Malware" },
      { id: 6, short: "mn", long: "Forschungsfragen/Metafragen" },
      { id: 7, short: "el", long: "E-Mails" },
      { id: 8, short: "pe", long: "Passwörter" },
      { id: 9, short: "ng", long: "Social Engineering" },
      { id: 10, short: "se", long: "Software" },
      { id: 12, short: "co", long: "Kryptographie" },
      { id: 13, short: "wb", long: "Verhalten im Web" },
      { id: 14, short: "rt", long: "Recht" },
      { id: 15, short: "dp", long: "Entwickler/Admins" },
      { id: 99, short: "nf", long: "Neue Fragen" },
    ];

Ausgangslage
------------

Darstellung als Tabelle mit Mustereinträgen.

.. table::

    +------+---------------------------------+------------------------------+-----+---------+----------+-----------------+
    | user | question                        | answers                      |  id | correct | category | feedback        |
    +------+---------------------------------+------------------------------+-----+---------+----------+-----------------+
    | xxx  | Die Schlüssellänge ist ...      | ["130 Bit", "194 Bit",'...'] | 162 |       2 |       12 | 256 ist korrekt |
    +------+---------------------------------+------------------------------+-----+---------+----------+-----------------+
    | yyy  | Teilen Sie bei  einem Anruf ... | ["Ja.", "Nein."]             | 163 |      99 |        6 | \               |
    +------+---------------------------------+------------------------------+-----+---------+----------+-----------------+

Entitäten
---------

Aus den gezeigten Beispieldaten ergeben sich vier wichtige
Entitätstypen (Objekttypen).

- Kategorien

- Fragen

- Antworten

- History

- Teilnehmer

  - userid

  - queryid

  - answer

  - timestarted

  - timefinished

- Fragen

  - queryid

  - catid

  - correct

  - question

  - answers

  - feedback

- Category

  - catid

  - short

  - long

- History

  - userid

  - queryid

  - answer

  - timestarted

  - timefinished
