===================
 Ausgangssituation
===================

.. INDEX:: Demo: Modellierung; Lost Art
.. INDEX:: Lost Art; Demo: Modellierung

.. image:: ./images/sesec-startseite.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('/_images/sesec-startseite.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="/_images/sesec-startseite.webp">
   </a>

.. sidebar:: Modellierung

   |b|

|a|

Für das Projekt »ALARM«  https://alarm.wildau.biz  wurde eine
Quiz-Anwendung entwickelt. Mit jedem »Test« werden die Ergebnisse der
Befragung(en) anonymisiert gespeichert. Die Daten bieten die
Möglichkeit für Auswertungen bzw. eine Einordnung des aktuellen
Wissensstandes zu Themen der Sicherheit.

Gezeigt werden:
===============

1. Ausgehend von einer allgemeinen Beschreibung die Schritte der
   Modellierung. Das Ergebnis der verschiednene Phasen ergeben eine
   Datenbankstruktur, die in der Anwendung genutzt wird.

2. Extraktion der Objekte und Zuordnung als Entität, Entitätstyp und
   Zuordnung der Attribute ...

   Verwendet werden:
   
   - Darstellungen das Textformat und
   - der Online-Diagram-Editor unter: https://app.diagrams.net

3. Anhand des ER-Diagrammes wird eine Textversion erstellt

4. Normalisierung der im ER-Diagramm gezeigen Strukturen und Beziehungen.
   
5. Die Definiton der Relationen inklusive der Datentypen führt zu
   einem Skript, das als Zielsystem eine PostgreSQL-Datenbank mit den
   Relationen anlegt.
   
   Verwendet werden:

   a) MySql-Workbench (nach dem Export sind Anpassungen am Skript notwendig).
   b) Der Desinger in PgAdmin.
   c) SQAlchemy mit der Programmiersprache Python
   

  
