==========================
 ER-Diagramm und Tabellen
==========================

.. INDEX:: ER-Diagramm; Quiz
.. INDEX:: Quiz; ER-Diagramm


ER-Diagramm
-----------


.. image:: ./images/quiz-er-diragramm.svg


.. INDEX:: Tabellenstruktur; Quiz
.. INDEX:: Quiz; Tabellenstruktur

	   
Tabellenstruktur
----------------

.. image:: ./images/quiztables.svg
