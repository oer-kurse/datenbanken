===========================
Lösungsansatz: Badegewässer
===========================

Eine Website soll Touristen über die Badegewässer in der Nähe ihres
Urlaubsortes informieren. Darüber hinaus sollen weitere Informationen,
die einen direkten Bezug zum Badegewässer haben oder ergänzende
Angaben die für den Besuch ebenfalls interessant sein könnten, in die
Datenbasis einfließen.

1. Besuchen Sie die folgenden Seiten, die für jedes Bundesland
   vorhanden sind (siehe Linkliste weiter unten).

2. Welche Informationen könnten von Interesse sein und sollten erfaßt
   werden? Erstellen Sie eine Mind- bzw. Clustermap.
   
3. Sammeln und klassifizieren Sie die relevanten Begriffe
   (Arbeitsblatt 1).

4. Definieren Sie die Relationen und die dazugehörigen Attribute
   (Arbeitsblatt 2).

5. Zeichnen Sie ein ER-Diagramm zu den Relationen.

   Legen Sie die Beziehungen, Kardinalitäten sowie Primär- und
   Fremdschlüssel fest.

zu 2. Erweiterte Datenbasis
===========================

- Neben der Wasserqualität könnten noch zusätzliche Angaben
  erfasst werden.

  Beispiel: `Angaben zur Badeanstalt in Caputh
  <https://badestellen.brandenburg.de/badestelle/-/details/187>`_

.. image:: ./konzept-map-badegewaesser.svg
   
zu. 5. ER-Diagramme
===================

Version 1
---------


.. image:: ./er-diagramm-bb-gewaesser01.svg


Version 2 
---------

- Landkreis als eigene Entity
- offene Fragen:
  - hat eine Ortslage mehrere Gewässer? ja
  - Wie die Zusatzangaben zuordnen?
    
.. image:: ./er-diagramm-bb-gewaesser02.svg

	   
Relationales Model (Text-Version)
=================================

::
   
   gewaesser (_id_, ort, merkmal, bezeichnung, anlagenart) » orte.id 
   bewertungen (gewaesser, zeitraum, praedikat) gewaesser  » gewaesser.id
                                                praedikat  » praedikate.praedikat 
   landkreise (_kurz_, lang)
   orte (_id_, name, landkreis)                 landkreis » landkreise.kurz 
   praedikate (_praedikat_, symbol)
