=======================
 Aufgabe: Badegewässer
=======================

.. |a| image:: images/baden-wuertemberg-80px.png		      
.. |b| image:: images/bayern-80px.png		      
.. |c| image:: images/berlin-80px.png		      
.. |d| image:: images/brandenburg-80px.png		      
.. |e| image:: images/bremen-80px.png		      
.. |f| image:: images/hamburg-80px.png		      
.. |g| image:: images/hessen-80px.png		      
.. |h| image:: images/mecklenburg-vorpommern-80px.png 
.. |i| image:: images/niedersachsen-80px.png	      
.. |j| image:: images/nordrhein-westfalen-80px.png    
.. |k| image:: images/rheinland-pfalz-80px.png	      
.. |l| image:: images/saarland-80px.png		      
.. |m| image:: images/sachsen-80px.png		      
.. |n| image:: images/sachsen-anhalt-80px.png	      
.. |o| image:: images/schleswig-holstein-80px.png     
.. |p| image:: images/thueringen-80px.png	      


.. index:: Badegewässer

.. image:: ./images/deutschland-2010.png
   :width: 0px


.. |y| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/deutschland-2010.png')"></span>
   </a>

.. |z| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/deutschland-2010.png">
   </a>

.. sidebar:: SQL-Kurs

   Deutschlandkarte
   |z|
   Serie: Geographie

|y|
	       
Eine Website soll Touristen über die Badegewässer in der Nähe ihres
Urlaubsortes informieren. Darüber hinaus sollen weitere Informationen,
die einen direkten Bezug zum Badegewässer haben oder ergänzende
Angaben die für den Besuch ebenfalls interessant sein könnten, in die
Datenbasis einfließen.
Das neue Informationssystem soll die Auskunft deutschlandweit
anbieten. Siehe auch die Forderungen der `Open Knowledge Foundation
Deutschland`_ weiter unten.


1. Besuchen Sie die folgenden Seiten, die für jedes Bundesland
   vorhanden sind (siehe Linkliste weiter unten).

2. Welche Informationen könnten von Interesse sein und sollten erfaßt
   werden? Erstellen Sie eine Mind- bzw. Clustermap.
   
3. Sammeln und klassifizieren Sie die relevanten Begriffe
   (Arbeitsblatt 1).

4. Definieren Sie die Relationen und die dazugehörigen Attribute
   (Arbeitsblatt 2).

5. Zeichnen Sie ein ER-Diagramm zu den Relationen.

   Legen Sie die Beziehungen, Kardinalitäten sowie Primär- und
   Fremdschlüssel fest.
   
   
Badegewässerinformationen anderer Bundesländer
==============================================
Aus Linkliste: http://www4.lubw.baden-wuerttemberg.de/servlet/is/218015/ 

+----------+-----------+---------------------------------------+
|  Bild    | Kürzel    |  Bezeichnung                          |        
+==========+===========+=======================================+
|  |a|     | BW        |  Baden-Würtemberg                     |             
+----------+-----------+---------------------------------------+
|  |b|     | BY        |  Bayern                               | 
|          |           |                                       |   
|          |           |  - `BY Badeseen`_                     |
|          |           |  - `BY Badewasserqualität`_           |
|          |           |  - `BY Badegewässer`_                 |
|          |           |  - `BY Badegewässer -- Gesetze`_      |
+----------+-----------+---------------------------------------+
|  |c|     | BE        |  Berlin                               |
|          |           |                                       |
|          |           |  - `BE Badegewässer`_                 |
|          |           |  - `BE Badegewässer (LAGESO)`_        |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |d|     | BB        |  Brandenburg                          |        
|          |           |                                       |
|          |           |  - `BB Luis -- Badegewässer (Neu)`_   |
|          |           |  - `BB Badegewässer (Alt)`_           |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |e|     | HB        |  Bremen                               |   
|          |           |                                       |
|          |           |  - `HB Badegewässer`_                 |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |f|     | HH        |  Hamburg                              |    
|          |           |                                       |
|          |           |  - `HH Badegewässer`_                 |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |g|     | HE        |  Hessen                               |   
|          |           |                                       |
|          |           |  - `HE Badegewässer`_                 |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |h|     | MV        |  Mecklenburg-Vorpommern               |   
|          |           |                                       |
|          |           |  - `MV Badegewässer`_                 |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |i|     | NI        |  Niedersachsen                        |          
|          |           |                                       |
|          |           |  - `NI Badegewässer`_                 |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |j|     | NW        |  Nordrhein-Westfahlen                 |                 
|          |           |                                       |
|          |           |  - `NW Badegewässer`_                 |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |k|     | RP        |  Rheinland-Pfalz                      |            
|          |           |                                       |
|          |           |  - `RP Badegewässer`_                 |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |l|     | SL        |  Saarland                             |     
|          |           |                                       |
|          |           |  - `SL Badegewässer`_                 |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |m|     | SN        |  Sachsen                              |    
|          |           |                                       |
|          |           |  - `SN Badegewässer`_                 |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |n|     | ST        |  Sachsen-Anhalt                       |           
|          |           |                                       |
|          |           |  - `ST Badegewässer`_                 |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |o|     | SH        |  Schleswig-Holstein                   |               
|          |           |                                       |
|          |           |  - `SH Badegewässer`_                 |
|          |           |                                       |   
+----------+-----------+---------------------------------------+
|  |p|     | TH        |  Thüringen                            |
|          |           |                                       |
|          |           |  - `TH Badegewässer`_                 |
|          |           |                                       |   
+----------+-----------+---------------------------------------+


Infos zum Thema Badegewässer
============================

- http://www.seen.de
- http://www.seenweg.de
  
.. Bayern

.. _BY Badeseen: https://www.lgl.bayern.de/gesundheit/hygiene/wasserhygiene/badeseen/index.htm


.. http://www.lgl.bayern.de/gesundheit/hygiene/wasser/badeseen/index.htm

.. Andere Links

.. _BY Badewasserqualität: https://www.lgl.bayern.de/gesundheit/hygiene/wasserhygiene/badeseen/index.htm
.. _BY Badegewässer: https://www.lgl.bayern.de/gesundheit/hygiene/wasserhygiene/badeseen/eu_badestellen_bayern_links.htm
.. _BY Badegewässer -- Gesetze: http://www.gesetze-bayern.de/Content/Document/BayBadeGewV?hl=true&AspxAutoDetectCookieSupport=1

.. Berlin

.. _BE Badegewässer: http://www.berlin.de/badegewaesser
.. _BE Badegewässer (LAGESO): http://www.berlin.de/lageso/gesundheit/gesundheitsschutz/badegewaesser/

.. Brandenburg

.. _BB Badegewässer (alt): http://www.brandenburg.de/badestellen (existiert nicht menr)

.. _BB Luis -- Badegewässer (Neu) : https://badestellen.brandenburg.de/

.. Bremen

.. _HB Badegewässer: http://www.bauumwelt.bremen.de/detail.php?gsid=bremen213.c.49250.de

.. kaputt http://www.umwelt.bremen.de/de/detail.php?gsid=bremen179.c.10062.de

.. Hamburg

.. _HH Badegewässer: http://www.badegewaesser.hamburg.de/

.. Hessen

.. _HE Badegewässer: http://badeseen.hlug.de/

.. Mecklenburg-Vorpommern

.. _MV Badegewässer: https://www.regierung-mv.de/Landesregierung/wm/gesundheit/badewasserkarte/

.. https://www.regierung-mv.de/Landesregierung/wm/Wirtschaft/Wettbewerbe/Ideenwettbewerb-Kultur–-und-Kreativwirtschaft-2017/

   http://www.gaia-mv.de/badewasser/

.. Niedersachsen

.. _NI Badegewässer: http://www.apps.nlga.niedersachsen.de/eu/batlas/index.php?p=h

.. Nordrhein-Westfalen

.. _NW Badegewässer: http://www.badegewaesser.nrw.de/bg1.htm

.. Rheinland-Pfalz

.. _RP Badegewässer: http://www.badeseen.rlp.de/servlet/is/1100/

.. Saarland

.. _SL Badegewässer: http://www.saarland.de/103138.htm

.. Sachsen

.. _SN Badegewässer: http://www.umwelt.sachsen.de/umwelt/wasser/7249.htm

.. Sachsen-Anhalt

.. _ST Badegewässer: http://www.ms.sachsen-anhalt.de/themen/gesundheit/daten-zur-gesundheit/badegewaesser/?&amp;q=badegew%C3%A4sser

.. Schleswig-Holstein

.. _SH Badegewässer: http://www.badewasserqualitaet.schleswig-holstein.de/"

.. Thüringen

.. _TH Badegewässer: http://www.twisth.thueringen.de/index.php"


Forderungen der Open Knowledge Foundation Deutschland
=====================================================
:Quelle: `Open Knowledge Foundation Deutschland`_

   
4. Wir fordern, dass digitale Schnittstellen zwischen Verwaltung und
   Entwicklerinnen und Entwicklern ausgebaut werden. Dies fängt bei
   Schnittstellen für Webseiten der Verwaltung an. Wir brauchen mehr
   personelle Ressourcen in Behörden, um die Digitalisierung der
   Verwaltung voranzubringen und eine offene Kultur gegenüber
   Entwicklerinnen und Entwicklern zu etablieren. Dabei können eigene
   Innovations-Einheiten (siehe USA und GB) in der Verwaltung helfen, die
   innerhalb der Verwaltung wirken.
   
   Davon profitieren nicht nur Bürgerinnen und Bürger, sondern auch die
   Verwaltung selbst. Offene Daten, Schnittstellen und einfache Nutzung
   von digitalen Dienstleistungen steigern die Effizienz und fördern
   Nahbarkeit und Vertrauen in die Verwaltung.


.. _Open Knowledge Foundation Deutschland: https://okfn.de/blog/2017/09/Zivilgesellschaftliche-Forderungen/
.. _Flaggen der Bundesländer: https://de.wikipedia.org/wiki/Land_(Deutschland)
