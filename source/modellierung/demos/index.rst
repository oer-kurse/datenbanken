====================
Demos (Modellierung)
====================

.. toctree::
   :maxdepth: 2
	     
   quiz/index
   badegewaesser/aufgabe-badegewaesser
   badegewaesser/loesung-badegewaesser
   lost-art/aufgabe-lost-art

Migration
=========

.. toctree::
   :maxdepth: 2
	     
   landkreise/14m1x1--migration__excel-zu-datenbank
