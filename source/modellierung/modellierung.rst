=========================
Begriffe zur Modellierung
=========================

.. index:: Modellierung

.. image:: ./images/sonnenuntergang_caputh_2017.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/sonnenuntergang_caputh_2017.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/sonnenuntergang_caputh_2017.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Sonnenuntergang am Schwielowsee
   |b|
   Serie: Natur

|a|


Perspektiven der Modellierung (Ebenen)

- konzeptionelle Ebene 
- logische Ebene
- physikalische Ebene

  
ER-Modellierung
+++++++++++++++
Fällt in die konzeptionelle Ebene, wobei die folgenden Fälle vermieden werden sollten:


- redundante Daten
- Null-Sättigung (siehe auch
  `How To Handle Missing Information Without Using NULL
  <http://www.dcs.warwick.ac.uk/~hugh/TTM/Missing-info-without-nulls.pdf>`_)
- enge Kopplung


Ein Modell besteht aus Entitäten, Attributen und Beziehungen.

:Entity:
   
   Eine Entity ist das Symbol für ein reales Objekt, z.B. eine Libelle

:Entity-Typ:
   Mehr als eine konkrete Entity, interessiert der Typ, also welche
   Attribute alle Entitäten gemeinsam haben.

   Libelle {lateinischer Name, Unterordnung, Fundort}

:Entity-Set:

   Ist eine Gruppe von Entitäten vom gleichen Entitäts-Typ.

:Attribute:

   Charakterisieren eine Entity näher und 
   können unterschiedliche Eigenschaften aufweisen:
   
   - kann atomar sein (d.h. es kann nicht weiter in kleinere Einheiten zerlegt werden)
   - kann zusammengesetzt sein
     
     Beispiel:
     
     Eine Adresse, besteht aus PLZ, Ort, Straße, Hausnummer. Teile der
     Adresse sind für sich genommen unbrauchbar.
   - kann mehrwertig sein, z.B. die Farben eines Vogels, wie scharz-rot,
     blau-gelb, ...
     
   - abgeleitete Attribute

     z.B. läßt sich das Alter über das Geburtsdatum ermitteln

:Domain:

   Beschreibt den zulässigen Wertebereich für ein Attribut.

 
Für die Modellierung, Analyse und Konzeption sind, neben den Entitäten
und deren Attributen, folgende Angaben von Bedeutung:

- Beziehungen
- Kardinalität zwischen den Entitäten
- Primär- und Fremdschlüssel


Zwei Phasen des Entwurfs
++++++++++++++++++++++++

1. Konzeptionelles Datenmodell

   Beschreibt die Möglichkeiten, es ist noch keine konkrete Umsetzung geplant.

   .. INDEX:: Modellierung; Online-Zeichenprogramm
   .. INDEX:: Online-Zeichenprogramm; Modellierung
   .. INDEX:: Modellierung; Diagramme
   .. INDEX:: Diagramme; Modellierung

2. Physisches Datenmodell

   Die gefundenen Beziehungen werden in konkrete Relationen mit
   Attributen und Schlüsseln definiert.

3. Werkzeuge
   
   Online-Tool: https://app.diagrams.net/
   Weitere Anbieter: :ref:`tools`
   
Bei der Überführung des ER-Modells in das Relationale (Datenbank)
Modell ändern sich die Bezeichnungen. Nachfolgend eine
Gegenüberstellung:

+----------------------------+-----------------------+---------------------+---------------------+
| Entity-Relationship-Modell |                       | Relationales Modell | umgangssprachlich   |
+----------------------------+-----------------------+---------------------+---------------------+
| Entity                     | Relationship          | Tupel               | Datensatz/Zeile     |
+----------------------------+-----------------------+---------------------+---------------------+
| Entity Set                 | Relationship Set      | Relation            | Tabelle             |
+----------------------------+-----------------------+---------------------+---------------------+
| Attribut                   | Attribut              | Attribut            | Spalte/Wert         |
+----------------------------+-----------------------+---------------------+---------------------+
| Entityschlüssel            | Relationshipschlüssel | Primärschlüssel     | Primärschlüssel     |
+----------------------------+-----------------------+---------------------+---------------------+


   

  


  
