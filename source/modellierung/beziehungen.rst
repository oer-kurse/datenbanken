==========================
Beziehungen (Modellierung)
==========================

.. INDEX:: Beziehungen; Modellierung
.. INDEX:: Modellierung; Beziehungen


.. image:: ./images/blumen-und-schnee.jpg
   :width: 0px


.. |x| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/blumen-und-schnee.jpg')"></span>
   </a>

.. |y| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/blumen-und-schnee.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |y|
   Serie: Pflanzen

|x|
	     

==========
Namenswahl
==========

Quelle: https://dev.to/ovid/database-naming-standards-2061

Fragen 1: CamleCase oder Unerstrich?
====================================

Weil mit SQL die Groß- und Kleinschreibung zu beachten ist empfehlt
sich für die Namenswahl die durchgehende Kleinschreibung.  Für die
bessere Lesbarkeit kann der Unterstrich als Verbindungselement in
Wortkombinationen dienen.

Niemand hält Sie davon ab die CamleCase-Schreibweise zu benutzen.

Frage 2: Deutschsprachige Bezeicher oder englischsprachige Wortwahl?
====================================================================

Fakt ist, im Umfeld der Komputertechnik ist das »Englische« die »Amtssparche« und
liefert oft die kürzeren Bezeichner.

Frage 3: Plural oder Singualar für Tabellen?
============================================

Also »Audit«  oder »Audits«  als Tabellenname?
Besser ist der Plural, weil man damit nicht so schnell Gefahr läuft
Schlüsselworte zu verwenden. Das Wort »autid« ist z.B. ein
Schlüsselwort in SQL...

Frage 4: ID mti Tabellenbezug?
==============================

Also users_id oder id in der Realation users?

Um die Verweschlunsgefahr zu minimerien, ist der users_id den Vorrang
einzuräumen. Noch besser ist dann die Kombination aus Relation und
Attributname: user.users_id, ....








