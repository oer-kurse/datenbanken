=========================
Notation und Kardinalität
=========================

.. index:: Kardinalität

.. image:: ./images/blumen-und-schnee.jpg
   :width: 0px


.. |x| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/blumen-und-schnee.jpg')"></span>
   </a>

.. |y| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/blumen-und-schnee.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |y|
   Serie: Pflanzen

|x|
	     
Für die Planung einer neuen Datenbank eignen sich unterschiedlichste
graphische oder textliche Darstellungsformen.
Am weitesten verbreitet sind die Chen-Notation, mit der Erweiterung
der Martin- bzw. Krähenfuss-Notation, möglich sind auch UML-Diagramme und auf Text
basierende Darstellungen.

.. |a| image:: images/entity.jpg
.. |b| image:: images/entity-attribute.jpg
.. |c| image:: images/entity-schwach.jpg
.. |d| image:: images/attribut.jpg
.. |e| image:: images/beziehung.jpg
.. |f| image:: images/martin-0-1.jpg
       :width: 400px

.. |g| image:: images/martin-0-n.jpg
.. |h| image:: images/martin-1-n.jpg
.. |i| image:: images/martin-1-1.jpg
.. |j| image:: images/martin-1.jpg
.. |k| image:: images/martin-n.jpg

+--------------+----------------------------------------------------------------------------+
|     Bild     |   Kommentar                                                                |
+==============+============================================================================+
|  |a|         | Entity                                                                     |
+--------------+----------------------------------------------------------------------------+
|  |b|         | Entity mit Attributen                                                      |
+--------------+----------------------------------------------------------------------------+
|  |c|         | Entity (schwach) z.B Positionen einer Rechnung passend zur Rechungsnummer. |
+--------------+----------------------------------------------------------------------------+
|  |d|         | Attribut                                                                   |
+--------------+----------------------------------------------------------------------------+
|  |e|         | Relation                                                                   |
+--------------+----------------------------------------------------------------------------+

Über Beziehungen sind die Zusammenhänge wieder hergestellt.


+--------------------------+---------------------------------------------------+
|                          | Martin- oder Krähenfuß-Notation                   |
|                          | für die Kardinalitäten                            |
+==========================+===================================================+
|  |f|                     | 1,0                                               |
+--------------------------+---------------------------------------------------+
|  |g|                     | N,0                                               |
+--------------------------+---------------------------------------------------+
|  |h|                     | N,1                                               |
+--------------------------+---------------------------------------------------+
|  |i|                     | 1,1                                               |
+--------------------------+---------------------------------------------------+
|  |j|                     | 1                                                 |
+--------------------------+---------------------------------------------------+
|  |k|                     | N                                                 |
+--------------------------+---------------------------------------------------+


Die Kardinalitäten der Martin-Notation können auch durch die folgenden
Angaben ergänzt bzw. ersetzt werden.
	       

+-----------+------------------------+----------+----------------------+
| Abkürzung | Assoziationstyp Anzahl | Anzahl   | Anmerkung            |
+===========+========================+==========+======================+
| 1         | einfach                | 1        | genau ein Tupel      |
+-----------+------------------------+----------+----------------------+
| c         | konditional            | 0 oder 1 | ein oder kein Tupel  |
+-----------+------------------------+----------+----------------------+
| m         | mehrfach               | >= 1     | mindestens ein Tupel |
+-----------+------------------------+----------+----------------------+
| mc        | mehrfach/konditional   | >=0      | keiner bis beliebig  |
|           |                        |          | viele Tupel          |
+-----------+------------------------+----------+----------------------+

Beispiel einer textuellen Darstellung
=====================================



 |  ABTEILUNG (**ABTNR** , ABTNAME , BUDGET , *CHEFNR*)
 |  AKTE (**PERSNR**, DATUM , POSITION , GEHALT)	
 |  PERSONAL (**PERSNR**, VNAME , NNAME , *PROJNR* , TELEFONNR , GEHALT)
 |  PROJEKT (**PROJNR**, BUDGET , *ABTNR*)	

