==============
Normalisierung
==============

.. index:: Normalisierung
.. index:: Normalformen

.. image:: ./images/hasenklee.png
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/hasenklee.png')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/hasenklee.png">
   </a>

.. sidebar:: SQL-Kurs

   Hasenklee
   |b|
   Serie: Pflanzen

|a|

Fragen zum Datenbankentwurf:

1. Welchen Zwecke erfüllt die zukünfigte  Datenbank (in einem Satz)?
2. Welche Fragen sollem mit den Datenbankabfragen beantwortet werden?

   


Mit der Normalisierung werden:

- Redundanzen vermieden
- Datenkonsistenz befördert, also Anomalien verhindert:

  - Update-Anomalie
  - Einfüge-Anomalie
  - Lösch-Anomalie


Normalform I
============
- keine Wiederholung von Attributen
- keine Verbunde von Attributen
   
Typische Kennzeichen von Verstößen gegen die 1. Normalform:

- Attribut-Wiederholungen

  z.B:  Kind1, Kind2, Kind3, ...

- Attribut-Werte die Trennzeichen enthalten wie z.B Leerzeichen,
  Semikolon und Komma.
  
  z.B: Schmerberger Weg 92 a
  
- Tragen Sie Beispieldaten in die Tabellen ein.

  Wie unterscheiden sich die Längen in den Zeilen?

Normalform II
=============
Alle Relationen, die in der 1. Normalform sind und einen
Primärschlüssel aus einem einzigen Attribut besitzen, sind automatisch
in der 2. Normalform.

Was ist bei zusammengesetzten Schlüsseln zu beachten?

- Attribute müssen vom ganzen Schlüssel und nicht nur einem Teil des
  zusammengesetzten Schlüssel abhängig sein.
- Attribut-Werte sollten sich in den Tupeln nicht wiederholen, was durch
  eine getrennte Speicherung der Werte in zwei Relationen gelöst wird.
- Der Zusammenhang wird anschließend durch Fremdschlüssel wieder hergestellt.

Normalform III
==============

- Alle Relationen befinden sich in der zweiten Normalform.
- Alle Attribute die nicht Teil des Schlüssels sind, sind auch nicht
  abhängig von anderen Attributen, die ebenfalls nicht Teil des
  Schlüssels sind.

  Angenommen eine Relation hat die Spalten:
  
  artikel_nummer (PK), bezeichnung, preis, rabatt

  Hier ist der Rabatt abhängig vom aktuellen Preis, also als
  Nichtschlüsselattribut von einem anderen Nichtschlüsselattribut
  abhängig! Eine Änderung des 
  

  
  
  

Alle Normalformen als Video
===========================

.. raw:: html
	 
   <iframe width="560" height="315"
   src="https://www.youtube.com/embed/Hn6qyRkbPqI?rel=0&amp;controls=0&amp;showinfo=0"
   frameborder="0" allowfullscreen>
   </iframe>
