.. meta::

   :description lang=de: SQL, Datenbanken, Regeln für Namen
   :keywords: SQL, Namen, Regeln

======================
Bezeichner: Namenswahl
======================

.. INDEX:: Beziehungen; Modellierung
.. INDEX:: Modellierung; Beziehungen
.. INDEX:: Namenskonventionen; Bezeichner
.. INDEX:: Bezeichner; Namenskonventionen

.. image:: ./images/blumen-und-schnee.jpg
   :width: 0px


.. |x| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/blumen-und-schnee.jpg')"></span>
   </a>

.. |y| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/blumen-und-schnee.jpg">
   </a>



.. sidebar:: SQL-Kurs

   |y|
   Serie: Pflanzen

|x|
	     

F 1: CamelCase oder Unterstrich?
================================

Weil mit SQL die Groß- und Kleinschreibung zu beachten ist, empfehlt
sich für die Namenswahl die durchgehende Kleinschreibung.  Für die
bessere Lesbarkeit kann der Unterstrich als Verbindungselement in
Wortkombinationen dienen.

Niemand hält Sie davon ab die CamleCase-Schreibweise zu benutzen.

F 2: ID mit Tabellenbezug?
==========================

Also »user_id«  oder nur  »id« in der Realation users?

Um die Verwechslunsgefahr zu minimerien, ist der *user_id* den Vorrang
einzuräumen. Noch besser ist dann bei Abfragen die Kombination aus Relations- und
Attributname: users.user_id, ....

F 3: Deutsche oder englische Bezeicher?
=======================================

Fakt ist, im Umfeld der Computertechnik ist »English« die »Amtssprache« und
liefert oft die kürzeren Bezeichner.

F 4: Plural oder Singular für Tabellen?
=======================================

Also »Audit«  oder »Audits«  als Tabellenname?
Besser ist der Plural, weil man damit nicht so schnell Gefahr läuft
Schlüsselworte zu verwenden. Das Wort »audit« ist z.B. ein
Schlüsselwort in SQL...


Quelle: https://dev.to/ovid/database-naming-standards-2061







