


Beispiel (Sport)
----------------

Normalisierung ist der Prozess der Reduzierung von Daten als »Kopien« in einer Datenbank.

Struktur I
~~~~~~~~~~

.. code:: text



    |----+----------+-------------------+---------------|
    | id | sportart | heim_manschaft    | gaeste        |
    |----+----------+-------------------+---------------|
    |  1 | Fußball  | SV Babelsberg  03 | FSV Union     |
    |  2 | Fußball  | FC Viktoria       | BFV 08        |
    |  3 | Fußball  | Lok Leipzig       | Chemnitzer FC |
    |----+----------+-------------------+---------------|
          ^
          |
          o-------- Wiederholungen...

Struktur II
~~~~~~~~~~~

Sportart

.. code:: text




    |-------------+----------|
    | sportart_id | sportart |
    |-------------+----------|
    |           1 | Fußball  |
    |-------------+----------|

Spielplan



.. code:: text




    |--------------+-------------+-------------------+---------------|
    | spielplan_id | sportart_id | heim_mannschaft   | gaeste        |
    |--------------+-------------+-------------------+---------------|
    |            1 |           1 | SV Babelsberg  03 | FSV Union     |
    |            2 |           1 | FC Viktoria       | BFV 08        |
    |            3 |           1 | Lok Leipzig       | Chemnitzer FC |
    |--------------+-------------+-------------------+---------------|

Erweiterte Struktur III
~~~~~~~~~~~~~~~~~~~~~~~

Weitere Aufteilung durch eine Relation für die Mannschaften.

.. code:: text


    |--------------+-------------+-------------------+----------|
    | spielplan_id | sportart_id | heim_mannschaft   | gaeste   |
    |--------------+-------------+-------------------+----------|
    |            1 |           1 | 1                 |        5 |
    |            2 |           1 | 2                 |        6 |
    |            3 |           1 | 3                 |        7 |
    |--------------+-------------+-------------------+----------|

.. code:: text

    +----------------+-------------------+
    | mannschafts_id | bezeichnung       |
    +----------------+-------------------+
    |              1 | SV Babelsberg  03 |
    |              2 | FC Viktoria       |
    |              3 | Lok Leipzig       |
    |              4 | FSV Union         |  
    |              5 | BFV 08            |
    |              6 | Chemnitzer FC     |
    +----------------+-------------------+
