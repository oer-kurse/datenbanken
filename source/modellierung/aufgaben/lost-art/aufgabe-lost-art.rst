===================
 Aufgabe: Lost-Art
===================



.. INDEX:: Übung: Modellierung; Lost Art
.. INDEX:: Lost Art; Übung: Modellierung


.. image:: ./images/lost-art.webp
   :width: 0px
	   
.. image:: ./images/lost-art-preview.webp
   :width: 0px


.. |y| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/lost-art.webp')"></span>
   </a>

.. |z| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/lost-art-preview.webp">
   </a>

.. sidebar:: SQL-Kurs

   Lost-Art
   |z|
   Website

|y|
	       
Auf der Website https://www.lostart.de/de/start werden Kulturgüter
gelistet, deren Herkunft (Provinence) unklar oder ein Zuordnung
zu den Eigentümern ungeklärt ist. Im engeren Sinne spricht man hier
von »Raubkunst«. Die Suche, wie die Erfassung erfolgt über eine
Datenbank.

Für die Modellierung stehen die formulare und andere
Hinweise für die Erfassung zur Verfügung.

Aufgaben/Arbeitsschritte
========================

1. Laden Sie die Vorlagen für die Formulare herunter und analysieren
   Sie die Struktur(en), um die Verwaltung in einer Datenbank zu
   realisieren.

2. Stellen Sie im ersten Schritt ein ER-Diagramm.

   https://app.diagrams.net

   
3. Erstellen Sie basierend auf dem ER-Diagramm eine textuelle
   Darstellung (Vergleiche mit dem Beispiel zur Personaldatenbank)

   .. code:: bash

      ABTEILUNG	( ABTNR , ABTNAME , BUDGET , CHEFNR )	  CHEFNR » PERSONAL.PERSNR
      AKTE	( PERSNR , DATUM , POSITION , GEHALT )	  PERSNR » PERSONAL.PERSNR
      PERSONAL	( PERSNR , VNAME , NNAME , PROJNR ,
                 TELEFONNR , GEHALT )                    PROJNR » PROJEKT.PROJNR
      PROJEKT	( PROJNR , BUDGET , ABTNR )	          ABTNR » ABTEILUNG.ABTNR
   

4. Definieren Sie abschließend eine Tabellendarstellung inklusive der
   Datentypen. Das Zielsystem ist eine PostgreSQL-Datenbank. Verwenden
   Sie:

   a) MySql-Workbench (nach dem Export sind Anpassungen am Skript notwendig).
   b) Den Desinger in PgAdmin.
   c) ein anderes Tool, welches das generieren von SQL-Skripten
      unterstützt.
      
   Legen Sie die Beziehungen, Kardinalitäten sowie Primär- und
   Fremdschlüssel fest.
   
5. Fügen Sie dem Skript Einfügeoperationen mit Beispieldaten hinzu.


Ausgangsdaten
=============

Laden Sie die folgenden Rohdaten herunter:

Download: :download:`Rohdaten (zip-Datei) <files/lost-art-material.zip>`

Das Archiv enthält folgende Dokumente:

- Meldeformulare als Exceldatei
- Kommentare und Erläuterungen aus den Meldeformularen
  (als Text-Version)
- für die Analyse: ein Suchergebnis für den Namen »Liebermann« im CSV-Format 
- Dictionary (Python), transformiert und generiert aus dem Suchergebnis (CSV-Format)

  
