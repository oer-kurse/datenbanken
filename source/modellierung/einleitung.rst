===================
 Datenbank-Modelle
===================

.. index:: Modellierung; Datenbankmodelle
.. index:: Datenbankmodelle; Modellierung

.. image:: ./images/kleider.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/kleider.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/kleider.jpg">
   </a>


.. sidebar:: SQL-Kurs

   |b|
   Serie: Mode

|a|
     

Hierarchische DB
++++++++++++++++

Ein Hierarchisches Datenbankmodell ist das älteste Datenbankmodell;
es bildet die reale Welt durch eine hierarchische Baumstruktur
ab. Jeder Satz (engl. Record) hat also genau einen übergeordneten
Vorgänger, mit Ausnahme genau eines Satzes, nämlich der Wurzel der
so entstehenden Baumstruktur. [#f1]_
  
Netzwerk – DB
+++++++++++++

  
Das Netzwerk-Modell fordert keine strenge Hierarchie und kann deswegen
auch m:n-Beziehungen abbilden, d. h. ein Datensatz kann mehrere
Vorgänger haben. Auch können mehrere Datensätze an oberster Stelle
stehen. Es existieren meist unterschiedliche Suchwege, um zu einem
bestimmten Datensatz zu kommen. Man kann es als eine Verallgemeinerung
des hierarchischen Datenbankmodells sehen. [#f2]_

 
Objektorientierte DB
++++++++++++++++++++

Eine Objektdatenbank oder objektorientierte Datenbank ist eine
Datenbank, die auf dem Objektdatenbankmodell basiert. Im Unterschied
zur relationalen Datenbank werden Daten hier als Objekte im Sinne
der Objektorientierung verwaltet.

Ein Objekt modelliert normalerweise einen Gegenstand oder Begriff
und enthält insbesondere dazugehörige Attribute; so gehört zum
Beispiel die Farbe und das Gewicht eines Autos zu dem Objekt
Auto. Attribute beschreiben ein Objekt näher. Daten und Methoden
(die Funktionen zum Zugriff auf die Daten) werden in den Objekten
zusammen abgelegt. [#f3]_
  
  
  
Data Warehouses
+++++++++++++++

Ein Data Warehouse (kurz DWH oder DW; wörtlich „Datenlager“, im
Deutschen dominiert die englische Schreibweise, die Schreibweise
Datawarehouse wird jedoch auch verwendet) ist eine für Analysezwecke
optimierte zentrale Datenbank, die Daten aus mehreren, in der Regel
heterogenen Quellen zusammenführt. [#f4]_
  
Data Lake
+++++++++

Ein Data Lake (wörtlich übersetzt "Datensee") ist in der
Wirtschaftsinformatik ein System oder ein Repository von Daten, die
im Rohdatenformat gespeichert sind, normalerweise Blobs oder Dateien. [#f5]_

OpenSoure-Lösung: https://delta.io/
  
Relationale DB
++++++++++++++
   
Eine relationale Datenbank nutzt ein tabellenbasiertes
relationalen Datenbankmodell.

Das zugehörige Datenbankmanagementsystem wird als relationales
Datenbankmanagementsystem oder RDBMS (Relational Database Management
System) bezeichnet. Zum Abfragen und Manipulieren der Daten wird
überwiegend die Datenbanksprache SQL (Structured Query Language)
eingesetzt, deren theoretische Grundlage die relationale Algebra
ist. [#f6]_
  
  
ER – Datenbankmodell
++++++++++++++++++++

(konzeptionelles Modell)

Das Entity-Relationship-Modell – kurz ER-Modell oder ERM; deutsch so
viel wie: Modell (zur Darstellung) von Dingen, Gegenständen,
Objekten (= ‚entities‘) und der Beziehungen/Zusammenhänge zwischen
diesen (= ‚relationship‘) – dient dazu, im Rahmen der semantischen
Datenmodellierung den in einem gegebenen Kontext (z. B. einem
Projekt zur Erstellung eines Informationssystems) relevanten
Ausschnitt der realen Welt zu bestimmen und darzustellen. Das
ER-Modell besteht im Wesentlichen aus einer Grafik (ER-Diagramm,
Abk. ERD) sowie einer Beschreibung der darin verwendeten Elemente. [#f7]_


Fußnoten
++++++++

.. [#f1] https://de.wikipedia.org/wiki/Hierarchisches_Datenbankmodell
.. [#f2] https://de.wikipedia.org/wiki/Netzwerkdatenbankmodell  
.. [#f3] https://de.wikipedia.org/wiki/Objektdatenbank 
.. [#f4] https://de.wikipedia.org/wiki/Data_Warehouse
.. [#f5] https://de.wikipedia.org/wiki/Data_Lake
.. [#f6] https://de.wikipedia.org/wiki/Relationale_Datenbank
.. [#f7] https://de.wikipedia.org/wiki/Entity-Relationship-Modell
	 
