============
Modellierung
============


.. image:: ./images/datenbankentwurfs-phasen.svg
   

.. toctree::
   :maxdepth: 1
	     
   einleitung
   modellierung
   kardinalitaeten
   namenswahl
   normalisierung
   normalisierung-sport
   werkzeuge
   demos/index

.. image:: ./images/phasenmodell--db__entwurf.svg
   
.. @startuml
   start

   repeat :Anforderungsanalyse;
     : konzeptioneller Entwurf;
     : Verteilungsentwurf;
     : logischer Entwurf;
     : Datendefinition;
     : physischer Entwurf;
     : Implementierung & Wartung;
   backward: nächste Iteration;
   repeat while (Neue Informationen?)

   stop
   @enduml
   
