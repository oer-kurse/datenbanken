====================
Übung: Begriffe (II)
====================

Suchen Sie über ihre Lieblingssuchmaschine nach kompakten Definitionen.

..  image:: ./images/big-bratislava-man-at-work.webp
    :width: 0	    

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/big-bratislava-man-at-work.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
     <img width="250px" src="../_images/big-bratislava-man-at-work.webp">
   </a>


.. sidebar:: SQL-Kurs

   |b|

   Serie: Natur


|a|

- :download:`Download PDF <uebung-fachbegriffe.pdf>`
- Notieren Sie jeden der folgenden Begriffe
- schreiben Sie darunter eine kurze  Definition
- Verwenden sie im ersten Schritt keine Hilfsmittel,
  nur das was in ihrem Kopf ist, zählt!

:Kartesisches Produkt:

   ...
   
:Kreuzprodukt:
   ...
   
:DDL:
   ...
   
:DML:

   ...
   
:DCL:

   ...
   
   
:Codd:

   ...
   
:Tuple:

   ...
   
:Attribute:

   ...
   
:referential integrity:

   ...
   
:Constraint:

   ...
   
:ACID:

   ...
   
:Operatoren:

   ...
   
:Transaktion:

   ...
   
:ROLLBACK:

   ...
   
:COMMIT:

   ...
   
:SQL Injection:

   ...
   
:NoSQL:

   ...
   
:tablespace:

   ...
   
:Relation:

   ...
   
:Redundanz:

   ...
   
:Normalisierung:

   ...
   



