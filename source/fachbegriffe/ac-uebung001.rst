===================
Übung: Begriffe (I)
===================
Welche kennen Sie schon/noch?

..  image:: ./images/telegrafenmast.jpg
    :width: 0	    

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/telegrafenmast.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
     <img width="250px" src="../_images/telegrafenmast.jpg">
   </a>


.. sidebar:: SQL-Kurs

   |b|

   Serie: Natur


|a|


:Benötigt werden:
   
   - ein Stift
   - ein Blatt Papier
:Aufgabe:

   - Notieren Sie im Zentrum eines leeren Blattes
     den Begriff »Datenbanken«.
   - Ziehen Sie einen Kreis um den Begriff.
   - Schreiben Sie nun weitere Begriffe um den zentralen Begriff.
   - Verbinden Sie eventuelle zusammengehörige Begriffe durch Linien.
   - Verwenden Sie im ersten Schritt keine Hilfsmittel,
     nur das was in ihrem Kopf ist, zählt!

Dieses Verfahren ist auch unter den Begriffen Mindmapping bzw. Clustering bekannt.


