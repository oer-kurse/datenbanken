====================
Glossar: Datenbanken
====================
.. glossary::
   :sorted:

   2PC

      two-phase commit

      siehe auch: Transaktion

   ACID
   
      :Quelle: https://de.wikipedia.org/wiki/ACID


      - Atomarität (Atomicity): Eine Transaktion wird entweder ganz oder
	gar nicht ausgeführt. Transaktionen sind also »unteilbar«. Wenn
	eine atomare Transaktion abgebrochen wird, ist das System
	unverändert.
	
      - Konsistenz (Consistency): Nach Ausführung der Transaktion muss
	der Datenbestand in einer konsistenten Form sein, wenn er es
	bereits zu Beginn der Transaktion war. Führt eine Transaktion
	nicht zu einem neuen konsistenten Zustand, erfolgt ein
	automatisches ROLLBACK.
	     
      - Isolation (Isolation): Bei gleichzeitiger Ausführung mehrerer
	Transaktionen dürfen sich diese nicht gegenseitig
	beeinflussen. Ein Lock-Mechanismus sichert den jeweils
	erforderlichen Isoaltionszustand.
	
     
      - Dauerhaftigkeit (Durability): Die Auswirkungen einer Transaktion
	müssen im Datenbestand dauerhaft bestehen bleiben. Die Effekte
	von Transaktionen dürfen also nicht verloren gehen oder mit der
	Zeit verblassen. Eine Verschachtelung von Transaktionen ist wegen
	dieser Eigenschaft streng genommen nicht möglich, da ein
	Zurücksetzen (ROLLBACK) einer äußeren die Dauerhaftigkeit einer
	inneren, bereits ausgeführten Transaktion verletzen würde.
   
   Attribute

     Ein Attribut enspricht einer Spalte einer Relation (Tabelle).

   Codd Edgar F.
   

      Er hat 1970 erstmals den Vorschläge für den SQL-Standard
      (Structured Query Language) gemacht.
      

   COMMIT	   

     Ist der Abschluss einer Transaktion.

   Constraint

     Anderes Wort für Einschränkung, sie legen die Regeln für gültige
     Werte fest.

   CRUD-Anwendungen

      benuzten die vier grundlegenden Operationen zur Verwaltung von Daten in einer Datenbank:

      +-----------------+--------------+---------------+
      | CRUD-Operation  |    SQL-92    | HTTP (REST)   |
      +=================+==============+===============+
      | Create          | INSERT       | PUT oder POST |
      +-----------------+--------------+---------------+
      | Read (Retrieve) | SELECT       | GET           |
      +-----------------+--------------+---------------+
      | Update          | UPDATE       | PATCH oder PUT|
      +-----------------+--------------+---------------+
      | Delete (Destroy)| DELETE       | DELETE        |
      +-----------------+--------------+---------------+

   CQRS (Command Query Responsibility Segregation)

      Entwurfsmuster mit zwei unterschiedlichen Zugriffsmechanismen
      auf die Datenbank:

      - nur lesend Zugriffe
      - für schreibenden Zugriffe

      siehe auch: `Wikipedia <https://de.wikipedia.org/wiki/Command-Query-Responsibility-Segregation>`_

	
   Datenbankmodell

      Ist eine abstrahierte Darstellung der Daten und der zwischen diesen
      Daten bestehenden Beziehungen.

   DCL

     Data Control Language - Vergabe oder Entzug von Berechtigungen
     Befehle:
     
     - GRANT
     - REVOKE

   DDL

     Die Data Definition Language (Datendefinitionssprache) ist eine
     Datenbanksprache, die verwendet wird, um Datenstrukturen und
     verwandte Elemente zu beschreiben, zu ändern oder zu entfernen.
     
     Befehle:
     
     - CREATE TABLE
     - DROP TABLE
     - ALTER TABLE
   
   DML
   
     Die DML (Data Manipulation Language) ist ein Teil der
     SQL-Sprache, die sich mit allen Auswahl-/Einfüge-/Änderungs- und
     Löschoperationen beschäftigt.
     
     Befehle:
     
     - INSERT
     - UPDATE
     - DELETE

   Domain/Domäne

      Lateinisch »dominium« für »Herrschaftsbereich«. Die Werte eines
      Attributes werden klar definiert und schränken die Anzahl
      erlaubter Werte ein z.B. Die Farben einer Ampel (rot, gelb,
      grün). Alle anderen Farbwerte gehören nicht zur Domäne. Der
      Bereich einer Domain kann durch die Definiton von  Constraints
      durchgesetzt werden.
      
   ETL

      Oberbegriff für Hilfsprogramme, die den Import von Daten unterstützen.
      
      - Extraktion: der relevanten Daten aus verschiedenen Quellen
      - Transformation:  der Daten in das Schema und Format der Zieldatenbank
      - Laden: der Daten in die Zieldatenbank
      
   Fremdschlüssel

      Schlüssel, die in einer Relation enthalten sind und sich auf
      Schlüssel einer anderen Relation (deren Primärschlüssel)
      beziehen, heißen Fremdschlüssel.

   HADR

      High Availability Disaster Recovery (HADR) bietet eine
      Hochverfügbarkeitslösung sowohl für teilweise als auch für
      vollständige Standortausfälle. HADR schützt vor Datenverlust, indem
      Datenänderungen von einer Quelldatenbank, der sogenannten
      Primärdatenbank, in die Zieldatenbanken, den sogenannten
      Standby-Datenbanken, repliziert werden.
      
   Kartesisches Produkt

      Das kartesische Produkt zweier Mengen ist die Menge aller
      geordneten Paare von Elementen der beiden Mengen, wobei die erste
      Komponente ein Element der ersten Menge und die zweite Komponente
      ein Element der zweiten Menge ist.
   
      Wenn alle Zeilen aus zwei Tabellen (Relationen) ohne Bedingung
      verknüpft werden, entsteht das Kartesiche Produkt
      bzw. Kreuzprodukt. Es entsteht unter Umständen eine große temporäre
      Tabelle.
   
   Kreuzprodukt

     siehe Kartesisches Produkt


   MVCC

      Multiversion Concurrency Control

      Jeder Nutzer kann lesend auf die Daten zugreifen, während andere
      Nutzer die Daten ändern.
      
   Modell
   
      Ist eine vereinfachte, abstrahierte Darstellung der Welt.   

   Namespaces:

      Verhindern Namenskonflikte, damit können zwei Benutzer einer
      Datenbank den gleichen Namen für eine Relation verwenden.
      
   Normalisierung:

      Definiert formale Kriterien, die eine redundanz- und
      wiederspruchsfreie Speicherung von Daten sicher stellt.

      *Anders ausgedrückt:*

      Durch Normalisierunge verfolgt man das Ziel, funktionale Abhängigkeiten
      zu vermeiden, damit die Daten nach dem Laden leichter zu verstehen,
      zu überprüfen und zu verarbeiten sind.
      
   NoSQL

     Englisch für:  Not only SQL 
     Bezeichnet Datenbanken, die einen nicht-relationalen Ansatz verfolgen.
     z.B.: OrientDB, CouchDB, MongoDB oder Redis
   
   Operatoren

      +, -, <>, not, and, or, ...
   
   Primärschlüssel

      - Schlüssel (Keys), die die Datensätze einer Relation eindeutig
	identifizieren, heißen Primärschlüssel.
      - Ein Schlüssel besteht aus einem oder mehreren Attributen.
  
   NULL - Werte
   
      - Der NULL – Wert sind nicht zu verwechseln mit der Ziffer »0« im
	Zahlenbereich!
      - Ein NULL – Wert bedeutet, dass der entsprechende Attributwert
	nicht vorhanden ist, also keine Angabe gemacht wurde.
      - Platzhalter
	  
   ROLLBACK

     Nennt man das zurücksetzen einer Transaktion.
   
   
   Relation

      - Eine Relation ist mit einer Tabelle gleichzusetzen (eine einfache Benutzersicht)
      - eine Relation ist eine Menge (dadurch ist eine Reihenfolge nicht definiert)
      - in einer Relation sind alle Elemente (Tuple) voneinander verschieden
      - Relationen sind eigenständig, d.h. sie besitzen keine Verbindung zueinander;
	eine Verknüpfung von Relationen erfolgt über gemeinsame Attribute
   
   Redundanz

     - Redundanzen sind doppelte Informationen in einer Datenbank bzw. Relation.
     - Eine Informationseinheit ist dann redundant, wenn sie ohne
       Informationsverlust weggelassen werden kann.
     
   Referential Integrity

     - Beziehungen zwischen Tabellen müssen synchronisiert bleiben.
     - Die referentielle Integrität (Beziehungsintegrität) besagt, dass
       Attributwerte von einem Fremdschlüssel auch als Attributwert des
       Primärschlüssels vorhanden sein müssen.
     - Im Bereich der relationalen Datenbanken wird die referentielle
       Integrität dazu verwendet die Konsistenz der Daten zu erhalten.
     
   SQL Injection

     - Unerlaubtes Ausführen eines SQL-Befehls in einer Datenbank.
     - Ausnutzen einer Sicherheitslücke.

   
   SQL

      (Structured Query Language)
   
      - Ist die Standardsprache zur Kommunikation mit relationalen
	Datenbanksystemen.
      - Mit SQL ist es möglich, Datenbanken zu erzeugen und zu verwalten,
	Daten in die Datenbank zu speichern und diese zu verändern bzw. aus
	der Datenbank zu entfernen.
   
   Tablespace

      Ein Tablespace bezeichnet im Datenbankbereich den Speicherort,
      in den Tabellen, Indizes und andere Datenobjekte zusammegefaßt
      werden.

   TOAST

      Abkürzung für oversize attribute storage technique
      Damit werden Tabellen verwaltet, die das Limit eines Speicherblocks
      (Standard: 8Kbyte) überschreiten.
   
   Tuple

      Synonym für die Zeile einer Relation (Tabelle) oder anders
      ausgedrückt ein Datensatz.
   
   Transaktion

     So bezeichnet man in der Informatik eine Folge von Programmschritten.
     Transaktionen kommen meist bei Datenbanksystemen zum Einsatz.
     Fehlerhafte Transaktionen müssen abgebrochen und die
     bisherigen Änderungen in der Datenbank rückgängig gemacht werden, so
     dass sie keine negativen Auswirkungen auf den Zustand der
     Datenbank haben.
     
   
   



