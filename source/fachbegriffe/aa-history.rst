SQL-Standards
=============
	   
..  image:: ./images/wolke-cumulus.jpg
    :width: 0	    

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/wolke-cumulus.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
     <img width="250px" src="../_images/wolke-cumulus.jpg">
   </a>


.. sidebar:: SQL-Kurs

   |b|

   Serie: Natur


|a|


Was ist SQL
===========


Eine strukturierte Abfragesprache für Tabellen 
in Datenbanken. Die Sprachkostrukte lassen sich 
in Gruppen einteilen und erlauben die Konstruktion 
spezieller Fragestellungen.

	   
Die Entwicklung der :term:`SQL` Standards.


:ca. 1970:

   Erste Umsetzung durch IBM

:1976:

   Veröffentlichung als SEQUEL

:SQL-86:

   SQL-1 Standard durch ANSI

:1987:

   SQL-1 Standard durch ISO

:SQL-92:

   SQL-2 Standard durch ANSI

:SQL-1999:

   SQL-3/SQL-99
   - Erweiterungen für objektrelationale Daten

:SQL-2003:

   - Erweiterungen für in XML strukturierte Daten

:SQL-2008:

   - `Regelwerk in BNF-Noatation <https://jakewheat.github.io/sql-overview/sql-2003-foundation-grammar.html>`_
     
:SQL-2011:

   - `Temporal Data Support <https://cs.ulb.ac.be/public/_media/teaching/infoh415/tempfeaturessql2011.pdf>`_

   - DELETE in MERGE   
   - piplined DML
     CALL (Verbesserungen)
   - Einschränkungen für fetch
   - Collectiontyp (Verbesserungen)
   - nicht erzwungene Tabelleneinschränkungen
   - Window-Fuktionen (Verbesserungen)


:SQL-2016:

   - Row Pattern Recognition
   - JSON Datentyp
   - Polymorphic Table Functions
     (`Set Returning Funktions in Postgres <https://www.postgresql.org/docs/current/functions-srf.html>`_)


:SQL-2019:

   - multi-demensionale Arrays

:SQL-2023:

   - Propterty Graph Queries (SQL/PGQ)

     
