============
Linksammlung
============


.. _20240305T091456:

.. INDEX:: Linksammlung;Datenbanken 
.. INDEX:: Datenbanken; Linksammlung 

.. image:: ./images/potsdam-kirche-tor-fh.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/potsdam-kirche-tor-fh.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/potsdam-kirche-tor-fh.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Gebäude

|a|

Kursmaterialien
---------------

- `Musterlösungen zum Kurs <https://db-kurs.methopedia.eu>`_

- `Doku Postgres <https://www.postgresql.org/docs/>`_

- `Entwicklunsgeschichte Postgres <https://www.crunchydata.com/blog/when-did-postgres-become-cool>`_

- Stolperfallen

  - `Postgres <https://sql-info.de/postgresql/postgres-gotchas.html>`_

  - `MySQL <https://sql-info.de/mysql/gotchas.html>`_

- `SQL-Map (Security-Tests) <https://highon.coffee/blog/sqlmap-cheat-sheet/>`_

Datenbanken (Training)
----------------------

Filmdatenbank als Alternative zur Northwind-DB von Mikrosoft.

Download: `https://github.com/lerocha/chinook-database <https://github.com/lerocha/chinook-database>`_

mit Skripten für folgende Datenbanksysteme:

- MySQL

- SQL Server

- SQL Server Compact

- SQLite

- PostgreSQL

- Oracle

- DB2

Datenbank über Datenbanken
--------------------------

Datenbanken -- eine Auswahl nach vielen Kriterien

- `https://dbdb.io/browse <https://dbdb.io/browse>`_

- `Umfrage Stackoverflow 2023 zu Datenbanken <https://survey.stackoverflow.co/2023/#section-most-popular-technologies-databases>`_

SQL
---

- `https://www.techonthenet.com/sql/index.php <https://www.techonthenet.com/sql/index.php>`_ (EN/alle DB)

- `https://www.w3schools.com/sql/sql_intro.asp <https://www.w3schools.com/sql/sql_intro.asp>`_ (EN)

- `http://www.tutorialspoint.com/sqlite/ <http://www.tutorialspoint.com/sqlite/>`_  (EN/Sqlite)

- `https://www.guru99.com/sqlite-tutorial.html <https://www.guru99.com/sqlite-tutorial.html>`_ (EN/Sqlite)

- `http://wwwlgis.informatik.uni-kl.de/extra/game/ <http://wwwlgis.informatik.uni-kl.de/extra/game/>`_ (DE/Online Spiel)

Postgres
--------

:Tutorials:

- Sammlung: `http://www.postgresqltutorial.com <http://www.postgresqltutorial.com>`_ (EN)

- Flughafenbeispiel von Peter Andreas Möller:

  `http://workshop-postgresql.de <http://workshop-postgresql.de>`_ (DE)

:Support:

- Professionell (Übersicht)

  `https://www.postgresql.org/support/professional_support/europe/ <https://www.postgresql.org/support/professional_support/europe/>`_

:Übersicht/Nachschlagewerke:

- `https://www.postgresql.org/docs/12/static/index.html <https://www.postgresql.org/docs/12/static/index.html>`_ (EN)

- `https://www.techonthenet.com/postgresql <https://www.techonthenet.com/postgresql>`_ (EN)

:Folien:

- Datentyp: JSON

  `https://de.slideshare.net/hansjurgenschonig/postgresql-the-nosql-way <https://de.slideshare.net/hansjurgenschonig/postgresql-the-nosql-way>`_ (DE)

MS-SQL-Server
-------------

- WiltonDB: `Open-source DB die mit Microsoft SQL Server-Anwendungen umgehen kann <https://wiltondb.com/>`_
