=========
Vergleich
=========
.. image:: ./images/mond.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/mond.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/mond.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Natur

|a|
	     

.. list-table:: 
   :widths: 60  10 10 10 10
   :header-rows: 1		 

   * - Befehl
     - Sqlite
     - MySql
     - Postgres
     - Oracle
   * - select current_database();
     - 
     - 
     - x
     - 
   * - SELECT DATABASE();
     - 
     - x
     - 
     - 
   * - TABLE ...
     - 
     - 
     - 
     - 
   * - ALTER TABLE ...
     - x
     - x
     - x
     - x
   * - CREATE TABLE
     
       - :ref:`SQLite <create_tabele_sqlite>`
       - :ref:`Postgres <create-tables-constraint>`	 
       - :ref:`CREATE TABLE AS <create-table-as>`
       - :ref:`CREATE TABLE (bkzfrau) <create-table-bkzfrau>`
	 
     - x
     - x
     - x
     - x
   * - DROP TABLE
     - x
     - x
     - x
     - x
       
   * - LIMIT
       - :ref:`Vergleich <limit>`
     - x
     - x
     - x
     - x  

   * - SELECT

       - :ref:`Grundstruktur <select-struktur>` 	 
       - :ref:`SELECT DISTINCT <select>` ...

     - x
     - x
     - x
     - x
   * - INSERT ...
     - x
     - x
     - x
     - x
   * - SELECT

       - :ref:`Grundstruktur <select-struktur>` 	 
       - :ref:`SELECT DISTINCT <select>` ...

     - x
     - x
     - x
     - x

   * - T1 LEFT JOIN T2 on  ...
     - x
     - x
     - x
     - x
   * - T1 RIGHT JOIN T2 on  ...
     - 
     - x
     - x
     - x
   * - T1 FULL JOIN T2 on  ...
     - 
     - x
     - x
     - x
   * - T1 as t1 SELF JOIN T1 as t2 on  ...
     - 
     - x
     - x
     - x
   * - :ref:`CREATE DATABASE (Postgres) <create_database_postgres>`
     - 
     - 
     - x
     -
   * - :ref:`CREATE INDEX (Constraints/Postgres) <create-postgres-indices>`
     - x
     - x
     - x
     - x
   * - UPDATE ...
     - x
     - x
     - x
     - x
