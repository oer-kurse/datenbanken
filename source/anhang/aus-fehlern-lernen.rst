.. _ausfehlernlernen:

.. meta::

   :description lang=de: Datenbankkurs: Aus fehlern lernen
   :keywords: Sortierung, Eingaben prüfen, Fehler Datenbanken

====================
 Aus Fehlern lernen
====================
.. INDEX:: Fehler (lernen aus); lernen (aus Fehlern)
.. INDEX:: lernen (aus Fehlern); Fehler (lernen aus)

All zu oft werden uns Erfolgsgeschichten erzählt, die ohne Fehl und
Tadel sind. Was wir aber heute benutzen und wie wir etwas umsetzen, ist
das Ergebnis einer langen Kette von Versuch und Fehler. Deshalb gilt
immer noch der Grundsatz: »Nur aus Fehlern kann man lernen«! Hier eine
Liste offengelegter Fehler, die bei der Umsetzung eigener Projekte
vermieden werden sollten.


Wichtige Änderungen
===================

Eine Passwortänderung mit Folgen.

https://www.dancowell.com/balls-the-day-i-locked-ev/

Merke: Wichtige Änderungen nicht ohne Transaktionen!


Datensätze sortieren
====================  
Eine nicht reporduzierbare Ausgabe kann zu Irritationen führen.

https://www.danisch.de/blog/2022/04/05/elendes-bankgewurschtel/
  
Formulardaten I
===============

Daten sammeln, klar...

.. literalinclude:: ./files/htm-snippet.txt
   :language: html
   :linenos:
   :emphasize-lines: 8
   
 
       
Was akzeptiert die DB?
----------------------

Eigener Datentyp!

.. image:: ./images/db-04.png

Erkenntnis
    
  Prüfe was Du speichern darfst, vor allem die Schreibweise!

Formulardaten II
================

Akzeptierte Werte (os) für das verwendete Betriebssyste laut Datentyp-Definition:

.. image:: ./images/db-03.png


Daten erwarten vs. erhalten
---------------------------

.. literalinclude:: ./files/php-snippet.txt
   :language: php
   :linenos:
   :emphasize-lines: 10


Erkenntnis
    
  Prüfe was Du speichern willst, die gesendeten Daten können von
  bekannten Namen und Mustern abweichen!


Fehlermeldungen im Web gefunden
-------------------------------

.. index:: Fehlermeldung

.. http://mymuse.se/marke/spanx

:: 

   {"success":false,"error":"SQLSTATE[42S22]:
     Column not found: 1054 Unknown column
     'shop_product_categories.shop_category_id' in 'order clause'"
   }

Erkenntnis

  - Es ist also eine MySQL-DB
  - Mehr und intensiver testen...
   
    
