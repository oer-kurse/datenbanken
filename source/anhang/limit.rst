.. _limit:

=====
LIMIT
=====

.. index:: LIMIT; Standard
.. index:: Standard; LIMIT   

Limit ist ein gutes Beispiel dafür, wie ein nicht im Standard
definiertes Schlüsselwort unterschiedlich implementiert wurde.

TIPP: Halten sie sich möglichst streng an Standards.

Alle hier genannten DB
======================

::

   select * from runners
   order by meter
   offset 3 limit 3

Firebird
========

::

   select * from runner
   order by meter
   rows 4 to 6

Weiterführende Übersichten
==========================

- http://www.jooq.org/doc/latest/manual/sql-building/sql-statements/select-statement/limit-clause/

=============
Sonderzeichen
=============

.. INDEX:: Sonderzeichen; Escape
.. INDEX:: Escape; Sonderzeichen


Zeichenketten werden in Anführungszeichen einfach (') oder doppelt (") eingeschlossen.

**Beispiele:**

.. code:: text

   select 'Peter und seine Favoriten';

   -- aber Fehler:

   select 'Peter's Favoriten';

   -- die Lösung, um die ursprüngliche Funktion eines
   -- Apostrophs aufzuheben, für SQLite -- Verdoppelung
   
   select 'Peter''s Favoriten';

   -- Postgres (zweite Lösung)

   select E'Peter\'s Favoriten';
