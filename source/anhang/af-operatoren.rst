=======================
Operatoren -- Übersicht
=======================

.. image:: ./images/schmetterlingspuppen.jpg
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/schmetterlingspuppen.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/schmetterlingspuppen.jpg">
   </a>



.. sidebar:: SQL-Kurs

   Statt der Werbung...
   |b|
   Serie: Insekten

|a|


Die wichtigsten Kombinationen...

Arithmetische Operatoren
========================

Wenn a = 10 und b = 20, dann

.. list-table:: 
   :widths: 20 40
   :header-rows: 1		 

   * - Operator
     - Beispiel
   * - \+
     - a + b -> 30
   * - \-
     - a - b -> -10
   * - \*
     - a * b -> 200
   * - \/
     - b / a -> 2
   * - %
     - b % a -> 0
       


Vergleichsoperatoren
====================
.. list-table:: 
   :widths: 20 40
   :header-rows: 1		 

   * - Operator
     - Beispiel
   * - ==
     - (b == a) -> not true
   * - =
     - (b = a) -> not true
   * - !=
     - (b != a) -> true
   * - <>
     - (b <> a) -> true
   * - >
     - (b > a) -> not true
   * - <
     - (b < a) -> true
   * - >=
     - (b >= a) -> not true
   * - <=
     - (b <= a) -> true
   * - !<
     - (b !< a) -> not true
   * - !>
     - (b !> a) -> true


Logische Operatoren
===================

AND, BETWEEN, EXISTS, IN, NOT IN, LIKE, NOT, OR, IS NULL, IS, IS NOT, ||, UNIQUE

Binäre Operatoren
=================
.. list-table:: 
   :widths: 20 20 20 20
   :header-rows: 1		 

   * - p
     - q
     - p & q
     - p | q
       
   * - 0
     - 0
     - 0
     - 0
       
   * - 0
     - 1
     - 0
     - 1

   * - 1
     - 1
     - 1
     - 1

   * - 1
     - 0
     - 0
     - 1
       
Wenn A = 60; und B = 13; dann wird daraus im Binaärformat:


A = 0011 1100

B = 0000 1101

.. list-table:: 
   :widths: 20 40
   :header-rows: 1		 

   * - Operator
     - Beispiel
   * - A & B
     - 0000 1100
   * - A | B
     - 0011 1101
   * - ~A
     - 1100 0011
