=================
Mindestwortschatz
=================



.. _select:

..  image:: ./images/biggi-privat.jpg
    :width: 0	    


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_images/biggi-privat.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_images/biggi-privat.jpg">
   </a>

.. sidebar:: Besitzverhältnisse

   |b|
   
|a|

Download: :download:`Mindestwortschatz als PDF <./files/sql-mindestwortschatz.pdf>`

Attribute auswählen
-------------------

Zeige alle Attribute eines Tuple.

.. code:: sql


    SELECT * 
    FROM mitarbeiter;

Zeige ausgwählte Attribute eines Tuple.

.. code:: sql


    SELECT name, vorname
    FROM mitarbeiter;

Unterdrücke doppelte Tuple.

.. code:: sql


    SELECT DISTINCT name
    FROM studenten;

Sortierte Ausgabe
~~~~~~~~~~~~~~~~~

.. INDEX:: order by; Sortierung 

ORDER BY

Sortiere die Tuple ...

.. code:: sql


    SELECT name, vorname
    FROM mitarbieter
    ORDER BY name, vorname DESC;

Beispieldatensätze
------------------

LIMIT
~~~~~

Eine definierte Anzahl Tuple ausgeben. 

.. code:: sql


    SELECT name, vorname
    FROM mitarbieter
    ORDER BY name, vorname DESC
    LIMIT 5;

Filtern (Text)
--------------

Gleichheit
~~~~~~~~~~

Filtern nach Bedingungen (Zeichenketten) 
mit genauer Übereinstimmung

.. code:: sql


    SELECT bezeichnung
    FROM berufe
    WHERE bezeichnung = 'Datenbankabfrageprofi';

LIKE (Mustervergleich)
~~~~~~~~~~~~~~~~~~~~~~

Filtern mit Suchmuster... 

.. code:: sql


    SELECT bezeichnung
    FROM berufe
    WHERE bezeichnung LIKE '%profi%';

Vergleichen (mathematische Operatoren)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: sql


    SELECT bezeichnung
    FROM berufe
    WHERE gehalt >=  10000;

Filtern (vergleichen)
~~~~~~~~~~~~~~~~~~~~~

Filtern nach einzelnen Zeichen
(Zeichen werden auch nur als Zahl gespeichert.

Siehe auch: ASCII-Tabelle

.. code:: sql


    SELECT geschlecht
    FROM personen
    WHERE geschlecht < 'M';

IN (Listen)
~~~~~~~~~~~

Filtern nach Wert(en) in einer Liste. 

.. code:: sql


    SELECT id
    FROM produkte
    WHERE jahr IN (2021, 2022, 2023);

BETWEEN (Bereiche)
~~~~~~~~~~~~~~~~~~

Filtern nach Bedingungen (Bereiche) 

.. code:: sql


    SELECT jahr
    FROM ergebnisse
    WHERE jahr BETWEEN 2020 AND 2030;

AND, OR
~~~~~~~

Filtern logisch verknüpfen ... 

.. code:: sql


    SELECT jahr
    FROM ergebnisse
    WHERE jahr = 2020
    AND jahr = 2030;

GROUP BY
~~~~~~~~

Filtern und gruppieren...  

.. code:: sql


    SELECT name
    FROM runners
    WHERE jahr = 2021
    GROUP BY alter;


.. image:: ./images/aggregat-funktionen.svg

HAVING
~~~~~~

Filtern in Gruppen, die durch GROUP BY entstanden sind.

.. code:: sql


    SELECT name
    FROM runners
    WHERE jahr = 2021
    GROUP BY alter;
    HAVING geschlecht = 'M';


.. image:: ./images/aggregat-group-by-having.svg

Funktionen
----------

Allgemein
~~~~~~~~~

führen Berechnungen durch oder 
fassen Ergebnisse zusammen.
Beziehen sich immer auf einzelne 
Datentypen:

- Zeichenkettenfunktionen

- arithmetische Funktionen

- Aggregatfunktionen

- logische Funktionen

- Funktionen für Datum und Zeit

- ...

Aggregat-Funktion
~~~~~~~~~~~~~~~~~

COUNT Anzahl Datensätze 

.. code:: sql


    SELECT count(*)
    FROM runners;


Durchschnitt  (Aggregat-Funktion)

AVG

.. code:: sql


    select avg(alter) 
    from runners;

Summe (Aggregat-Funktion)

SUM

.. code:: sql


    SELECT sum(gehalt)
    FROM personal;

Zeichenketten Funktionen
------------------------

Umwandlung vor dem Vergleich mit Upper/Lower ...

.. code:: sql


    SELECT UPPER(name)
    FROM runners;


.. code:: sql


    SELECT LOWER(name)
    FROM runners;

Längenvergleiche

.. code:: sql


    SELECT LENGTH(name)
    FROM runners;

Logische Funktion
-----------------

IS NULL

Prüfen, ob ein Wert vorhanden ist.

.. code:: sql


    SELECT name, vorname
    FROM runners
    WHERE alter IS NULL;

Cast-Operatoren
~~~~~~~~~~~~~~~

Umwandeln von einem Datentyp in einen anderen.

.. code:: sql


    SELECT TO_DATE('20211206','YYYYMMDD');
    SELECT to_number('1210.73', '9999.99');
    ROUND(CAST(FLOAT8 '3.1415927' AS NUMERIC),2);
    SELECT ROUND('3.1415927'::numeric,2);

Nullwerte
~~~~~~~~~

NULL umwandeln

- ist ein nicht definierter Wert

- für Operationen ist ein cast notwendig

  coalece-Funktion (Postgres)

  .. code:: sql


      select anrede || coalesce(name,''))
      from runners;

Hat das Attribut den Wert NULL

.. code:: sql


    SELECT name, vorname
    FROM runners
    WHERE alter IS NULL;

Verneinung kombiniert mit NOT

.. code:: sql


    SELECT name, vorname
    FROM runners
    WHERE alter IS NOT NULL;

Alias
~~~~~

- Ohne »AS« wird der 
  Attribut- bzw. Tabellenname verwendet.

- Mit »AS« kann eine 
  neue Bezeichnung definiert werden.

- Wenn Leerzeichen, dann in 
  Anführungszeichen einschließen.

.. code:: sql


    SELECT vname || ' ' || nname AS Läufer
    FROM runners;

Concatenation/Stringverkettung
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mit  || und +

Abhängig vom System!

.. code:: sql

    SELECT vname || ' ' || nname AS Läufer
    FROM runners;

    oder

    SELECT vname + nname AS Läufer
    FROM runners;


eine Funktion zum Verknüpfen...

.. code:: sql

    SELECT concat(vname,nname) AS Läufer
    FROM runners;

Logischer Operator
~~~~~~~~~~~~~~~~~~

EXISTS

EXISTS prüft eine Unterabfrage, ob ein 
oder mehrere Tupel gefunden wurden.

.. code:: sql


    SELECT name, vornaem FROM  runners
    WHERE EXISTS 
     (SELECT * FROM timetable
     runners.id = timetable.startnummer);

Mengenoperatoren
----------------

UNION -- Vereinigungsmenge

.. code:: sql


    SELECT name, vorname FROM schuhbar
    UNION
    select name, vorname from outdoor;

Inklusive Doppler...

.. code:: sql


    SELECT name, vorname FROM schuhbar
    UNION ALL
    select name, vorname from outdoor;

Mengenoperator: INTERSECT
~~~~~~~~~~~~~~~~~~~~~~~~~

INTERSECT --  Durchschnittsmenge

.. code:: sql


    SELECT name, vorname FROM schuhbar
    INTERSECT
    select name, vorname from outdoor;

Mengenoperator -- EXCEPT/MINUS

Join
~~~~

Daten Für die nachfolgenden Beispiele:

.. code:: sql


    select * from t1;    select * from t2;    
     num | name           num | value              
    -----+------         -----+-------             
       1 | a                1 |                      
       2 | b                2 | xxx             
       3 | c                3 | yyy             
       4 | d                4 | zzz             
       5 | e             (4 rows)          
       6 | e
    (6 rows)

Join: CROSS -- Kreuztabelle

Keine where-Klausel!

.. code:: sql


    SELECT * FROM t1, t2;
    --oder explizit ...
    SELECT * FROM t1 CROSS JOIN t2;

Join: INNER
~~~~~~~~~~~

Auch als Equi-Join benannt, weil ein Gleichheitszeichen die 
Tabellen verbindet.

.. code:: sql

     SELECT * FROM t1 
     INNER JOIN t2 
     ON t1.num = t2.num; 


     num | name | num | value
    -----+------+-----+-------                                    
       1 | a    |   1 |
       2 | b    |   2 | xxx 
       3 | c    |   3 | yyy 
       4 | d    |   4 | zzz 

LEFT JOIN

Tabelle t1  wird ausgegeben, ergänzt durch Werte  aus der Tabelle t2.

.. code:: sql


     SELECT * FROM t1 LEFT JOIN t2 
     ON t1.num = t2.num;

     num | name | num | value 
    -----+------+-----+-------
            1 | a    |   1 | 
            2 | b    |   2 | xxx
            3 | c    |   3 | yyy
            4 | d    |   4 | zzz
            5 | e    |     | 
            6 | e    |     | 
    (6 rows)


RIGHT JOIN

Tabelle t2  wird ausgegeben, ergänzt durch Werte  aus der Tabelle t1

.. code:: sql


     SELECT * FROM t1 RIGHT JOIN t2
     ON t1.num = t2.num;
     num | name | num | value 
    -----+------+-----+-------
       1 | a    |   1 | 
       2 | b    |   2 | xxx
       3 | c    |   3 | yyy
       4 | d    |   4 | zzz
    (4 rows)

SELF JOIN

Zwei unterschiedliche Namen für eine Tabelle.

.. code:: sql


     SELECT * FROM t1 AS a, t1 AS b 
     WHERE a.num=b.num;

     num | name | num | name 
    -----+------+-----+------
       1 | a    |   1 | a
       2 | b    |   2 | b
       3 | c    |   3 | c
       4 | d    |   4 | d
       5 | e    |   5 | e
       6 | e    |   6 | e
    (6 rows)

Operatoren: mathematische

Wenn a = 20,  b = 10, c = 3 

.. table::

    +---------------------+--------+----------------------+
    | Operator            | Syntax |             Ergebins |
    +=====================+========+======================+
    | + (Addition)        | a + b  |                   30 |
    +---------------------+--------+----------------------+
    | - (Subtraktion)     | a - b  |                  -10 |
    +---------------------+--------+----------------------+
    | \* (Multiplication) | a \* b |                  200 |
    +---------------------+--------+----------------------+
    | / (Division)        | a / a  |                    2 |
    +---------------------+--------+----------------------+
    | % (Modulo)          | a % c  |                    2 |
    +---------------------+--------+----------------------+
    | \                   | \      | (3\*6 = 18 Rest = 2) |
    +---------------------+--------+----------------------+


Subquery -- Unterabfrage

In der where-Klausel kann ein neues SELECT 
Daten zur Verfügung stellen, die in der 
Hauptabfrage ausgewertet werden können.


.. code:: text


    SELECT id, bezeichnung
    FROM produkte
    WHERE jahr IN (SELECT jahr FROM einkauf
                   WHERE jahr 
                   BETWEEN '2010' and '2030')
