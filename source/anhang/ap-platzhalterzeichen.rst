==================
Platzhalter (like)
==================

.. index:: Platzhalter; LIKE
.. index:: LIKE; Platzhater	   
.. index:: Suchmuster; LIKE

Unterschiede Windows/Access und SQL
-----------------------------------

.. table::

    +--------------------------+--------+-----+
    | Funktion                 | Access | SQL |
    +==========================+========+=====+
    | beliebige Anzahl Zeichen | \*     | %   |
    +--------------------------+--------+-----+
    | ein einzelnes Zeichen    | ?      | _   |
    +--------------------------+--------+-----+
