================================
Entwicklung der Datenbanksysteme
================================


Datenbank: SQLite
=================

.. raw:: html

   <iframe width="560"
	 height="315"
	 src="https://www.youtube.com/embed/kjdxiG17hGM"
	 title="YouTube video player"
	 frameborder="0"
	 allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture; web-share"
	 allowfullscreen>
   </iframe>


Datenbank: Postgres
===================

.. raw:: html

   <iframe width="560"
	 height="315"
	 src="https://www.youtube.com/embed/suxjkAIHJI0"
	 title="YouTube video player"
	 frameborder="0"
	 allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture; web-share"
	 allowfullscreen>
   </iframe>

