.. _select-struktur:

=========================
 SELECT -- Grundstruktur
=========================


.. index:: DML; select (Reihenfolge der Auswertung)
.. index:: Reihenfolge der Auswertung; select
	   
:Aufbau und Schrittfolge der Abarbeitung:

.. list-table:: 
   :widths: 20 20 50
   :header-rows: 1		 

   * - Reihenfolge der Klauseln
     - Reihenfolge der Abarbeitung
     - Anmerkung
   * - select 
     - 5
     - streiche alle Attribute, die nicht gesucht werden
   * - from
     - 1
     - wähle alle Relationen und bilde das kartesische Produkt
   * - where
     - 2
     - streiche alle Tupel, die nicht zur Bedingung gehören
   * - group by 
     - 3
     - gruppiere die verbliebenen Tupel
   * - having
     - 4
     - steiche nach der Gruppierung alle Tupel die die Bedingung nicht
       erfüllen
   * - order by
     - 6
     - sortiere die verbliebene Ergebnismenge nach den Attributen 
   * - limit 
     - 7
     - gib aber nicht alle Tupel aus, sondern nur die gegebene Anzahl 
   * - Window-Funktionen
     - 8  
     - führe die Fenster-Funktionen aus, die sich nun im Result-Set befinden

Syntax (allgemein)
==================

::

   Select [DISTINCT] *|Datenfelder FROM Tabellenname
   [WHERE Bedingung]
   [GROUP BY Datenfelder [HAVING Bedingung]]
   [ORDER BY Datenfelder [ASC|DESC]]
   [LIMT [Start,] Anzahl];

Template für Abfragen
=====================

.. index:: Template für Select-Abfrage

::

   -- Entfernen Sie die Zeichenfolge '-- ' (Kommentar)
   -- vor dem groß geschriebenen Schlüsslelwort und
   -- und ergänzen Sie in der Leerzeile darunter...

   SELECT    --  wähle Spalte(n) oder Funktion(en)

   FROM      -- aus Relation1, Relation2, ...

   -- WHERE     -- wobei (Vergleich/Bedingung)

   -- GROUP BY  -- gruppiert durch (Vergleich/Bedingung)

   -- HAVING    -- wobei die Gruppen zum Ergebnis hinzgefügt wird, wenn ...

   -- ORDER BY  -- sortiert nach Attribut(en)
                -- die zw. SELECT und FROM ausgewäht wurden
   ;
